package com.aeologic.cctv.OfficersActivities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.widget.Button;

import com.aeologic.cctv.Activity.InProgressDetailActivity;
import com.aeologic.cctv.Activity.SendComplaintActivity;
import com.aeologic.cctv.Database.Database;
import com.aeologic.cctv.R;
import com.aeologic.cctv.Utility.MultipartUtility;
import com.aeologic.cctv.Utility.Prefrence;
import com.aeologic.cctv.Utility.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;


/**
 * Created by u105 on 19/8/16.
 */
public class ManagerSendDetailsAsyncTask extends AsyncTask<String, Integer, HashMap<String, String>> {
    private String TAG = ManagerSendDetailsAsyncTask.class.getSimpleName();
    private Button send;
    private Context context;
    ProgressDialog progressDialog;
    Database database;

    String rcNumber, offence, comment, imageUri, videoUri, audioUri, address, manualAddress, surveyMode, timeStamp, id;
    double lat, lng;


    public ManagerSendDetailsAsyncTask(Context context, Button send) {
        this.context = context;
        this.send = send;
        database = new Database(context);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(context.getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public ManagerSendDetailsAsyncTask(Context context) {
        this.context = context;
        database = new Database(context);
    }

    @Override
    protected HashMap<String, String> doInBackground(String[] params) {
        try {
//            MultipartUtility utility = new MultipartUtility(params[0], "UTF-8");
            MultipartUtility utility = new MultipartUtility(params[0], "UTF-8");
            Log.v("Create_complaint_url ", params[0]);
            utility.addFormField("token", Prefrence.getToken(context));
            rcNumber = params[1];
            // utility.addFormField("rc_number", params[1]);
            offence = params[2];
            utility.addFormField("offences", params[2]);
            utility.addFormField("comment", params[3]);
            if (params[4] != null) {
                imageUri = params[4];
//                String check = getEncodedString(params[4]);
//                utility.addFormField("photo", check);
                utility.addFilePart("photo", new File(params[4]));
                Log.e("SendDetailstask", "doInBackground: " + params[4]);
            } /*else {
                imageUri = null;
                utility.addFormField("photo", null);
            }*/

            if (params[5] != null) {
                //    String check1=getEncodedString(params[4]);
                videoUri = params[5];
                utility.addFilePart("video", new File(params[5]));
                Log.e("SendDetailstask", "doInBackground: " + params[5]);
            }
            if (params[6] != null) {
                //    String check1=getEncodedString(params[4]);
                audioUri = params[6];
                utility.addFilePart("audio", new File(params[6]));
                Log.e("SendDetailstask", "doInBackground: " + params[6]);
            }
            /* else {
                videoUri = null;
                utility.addFormField("video", null);
            }*/

            address = params[7];
            utility.addFormField("address", params[7]);

            lat = Double.parseDouble(params[8]);
            utility.addFormField("latitude", params[8]);
            lng = Double.parseDouble(params[9]);
            utility.addFormField("longitude", params[9]);
            timeStamp = params[10];
            utility.addFormField("survey_date", params[10]);
            utility.addFormField("survey_mode", params[11]);
            utility.addFormField("reward_point", "1");
            surveyMode = params[11];
            id = params[12];
            utility.addFormField("manual_address", params[13]);
            manualAddress = params[13];
            // utility.addFormField("vehicle_info", params[13]);
            utility.addFormField("is_android", "1");
            utility.addFormField("other_offence", params[14]);
            // Log.e("doInBackground: ", String.valueOf(Prefrence.getCircleID(context)));
            // utility.addFormField("circle_id", String.valueOf(Prefrence.getCircleID(context)));
            String response = utility.finish();
            if (response != null) {
                Log.e("SERVER_RESPONSE", response);
                return sendDetailResponse(response);
            }
        } catch (IOException e) {

            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private HashMap sendDetailResponse(String response) throws JSONException {
        try {
            HashMap<String, String> responseData = new HashMap<>();
            JSONObject object = new JSONObject(response);
            responseData.put("status", object.getString("status"));
            responseData.put("message", object.getString("message"));
            if (object.getString("status").equals("1")) {
                JSONObject resultObj = object.getJSONObject("data");
                String obj = resultObj.getString("complaint_number");
                responseData.put("complaintNumber", obj);
            }
            return responseData;
        } catch (JSONException je) {
            je.printStackTrace();
            if (surveyMode.equals("online")) {
                showDatabaseDialog(new JSONObject(response).getString("message").toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (surveyMode.equals("online")) {
                showDatabaseDialog(new JSONObject(response).getString("message").toString());
            }
        }
        return null;
    }


    @Override
    protected void onPostExecute(HashMap<String, String> result) {
        super.onPostExecute(result);
        progressDialog.dismiss();
        if (send != null) {
            send.setEnabled(true);
            send.setAlpha(1f);
        }
        if (result != null) {
            if (result.get("status").equals("1")) {
                Log.e("onPostExecute: ", result.get("message"));

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                alertDialog.setCancelable(false);
                alertDialog.setMessage("Your complaint ticket number #" + result.get("complaintNumber").toString() +
                        " has been uploaded successfully");
                alertDialog.setPositiveButton(context.getResources().getString(R.string.Ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        if (surveyMode.equals("offline")) {
                            database.deleteComplaint(id);
                        }
                        Intent intent = new Intent(context, ManagerHomeActivity.class);
                        context.startActivity(intent);
                        ((Activity) context).finish();
                        dialog.dismiss();
                    }
                });
                alertDialog.show();

            } else {
                Log.e("onPostExecute: ", result.get("message"));
                if (surveyMode.equals("online")) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        Log.e(TAG, "onPostExecute: Job Scheduled");
                        Util.scheduleJob(context);
                    }
                    saveinDatabase();
//                    showDatabaseDialog(result.get("message"));
                    String message = result.get("message");
                    if (message != null) {
                        if (message.contains("Your session has expired"))
                            Util.showSessionExpiredDialog(context);
                    } else {
                        Util.showAlert(context, context.getString(R.string.pta));
                    }
                } else {
                    Util.showAlert(context, context.getString(R.string.pta));
                    ((InProgressDetailActivity) context).send.setEnabled(true);
                    ((InProgressDetailActivity) context).send.setAlpha(1.0f);
                }
            }
        }
    }

    private void saveinDatabase() {
        boolean status = database.AddDataValueOffline(rcNumber,
                offence,
                comment,
                imageUri,
                videoUri,
                audioUri,
                address,
                manualAddress,
                lat,
                lng,
                0,
                timeStamp);

        if (status) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
            String finalTimeStamp = Util.formatDateFromTimeStamp("yyyy-MM-dd HH:mm:ss", "yyyyMMddHHmmss", timeStamp);
            alertDialog.setMessage("Your complaint ticket number #TEMP_" +
                    finalTimeStamp + " has been saved successfully.");
            alertDialog.setCancelable(false);
            alertDialog.setPositiveButton(context.getString(R.string.Ok), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    context.startActivity(new Intent(context, ManagerHomeActivity.class));
                    ((Activity) context).finish();
                    dialog.dismiss();
                }
            });
            alertDialog.show();
        }
    }

    public void showDatabaseDialog(String message) {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setMessage(message);
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton(context.getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                saveinDatabase();
            }
        });

        alertDialog.setNegativeButton(context.getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                ((SendComplaintActivity) context).send.setEnabled(true);
                ((SendComplaintActivity) context).send.setAlpha(1.0f);
                dialog.dismiss();
            }
        });

        alertDialog.show();
    }
}
