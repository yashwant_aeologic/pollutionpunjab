package com.aeologic.cctv.OfficersActivities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.aeologic.cctv.Activity.UserLoginActivity;
import com.aeologic.cctv.Async.LoginAsyncTask;
import com.aeologic.cctv.Connection.ConnectionDetector;
import com.aeologic.cctv.R;
import com.aeologic.cctv.Utility.Prefrence;
import com.aeologic.cctv.Utility.Util;

import java.util.regex.Pattern;


public class ManagerLoginActivity extends AppCompatActivity {
    public static final String MyPREFERENCES = "MyPrefs";
    private static final int MY_PERMISSIONS_REQUEST_ACCESS = 1;
    private static final int MY_PERMISSION_SMS_ACCESS = 2;
    Button loginButton;
    EditText emailTxt;
    EditText password;
    boolean doubleBackToExitPressedOnce = false;
    private ConnectionDetector detector;
    String device_id;
    boolean forgotpassStatus = false;
    SharedPreferences sharedpreferences;
    CheckBox showPassword;
    private TextView userLogin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login1);
        initViews();
        emailTxt.setFocusable(true);
        emailTxt.requestFocus();

        if (checkPhoneStatePermission()) {
            saveImei();
        }

        if (getIntent() != null) {
            forgotpassStatus = getIntent().getBooleanExtra("forgotPassStatus", false);
        }

        showPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                } else {
                    password.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (detector.isConnectingToInternet()) {

                    if (emailTxt.getText().toString().trim().length() != 0) {

                        if (password.getText().toString().trim().length() != 0) {
                            LoginAsyncTask loginAsyncTask = new LoginAsyncTask(ManagerLoginActivity.this);
                            loginAsyncTask.execute(getString(R.string.API_BASE_URL) + getString(R.string.LOGIN_API), emailTxt.getText().toString().trim(), password.getText().toString().trim());
                        } else {
                            Util.showAlert(ManagerLoginActivity.this, "Please enter valid password.");
                        }


                    } else {
                        Util.showAlert(ManagerLoginActivity.this, "Please enter valid user ID.");
                    }


                } else {
                    Util.showAlert(ManagerLoginActivity.this, getResources().getString(R.string.please_check_internet));
                }


            }
        });
        userLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ManagerLoginActivity.this, UserLoginActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);
                finish();
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
//        Util.finishActivity(ManagerLoginActivity.this);
    }

    private boolean isValidEmaillId(String email) {

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }


    private void initViews() {
        loginButton = (Button) findViewById(R.id.otp);
        emailTxt = (EditText) findViewById(R.id.number_et);
        password = (EditText) findViewById(R.id.pwd_et);
        detector = new ConnectionDetector(getApplicationContext());
        showPassword = (CheckBox) findViewById(R.id.checkbox);
        userLogin = (TextView) findViewById(R.id.hyperlink);

    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, getResources().getString(R.string.confirm_exit), Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    private void saveImei() {
        TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        device_id = tm.getDeviceId();
        Prefrence.setIMEI(ManagerLoginActivity.this, device_id);
    }


    public boolean checkPhoneStatePermission() {
        if (ActivityCompat.checkSelfPermission(ManagerLoginActivity.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {


            ActivityCompat.requestPermissions(ManagerLoginActivity.this,
                    new String[]{Manifest.permission.READ_PHONE_STATE},
                    MY_PERMISSIONS_REQUEST_ACCESS);
            return false;
        }
        return true;
    }

    public boolean checkSmsPermission() {
        if (ActivityCompat.checkSelfPermission(ManagerLoginActivity.this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED) {


            ActivityCompat.requestPermissions(ManagerLoginActivity.this,
                    new String[]{Manifest.permission.READ_SMS},
                    MY_PERMISSION_SMS_ACCESS);
            return false;
        }
        return true;
    }

    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission is granted do the task here
                    saveImei();
                }
                break;

            }


        }
    }


}
