package com.aeologic.cctv.OfficersActivities;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.aeologic.cctv.Activity.DoneDetailActivity;
import com.aeologic.cctv.Async.ImageLoadTask1;
import com.aeologic.cctv.Async.ImageLoadTask2;
import com.aeologic.cctv.R;
import com.aeologic.cctv.Receiver.ConnectionChangeReceiver;


/**
 * Created by u104 on 16/5/17.
 */

public class ManagerFullImageActivity extends AppCompatActivity {
    ImageView cancel;
    ImageView fullImageView;
    ConnectionChangeReceiver connectionChangeReceiver;
    /* TextView timeStamp;
     TextView latlng;*/
    String TAG = "FullImageActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
//Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.dialog_full_image);
        initViews();

//        connectionChangeReceiver = new ConnectionChangeReceiver();
//        this.registerReceiver(connectionChangeReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));

        if (getIntent() != null) {
            String activityName = getIntent().getStringExtra("activityName");

            Bitmap bitmap = null;
            Log.e(TAG, "onCreate: "+getIntent().getStringExtra("imageUri") );

            if (activityName.equals("inProgress")) {
                fullImageView.setImageURI(Uri.parse(getIntent().getStringExtra("imageUri")));


//                Bitmap tempBitmap = BitmapFactory.decodeFile(getIntent().getStringExtra("imageUri"));
//                bitmap = drawTextToBitmap(FullImageActivity.this,
//                        tempBitmap,
//                        getIntent().getStringExtra("timeStamp")
//                        , getIntent().getStringExtra("latlng"));

            } else if(activityName.equals("done")){
                String img = getIntent().getStringExtra("IMG");
                new ImageLoadTask2(ManagerFullImageActivity.this, fullImageView, null).execute(img);
               // fullImageView.setImageBitmap(ManagerDoneDetailActivity.imageBitmap);
//                bitmap = drawTextToBitmap(FullImageActivity.this,
//                        DoneDetailActivity.imageBitmap,
//                        getIntent().getStringExtra("timeStamp")
//                        , getIntent().getStringExtra("latlng"));
            }else{
                String img = getIntent().getStringExtra("IMG");
                new ImageLoadTask2(ManagerFullImageActivity.this, fullImageView, null).execute(img);
              //  fullImageView.setImageBitmap(ManagerDoneDetailActivity.imageBitmap1);
            }

            if (bitmap != null) {
                fullImageView.setImageBitmap(bitmap);
            }

//            Bitmap b = ((BitmapDrawable) fullImageView.getBackground()).getBitmap();


           /* timeStamp.setText(getIntent().getStringExtra("timeStamp"));
            latlng.setText(getIntent().getStringExtra("latlng"));*/
        }

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ManagerDoneDetailActivity.playpauseClicked = false;
                finish();
            }
        });

        /*DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        Log.e(TAG, "onCreate: screen dimension: " + height + "*" + width);*/


    }

    private void initViews() {
        cancel = (ImageView) findViewById(R.id.cancel);
        fullImageView = (ImageView) findViewById(R.id.full_image);
       /* latlng = (TextView) findViewById(R.id.latlng);
        timeStamp = (TextView) findViewById(R.id.timeStamp);*/
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        ManagerDoneDetailActivity.playpauseClicked = false;
        finish();
    }

    @Override
    protected void onStop() {
        super.onStop();
        /*try {
            unregisterReceiver(connectionChangeReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }*/

    }

//    public Bitmap drawTextToBitmap(Context mContext, Bitmap bitmap, String text1, String text2) {
//        try {
//            Resources resources = mContext.getResources();
//            float scale = resources.getDisplayMetrics().density;
////Bitmap bitmap = BitmapFactory.decodeResource(resources, resourceId);
//
//            android.graphics.Bitmap.Config bitmapConfig = bitmap.getConfig();
//// set default bitmap config if none
//            if (bitmapConfig == null) {
//                bitmapConfig = android.graphics.Bitmap.Config.ARGB_8888;
//            }
//// resource bitmaps are imutable,
//// so we need to convert it to mutable one
//            bitmap = bitmap.copy(bitmapConfig, true);
//
//            Canvas canvas = new Canvas(bitmap);
//// new antialised Paint
//            Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
//// text color - #3D3D3D
//            paint.setColor(Color.rgb(255, 0, 0));
//// text size in pixels
//            paint.setTextSize((int) (9 * scale));
//// text shadow
//            paint.setShadowLayer(1f, 0f, 1f, Color.DKGRAY);
//
//// draw text to the Canvas center
//            Rect bounds = new Rect();
//            paint.getTextBounds(text1, 0, text1.length(), bounds);
//            int x = 20;
//            int y = bitmap.getHeight() - 30;
//            Log.e(TAG, "drawTextToBitmap: iamgeSize: " + bitmap.getWidth() + "*" + bitmap.getHeight());
//            Log.e(TAG, "drawTextToBitmap: text: " + text1);
//            canvas.drawText(text1, x, y, paint);
//            canvas.drawText(text2, x, y - 25, paint);
//
//            return bitmap;
//        } catch (Exception e) {
//// TODO: handle exception
//
//
//            return null;
//        }
//
//    }
}
