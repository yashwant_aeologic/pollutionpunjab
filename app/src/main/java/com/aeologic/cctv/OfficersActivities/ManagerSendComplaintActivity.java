package com.aeologic.cctv.OfficersActivities;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.media.ThumbnailUtils;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.aeologic.cctv.Async.DownloadImagesAsync;
import com.aeologic.cctv.BuildConfig;
import com.aeologic.cctv.Connection.ConnectionDetector;
import com.aeologic.cctv.Database.Database;
import com.aeologic.cctv.GooglePlaceAPI.DownloadLatLongAsync;
import com.aeologic.cctv.GooglePlaceAPI.GetAddressAsyncTask;
import com.aeologic.cctv.R;
import com.aeologic.cctv.Utility.Prefrence;
import com.aeologic.cctv.Utility.Util;
import com.aeologic.cctv.map.TouchableMapFragment;
import com.afollestad.materialcamera.MaterialCamera;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.skyfishjy.library.RippleBackground;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;


public class ManagerSendComplaintActivity extends AppCompatActivity implements
        LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, DownloadLatLongAsync.OnGeocodeComplete
        /* MultiSelectSpinner.OnMultipleItemsSelectedListener */ {
    private static final String TAG = "SendComplaintActivity";
    private static final int REQUEST_WRITE_STORAGE = 112;
    private static final int RESULT_LOAD_IMAGE = 15;
    private final int REQUEST_ID_MULTIPLE_PERMISSIONS = 3;
    private final int MY_PERMISSIONS_REQUEST_ACCESS_CAMERA = 2;
    private final int MY_PERMISSIONS_REQUEST_ACCESS_VIDEO = 4;
    private final int MY_PERMISSIONS_REQUEST_ACCESS_LOCATION = 5;
    GoogleMap googleMap;
    public static double mLatitude, cLatitude;
    public static double mLongitude, cLongitude;
    //    TouchableMapFragment mapFragment;
//    private Marker mapMarker;
//    private LinearLayout editableMap;
    //    public TextView address;
    private String outputFile = null;
    private String outVideoFile = null;
    private String outImageFile = null;
    private String outMerchantName = null;
    //    ImageView back;
    ImageView camera;
    ImageView videoCamera;
    ImageView audio;
    private final int REQUEST_VIDEO_CAPTURE = 1;
    private final int REQUEST_IMAGE_CAPTURE = 100;
    private MediaRecorder myRecorder;
    //    private boolean isRecording = true;
    private boolean isRecording = false;
    ScrollView scrollView;
    CountDownTimer countDownTimer;
    private GoogleMap googleMap1;
    TextView textViewIssueType;
    Marker mapMarker1;
    private SupportMapFragment mapFragment1;
    //    private AlertDialog mapDialog;
    boolean isCameraOpenFirstTime = true;
    boolean isVedioCapturedFirstTime = true;
    boolean isAudioRecordingFirstTime = true;
    //    private AlertDialog photoDialog;
    private Bitmap bitmap;
    private ImageView imageViewContainer;

    private Uri videoUri;
    private Uri compressedVideoUri;
    private Uri finalVideoUri;
    //    private AlertDialog videoDialog;
    private Uri mImageCaptureUri;
    private Uri finalImageCaptureUri = null;
    private File mediaStorageDir;
    File audioFile;
    private Dialog audioDialog;
    boolean isVideoPlaying = false;
    private boolean videoResumed = false;
    private int stopVideoPosition;
    private File videomediaFile;
    private File compressedVideoMediaFile;
    private File finalvideomediaFile;
    private VideoView videoView;
    private TextView timer, timerPlay, sendComplaint;
    private ImageView retake;
    public boolean startButtonActivate = true;
    public boolean playButtonActivate = false;
    public boolean stopButtonActivate = false;
    public boolean pauseButtonActivate = false;
    private ImageView start, satellite;
    private MediaPlayer mp;
    private Handler myHandler = new Handler();

    private int audioTotaltime;
    private RippleBackground rippleBackground;
    private int audioSeektime = 0;

    private int audioStartTime;
    private TimerTask timerTask;
    private Timer timer1;
    private Dialog photoDialog;
    private Dialog VideoDialog;
    public static TextView addressText;
    private Dialog mapDialog;
    private boolean videoPauseButtonActivate = false;
    private ImageView satelite;
    public Button send;
    private EditText comment, address, manualAddress;
    private TextInputLayout addressTEL;
    private EditText date, time;
    String merchantDetails, commentDetails;
    String workFolder = null;
    String demoVideoFolder = null;
    String demoVideoPath = null;
    String vkLogPath = null;
    private boolean commandValidationFailedFlag = false;
    private String timeStamp;
    private String videoPath;
    public boolean confirmButtonAvtivate = false;
    private ImageView audioCofirm;
    private String token = null;

    private ConnectionDetector detector;
    public boolean isActivityRunning;
    public TextView centerView;
    public RelativeLayout connectionInfo;
    public String rcResponse;
    String languageSelected;
    private static final long INTERVAL = 1000 * 30;
    private static final long FASTEST_INTERVAL = 1000 * 5;
    private static final long ONE_MIN = 1000 * 60;
    private static final long REFRESH_TIME = ONE_MIN * 5;
    private static final float MINIMUM_ACCURACY = 50.0f;
    Activity locationActivity;
    private LocationRequest locationRequest;
    private GoogleApiClient googleApiClient;
    private Location location;
    private FusedLocationProviderApi fusedLocationProviderApi = LocationServices.FusedLocationApi;
    HandlerThread mHandlerThread;
    Handler mThreadHandler;
    Database database;
    RecyclerView recyclerView;
    ImageView orientation;
    boolean orientationFlag = true;// true for horizontal and false for vertical
    private Dialog imageSelecetioDialog;
    public boolean userInteractionStatus = false;
    private String offence;
    BroadcastReceiver connectionChangeReceiver;
    private boolean locationFirstTime = true;
    boolean[] statusArray;
    public static boolean allImageDownload = false;
    ImageView back;
    private String otherOffence;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_complaint);

        initViews();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userInteractionStatus ||
                        !comment.getText().toString().isEmpty()) {
                    showWarning();
                } else {
                    startActivity(new Intent(ManagerSendComplaintActivity.this, ManagerHomeActivity.class));
                    overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);
                    finish();
                }
            }
        });
        connectionChangeReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                Log.e(TAG, "onReceive:intent " + intent);
                ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetInfo = connectivityManager.getActiveNetworkInfo();
                Log.e(TAG, "onReceive: ");

                if (activeNetInfo != null && activeNetInfo.isConnected()) {

                    Log.d(TAG, "onReceive: net connected");
                    Log.e(TAG, "onReceive: " + isActivityRunning);//check this flag and show the and hide the view
                    if (isActivityRunning) {
                        connectionInfo.setVisibility(View.GONE);
                        send.setText(context.getResources().getString(R.string.send));
                        new GetAddressAsyncTask(ManagerSendComplaintActivity.this, address, addressText, manualAddress)
                                .execute(getString(R.string.API_ADDRESS) +
                                        getString(R.string.mapApiKey) + "&latlng=" +
                                        String.valueOf(mLatitude) + "," + String.valueOf(mLongitude));

                    }
                } else {
                    if (isActivityRunning) {
                        Log.e(TAG, "onReceive: Reciever");
                        connectionInfo.setVisibility(View.VISIBLE);
                        send.setText(context.getResources().getString(R.string.save));
                        centerView.setText("" + mLatitude + "," + mLongitude);
                    }
                    Log.e(TAG, "onReceive: " + isActivityRunning);
                }
            }
        };

        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(INTERVAL);
        locationRequest.setFastestInterval(FASTEST_INTERVAL);
        this.locationActivity = locationActivity;

        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        if (googleApiClient != null) {
            googleApiClient.connect();
        }

       /* if (!checkLocationOnOFF()) {
            locationChecker(googleApiClient, SendComplaintActivity.this);
        }*/

        boolean hasPermission = (ContextCompat.checkSelfPermission(ManagerSendComplaintActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
        if (!hasPermission) {
            ActivityCompat.requestPermissions(ManagerSendComplaintActivity.this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_WRITE_STORAGE);
        }


        final ArrayList<HashMap<String, String>> offenceList = database.getOffenceList();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
//                if (!allImageDownload) {
                if (detector.isConnectingToInternet()) {
                    HashMap hashmap;
                    for (int i = 0; i < offenceList.size(); i++) {
                        hashmap = offenceList.get(i);
                        if (hashmap.get("imagePath") == null) {
                            // call async to download the images
                            new DownloadImagesAsync(ManagerSendComplaintActivity.this).execute(offenceList);
                            return;
                        }
                    }
//                    }
                }
                HashMap hashmap;
                for (int i = 0; i < offenceList.size(); i++) {
                    hashmap = offenceList.get(i);
                    if (hashmap.get("imagePath") == null) {
                        // call async to download the images
                        new DownloadImagesAsync(ManagerSendComplaintActivity.this).execute(offenceList);
                    }
                }
            }
        }, 5000);
//        final LinkedHashMap<String, String> offenceMap = database.getOffenceMap();
        statusArray = new boolean[offenceList.size()];
        Arrays.fill(statusArray, false);

        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(),
                LinearLayoutManager.HORIZONTAL, false));

        final ManagerOffenceListAdapter adapter = new ManagerOffenceListAdapter(ManagerSendComplaintActivity.this, offenceList, statusArray);
        recyclerView.setAdapter(adapter);

        orientation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (orientationFlag) {
                    recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(),
                            LinearLayoutManager.VERTICAL, false));
//                    adapter.notifyDataSetChanged();
                    orientation.setImageResource(R.drawable.up_tri);
                    orientationFlag = false;
                } else {
                    recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(),
                            LinearLayoutManager.HORIZONTAL, false));
//                    adapter.notifyDataSetChanged();
                    orientation.setImageResource(R.drawable.down_tri);
                    orientationFlag = true;
                }
            }
        });

//        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL,false));

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otherOffence = Prefrence.getOtherComment(ManagerSendComplaintActivity.this);
                if (mLatitude != 0 || mLongitude != 0) {
                    if (manualAddress != null && !manualAddress.getText().toString().trim().isEmpty()) {
                        HashMap offenceData;
                        offence = "";
                        for (int i = 0; i < statusArray.length; i++) {
                            if (statusArray[i]) {
                                offenceData = offenceList.get(i);
                                offence = offence + offenceData.get("offenceId").toString() + ",";
                            }
                        }
                        if (offence != null && !offence.isEmpty() && offence.charAt(offence.length() - 1) == ',') {
                            Log.d(TAG, "onClick: selected offence: " + offence);
                            if (offence.contains("109") ||
                                    offence.contains("110") ||
                                    offence.contains("111")) {
                                //make the video mandatory and image optional;
                                if (finalVideoUri == null) {
                                    Util.showAlert(ManagerSendComplaintActivity.this, getString(R.string.video_mandatory));
                                    return;
                                }
                            }
                            if (offence.contains("101") ||
                                    offence.contains("102") ||
                                    offence.contains("103") ||
                                    offence.contains("104") ||
                                    offence.contains("105") ||
                                    offence.contains("106") ||
                                    offence.contains("107") ||
                                    offence.contains("108")) {
                                //make image mandotory
                                if (finalImageCaptureUri == null) {
                                    Util.showAlert(ManagerSendComplaintActivity.this, getString(R.string.image_mandatory));
                                    return;
                                }
                            } else {
                                // make the image mandatory and video optional
                                if (mImageCaptureUri != null) {
                                    // do the task here
                                    callSendDetailAsync();
                                } else {
                                    Util.showAlert(ManagerSendComplaintActivity.this,
                                            getString(R.string.image_mandatory));
                                }
                            }
                            //  callSendDetailAsync();

                        } else {
                            Util.showAlert(ManagerSendComplaintActivity.this,
                                    getString(R.string.please_select_offence));

                        }
                    } else {
                        Util.showAlert(ManagerSendComplaintActivity.this,
                                getString(R.string.please_enter_address));
                    }
                } else {
                    Util.showAlert(ManagerSendComplaintActivity.this,
                            getString(R.string.can_not_get_location));
                }


            }
        });

        address.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (address.getRight() -
                            address.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here
                        if (detector.isConnectingToInternet()) {
                            new GetAddressAsyncTask(ManagerSendComplaintActivity.this, address, addressText, manualAddress)
                                    .execute(getString(R.string.API_ADDRESS) +
                                            getString(R.string.mapApiKey) + "&latlng=" +
                                            String.valueOf(mLatitude) + "," + String.valueOf(mLongitude));
                            return true;
                        } else {
                            Util.showAlert(ManagerSendComplaintActivity.this, getString(R.string.please_check_internet));
                        }
                    }
                }
                return false;
            }
        });

        comment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                comment.requestFocus();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.e("onTextChanged: ", "");
                if (start == 249) {
                    comment.setInputType(0);
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(comment.getWindowToken(), 0);
                    Toast.makeText(ManagerSendComplaintActivity.this, getResources().getString(R.string.plz_summarize_in), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkCameraPermission()) {
                    captureImage();
                }
            }
        });

        audio.setOnTouchListener(new View.OnTouchListener()

                                 {
                                     @Override
                                     public boolean onTouch(View v, MotionEvent event) {
                                         //  audio.setImageResource(R.drawable.offence_mic_pressed);
                                         if (event.getAction() == MotionEvent.ACTION_UP) {
                                             // image released
                                             audio.setImageResource(R.drawable.offence_mic);
                                             if (CheckAudioPermission()) {
                                                 audioDialog.show();
                                                 confirmButtonAvtivate = false;
                                                 //    audioCofirm.setImageResource(R.drawable.crct);

                                             }
                                         }
                                         return true;
                                     }
                                 }

        );
        videoCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CheckVedioCameraPermission()) {
//                    activityInbackground = true;
                    if (isVedioCapturedFirstTime) {
                        mediaStorageDir = new File(Environment.getExternalStorageDirectory() + "/DCIM/", "pollution");

                        if (!mediaStorageDir.exists()) {
                            if (!mediaStorageDir.mkdirs()) {
                                Log.d("MyCameraApp", "failed to create directory");
                            }
                        }

                        startVideoRecorder();    // Starts the camera activity, the result will be sent back to the current Activity


                    } else {
                        if (videomediaFile != null) {
                            videoView.setVideoURI(finalVideoUri);
                            Bitmap thumb = ThumbnailUtils.createVideoThumbnail(finalvideomediaFile.toString(),
                                    MediaStore.Images.Thumbnails.MINI_KIND);
                            Log.e(TAG, "onTouch: " + thumb.getWidth() + "::" + thumb.getHeight());
                            BitmapDrawable bitmapDrawable = new BitmapDrawable(getResources(), thumb);
                            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                                videoView.setBackgroundDrawable(bitmapDrawable);
                            } else {
                                videoView.setBackground(bitmapDrawable);
                            }
                        }
                        VideoDialog.show();
                    }


                }
            }
        });


        satellite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (googleMap.getMapType() == GoogleMap.MAP_TYPE_NORMAL) {
                    satellite.setImageResource(R.drawable.satellite_on);
                    googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);

                } else if (googleMap.getMapType() == GoogleMap.MAP_TYPE_SATELLITE) {
                    googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                    satellite.setImageResource(R.drawable.satellite_off);
                    //satellite.setBackgroundColor(getResources().getColor(R.color.white));
                }
            }
        });

        date.setText(Util.getCurrentDate());
        time.setText(Util.getCurrentTime());


        try {
            showMap(mLatitude, mLongitude);
        } catch (Exception e) {
            e.printStackTrace();
        }
        ShowPhotoPreview();
        showVideoPreview();
        showAudioPreview();
        imageSelectionDialog();

        Log.e(TAG, "onCreate: register reciever for connectionchange");
        IntentFilter intentFilter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(connectionChangeReceiver, intentFilter);

        manualAddress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 250) {
                    Toast.makeText(ManagerSendComplaintActivity.this, "Character limit reached", Toast.LENGTH_SHORT).show();
                }
            }
        });

        comment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 500) {
                    Toast.makeText(ManagerSendComplaintActivity.this, "Character limit reached", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e(TAG, "onResume: ");


        isActivityRunning = true;
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetInfo != null && activeNetInfo.isConnected()) {
            Log.e(TAG, "onResume: Net connected");
        } else {
            Log.e(TAG, "onResume: Net Disconnected");
        }
        if (finalVideoUri != null) {
            videoView.setVideoURI(finalVideoUri);
            videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.seekTo(stopVideoPosition);
                }
            });
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "onDestroy: ");
        if (googleApiClient != null) {
            googleApiClient.disconnect();
        }
        try {
            unregisterReceiver(connectionChangeReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        isActivityRunning = false;
    }

    private void showAudioPreview() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this, R.style.Theme_Transparent);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_audio, null);
        dialogBuilder.setView(dialogView);
        audioDialog = dialogBuilder.create();

        final ImageView mic = (ImageView) dialogView.findViewById(R.id.mic);
        start = (ImageView) dialogView.findViewById(R.id.start);
        final ImageView cancel = (ImageView) dialogView.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (confirmButtonAvtivate == true) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(ManagerSendComplaintActivity.this);
                    alertDialog.setMessage(getResources().getString(R.string.audio_warn));
                    alertDialog.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            countDownTimer.cancel();
                            audio.setImageResource(R.drawable.offence_mic);
                            //stop();
                            stopButtonActivate = false;
                            start.setImageResource(R.drawable.audio_play);
                            playButtonActivate = true;
                            audioSeektime = 0;
                            retake.setEnabled(true);
                            retake.setImageResource(R.drawable.audio_restart);
                            rippleBackground.stopRippleAnimation();
                            confirmButtonAvtivate = true;
                            audioCofirm.setImageResource(R.drawable.crct1);
                            timer.setText("00:30 Sec");
                            if (timerPlay.getVisibility() == View.VISIBLE) {
                                timerPlay.setVisibility(View.GONE);
                                timer.setVisibility(View.VISIBLE);

                            }

                            start.setImageResource(R.drawable.audio_start);
                            startButtonActivate = true;
                            retake.setEnabled(false);
                            retake.setImageResource(R.drawable.audio_restarthide);
                            confirmButtonAvtivate = false;
                            audioCofirm.setImageResource(R.drawable.crct);
                            dialog.dismiss();
                            audioDialog.dismiss();

                        }
                    });
                    alertDialog.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    alertDialog.show();
                } else if (isRecording == true) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(ManagerSendComplaintActivity.this);
                    alertDialog.setMessage(getResources().getString(R.string.audio_warn));
                    alertDialog.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            countDownTimer.cancel();
                            // audio.setImageResource(R.drawable.offence_mic_filled);
                            //stop();
                            stopButtonActivate = false;
                            start.setImageResource(R.drawable.audio_play);
                            playButtonActivate = true;
                            audioSeektime = 0;
                            retake.setEnabled(true);
                            retake.setImageResource(R.drawable.audio_restart);
                            rippleBackground.stopRippleAnimation();
                            confirmButtonAvtivate = true;
                            audioCofirm.setImageResource(R.drawable.crct1);
                            timer.setText("00:30 Sec");
                            if (timerPlay.getVisibility() == View.VISIBLE) {
                                timerPlay.setVisibility(View.GONE);
                                timer.setVisibility(View.VISIBLE);

                            }

                            start.setImageResource(R.drawable.audio_start);
                            startButtonActivate = true;
                            retake.setEnabled(false);
                            retake.setImageResource(R.drawable.audio_restarthide);
                            confirmButtonAvtivate = false;
                            audioCofirm.setImageResource(R.drawable.crct);
                            dialog.dismiss();
                            audioDialog.dismiss();

                        }
                    });
                    alertDialog.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    alertDialog.show();

                } else if (confirmButtonAvtivate == false && retake.isEnabled()) {
                    //audio.setImageResource(R.drawable.offence_mic_filled);
                    audioDialog.dismiss();
                } else {
                    audioDialog.dismiss();
                }
            }
        });

        timer = (TextView) dialogView.findViewById(R.id.timer);
        timerPlay = (TextView) dialogView.findViewById(R.id.timer1);
        retake = (ImageView) dialogView.findViewById(R.id.retake);
        audioCofirm = (ImageView) dialogView.findViewById(R.id.audio_confirm);
        rippleBackground = (RippleBackground) dialogView.findViewById(R.id.content);
        if (isAudioRecordingFirstTime) {
            retake.setEnabled(false);
            isAudioRecordingFirstTime = false;
        } else {
            retake.setEnabled(true);
            retake.setImageResource(R.drawable.audio_restart);
            start.setImageResource(R.drawable.audio_play);
            stopButtonActivate = false;
            playButtonActivate = true;
        }


        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (startButtonActivate) {
                    start();
                    isRecording = true;
                    start.setImageResource(R.drawable.audio_stop);
                    rippleBackground.startRippleAnimation();
                    startButtonActivate = false;
                    stopButtonActivate = true;
                } else if (stopButtonActivate) {
                    countDownTimer.cancel();
                    audio.setImageResource(R.drawable.offence_mic_filled);
                    stop();
                    stopButtonActivate = false;
                    start.setImageResource(R.drawable.audio_play);
                    playButtonActivate = true;
                    audioSeektime = 0;
                    retake.setEnabled(true);
                    retake.setImageResource(R.drawable.audio_restart);
                    rippleBackground.stopRippleAnimation();
                    confirmButtonAvtivate = true;
                    audioCofirm.setImageResource(R.drawable.crct1);
                } else if (playButtonActivate == true) {

                    mp = new MediaPlayer();
                    try {
                        //you can change the path, here path is external directory(e.g. sdcard) /Music/maine.mp3
                        mp.setDataSource(outputFile);
                        mp.prepare();
                        mp.start();

                        audioTotaltime = mp.getDuration();
                        audioStartTime = mp.getCurrentPosition();
                        if (timer.getVisibility() == View.VISIBLE) {
                            timer.setVisibility(View.GONE);
                            timerPlay.setVisibility(View.VISIBLE);
                            timerPlay.setText(String.format("%d:%d Sec",
                                    TimeUnit.MILLISECONDS.toMinutes((long) audioStartTime),
                                    TimeUnit.MILLISECONDS.toSeconds((long) audioStartTime) -
                                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.
                                                    toMinutes((long) audioStartTime))));
                        }
                        myHandler.postDelayed(UpdateSongTime, 100);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (audioSeektime != 0) {
                        mp.seekTo(audioSeektime);
                        audioStartTime = audioSeektime;
                        audioTotaltime = mp.getDuration();
                        mp.start();

                        timer.setText(String.format("%d min, %d sec",
                                TimeUnit.MILLISECONDS.toMinutes((long) audioStartTime),
                                TimeUnit.MILLISECONDS.toSeconds((long) audioStartTime) -
                                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long) audioStartTime))));
                        myHandler.postDelayed(UpdateSongTime, 100);
                    }
                    mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            start.setImageResource(R.drawable.audio_play);
                            playButtonActivate = true;
                            audioSeektime = 0;
                        }
                    });

                    playButtonActivate = false;
                    start.setImageResource(R.drawable.audio_pause);
                    pauseButtonActivate = true;
                } else if (pauseButtonActivate) {
                    mp.pause();
                    pauseButtonActivate = false;
                    start.setImageResource(R.drawable.audio_play);
                    playButtonActivate = true;
                    audioSeektime = mp.getCurrentPosition();
                }
            }
        });

        retake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timer.setText("00:30 Sec");
                if (timerPlay.getVisibility() == View.VISIBLE) {
                    timerPlay.setVisibility(View.GONE);
                    timer.setVisibility(View.VISIBLE);

                }

                start.setImageResource(R.drawable.audio_start);
                startButtonActivate = true;
                retake.setEnabled(false);
                retake.setImageResource(R.drawable.audio_restarthide);
                confirmButtonAvtivate = false;
                audioCofirm.setImageResource(R.drawable.crct);
            }
        });

        audioCofirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (confirmButtonAvtivate) {
                    //create the final audio uri here
                    audio.setImageResource(R.drawable.offence_mic_filled);
                    audioDialog.dismiss();
                }
            }
        });


    }

    private boolean CheckAudioPermission() {
        List<String> audioPermissionList = new ArrayList<String>();
        if (ActivityCompat.checkSelfPermission(ManagerSendComplaintActivity
                .this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {

            audioPermissionList.add(Manifest.permission.RECORD_AUDIO);
        }
        if (ActivityCompat.checkSelfPermission(ManagerSendComplaintActivity
                .this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            audioPermissionList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (!audioPermissionList.isEmpty()) {
            ActivityCompat.requestPermissions(this, audioPermissionList.toArray(new String[audioPermissionList.size()]),
                    REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    private void startVideoRecorder() {

        MaterialCamera materialCamera =
                new MaterialCamera(ManagerSendComplaintActivity.this)
                        .saveDir(mediaStorageDir)
                        .showPortraitWarning(false)
                        .defaultToFrontFacing(false)
                        .allowChangeCamera(false)
                        .allowRetry(true)
                        .defaultToFrontFacing(false)
                        .autoSubmit(false)
                        .primaryColor(Color.WHITE)
                        .countdownSeconds(20f)
//                        .videoFrameRate(24)
                        .videoEncodingBitRate(1000 * 1024)
                        .audioEncodingBitRate(50000)
                        .videoPreferredAspect(16f / 9f)           // Sets a custom icon used to pause playback
                        .qualityProfile(MaterialCamera.QUALITY_480P);

        materialCamera.start(REQUEST_VIDEO_CAPTURE);
    }

    private Runnable UpdateSongTime = new Runnable() {
        public void run() {
            audioStartTime = mp.getCurrentPosition();
            //  setTimer(audioStartTime);
            timerPlay.setText(String.format("%d:%d Sec",
                    TimeUnit.MILLISECONDS.toMinutes((long) audioStartTime),
                    TimeUnit.MILLISECONDS.toSeconds((long) audioStartTime) -
                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.
                                    toMinutes((long) audioStartTime))));
            myHandler.postDelayed(this, 100);
        }
    };

    private void stop() {
        try {
            myRecorder.stop();
        } catch (RuntimeException stopException) {
            //handle cleanup here
        }
        myRecorder.release();
        myRecorder = null;
        isRecording = false;
        retake.setEnabled(true);
        retake.setImageResource(R.drawable.audio_restart);
        start.setImageResource(R.drawable.audio_play);
        // Toast.makeText(getApplicationContext(), "Audio recorded successfully", Toast.LENGTH_LONG).show();
    }

    public void start() {

        mediaStorageDir = new File(Environment.getExternalStorageDirectory() + "/pollution/", "pollution");
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("MyCameraApp", "failed to create directory");
            }
        }

        final String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        audioFile = new File(mediaStorageDir.getPath() + File.separator + "pollution" + timeStamp + ".mp3");
        outputFile = audioFile.toString();
        myRecorder = new MediaRecorder();
        myRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        myRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        myRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
        myRecorder.setOutputFile(outputFile);

        countDownTimer = new CountDownTimer(20000, 1000) {

            public void onTick(long millisUntilFinished) {
                timer.setText("00:" + millisUntilFinished / 1000 + " Sec");
                try {
                    myRecorder.prepare();
                    myRecorder.start();
                   /* Toast.makeText(getApplicationContext(), "Start recording...",
                            Toast.LENGTH_SHORT).show();
*/
//                    Log.e("Time Remaaing:", "" + millisUntilFinished / 1000);

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
            }

            public void onFinish() {

                myRecorder.stop();
                myRecorder.release();
                myRecorder = null;
                //     Toast.makeText(getApplicationContext(), "Audio record reach its maximum Length. " +
                //            "Audio recorded successfully", Toast.LENGTH_LONG).show();
                isRecording = false;
                retake.setEnabled(true);
                retake.setImageResource(R.drawable.audio_restart);
                start.setImageResource(R.drawable.audio_play);
                audio.setImageResource(R.drawable.mic2);
                playButtonActivate = true;
                stopButtonActivate = false;
                timer.setText("00:00 Sec");
                audioSeektime = 0;
                retake.setEnabled(true);
                retake.setImageResource(R.drawable.audio_restart);
                rippleBackground.stopRippleAnimation();
                // stop Video recording
                // call a method which will send video via email.
            }
        }.start();


    }

    private void captureImage() {
        if (isCameraOpenFirstTime) {
//                        imageSelecetioDialog.show();
            Log.e(TAG, "onClick: isCameraOpenFirstTime");
            mediaStorageDir = new File(Environment.getExternalStorageDirectory() + "/DCIM/", "pollution");
            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    Log.d("MyCameraApp", "failed to create directory");
                }
            }
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

            File mediaFile = new File(mediaStorageDir.getPath() + File.separator + "pollution" + timeStamp + ".jpg");

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                mImageCaptureUri = FileProvider.getUriForFile(ManagerSendComplaintActivity.this,
                        BuildConfig.APPLICATION_ID + ".provider",
                        mediaFile);
            } else {
                mImageCaptureUri = Uri.fromFile(mediaFile);
            }
            finalImageCaptureUri = mImageCaptureUri;
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
            startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
            Log.e(TAG, "onClick: " + mImageCaptureUri.getPath());

        } else {
            if (finalImageCaptureUri != null) {
                Log.e(TAG, "captureImage: " + finalImageCaptureUri);
                imageViewContainer.setImageBitmap(decodeMyFile(new File(finalImageCaptureUri.getPath())));//I(finalImageCaptureUri);
                String ori = getOrientation(finalImageCaptureUri);
                if (ori.equalsIgnoreCase("portrait")) {
                    imageViewContainer.setScaleType(ImageView.ScaleType.FIT_XY);
                } else {
                    imageViewContainer.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                }
                photoDialog.show();
            }
        }
    }


    private void setAlpha(float alpha, View... views) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            for (View view : views) {
                view.setAlpha(alpha);
            }
        }
    }

    public static Bitmap Rotate(Bitmap _input, float _angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(_angle);
        return Bitmap.createBitmap(_input, 0, 0, _input.getWidth(), _input.getHeight(), matrix, true);
    }

    private void callSendDetailAsync() {
        String dateString = date.getText().toString();
      //  Log.e(TAG, "callSendDetailAsync: date: " + dateString);
        String timeString = time.getText().toString();
     //   Log.d(TAG, "callSendDetailAsync: Time:" + timeString);
        final String timeStamp = Util.formatDateFromTimeStamp("dd/MM/yyyyhh:mm a", "yyyy-MM-dd HH:mm", dateString + timeString);
   //     Log.e(TAG, "callSendDetailAsync: timeStamp: " + timeStamp);
        if (finalImageCaptureUri != null) {
//            outImageFile = finalImageCaptureUri.getPath();
            outImageFile = compressImage(finalImageCaptureUri.getPath());
        }
        if (finalVideoUri != null) {
            outVideoFile = finalVideoUri.getPath();
        }

        if (detector.isConnectingToInternet()) {
            send.setAlpha(0.4f);
            send.setEnabled(false);
            new ManagerSendDetailsAsyncTask(ManagerSendComplaintActivity.this, send).
                    execute(getString(R.string.CREATE_COMPLAINT_API1),
                            "",
                            offence,
                            comment.getText().toString().trim(),
                            outImageFile,
                            outVideoFile,
                            outputFile,
                            address.getText().toString().trim(),
                            String.valueOf(mLatitude),
                            String.valueOf(mLongitude),
                            timeStamp,
                            "online", "",
                            manualAddress.getText().toString(),
                            otherOffence);
            /*int size = (outImageFile.length() + outVideoFile.length());

            Log.e(TAG, "TOTAL SIZE : "+size);*/


        } else {

            final AlertDialog.Builder alertDialog = new AlertDialog.Builder(ManagerSendComplaintActivity.this);
            alertDialog.setMessage(R.string.do_you_want_save_offence);
            alertDialog.setCancelable(false);
            alertDialog.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    boolean status = database.AddDataValueOffline(
                            otherOffence,
                            offence,
                            comment.getText().toString(),
                            outImageFile,
                            outVideoFile,
                            outputFile,
                            address.getText().toString(),
                            manualAddress.getText().toString(),
                            mLatitude,
                            mLongitude,
                            0,
                            timeStamp);

                    if (status) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            Log.e(TAG, ": Job Scheduled");
                            Util.scheduleJob(ManagerSendComplaintActivity.this);
                        }
                        AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(ManagerSendComplaintActivity.this);
                        alertDialog1.setMessage("Your complaint ticket number #TEMP_" +
                                new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) +
                                " has been saved successfully.");
                        alertDialog1.setCancelable(false);
                        alertDialog1.setPositiveButton(getResources().getString(R.string.Ok), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                startActivity(new Intent(ManagerSendComplaintActivity.this, ManagerHomeActivity.class));
                                overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);
                                finish();
                                dialog.dismiss();
                            }
                        });
                        alertDialog1.show();
                    }
                }
            });

            alertDialog.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    send.setAlpha(1.0f);
                    send.setEnabled(true);
                    dialog.dismiss();
                }
            });

            alertDialog.show();
        }
    }

    private Bitmap decodeMyFile(File f) {
        try {
            Bitmap tempBitmap = null;
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            tempBitmap = BitmapFactory.decodeStream(new FileInputStream(f), null, o);
            // The new size we want to scale to
            final int REQUIRED_SIZE = 640;
            // Find the correct scale value. It should be the logout of 2.
            int scale = 1;
            Matrix matrix = new Matrix();
            while (o.outWidth / scale >= REQUIRED_SIZE
                    && o.outHeight / scale >= REQUIRED_SIZE) {
                if (scale <= 8) {
                    scale *= 2;
                } else {
                    break;
                }
            }
            Log.e(TAG, "decodeMyFile:scale " + scale);
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            ExifInterface ei = new ExifInterface(f.toString());
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    matrix.postRotate(90);
                    tempBitmap = BitmapFactory.decodeStream(new FileInputStream(f),
                            null, o2);
                    tempBitmap = Bitmap.createBitmap(tempBitmap, 0, 0,
                            tempBitmap.getWidth(), tempBitmap.getHeight(), matrix,
                            true);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    matrix.postRotate(180);
                    tempBitmap = BitmapFactory.decodeStream(new FileInputStream(f),
                            null, o2);
                    tempBitmap = Bitmap.createBitmap(tempBitmap, 0, 0,
                            tempBitmap.getWidth(), tempBitmap.getHeight(), matrix,
                            true);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    matrix.postRotate(270);
                    tempBitmap = BitmapFactory.decodeStream(new FileInputStream(f),
                            null, o2);
                    tempBitmap = Bitmap.createBitmap(tempBitmap, 0, 0,
                            tempBitmap.getWidth(), tempBitmap.getHeight(), matrix,
                            true);
                    break;
                default:
                    matrix.postRotate(0);
                    tempBitmap = BitmapFactory.decodeStream(new FileInputStream(f),
                            null, o2);
                    tempBitmap = Bitmap.createBitmap(tempBitmap, 0, 0,
                            tempBitmap.getWidth(), tempBitmap.getHeight(), matrix,
                            true);
                    break;
            }
            Log.e(TAG, "decodeMyFile: " + tempBitmap.getWidth() + "::" + tempBitmap.getHeight());
            return tempBitmap;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable("uri", finalImageCaptureUri);
        outState.putParcelable("uriVideo", finalVideoUri);
        Log.e(TAG, "onSaveInstanceState: " + finalImageCaptureUri);
        super.onSaveInstanceState(outState);
    }

    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        finalImageCaptureUri = savedInstanceState.getParcelable("uri");
        finalVideoUri = savedInstanceState.getParcelable("uriVideo");
        Log.e(TAG, "onRestoreInstanceState: " + finalImageCaptureUri);
        super.onSaveInstanceState(savedInstanceState);
    }


    private void showVideoPreview() {

        VideoDialog = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        VideoDialog.setContentView(R.layout.dialog_video);
        VideoDialog.setCanceledOnTouchOutside(true);
        Window window = VideoDialog.getWindow();
        final ImageView _continue = (ImageView) VideoDialog.findViewById(R.id._continue);
        final ImageView remove = (ImageView) VideoDialog.findViewById(R.id.remove);
        final ImageView retake = (ImageView) VideoDialog.findViewById(R.id.retake);
        videoView = (VideoView) VideoDialog.findViewById(R.id.videoview);
        final ImageView play = (ImageView) VideoDialog.findViewById(R.id.play);

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoView.setBackgroundResource(0);
                if (videoResumed) {
                    videoView.seekTo(stopVideoPosition);

                    videoView.start();
                    play.setVisibility(View.GONE);
                    retake.setVisibility(View.GONE);
                    _continue.setVisibility(View.GONE);
                    remove.setVisibility(View.GONE);
                    play.setImageResource(R.drawable.pause);
                    isVideoPlaying = true;
                    videoResumed = false;
                    videoPauseButtonActivate = true;
                } else if (isVideoPlaying == false) {
                    try {
                        videoView.setVideoURI(finalVideoUri);
                        videoView.requestFocus();
                        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                            @Override
                            public void onPrepared(MediaPlayer mp) {
                                mp.start();
                            }
                        });
                        isVideoPlaying = true;
                        play.setImageResource(R.drawable.pause);
                        play.setVisibility(View.GONE);
                        remove.setVisibility(View.GONE);
                        videoPauseButtonActivate = true;
                        _continue.setVisibility(View.GONE);
                        retake.setVisibility(View.GONE);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    stopVideoPosition = videoView.getCurrentPosition();
                    videoView.pause();
                    isVideoPlaying = false;
                    videoResumed = true;
                    play.setImageResource(R.drawable.play);
                    _continue.setVisibility(View.VISIBLE);
                    retake.setVisibility(View.VISIBLE);
                    remove.setVisibility(View.VISIBLE);
                    videoPauseButtonActivate = false;
                }


            }
        });

        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                play.setImageResource(R.drawable.play);
                play.setVisibility(View.VISIBLE);
                isVideoPlaying = false;
                videoResumed = false;
                _continue.setVisibility(View.VISIBLE);
                retake.setVisibility(View.VISIBLE);
                remove.setVisibility(View.VISIBLE);
                videoPauseButtonActivate = false;
            }
        });

        videoView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (play.getVisibility() == View.GONE) {
                    play.setVisibility(View.VISIBLE);
                } else if (videoPauseButtonActivate) {
                    play.setVisibility(View.GONE);
                }
                return false;
            }
        });

        _continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                new TranscdingBackground(SendComplaintActivity.this).execute();
                VideoDialog.dismiss();
                videoCamera.setImageResource(R.drawable.offence_video_filled);
            }
        });

        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeVideoWarning();
            }
        });

        retake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaStorageDir = new File(Environment.getExternalStorageDirectory() + "/DCIM/", "traffic_sentinel");
                if (!mediaStorageDir.exists()) {
                    if (!mediaStorageDir.mkdirs()) {
                        Log.e("MyCameraApp", "failed to create directory");
                    }
                }
                startVideoRecorder();
                VideoDialog.dismiss();
            }
        });
    }

    public void removeVideoWarning() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ManagerSendComplaintActivity.this);
        alertDialog.setMessage((getResources().getString(R.string.warning_remove_video)));
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                //null the image uri and open as first time
                finalVideoUri = null;
                isVedioCapturedFirstTime = true;
                videoCamera.setImageResource(R.drawable.video_normal);
                VideoDialog.dismiss();
                dialog.dismiss();
            }
        });

        alertDialog.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertDialog.show();
    }

    private void ShowPhotoPreview() {
        photoDialog = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        photoDialog.setContentView(R.layout.dialog_photo);
        final ImageView _continue = (ImageView) photoDialog.findViewById(R.id._continue);
        final ImageView remove = (ImageView) photoDialog.findViewById(R.id.remove);
        final ImageView retake = (ImageView) photoDialog.findViewById(R.id.retake);
        imageViewContainer = (ImageView) photoDialog.findViewById(R.id.image_container);


        _continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                camera.setImageResource(R.drawable.offence_camera_filled);
                photoDialog.dismiss();
            }
        });

        retake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaStorageDir = new File(Environment.getExternalStorageDirectory() + "/DCIM/", "pollution");
                if (!mediaStorageDir.exists()) {
                    if (!mediaStorageDir.mkdirs()) {
                        Log.d("MyCameraApp", "failed to create directory");
                    }
                }
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

                File mediaFile = new File(mediaStorageDir.getPath() + File.separator + "pollution" + timeStamp + ".jpg");
                // mImageCaptureUri = Uri.fromFile(mediaFile);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    mImageCaptureUri = FileProvider.getUriForFile(ManagerSendComplaintActivity.this,
                            BuildConfig.APPLICATION_ID + ".provider",
                            mediaFile);
                } else {
                    mImageCaptureUri = Uri.fromFile(mediaFile);
                }
                intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
                startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
                photoDialog.dismiss();

                photoDialog.dismiss();
//                imageSelecetioDialog.show();
            }
        });

        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removePhotoWarning();
            }
        });
    }

    public void removePhotoWarning() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ManagerSendComplaintActivity.this);
        alertDialog.setMessage((getResources().getString(R.string.warning_remove_photo)));
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                //null the image uri and open as first time
                finalImageCaptureUri = null;
                isCameraOpenFirstTime = true;
                camera.setImageResource(R.drawable.camera_normal);
                photoDialog.dismiss();
                dialog.dismiss();
            }
        });

        alertDialog.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertDialog.show();
    }

    private void imageSelectionDialog() {
        imageSelecetioDialog = new Dialog(this, R.style.CustomBottomDialog);
        imageSelecetioDialog.setContentView(R.layout.dialog_image_selection);
        imageSelecetioDialog.setCanceledOnTouchOutside(true);
        Window window = imageSelecetioDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.BOTTOM;
        wlp.flags &= ~WindowManager.LayoutParams.ALPHA_CHANGED;
        window.setAttributes(wlp);
        imageSelecetioDialog.getWindow().setLayout(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.WRAP_CONTENT);

        Button cancel = (Button) imageSelecetioDialog.findViewById(R.id.cancel_btn);
        TextView openCamera = (TextView) imageSelecetioDialog.findViewById(R.id.open_camera);
        TextView chooseFromGallery = (TextView) imageSelecetioDialog.findViewById(R.id.gallery_select);

        openCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                activityInbackground = true;

                mediaStorageDir = new File(Environment.getExternalStorageDirectory() + "/DCIM/", "pollution");
                if (!mediaStorageDir.exists()) {
                    if (!mediaStorageDir.mkdirs()) {
                        Log.d("MyCameraApp", "failed to create directory");
                    }
                }
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

                File mediaFile = new File(mediaStorageDir.getPath() + File.separator + "pollution" + timeStamp + ".jpg");

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    mImageCaptureUri = FileProvider.getUriForFile(ManagerSendComplaintActivity.this,
                            BuildConfig.APPLICATION_ID + ".provider",
                            mediaFile);
                } else {
                    mImageCaptureUri = Uri.fromFile(mediaFile);
                }
                intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
                startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
                Log.e(TAG, "onClick: " + mImageCaptureUri.getPath());
                //dismiss the dialog
                imageSelecetioDialog.dismiss();
            }
        });

        chooseFromGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                activityInbackground = true;
                // Create intent to Open Image applications like Gallery, Google Photos
                Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                // Start the Intent
                startActivityForResult(galleryIntent, RESULT_LOAD_IMAGE);

                //dismiss the dialog
                imageSelecetioDialog.dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageSelecetioDialog.dismiss();
            }
        });


    }

    private void setLatLng(double mLatitude, double mLongitude) {
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLatitude, mLongitude), 17f));

        if (locationFirstTime) {
            new GetAddressAsyncTask(ManagerSendComplaintActivity.this, address, addressText, manualAddress)
                    .execute(getString(R.string.API_ADDRESS) +
                            getString(R.string.mapApiKey) + "&latlng=" +
                            String.valueOf(mLatitude) + "," + String.valueOf(mLongitude));
            locationFirstTime = false;
        }

        // rippleBackground.startRippleAnimation();

    }

    public String compressImage(String imageUri) {


        String filePath = getRealPathFromURI(imageUri);
        Bitmap scaledBitmap = null;

        scaledBitmap = decodeMyFile(new File(filePath));
        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);
//          write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);
            Log.e(TAG, "compressImage: " + scaledBitmap.getWidth() + "x" + scaledBitmap.getHeight());

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return filename;

    }

    private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "pollution/Images");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e(TAG, "onActivityResult: RequestCOde: " + requestCode);
        //for image capture
//        activityInbackground = false;
        Log.e(TAG, "onActivityResult: " + mImageCaptureUri);
        if (requestCode == REQUEST_IMAGE_CAPTURE) {
            if (resultCode == RESULT_OK) {
                try {
                    bitmap = decodeMyFile(new File(mImageCaptureUri.getPath()));
                    Log.e(TAG, "onActivityResult: " + bitmap.getWidth() + ":" + bitmap.getHeight());
                    isCameraOpenFirstTime = false;
                    camera.setImageResource(R.drawable.offence_camera_filled);
                    userInteractionStatus = true;
                    finalImageCaptureUri = mImageCaptureUri;
                } catch (Exception e) {
                    Log.e("onActivityResult: ", e.toString());
                    Toast.makeText(ManagerSendComplaintActivity.this, "Problem with camera...Please try again!", Toast.LENGTH_SHORT).show();
                }
            } else if (resultCode == RESULT_CANCELED) {
            }

        }

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            Log.e("MainActivity", "onActivityResult: ImagePath: " + picturePath);
            cursor.close();
            mImageCaptureUri = Uri.parse(picturePath);
            finalImageCaptureUri = mImageCaptureUri;
            Log.e(TAG, "onClick: " + mImageCaptureUri.getPath());
            imageViewContainer.setImageURI(mImageCaptureUri);
            String ori = getOrientation(mImageCaptureUri);
            if (ori.equalsIgnoreCase("portrait")) {
                imageViewContainer.setScaleType(ImageView.ScaleType.FIT_XY);
            } else {
                imageViewContainer.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            }

            isCameraOpenFirstTime = false;
            camera.setImageResource(R.drawable.offence_camera_filled);
            userInteractionStatus = true;

//            imageView.setImageURI(Uri.parse(picturePath));
        }

        //for video
        if (requestCode == REQUEST_VIDEO_CAPTURE) {
            if (resultCode == RESULT_OK) {
                final File videomediaFile = new File(data.getData().getPath());
                /*Toast.makeText(
                        this,
                        String.format("Saved to: %s, size: %s", videomediaFile.getAbsolutePath(), Util.fileSize(videomediaFile)),
                        Toast.LENGTH_LONG)
                        .show();*/
                Log.e(TAG, "onActivityResult: " + String.format("Saved to: %s, size: %s", videomediaFile.getAbsolutePath(), Util.fileSize(videomediaFile)));
                videoCamera.setImageResource(R.drawable.offence_video_filled);
                isVedioCapturedFirstTime = false;
                finalVideoUri = Uri.parse(videomediaFile.getPath());
                finalvideomediaFile = videomediaFile;
                userInteractionStatus = true;
            } else if (data != null) {
                Exception e = (Exception) data.getSerializableExtra(MaterialCamera.ERROR_EXTRA);
                if (e != null) {
                    e.printStackTrace();
                    Toast.makeText(this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    private String getOrientation(Uri uri) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        String orientation = "landscape";
        try {
            String image = new File(uri.getPath()).getAbsolutePath();
            BitmapFactory.decodeFile(image, options);
            int imageHeight = options.outHeight;
            int imageWidth = options.outWidth;
            if (imageHeight > imageWidth) {
                orientation = "portrait";
            }
        } catch (Exception e) {
            //Do nothing
        }
        return orientation;
    }

    private void initViews() {

        address = (EditText) findViewById(R.id.address);
        addressTEL = (TextInputLayout) findViewById(R.id.editTextLocation);
        manualAddress = (EditText) findViewById(R.id.manual_address_et);
//        editableMap = (LinearLayout) findViewById(R.id.editable_map);
        camera = (ImageView) findViewById(R.id.camera);
        satelite = (ImageView) findViewById(R.id.satellite);
        videoCamera = (ImageView) findViewById(R.id.video_camera);
        addressText = (TextView) findViewById(R.id.address1);
        scrollView = (ScrollView) findViewById(R.id.scrollView);
        send = (Button) findViewById(R.id.rejectBtn);
        //   rcNumber = (EditText) findViewById(R.id.rcNumber);
        comment = (EditText) findViewById(R.id.comment);
        satellite = (ImageView) findViewById(R.id.satellite);
        connectionInfo = (RelativeLayout) findViewById(R.id.connection_info);
        centerView = (TextView) findViewById(R.id.center_view);
        //  toolbar = (Toolbar) findViewById(R.id.toolbar);
        // customMarker = (ImageView) findViewById(R.id.marker);
        // rippleBackground = (RippleBackground) findViewById(R.id.content);
        detector = new ConnectionDetector(getApplicationContext());
        database = new Database(getApplicationContext());
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        orientation = (ImageView) findViewById(R.id.change_orientation);
        audio = (ImageView) findViewById(R.id.audio);
        date = (EditText) findViewById(R.id.date_et);
        time = (EditText) findViewById(R.id.time_et);
        back = (ImageView) findViewById(R.id.imageview);


    }

    @Override
    public void onBackPressed() {
        if (userInteractionStatus ||
                !comment.getText().toString().isEmpty()) {
            showWarning();
        } else {
            startActivity(new Intent(ManagerSendComplaintActivity.this, ManagerHomeActivity.class));
            overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);
            finish();
        }
    }

    public void showWarning() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ManagerSendComplaintActivity.this);
        alertDialog.setMessage((getResources().getString(R.string.data_loss_warn)));
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(ManagerSendComplaintActivity.this, ManagerHomeActivity.class));
                overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);
                finish();
                dialog.dismiss();
            }
        });

        alertDialog.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertDialog.show();
    }


    public void showMap(final Double latitiue, final Double longitude) throws Exception {

        TouchableMapFragment mapFragment = ((TouchableMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_view));
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap map) {

                googleMap = map;
                if (ActivityCompat.checkSelfPermission(ManagerSendComplaintActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(ManagerSendComplaintActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                    return;
                }
                googleMap.setMyLocationEnabled(false);
                googleMap.setTrafficEnabled(true);
                googleMap.setIndoorEnabled(true);
                googleMap.setBuildingsEnabled(true);
                googleMap.getUiSettings().setZoomControlsEnabled(false);
                googleMap.getUiSettings().setAllGesturesEnabled(false);
                googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                googleMap.getUiSettings().setScrollGesturesEnabled(false);

               /* rippleBackground.startRippleAnimation();
                new GetAddressAsyncTask(SendComplaintActivity.this, address)
                        .execute(getString(R.string.API_ADDRESS) + String.valueOf(mLatitude) + "," + String.valueOf(mLongitude));*/

            }
        });


    }


    @Override
    protected void onStop() {
        super.onStop();
        //rippleBackground.stopRippleAnimation();
        Log.e(TAG, "onStop: ");


        /*if (!activityInbackground) {
            Util.finishActivity(SendComplaintActivity.this);
        }*/

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    public boolean checkLocationPermission() {
        if (ActivityCompat.checkSelfPermission(ManagerSendComplaintActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(ManagerSendComplaintActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(ManagerSendComplaintActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_ACCESS_LOCATION);
            return false;

        }
        return true;
    }
   /* public boolean checkStaoragepermission(){
        if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED) {
            Log.v(TAG,"Permission is granted");
            return true;
        }
    }*/


    public boolean checkCameraPermission() {
        List<String> cameraPermissionList = new ArrayList<String>();
        if (ActivityCompat.checkSelfPermission(ManagerSendComplaintActivity
                .this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            cameraPermissionList.add(Manifest.permission.CAMERA);
        }

        if (ActivityCompat.checkSelfPermission(ManagerSendComplaintActivity
                .this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            cameraPermissionList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (!cameraPermissionList.isEmpty()) {
            ActivityCompat.requestPermissions(this, cameraPermissionList.toArray(new String[cameraPermissionList.size()]),
                    MY_PERMISSIONS_REQUEST_ACCESS_CAMERA);
            return false;
        }
        return true;
    }

    public boolean CheckVedioCameraPermission() {
        List<String> videoPermissionList = new ArrayList<String>();
        if (ActivityCompat.checkSelfPermission(ManagerSendComplaintActivity
                .this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            videoPermissionList.add(Manifest.permission.CAMERA);
        }
        if (ActivityCompat.checkSelfPermission(ManagerSendComplaintActivity
                .this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            videoPermissionList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (!videoPermissionList.isEmpty()) {
            ActivityCompat.requestPermissions(this, videoPermissionList.toArray(new String[videoPermissionList.size()]),
                    MY_PERMISSIONS_REQUEST_ACCESS_VIDEO);
            return false;
        }
        return true;


    }

    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        int a = 0;
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                    if (googleApiClient != null && googleApiClient.isConnected()) {
                        if (checkLocationPermission()) {
                            LocationServices.FusedLocationApi.requestLocationUpdates(
                                    googleApiClient, locationRequest, this);
                        }
                        Log.v(TAG, "Location update resumed .....................");
                    } else if (googleApiClient != null && !googleApiClient.isConnected()) {
                        googleApiClient.connect();
                    } else if (googleApiClient == null) {
                        locationRequest = LocationRequest.create();
                        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                        locationRequest.setInterval(INTERVAL);
                        locationRequest.setFastestInterval(FASTEST_INTERVAL);
                        googleApiClient = new GoogleApiClient.Builder(this)
                                .addApi(LocationServices.API)
                                .addConnectionCallbacks(this)
                                .addOnConnectionFailedListener(this)
                                .build();
                        if (googleApiClient != null)
                            googleApiClient.connect();
                    }
                } else {

                    Toast.makeText(ManagerSendComplaintActivity.this, R.string.location_permission_is_required_to_run_the_app,
                            Toast.LENGTH_SHORT).show();
                    finish();
                }
                break;

            }

            case MY_PERMISSIONS_REQUEST_ACCESS_CAMERA: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e(TAG, "onRequestPermissionsResult: imageSelecetioDialog.show()");
                    captureImage();
//                    imageSelecetioDialog.show();
                } else {
                    Toast.makeText(ManagerSendComplaintActivity.this, R.string.camera_permission_is_required_to_click_a_picture,
                            Toast.LENGTH_SHORT).show();

                }
                break;
            }

            case MY_PERMISSIONS_REQUEST_ACCESS_VIDEO: {
//                activityInbackground = true;

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mediaStorageDir = new File(Environment.getExternalStorageDirectory() + "/DCIM/", "pollution");
                    startVideoRecorder();
                    /*MaterialCamera materialCamera =
                            new MaterialCamera(SendComplaintActivity.this)
                                    .saveDir(mediaStorageDir)
                                    .showPortraitWarning(false)
                                    .defaultToFrontFacing(false)
                                    .allowChangeCamera(false)
                                    .autoSubmit(false)
                                    .primaryColor(Color.WHITE)
                                    .labelConfirm(R.string.mcam_use_video)
                                    .videoFrameRate(24)
                                    .videoEncodingBitRate(1000*1024)
                                    .audioEncodingBitRate(50000)
                                    .videoPreferredAspect(16f/9f)
                                    .qualityProfile(MaterialCamera.QUALITY_480P);

                    materialCamera.start(REQUEST_VIDEO_CAPTURE);*/
                   /* demoVideoFolder = Environment.getExternalStorageDirectory().getAbsolutePath() + "/DCIM/traffic_mitra/";
                    workFolder = getApplicationContext().getFilesDir().getAbsolutePath() + "/";
                    vkLogPath = workFolder + "vk.log";
                    GeneralUtils.copyLicenseFromAssetsToSDIfNeeded(SendComplaintActivity.this, workFolder);
                    GeneralUtils.copyDemoVideoFromAssetsToSDIfNeeded(SendComplaintActivity.this, demoVideoFolder);*/
                    /*Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                    takeVideoIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 10);
                    takeVideoIntent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0);
                    timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                    videomediaFile = new File(mediaStorageDir.getPath() + File.separator + "traffic_mitra" + timeStamp + ".mp4");

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        videoUri = FileProvider.getUriForFile(SendComplaintActivity.this,
                                BuildConfig.APPLICATION_ID + ".provider",
                                videomediaFile);
                    } else {
                        videoUri = Uri.fromFile(videomediaFile);
                    }
                    //  videoUri = Uri.fromFile(videomediaFile);
                    videoPath = videoUri.getPath();
                    takeVideoIntent.putExtra(MediaStore.EXTRA_OUTPUT, videoUri);
                    if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
                        startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE);
                    }*/
                } else {
                    Toast.makeText(ManagerSendComplaintActivity.this, R.string.camera_premission_is_reuquired_to_record_a_video,
                            Toast.LENGTH_SHORT).show();
                }
                break;
            }

            case REQUEST_ID_MULTIPLE_PERMISSIONS: {

                Map<String, Integer> perms = new HashMap<>();
                // Initialize the map with both permissions
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.RECORD_AUDIO, PackageManager.PERMISSION_GRANTED);
                // Fill with actual results from user
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    // Check for both permissions
                    if (perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED) {
                        //Permission Granted
                        audioDialog.show();

                    } else {

                        Toast.makeText(ManagerSendComplaintActivity.this, R.string.All_permission_are_reuired_to_record_an_audio,
                                Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            }


        }
    }

    @Override
    public void onGeocodeComplete(Double lat, Double lng) {
        Log.e(TAG, "onGeocodeComplete: Latitude: " + lat + " Longitude: " + lng);
        mLatitude = lat;
        mLongitude = lng;
    }


    @Override
    public void onConnected(Bundle connectionHint) {
        Log.e(TAG, "Location Client Connected!!!");


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        fusedLocationProviderApi.requestLocationUpdates(googleApiClient, locationRequest, this);
        Log.e(TAG, "Using CurrentLocation");
    }

    public void setLocationRequest() throws Exception {
        if (ActivityCompat.checkSelfPermission(
                locationActivity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(
                        locationActivity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        if (googleApiClient.isConnected()) {
            fusedLocationProviderApi.requestLocationUpdates(googleApiClient, locationRequest, this);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        //if the existing location is empty or
        //the current location accuracy is greater than existing accuracy
        //then store the current location
        Log.e("onLocationChange", getClass().getName() + location.getAccuracy());
        if (location.getAccuracy() <= MINIMUM_ACCURACY) {// location.getAccuracy() <= MINIMUM_ACCURACY
            //if the accuracy is not better, remove all location updates for this listener
            this.location = location;
        }

        if (checkLocationPermission()) {
            //Toast.makeText(SendComplaintActivity.this, "call permission outside", Toast.LENGTH_SHORT).show();

            Log.e(TAG, "onLocationChanged: lat: " + location.getLatitude());
            Log.e(TAG, "onLocationChanged: lng: " + location.getLongitude());
            mLatitude = location.getLatitude();
            mLongitude = location.getLongitude();
            //  Toast.makeText(SendComplaintActivity.this, "call permission inside", Toast.LENGTH_SHORT).show();
            cLatitude = location.getLatitude();
            cLongitude = location.getLongitude();
            try {
//                showMap(mLatitude, mLongitude);
                setLatLng(mLatitude, mLongitude);
            } catch (Exception e) {
                e.printStackTrace();
            }
            centerView.setText(String.format("%.4f", mLatitude) + "," + String.format("%.4f", mLongitude));
            Log.e("onNewLocationAvailable:", String.valueOf(mLatitude) + String.valueOf(mLongitude));

        }
    }


    public Location getLocation() {
        if (location != null) {
            Log.e(getClass().getName(), "Location from service" + location.getLongitude() + " x " + location.getLatitude());
            return this.location;
        } else {
            try {
                setLocationRequest();
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.e(getClass().getName(), "Sending Custom Location from service");
            Location tempLoc = new Location("");
            tempLoc.setLatitude(0.0d);
            tempLoc.setLongitude(0.0d);
            return tempLoc;
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }


    private boolean checkLocationOnOFF()

    {
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }

        return gps_enabled || network_enabled;
    }

    private void locationChecker(GoogleApiClient mGoogleApiClient, final Activity activity) {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {

                    case LocationSettingsStatusCodes.SUCCESS:

                        //  Toast.makeText(getApplicationContext(),"yes",Toast.LENGTH_SHORT).show();
                        Log.d("onlocation", "yes");


                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:

                        //Toast.makeText(getApplicationContext(),"yes_required",Toast.LENGTH_SHORT).show(); // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(
                                    activity, 1000);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:

                        //Toast.makeText(getApplicationContext(),"yesxsaxd_required",Toast.LENGTH_SHORT).show();
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });
    }
}
