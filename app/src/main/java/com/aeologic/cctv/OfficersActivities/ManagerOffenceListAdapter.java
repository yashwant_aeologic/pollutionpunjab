package com.aeologic.cctv.OfficersActivities;

import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.aeologic.cctv.Database.Database;
import com.aeologic.cctv.R;
import com.aeologic.cctv.Utility.Prefrence;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import static android.support.v4.content.ContextCompat.getColor;


public class ManagerOffenceListAdapter extends RecyclerView.Adapter<ManagerOffenceListAdapter.MyViewHolder> {
    private static final String TAG = "OffenceListAdapter";
    Context context;
    public ArrayList<HashMap<String, String>> list;
    Database database;
    EditText etComments;
    boolean[] statusArray;

    public ManagerOffenceListAdapter(Context context, ArrayList<HashMap<String, String>> list, boolean[] statusArray) {
        this.context = context;
        this.list = list;
        this.statusArray = statusArray;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.e(TAG, "onCreateViewHolder: view type:" + viewType);
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.offence_list_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        Log.e(TAG, "onBindViewHolder: position " + position);
        final HashMap offenceData = list.get(position);
        if (offenceData != null && offenceData.size() > 0) {
            holder.offencetText.setText(offenceData.get("offenceName").toString());
            if (offenceData.get("imagePath") != null) {
                holder.offenceImage.setImageURI(Uri.parse(offenceData.get("imagePath").toString()));
            }

            if (statusArray[position] == true) {
                holder.backGroundView.setCardBackgroundColor(getColor(context, R.color.colorPrimary));
                holder.offencetText.setTextColor(getColor(context, R.color.white));
            } else {
                holder.backGroundView.setCardBackgroundColor(getColor(context, R.color.offenceBackgroundColor));
                holder.offencetText.setTextColor(getColor(context, R.color.black));
            }
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ManagerSendComplaintActivity) context).userInteractionStatus = true;
                if (!statusArray[position]) {
                    Arrays.fill(statusArray, false);
                    holder.backGroundView.setCardBackgroundColor(getColor(context, R.color.colorPrimary));
                    holder.offencetText.setTextColor(getColor(context, R.color.white));
                    statusArray[position] = true;
                    notifyDataSetChanged();
                    if (position == list.size()-1) {
                        LayoutInflater layoutInflater = LayoutInflater.from(context);
                        final View view1 = layoutInflater.inflate(R.layout.dialog_other_category, null);
                        final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                        alertDialog.setTitle("Specify other category");
                        // alertDialog.setIcon("Icon id here");
                        alertDialog.setCancelable(false);
                        //  Constant.alertDialog.setMessage("Your Message Here");

                        alertDialog.setView(view1);

                        etComments  = (EditText) view1.findViewById(R.id.et_comments);

                        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Add", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (etComments.getText() != null && etComments.getText().length() > 4) {
                                    Prefrence.setOtherComment(context, String.valueOf(etComments.getText()));
                                }else{
                                    etComments.setError("Please enter valid category.");
                                }

                            }
                        });

                        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                alertDialog.cancel();
                            }
                        });



                        alertDialog.show();
                    }
                } else {
                    holder.backGroundView.setCardBackgroundColor(getColor(context, R.color.offenceBackgroundColor));
                    holder.offencetText.setTextColor(getColor(context, R.color.black));
                    statusArray[position] = false;
                }
            }

        });
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        //        LinearLayout linearLayout;
        CardView backGroundView;
        ImageView offenceImage;
        TextView offencetText;

        public MyViewHolder(View itemView) {
            super(itemView);
//            linearLayout = (LinearLayout) itemView.findViewById(R.id.layout);
            backGroundView = (CardView) itemView.findViewById(R.id.cardview);
            offenceImage = (ImageView) itemView.findViewById(R.id.offence_image);
            offencetText = (TextView) itemView.findViewById(R.id.offence_text);
        }
    }
}
