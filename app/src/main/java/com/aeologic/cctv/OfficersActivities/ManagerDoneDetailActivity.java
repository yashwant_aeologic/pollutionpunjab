package com.aeologic.cctv.OfficersActivities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.aeologic.cctv.Activity.ResolveActivity;
import com.aeologic.cctv.Async.ImageLoadTask1;
import com.aeologic.cctv.Async.RejectReportAsyncTask;
import com.aeologic.cctv.Connection.ConnectionDetector;
import com.aeologic.cctv.Database.Database;
import com.aeologic.cctv.R;
import com.aeologic.cctv.Utility.Prefrence;
import com.aeologic.cctv.Utility.SingleShotLocationProvider;
import com.aeologic.cctv.Utility.Util;

import java.util.HashMap;


/**
 * Created by u104 on 29/7/16.
 */
public class ManagerDoneDetailActivity extends AppCompatActivity implements LocationListener {
    private static final int MY_PERMISSIONS_REQUEST_ACCESS_LOCATION = 5;
    TextView rcNumber, address, comment, offense, circleName, date, time, reportedBy, wardName, mobileNumber, resolvedBy, resolverMobileNumber, resolvedBytitle, resolverMobileTitle;
    //Button send;
    LinearLayout resolvedByLL, resolvedMNLL, resololvedDetailsLL;
    ImageView image;
    ImageView image1;
    VideoView videoView;
    VideoView videoView1;
    int tabStatus;
    private int id;
    String TAG = ManagerDoneDetailActivity.class.getName();
    ImageView playPause;
    ImageView playPuase1;
    Toolbar toolbar;
    CardView videoContainer, forwardComplaintBtn;
    CardView videoContainer1;
    CardView imageContainer;
    CardView imageContainer1;
    CardView audioContainer;
    CardView audioContainer1;
    ImageView playAudio;
    ImageView playAudio1;
    SeekBar audioSeekbar;
    SeekBar audioSeekbar1;
    Uri videoUri;
    Uri videoUri1;
    ProgressBar progressBar;
    ProgressBar progressBar1;
    ProgressBar imageProgressBar;
    ProgressBar imageProgressBar1;
    private String videoPath;
    private String videoPath1;
    private String audioPath;
    private String audioPath1;
    Database database;
    private AlertDialog imageDialog;
    private ImageView fullImage;
    public static Bitmap imageBitmap;
    public static Bitmap imageBitmap1;
    public ImageView fullScreenImage;
    public ImageView fullScreenImage1;
    private AlertDialog videoDialog;
    private VideoView fullVideoView;
    public static boolean playpauseClicked = false;
    LinearLayout statusContainer;
    TextView status;
    TextView reason, actionReason;
    String latlng;
    String timeStmap;
    private MediaPlayer mediaPlayer;
    private MediaPlayer mediaPlayer1;
    private LinearLayout reasonConatiner;
    boolean isInternal = true;
    private LinearLayout btnContainer;
    Button resolveBtn, rejectBtn, delete, unrelated;
    private ConnectionDetector detector;
    ImageView reportedCall, resolvedCall;
    public double mLatitude, mLong;
    public static double dLatitude, dLong;
    LinearLayout actionCommentLL;
    TextView actionCommentText;
    TextView actionDate, actionTitle;
    ImageView mapCall;
    String reportedLat, reportedLong;
    String imageUrl;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_done_details);
        initViews();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                killMediaPlayer();
                finish();
            }
        });
        detector = new ConnectionDetector(ManagerDoneDetailActivity.this);
       /* if (Prefrence.getUserType(DoneDetailActivity.this).equalsIgnoreCase("2")) {
            btnContainer.setVisibility(View.VISIBLE);
        }*/
        if (checkLocationPermission()) {
            SingleShotLocationProvider.requestSingleUpdate(ManagerDoneDetailActivity.this,
                    new SingleShotLocationProvider.LocationCallback() {
                        @Override
                        public void onNewLocationAvailable(Location location) {
                            mLatitude = location.getLatitude();
                            mLong = location.getLongitude();
                            Log.e("onNewLocae: ", String.valueOf(mLatitude));

                        }
                    });
        }

        if (getIntent() != null) {
            id = getIntent().getIntExtra("id", 0);
            reportedLat = getIntent().getStringExtra("latitude");
            reportedLong = getIntent().getStringExtra("longitude");
            tabStatus = getIntent().getIntExtra("tabStatus", 0);
            Log.e(TAG, "onCreate: tabStatus:" + tabStatus);

            getSupportActionBar().setTitle("#" + id);

            new ManagerDoneDetailsAsync(ManagerDoneDetailActivity.this).execute(getString(R.string.COMPLAINT_DETAIL_API1)
                    , String.valueOf(id));

        }
        final Handler mSeekbarUpdateHandler = new Handler();
        final Runnable mUpdateSeekbar = new Runnable() {
            @Override
            public void run() {
                if (mediaPlayer != null && audioSeekbar != null) {
                    audioSeekbar.setProgress(mediaPlayer.getCurrentPosition());
                    mSeekbarUpdateHandler.postDelayed(this, 50);
                }

            }
        };

        final Handler mSeekbarUpdateHandler1 = new Handler();
        final Runnable mUpdateSeekbar1 = new Runnable() {
            @Override
            public void run() {
                if (mediaPlayer1 != null && audioSeekbar1 != null) {
                    audioSeekbar1.setProgress(mediaPlayer1.getCurrentPosition());
                    mSeekbarUpdateHandler1.postDelayed(this, 50);
                }

            }
        };

        resolveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ManagerDoneDetailActivity.this, ResolveActivity.class);
                intent.putExtra("id", String.valueOf(id));
                intent.putExtra("latitude", reportedLat);
                intent.putExtra("longitude", reportedLong);
                intent.putExtra("back_id", 1);
                startActivity(intent);
                finish();
            }
        });
        rejectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRejectDialog(String.valueOf(id));
            }
        });
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDeleteDialog(String.valueOf(id));
            }
        });
        unrelated.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showUnrelateDialog(String.valueOf(id));
            }
        });
        playPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playpauseClicked = true;
                if (videoView.isPlaying()) {
                    videoView.stopPlayback();
                    playPause.setImageResource(R.drawable.play);
                } else {
                    try {
                        // Start the MediaController
                        progressBar.setVisibility(View.VISIBLE);
                        MediaController mediacontroller = new MediaController(
                                ManagerDoneDetailActivity.this);
//                        mediacontroller.setAnchorView(videoView);
                        // Get the URL from String VideoURL
//                        Uri video = Uri.parse("http://download.itcuties.com/teaser/itcuties-teaser-480.mp4");
                        videoView.setMediaController(mediacontroller);
                        mediacontroller.setVisibility(View.GONE);
                        videoView.setVideoURI(videoUri);

                    } catch (Exception e) {
                        Log.e("Error", e.getMessage());
                        e.printStackTrace();
                    }

                    videoView.requestFocus();
                    videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        // Close the progress bar and play the video
                        public void onPrepared(MediaPlayer mp) {
                            if (playpauseClicked) {
                                progressBar.setVisibility(View.GONE);
                                playPause.setImageResource(R.drawable.pause);
                                videoView.start();
                            }
                        }
                    });
                }
            }

        });

        playPuase1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playpauseClicked = true;
                if (videoView1.isPlaying()) {
                    videoView1.stopPlayback();
                    playPuase1.setImageResource(R.drawable.play);
                } else {
                    try {
                        // Start the MediaController
                        progressBar1.setVisibility(View.VISIBLE);
                        MediaController mediacontroller = new MediaController(
                                ManagerDoneDetailActivity.this);
//                        mediacontroller.setAnchorView(videoView);
                        // Get the URL from String VideoURL
//                        Uri video = Uri.parse("http://download.itcuties.com/teaser/itcuties-teaser-480.mp4");
                        videoView1.setMediaController(mediacontroller);
                        mediacontroller.setVisibility(View.GONE);
                        videoView1.setVideoURI(videoUri1);

                    } catch (Exception e) {
                        Log.e("Error", e.getMessage());
                        e.printStackTrace();
                    }

                    videoView1.requestFocus();
                    videoView1.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        // Close the progress bar and play the video
                        public void onPrepared(MediaPlayer mp) {
                            if (playpauseClicked) {
                                progressBar1.setVisibility(View.GONE);
                                playPuase1.setImageResource(R.drawable.pause);
                                videoView1.start();
                            }
                        }
                    });
                }
            }

        });
        playAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    playAudio(ManagerDoneDetailActivity.this, audioPath);

                    mSeekbarUpdateHandler.postDelayed(mUpdateSeekbar, 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        playAudio1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    playAudio(ManagerDoneDetailActivity.this, audioPath1);

                    mSeekbarUpdateHandler1.postDelayed(mUpdateSeekbar1, 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                playPause.setImageResource(R.drawable.play);
            }
        });
        videoView1.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                playPuase1.setImageResource(R.drawable.play);
            }
        });

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*fullImage.setImageBitmap(imageBitmap);
                imageDialog.show();*/

                Intent intent = new Intent(ManagerDoneDetailActivity.this, ManagerFullImageActivity.class);
                intent.putExtra("activityName", "done");
                intent.putExtra("latlng", latlng);
                intent.putExtra("IMG", imageUrl);
                intent.putExtra("timeStamp", timeStmap);
                startActivity(intent);
            }
        });
        image1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*fullImage.setImageBitmap(imageBitmap);
                imageDialog.show();*/

                Intent intent = new Intent(ManagerDoneDetailActivity.this, ManagerFullImageActivity.class);
                intent.putExtra("activityName", "done1");
                intent.putExtra("latlng", latlng);
                intent.putExtra("timeStamp", timeStmap);
                startActivity(intent);
            }
        });

        videoContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* fullVideoView.setVideoURI(videoUri);
                fullVideoView.seekTo(100);
                videoDialog.show();*/



              /*  Intent intent = new Intent(DoneDetailActivity.this, FullVideoViewActivity.class);
                intent.putExtra("videoPath", videoUri.toString());
                startActivity(intent);*/
            }
        });
        audioSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                                                    @Override
                                                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                                                        if (fromUser)
                                                            if (mediaPlayer != null)
                                                                mediaPlayer.seekTo(progress);
                                                    }

                                                    @Override
                                                    public void onStartTrackingTouch(SeekBar seekBar) {

                                                    }

                                                    @Override
                                                    public void onStopTrackingTouch(SeekBar seekBar) {


                                                    }
                                                }
        );
        reportedCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CallOn(mobileNumber.getText().toString());
            }
        });
        resolvedCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CallOn(resolverMobileNumber.getText().toString());
            }
        });
        address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?saddr=" + mLatitude + "," + mLong + "&daddr=" + dLatitude + "," + dLong));
                startActivity(intent);
            }
        });
        mapCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?saddr=" + mLatitude + "," + mLong + "&daddr=" + dLatitude + "," + dLong));
                startActivity(intent);
            }
        });

        forwardComplaintBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showForwardComplainDialog();
            }
        });

    }

    private void showForwardComplainDialog() {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ManagerDoneDetailActivity.this, R.style.Theme_Transparent);
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_forward_report, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog rejectDialog = dialogBuilder.create();
        rejectDialog.setCanceledOnTouchOutside(false);
        rejectDialog.show();

        final ImageView cancelIV = (ImageView) dialogView.findViewById(R.id.cancel_iv);
        final RadioGroup radioGroup = (RadioGroup) dialogView.findViewById(R.id.radio_button);
        final Button submitBtn = (Button) dialogView.findViewById(R.id.submit_btn);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkId) {
                if (checkId == R.id.radio_internal) {
                    isInternal = true;
                } else if (checkId == R.id.radio_external) {
                    isInternal = false;
                }
            }
        });
        cancelIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rejectDialog.dismiss();
            }
        });
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (detector.isConnectingToInternet()) {

                    if (radioGroup.getCheckedRadioButtonId() != -1) {

                        v.setEnabled(false);
                        v.setAlpha(0.4f);
//                Util.hideKeyboard(ManagerDoneDetailActivity.this, comment);

                        Log.e(TAG, "isInternal: " + isInternal);
                        rejectDialog.dismiss();
                        Intent forwardActivity = new Intent(ManagerDoneDetailActivity.this, ManagerForwardComplaintActivity.class);
                        forwardActivity.putExtra("complaint_id", id);
                        forwardActivity.putExtra("isInternal", isInternal);
                        startActivity(forwardActivity);
                    } else {
                        Toast.makeText(ManagerDoneDetailActivity.this, getString(R.string.select_department), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(ManagerDoneDetailActivity.this, getString(R.string.con_fail), Toast.LENGTH_SHORT).show();
                }
            }
        });

       /* Intent complaintActivity = new Intent(ManagerDoneDetailActivity.this, ManagerForwardComplaintActivity.class);
        startActivity(complaintActivity);*/
    }

    private void CallOn(String s) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + s));
        startActivity(intent);
    }

    public boolean checkLocationPermission() {
        if (ActivityCompat.checkSelfPermission(ManagerDoneDetailActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(ManagerDoneDetailActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(ManagerDoneDetailActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_ACCESS_LOCATION);
            return false;

        }
        return true;
    }

    private void showRejectDialog(final String complaintNo) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ManagerDoneDetailActivity.this, R.style.Theme_Transparent);
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_reject_report, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog rejectDialog = dialogBuilder.create();
        rejectDialog.setCanceledOnTouchOutside(false);
        rejectDialog.show();

        final EditText comment = (EditText) dialogView.findViewById(R.id.comment_et);
        final ImageView cancelIV = (ImageView) dialogView.findViewById(R.id.cancel_iv);
        final Button submitBtn = (Button) dialogView.findViewById(R.id.submit_btn);
        cancelIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rejectDialog.dismiss();
            }
        });
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setEnabled(false);
                v.setAlpha(0.4f);
                Util.hideKeyboard(ManagerDoneDetailActivity.this, comment);
                if (detector.isConnectingToInternet()) {
                    if (comment.getText().toString().trim().length() > 0) {
                        RejectReportAsyncTask async = new RejectReportAsyncTask(ManagerDoneDetailActivity.this, rejectDialog, submitBtn);
                        async.execute(getString(R.string.SUBMIT_REJECT_REPORT),
                                complaintNo,
                                comment.getText().toString().trim()
                        );
                    } else {
                        comment.setError(getString(R.string.enter_comment));
                    }
                } else {
                    Toast.makeText(ManagerDoneDetailActivity.this, getString(R.string.con_fail), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void showDeleteDialog(final String complaintNo) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ManagerDoneDetailActivity.this, R.style.Theme_Transparent);
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_delete_report, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog rejectDialog = dialogBuilder.create();
        rejectDialog.setCanceledOnTouchOutside(false);
        rejectDialog.show();

        final EditText comment = (EditText) dialogView.findViewById(R.id.comment_et);
        final ImageView cancelIV = (ImageView) dialogView.findViewById(R.id.cancel_iv);
        final Button submitBtn = (Button) dialogView.findViewById(R.id.submit_btn);
        cancelIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rejectDialog.dismiss();
            }
        });
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setEnabled(false);
                v.setAlpha(0.4f);
                Util.hideKeyboard(ManagerDoneDetailActivity.this, comment);
                if (detector.isConnectingToInternet()) {
                    if (comment.getText().toString().trim().length() > 0) {

                        RejectReportAsyncTask async = new RejectReportAsyncTask(ManagerDoneDetailActivity.this, rejectDialog, submitBtn);
                        async.execute(ManagerDoneDetailActivity.this.getString(R.string.SUBMIT_delete_REPORT),
                                complaintNo,
                                comment.getText().toString().trim()
                        );


                    } else {
                        comment.setError(ManagerDoneDetailActivity.this.getString(R.string.enter_comment));
                    }
                } else {
                    Toast.makeText(ManagerDoneDetailActivity.this, getString(R.string.con_fail), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void showUnrelateDialog(final String complaintNo) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ManagerDoneDetailActivity.this, R.style.Theme_Transparent);
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_unrelate_report, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog rejectDialog = dialogBuilder.create();
        rejectDialog.setCanceledOnTouchOutside(false);
        rejectDialog.show();

        final EditText comment = (EditText) dialogView.findViewById(R.id.comment_et);
        final ImageView cancelIV = (ImageView) dialogView.findViewById(R.id.cancel_iv);
        final Button submitBtn = (Button) dialogView.findViewById(R.id.submit_btn);
        cancelIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rejectDialog.dismiss();
            }
        });
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setEnabled(false);
                v.setAlpha(0.4f);
                Util.hideKeyboard(ManagerDoneDetailActivity.this, comment);
                if (detector.isConnectingToInternet()) {
                    if (comment.getText().toString().trim().length() > 0) {

                        RejectReportAsyncTask async = new RejectReportAsyncTask(ManagerDoneDetailActivity.this, rejectDialog, submitBtn);
                        async.execute(ManagerDoneDetailActivity.this.getString(R.string.SUBMIT_UNRELATED_REPORT),
                                complaintNo,
                                comment.getText().toString().trim()
                        );


                    } else {
                        comment.setError(getString(R.string.enter_comment));
                    }
                } else {
                    Toast.makeText(ManagerDoneDetailActivity.this, getString(R.string.con_fail), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void setDatavalue(HashMap datavalue) {

        if (datavalue != null && datavalue.size() > 0) {
            Log.e(TAG, "setDatavalue: " + datavalue.toString());
            if (datavalue.get("latlng") != null && !datavalue.get("latlng").toString().trim().isEmpty()) {
                latlng = datavalue.get("latlng").toString();
            }
            if (datavalue.get("timeStamp") != null && !datavalue.get("timeStamp").toString().trim().isEmpty()) {
                timeStmap = datavalue.get("timeStamp").toString();
            }

            if (datavalue.get("manualAddress") != null && !datavalue.get("manualAddress").toString().trim().isEmpty()) {
                rcNumber.setText(datavalue.get("manualAddress").toString());
            }
            Log.e("setDatavalue: ", datavalue.get("offence").toString());
            Log.e("other_offence: ", datavalue.get("other_offence").toString());
            if (datavalue.get("offence") != null && !datavalue.get("offence").toString().trim().isEmpty()) {
                if (datavalue.get("offence").toString().equalsIgnoreCase("15")) {
                    if (datavalue.get("other_offence") != null && !datavalue.get("other_offence").toString().trim().isEmpty()) {
                        offense.setText("Others(" + datavalue.get("other_offence").toString() + ")");
                    } else {
                        offense.setText("Others(" + getString(R.string.nil) + ")");
                    }

                } else {
                    String offenceList = datavalue.get("offence").toString();
                    String[] offenceArray = offenceList.split(",");
                    offense.setText(database.getOffenceName(offenceArray));
                }

            } else {
                offense.setText("");
            }
            if (datavalue.get("comment") != null && !datavalue.get("comment").toString().trim().isEmpty()) {
                comment.setText(datavalue.get("comment").toString());
            } else {
                comment.setText("");
            }
            if (datavalue.get("name") != null && !datavalue.get("name").toString().trim().isEmpty()) {
                reportedBy.setText(datavalue.get("name").toString());
            } else {
                reportedBy.setText("");
            }
            if (datavalue.get("address") != null && !datavalue.get("address").toString().trim().isEmpty()) {
                address.setText(datavalue.get("address").toString());
            } else {
                address.setText("");
            }
            if (datavalue.get("action") != null && !datavalue.get("action").toString().trim().isEmpty()) {
                status.setText(datavalue.get("action").toString());
            } else {
                status.setText("");
            }
            if (datavalue.get("mobile") != null && !datavalue.get("mobile").toString().trim().isEmpty()) {
                mobileNumber.setText(datavalue.get("mobile").toString());
            } else {
                mobileNumber.setText("");
            }
            if (datavalue.get("ward_name") != null && !datavalue.get("ward_name").toString().trim().isEmpty()) {
                wardName.setText(datavalue.get("ward_name").toString());
            } else {
                wardName.setText("");
            }
            if (datavalue.get("action_comment") != null && !datavalue.get("action_comment").toString().trim().isEmpty()) {
                reason.setText(datavalue.get("action_comment").toString());
            } else {
                reason.setText("");
            }

            if (datavalue.get("action_at") != null && !datavalue.get("action_at").toString().trim().isEmpty()) {
                String date1 = Util.formatDateFromTimeStamp("yyyy-MM-dd HH:mm:ss", "dd/MM/yyyy", datavalue.get("action_at").toString());

                actionCommentText.setText(date1);
            } else {
                reason.setText("");
            }

            if (datavalue.get("timeStamp") != null && !datavalue.get("timeStamp").toString().trim().isEmpty()) {
                String date1 = Util.formatDateFromTimeStamp("yyyy-MM-dd HH:mm:ss", "dd/MM/yyyy", datavalue.get("timeStamp").toString());
                String time1 = Util.formatDateFromTimeStamp("yyyy-MM-dd HH:mm:ss", "hh:mm a", datavalue.get("timeStamp").toString());

                date.setText(date1);
                time.setText(time1);
            } else {
                date.setText("");
                time.setText("");
            }


            if ((datavalue.get("action") != null && !datavalue.get("action").toString().trim().isEmpty() && datavalue.get("action").toString().equalsIgnoreCase("Pending"))) {
                reasonConatiner.setVisibility(View.GONE);
            } else {
                reasonConatiner.setVisibility(View.VISIBLE);
            }
            if (datavalue.get("action").toString().equalsIgnoreCase("Rejected") || datavalue.get("action").toString().equalsIgnoreCase("Deleted")) {

                resolvedBy.setText(datavalue.get("resolved_by_email").toString());
                resolverMobileNumber.setText(datavalue.get("resolved_by_mobile").toString());

            } else if (datavalue.get("action").toString().equalsIgnoreCase("Resolved") || datavalue.get("action").toString().equalsIgnoreCase("Unrelated")) {


                resolvedBy.setText(datavalue.get("resolved_by_name").toString());
                resolverMobileNumber.setText(datavalue.get("resolved_by_mobile").toString());
            }

            Log.e(TAG, "getResolve: " + Prefrence.getResolve(ManagerDoneDetailActivity.this).equalsIgnoreCase("1"));
            //////////////////////////////////////
            if (Prefrence.getDelete(ManagerDoneDetailActivity.this).equalsIgnoreCase("1") || Prefrence.getResolve(ManagerDoneDetailActivity.this).equalsIgnoreCase("1")) {
                if (datavalue.get("action").toString().equalsIgnoreCase("Pending")) {

                    btnContainer.setVisibility(View.VISIBLE);
                } else {

                    btnContainer.setVisibility(View.GONE);
                   /* resolvedBy.setText(datavalue.get("resolved_by_name").toString());
                    resolverMobileNumber.setText(datavalue.get("resolved_by_mobile").toString());*/
                }
                if (Prefrence.getResolve(ManagerDoneDetailActivity.this).equalsIgnoreCase("1")) {
                    resolveBtn.setVisibility(View.VISIBLE);
                    unrelated.setVisibility(View.VISIBLE);
                    delete.setVisibility(View.GONE);
                    rejectBtn.setVisibility(View.GONE);


                } else if (Prefrence.getDelete(ManagerDoneDetailActivity.this).equalsIgnoreCase("1")) {
                    resolveBtn.setVisibility(View.GONE);
                    unrelated.setVisibility(View.GONE);
                    delete.setVisibility(View.VISIBLE);
                    rejectBtn.setVisibility(View.VISIBLE);

                }
            }
            ////////////////////////////////////


            if (datavalue.get("action") != null && datavalue.get("action").toString().length() > 2) {
                if (datavalue.get("action").toString().equalsIgnoreCase("Resolved")) {
                    resolvedBytitle.setText("Resolved By:");
                    actionDate.setText("Resolved Date");
                    actionReason.setText("Resolved Reason");
                    actionTitle.setText("Resolved Details");
                    resololvedDetailsLL.setVisibility(View.VISIBLE);
                    resolvedByLL.setVisibility(View.VISIBLE);
                    resolvedMNLL.setVisibility(View.VISIBLE);
                    actionCommentLL.setVisibility(View.VISIBLE);

                } else if (datavalue.get("action").toString().equalsIgnoreCase("Rejected")) {
                    resolvedBytitle.setText("Rejected By:");
                    actionDate.setText("Rejected Date");
                    actionReason.setText("Rejected Reason");
                    actionTitle.setText("Rejected Details");
                    resololvedDetailsLL.setVisibility(View.VISIBLE);
                    resolvedByLL.setVisibility(View.VISIBLE);
                    resolvedMNLL.setVisibility(View.GONE);
                    actionCommentLL.setVisibility(View.VISIBLE);


                } else if (datavalue.get("action").toString().equalsIgnoreCase("Deleted")) {
                    resolvedBytitle.setText("Deleted By:");
                    actionDate.setText("Deleted Date");
                    actionReason.setText("Deleted Reason");
                    actionTitle.setText("Deleted Details");
                    resololvedDetailsLL.setVisibility(View.VISIBLE);
                    resolvedByLL.setVisibility(View.VISIBLE);
                    resolvedMNLL.setVisibility(View.GONE);
                    actionCommentLL.setVisibility(View.VISIBLE);


                } else if (datavalue.get("action").toString().equalsIgnoreCase("Unrelated")) {
                    resolvedBytitle.setText("Unrelated By:");
                    actionDate.setText("Unrelated Date");
                    actionReason.setText("Unrelated Reason");
                    actionTitle.setText("Unrelated Details");
                    resololvedDetailsLL.setVisibility(View.VISIBLE);
                    resolvedByLL.setVisibility(View.VISIBLE);
                    resolvedMNLL.setVisibility(View.VISIBLE);
                    actionCommentLL.setVisibility(View.VISIBLE);


                } else {
                    resololvedDetailsLL.setVisibility(View.GONE);
                    resolvedByLL.setVisibility(View.GONE);
                    resolvedMNLL.setVisibility(View.GONE);
                    actionCommentLL.setVisibility(View.GONE);

                }

            }
            if (datavalue.get("imageUri") != null && !datavalue.get("imageUri").toString().trim().isEmpty()) {
                imageUrl = getString(R.string.storage_path) + datavalue.get("imageUri").toString();
                Log.e(TAG, "setDatavalue: imageUrl: " + imageUrl);
                if (!datavalue.get("action").toString().equalsIgnoreCase("Deleted")) {
                    imageContainer.setVisibility(View.VISIBLE);
                }
                new ImageLoadTask1(ManagerDoneDetailActivity.this, image, imageProgressBar).execute(imageUrl);
             /*   Glide.with(DoneDetailActivity.this)
                        .load(imageUrl)
                        .into(image);*/
            } else {
                imageContainer.setVisibility(View.GONE);
            }
            if (datavalue.get("videoUri") != null && !datavalue.get("videoUri").toString().trim().isEmpty()) {
                if (!datavalue.get("action").toString().equalsIgnoreCase("Deleted")) {
                    videoContainer.setVisibility(View.VISIBLE);
                }
                videoPath = getString(R.string.storage_path) + datavalue.get("videoUri").toString();
                Log.e(TAG, "setDatavalue: Video Path:" + videoPath);
                videoUri = Uri.parse(videoPath);
                videoView.setVideoURI(videoUri);
                progressBar.setVisibility(View.VISIBLE);
                videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        mp.seekTo(100);
                        progressBar.setVisibility(View.GONE);
                    }
                });
            } else {
                videoContainer.setVisibility(View.GONE);
            }

            if (datavalue.get("audio_path") != null && !datavalue.get("audio_path").toString().trim().isEmpty()) {
                if (!datavalue.get("action").toString().equalsIgnoreCase("Deleted")) {
                    audioContainer.setVisibility(View.VISIBLE);
                }
                audioPath = getString(R.string.storage_path) + datavalue.get("audio_path").toString();
                Log.e(TAG, "setDatavalue: Audio Path:" + audioPath);


            } else {
                audioContainer.setVisibility(View.GONE);
            }


            if (datavalue.get("resolved_photo_path") != null && !datavalue.get("resolved_photo_path").toString().trim().isEmpty()) {
                String imageUrl = getString(R.string.storage_path) + datavalue.get("resolved_photo_path").toString();
                Log.e(TAG, "setDatavalue: resolved_photo_path: " + imageUrl);
                imageContainer1.setVisibility(View.VISIBLE);
                new ImageLoadTask1(ManagerDoneDetailActivity.this, image1, imageProgressBar1).execute(imageUrl);
             /*   Glide.with(DoneDetailActivity.this)
                        .load(imageUrl)
                        .into(image);*/
            } else {
                imageContainer1.setVisibility(View.GONE);
            }
            if (datavalue.get("resolved_video_path") != null && !datavalue.get("resolved_video_path").toString().trim().isEmpty()) {
                videoContainer1.setVisibility(View.VISIBLE);
                videoPath1 = getString(R.string.storage_path) + datavalue.get("resolved_video_path").toString();
                Log.e(TAG, "setDatavalue: resolved_video_path:" + videoPath1);
                videoUri1 = Uri.parse(videoPath1);
                videoView1.setVideoURI(videoUri1);
                progressBar1.setVisibility(View.VISIBLE);
                videoView1.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        mp.seekTo(100);
                        progressBar1.setVisibility(View.GONE);
                    }
                });
            } else {
                videoContainer1.setVisibility(View.GONE);
            }

            if (datavalue.get("resolved_audio_path") != null && !datavalue.get("resolved_audio_path").toString().trim().isEmpty()) {
                audioContainer1.setVisibility(View.VISIBLE);
                audioPath1 = getString(R.string.storage_path) + datavalue.get("resolved_audio_path").toString();
                Log.e(TAG, "setDatavalue: resolved_audio_path" + audioPath1);


            } else {
                audioContainer1.setVisibility(View.GONE);
            }

        }
    }

    public void playAudio(final Context context, final String url) throws Exception {
        if (mediaPlayer == null) {
            mediaPlayer = MediaPlayer.create(context, Uri.parse(url));
            audioSeekbar.setMax(mediaPlayer.getDuration());
        }
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                killMediaPlayer();
                audioSeekbar.setProgress(0);
            }
        });
        mediaPlayer.start();
    }

    private void killMediaPlayer() {
        if (mediaPlayer != null) {
            try {
                mediaPlayer.reset();
                mediaPlayer.release();
                mediaPlayer = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void initViews() {
        rcNumber = (TextView) findViewById(R.id.entered_location);
        address = (TextView) findViewById(R.id.address);
        offense = (TextView) findViewById(R.id.offense);
        comment = (TextView) findViewById(R.id.comment);
        mapCall = (ImageView) findViewById(R.id.map_call);
        //  send = (Button) findViewById(R.id.send);
        // circleName = (TextView) findViewById(R.id.circle_name);
        playPause = (ImageView) findViewById(R.id.playPause);
        playPuase1 = (ImageView) findViewById(R.id.playPause1);
        image = (ImageView) findViewById(R.id.imageview);
        image1 = (ImageView) findViewById(R.id.imageview1);
        videoView = (VideoView) findViewById(R.id.videoview);
        videoView1 = (VideoView) findViewById(R.id.videoview1);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        forwardComplaintBtn = (CardView) findViewById(R.id.forward_complaint_btn);
        videoContainer = (CardView) findViewById(R.id.video_container);
        videoContainer1 = (CardView) findViewById(R.id.video_container1);
        imageContainer = (CardView) findViewById(R.id.image_container);
        imageContainer1 = (CardView) findViewById(R.id.image_container1);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        progressBar1 = (ProgressBar) findViewById(R.id.progress_bar1);
        imageProgressBar = (ProgressBar) findViewById(R.id.progress_bar_image);
        imageProgressBar1 = (ProgressBar) findViewById(R.id.progress_bar_image1);
        database = new Database(getApplicationContext());
        fullScreenImage = (ImageView) findViewById(R.id.full_screen);
        fullScreenImage1 = (ImageView) findViewById(R.id.full_screen1);
        fullScreenImage.setVisibility(View.INVISIBLE);
        fullScreenImage1.setVisibility(View.INVISIBLE);
        statusContainer = (LinearLayout) findViewById(R.id.statusContainer);
        reasonConatiner = (LinearLayout) findViewById(R.id.reasonContainer);
        reasonConatiner.setVisibility(View.GONE);
        statusContainer.setVisibility(View.VISIBLE);
        status = (TextView) findViewById(R.id.status);
        reason = (TextView) findViewById(R.id.reason);
        date = (TextView) findViewById(R.id.date);
        time = (TextView) findViewById(R.id.time);
        audioContainer = (CardView) findViewById(R.id.audio_container);
        audioContainer1 = (CardView) findViewById(R.id.audio_container1);
        playAudio = (ImageView) findViewById(R.id.audio_play);
        playAudio1 = (ImageView) findViewById(R.id.audio_play1);
        audioSeekbar = (SeekBar) findViewById(R.id.seekbar);
        audioSeekbar1 = (SeekBar) findViewById(R.id.seekbar1);
        reportedBy = (TextView) findViewById(R.id.reported_by);
        wardName = (TextView) findViewById(R.id.ward_name);
        mobileNumber = (TextView) findViewById(R.id.mobile_no);
   /*     latlng = (TextView) findViewById(R.id.latlng);
        timeStmap = (TextView) findViewById(R.id.timeStamp);*/

        btnContainer = (LinearLayout) findViewById(R.id.btn_container);
        btnContainer.setVisibility(View.GONE);
        resolveBtn = (Button) findViewById(R.id.resolve_btn);
        rejectBtn = (Button) findViewById(R.id.reject_btn);
        delete = (Button) findViewById(R.id.delete_btn);
        unrelated = (Button) findViewById(R.id.unrelated);
        resolvedBy = (TextView) findViewById(R.id.resolved_by);
        resolverMobileNumber = (TextView) findViewById(R.id.resolver_mobile_no);
        resolvedByLL = (LinearLayout) findViewById(R.id.resolved_by_ll);
        resolvedMNLL = (LinearLayout) findViewById(R.id.resolved_mn_ll);
        reportedCall = (ImageView) findViewById(R.id.reporter_call);
        resolvedCall = (ImageView) findViewById(R.id.resolver_call);
        resololvedDetailsLL = (LinearLayout) findViewById(R.id.resolved_details_ll);
        resolvedBytitle = (TextView) findViewById(R.id.resolved_by_ttl);
//        resolverMobileTitle = (TextView) findViewById(R.id.resolver_mobile_ttl);
        actionCommentLL = (LinearLayout) findViewById(R.id.action_comment_ll);
        actionCommentText = (TextView) findViewById(R.id.action_comment);
        actionDate = (TextView) findViewById(R.id.action_date);
        actionReason = (TextView) findViewById(R.id.action_reason);
        actionTitle = (TextView) findViewById(R.id.resolve_detail);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        killMediaPlayer();
        finish();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();

        //change the image of playpause and stop video if running
        playPause.setImageResource(R.drawable.play);
        playPuase1.setImageResource(R.drawable.play);
        if (videoUri != null) {
            progressBar.setVisibility(View.VISIBLE);
            videoView.seekTo(100);
        }
        videoView.stopPlayback();

    }


    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}

