package com.aeologic.cctv.OfficersActivities;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aeologic.cctv.Activity.ComplaintListActivity;
import com.aeologic.cctv.Activity.DoneDetailActivity;
import com.aeologic.cctv.Activity.ResolveActivity;
import com.aeologic.cctv.Async.RejectReportAsyncTask;
import com.aeologic.cctv.Connection.ConnectionDetector;
import com.aeologic.cctv.Database.Database;
import com.aeologic.cctv.R;
import com.aeologic.cctv.Utility.Prefrence;
import com.aeologic.cctv.Utility.Util;

import java.util.ArrayList;
import java.util.HashMap;

public class ManagerDoneListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    public ArrayList<HashMap<String, String>> responseList;
    String TAG = "DoneListAdapter";
    Context context;

    // The minimum amount of items to have below your current scroll position before loading more.
    private int visibleThreshold = 2;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;
    private Database database;
    private String offenceList;
    private String[] offenceArray;

    private ConnectionDetector detector;

    public ManagerDoneListAdapter(Context context, ArrayList<HashMap<String, String>> responseList, RecyclerView recyclerView) {
        this.responseList = responseList;
        this.context = context;
        database = new Database(context);
        detector = new ConnectionDetector(context);
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    if (dy > 0) {
                        totalItemCount = linearLayoutManager.getItemCount();
                        lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                        if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                            // End has been reached
                            // Do something
                            if (onLoadMoreListener != null) {
                                onLoadMoreListener.onLoadMore();
                            }
                            loading = true;
                        }
                    } else {

                        ManagerComplaintListActivity.progressBar.setVisibility(View.INVISIBLE);
                        // ((Activity)(context).progressBar.setVisibility(View.INVISIBLE);
                    }

                }
            });
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        //  if (viewType == VIEW_ITEM) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.manager_offence_list, parent, false);

        vh = new TextViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof TextViewHolder) {
            // Log.e(TAG, "onBindViewHolder: position " + position);
            final HashMap<String, String> detailsMap = responseList.get(position);
            if (detailsMap != null) {
                // ((TextViewHolder) holder).rcNumber.setText(detailsMap.get("rcNumber").toString());
                if (detailsMap.get("address").toString() != null && !detailsMap.get("address").toString().equalsIgnoreCase("null")) {
                    ((TextViewHolder) holder).address.setText(detailsMap.get("address").toString());
                } else {
                    ((TextViewHolder) holder).address.setText(R.string.nil);
                }

                if (detailsMap.get("reported_by").toString() != null && !detailsMap.get("reported_by").toString().equalsIgnoreCase("null")) {
                    ((TextViewHolder) holder).reportedBy.setText(detailsMap.get("reported_by").toString());
                } else {
                    ((TextViewHolder) holder).reportedBy.setText(R.string.nil);
                }

                ((TextViewHolder) holder).statustxt.setText(detailsMap.get("action").toString());
                if (detailsMap.get("action").toString().equalsIgnoreCase("Resolved")) {
                    ((TextViewHolder) holder).statustxt.setTextColor(ContextCompat.getColor(context, android.R.color.holo_green_dark));
                    ((TextViewHolder) holder).contentBar.setCardBackgroundColor(ContextCompat.getColor(context, android.R.color.holo_green_dark));
                    ((TextViewHolder) holder).bar.setBackgroundColor(ContextCompat.getColor(context, android.R.color.holo_green_dark));
                    // ((TextViewHolder) holder).btnContainer.setVisibility(View.GONE);
                } else if (detailsMap.get("action").toString().equalsIgnoreCase("Rejected")) {
                    ((TextViewHolder) holder).statustxt.setTextColor(ContextCompat.getColor(context, R.color.brown));
                    ((TextViewHolder) holder).contentBar.setCardBackgroundColor(ContextCompat.getColor(context, R.color.brown));
                    ((TextViewHolder) holder).bar.setBackgroundColor(ContextCompat.getColor(context, R.color.brown));
                    //  ((TextViewHolder) holder).btnContainer.setVisibility(View.GONE);
                } else if (detailsMap.get("action").toString().equalsIgnoreCase("Unrelated")) {
                    ((TextViewHolder) holder).statustxt.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                    ((TextViewHolder) holder).contentBar.setCardBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
                    ((TextViewHolder) holder).bar.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
                    //   ((TextViewHolder) holder).btnContainer.setVisibility(View.GONE);
                } else if (detailsMap.get("action").toString().equalsIgnoreCase("Deleted")) {
                    ((TextViewHolder) holder).statustxt.setTextColor(ContextCompat.getColor(context, android.R.color.darker_gray));
                    ((TextViewHolder) holder).contentBar.setCardBackgroundColor(ContextCompat.getColor(context, android.R.color.darker_gray));
                    ((TextViewHolder) holder).bar.setBackgroundColor(ContextCompat.getColor(context, android.R.color.darker_gray));
                    // ((TextViewHolder) holder).btnContainer.setVisibility(View.GONE);
                } else {
                    ((TextViewHolder) holder).statustxt.setTextColor(ContextCompat.getColor(context, R.color.reject));
                    ((TextViewHolder) holder).contentBar.setCardBackgroundColor(ContextCompat.getColor(context, R.color.reject));
                    ((TextViewHolder) holder).bar.setBackgroundColor(ContextCompat.getColor(context, R.color.reject));
                    //     ((TextViewHolder) holder).btnContainer.setVisibility(View.VISIBLE);
                }
//                offenceList = detailsMap.get("offence").toString();
//                offenceArray = offenceList.split(",");
//                ((TextViewHolder) holder).statustxt.setText(database.getOffenceName(offenceArray));


                ((TextViewHolder) holder).complaintNumber.setText("Report No.: " + detailsMap.get("id").toString());
                String createdAt = detailsMap.get("createdAt").toString();

                String date = Util.formatDateFromTimeStamp("yyyy-MM-dd HH:mm:ss", "dd/MM/yyyy", createdAt);
                String time = Util.formatDateFromTimeStamp("yyyy-MM-dd HH:mm:ss", "hh:mm a", createdAt);

                ((TextViewHolder) holder).date.setText(date);
                ((TextViewHolder) holder).time.setText(time);

                if (Prefrence.getDelete(context).equalsIgnoreCase("1") || Prefrence.getResolve(context).equalsIgnoreCase("1")) {
                    if (detailsMap.get("action").toString().equalsIgnoreCase("Pending")) {

                        ((TextViewHolder) holder).btnContainer.setVisibility(View.VISIBLE);
                    } else {

                        ((TextViewHolder) holder).btnContainer.setVisibility(View.GONE);
                    }
                    if (Prefrence.getResolve(context).equalsIgnoreCase("1")) {
                        ((TextViewHolder) holder).resolveBtn.setVisibility(View.VISIBLE);
                        ((TextViewHolder) holder).unrelated.setVisibility(View.VISIBLE);
                        ((TextViewHolder) holder).delete.setVisibility(View.GONE);
                        ((TextViewHolder) holder).rejectBtn.setVisibility(View.GONE);


                    } else if (Prefrence.getDelete(context).equalsIgnoreCase("1")) {
                        ((TextViewHolder) holder).resolveBtn.setVisibility(View.GONE);
                        ((TextViewHolder) holder).unrelated.setVisibility(View.GONE);
                        ((TextViewHolder) holder).delete.setVisibility(View.VISIBLE);
                        ((TextViewHolder) holder).rejectBtn.setVisibility(View.VISIBLE);

                    }
                }

                ((TextViewHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final HashMap<String, String> detailsMap = responseList.get(position);
                        if (detailsMap != null) {
                            int id = Integer.parseInt((detailsMap.get("id")));
                            Intent intent = new Intent(context, ManagerDoneDetailActivity.class);
                            intent.putExtra("latitude", responseList.get(position).get("latitude"));
                            intent.putExtra("longitude", responseList.get(position).get("longitude"));
                            intent.putExtra("id", id);
                            intent.putExtra("tabStatus", 1);
                            context.startActivity(intent);


//                    ((Activity) context).finish();
                        }
                    }
                });

                ((TextViewHolder) holder).resolveBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, ResolveActivity.class);
                        intent.putExtra("id", responseList.get(position).get("id"));
                        intent.putExtra("latitude", responseList.get(position).get("latitude"));
                        intent.putExtra("longitude", responseList.get(position).get("longitude"));
                        intent.putExtra("back_id", 0);
                        context.startActivity(intent);
                        ((Activity) context).finish();
                    }
                });
                ((TextViewHolder) holder).rejectBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showRejectDialog(responseList.get(position).get("id"));
                    }
                });
                ((TextViewHolder) holder).delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showDeleteDialog(responseList.get(position).get("id"));
                    }
                });
                ((TextViewHolder) holder).unrelated.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showUnrelateDialog(responseList.get(position).get("id"));
                    }
                });

            }

        } /*else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }*/


    }

    private void showRejectDialog(final String complaintNo) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context, R.style.Theme_Transparent);
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_reject_report, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog rejectDialog = dialogBuilder.create();
        rejectDialog.setCanceledOnTouchOutside(false);
        rejectDialog.show();

        final EditText comment = (EditText) dialogView.findViewById(R.id.comment_et);
        final ImageView cancelIV = (ImageView) dialogView.findViewById(R.id.cancel_iv);
        final Button submitBtn = (Button) dialogView.findViewById(R.id.submit_btn);
        cancelIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rejectDialog.dismiss();
            }
        });
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setEnabled(false);
                v.setAlpha(0.4f);
                Util.hideKeyboard(context, comment);
                if (detector.isConnectingToInternet()) {
                    if (comment.getText().toString().trim().length() > 0) {

                        RejectReportAsyncTask async = new RejectReportAsyncTask(context, rejectDialog, submitBtn);
                        async.execute(context.getString(R.string.SUBMIT_REJECT_REPORT),
                                complaintNo,
                                comment.getText().toString().trim()
                        );


                    } else {
                        comment.setError(context.getString(R.string.enter_comment));
                    }
                } else {
                    Toast.makeText(context, context.getString(R.string.con_fail), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void showDeleteDialog(final String complaintNo) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context, R.style.Theme_Transparent);
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_delete_report, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog rejectDialog = dialogBuilder.create();
        rejectDialog.setCanceledOnTouchOutside(false);
        rejectDialog.show();

        final EditText comment = (EditText) dialogView.findViewById(R.id.comment_et);
        final ImageView cancelIV = (ImageView) dialogView.findViewById(R.id.cancel_iv);
        final Button submitBtn = (Button) dialogView.findViewById(R.id.submit_btn);
        cancelIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rejectDialog.dismiss();
            }
        });
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setEnabled(false);
                v.setAlpha(0.4f);
                Util.hideKeyboard(context, comment);
                if (detector.isConnectingToInternet()) {
                    if (comment.getText().toString().trim().length() > 0) {

                        RejectReportAsyncTask async = new RejectReportAsyncTask(context, rejectDialog, submitBtn);
                        async.execute(context.getString(R.string.SUBMIT_delete_REPORT),
                                complaintNo,
                                comment.getText().toString().trim()
                        );


                    } else {
                        comment.setError(context.getString(R.string.enter_comment));
                    }
                } else {
                    Toast.makeText(context, context.getString(R.string.con_fail), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void showUnrelateDialog(final String complaintNo) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context, R.style.Theme_Transparent);
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_unrelate_report, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog rejectDialog = dialogBuilder.create();
        rejectDialog.setCanceledOnTouchOutside(false);
        rejectDialog.show();

        final EditText comment = (EditText) dialogView.findViewById(R.id.comment_et);
        final ImageView cancelIV = (ImageView) dialogView.findViewById(R.id.cancel_iv);
        final Button submitBtn = (Button) dialogView.findViewById(R.id.submit_btn);
        cancelIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rejectDialog.dismiss();
            }
        });
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setEnabled(false);
                v.setAlpha(0.4f);
                Util.hideKeyboard(context, comment);
                if (detector.isConnectingToInternet()) {
                    if (comment.getText().toString().trim().length() > 0) {

                        RejectReportAsyncTask async = new RejectReportAsyncTask(context, rejectDialog, submitBtn);
                        async.execute(context.getString(R.string.SUBMIT_UNRELATED_REPORT),
                                complaintNo,
                                comment.getText().toString().trim()
                        );


                    } else {
                        comment.setError(context.getString(R.string.enter_comment));
                    }
                } else {
                    Toast.makeText(context, context.getString(R.string.con_fail), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void setLoaded() {
        loading = false;
    }

    @Override
    public int getItemCount() {
        return responseList.size();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public interface OnLoadMoreListener {
        void onLoadMore();
    }

    public static class TextViewHolder extends RecyclerView.ViewHolder {
        TextView rcNumber, date, time, statustxt, complaintNumber, address, reportedBy;
        CardView contentBar;
        LinearLayout btnContainer;
        Button resolveBtn, rejectBtn, delete, unrelated;
        View bar;

        public TextViewHolder(View v) {
            super(v);
            date = (TextView) itemView.findViewById(R.id.date);
            time = (TextView) itemView.findViewById(R.id.time);
            statustxt = (TextView) itemView.findViewById(R.id.status_txt);
            rcNumber = (TextView) itemView.findViewById(R.id.rcNumber);
            complaintNumber = (TextView) itemView.findViewById(R.id.complaint_number);
            address = (TextView) itemView.findViewById(R.id.address);
            bar = itemView.findViewById(R.id.bar);
            contentBar = (CardView) itemView.findViewById(R.id.content_bar);
            reportedBy = (TextView) itemView.findViewById(R.id.reported_by_text);

            btnContainer = (LinearLayout) itemView.findViewById(R.id.btn_container);
            btnContainer.setVisibility(View.GONE);
            resolveBtn = (Button) itemView.findViewById(R.id.resolve_btn);
            rejectBtn = (Button) itemView.findViewById(R.id.reject_btn);
            delete = (Button) itemView.findViewById(R.id.delete_btn);
            unrelated = (Button) itemView.findViewById(R.id.unrelated);
        }
    }

    /*public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
        }
    }*/
}