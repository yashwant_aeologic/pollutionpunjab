package com.aeologic.cctv.OfficersActivities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.aeologic.cctv.Activity.ReOpenActivity;
import com.aeologic.cctv.Connection.ConnectionDetector;
import com.aeologic.cctv.R;
import com.aeologic.cctv.Utility.AppController;
import com.aeologic.cctv.Utility.Prefrence;
import com.aeologic.cctv.Utility.Util;
import com.aeologic.cctv.Utility.VolleyMultipartRequest;
import com.aeologic.cctv.model.DeptMemberData;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ManagerForwardComplaintActivity extends AppCompatActivity {

    private static final String TAG = ManagerForwardComplaintActivity.class.getName();
    private Spinner spinnerMember, spinnerDepartment;
    private LinearLayout departmentLL, memberLL;
    private EditText reasonET;
    int complaintId;
    private Button submitBtn;
    private ConnectionDetector detector;
    private Toolbar toolbar;
    private VolleyMultipartRequest multipartRequest;
    private boolean isInternal;
    private List<String> memberNameList;
    private List<DeptMemberData> departmentList;
    private int selectedDeptId = 0;
    private int selectedMemberId = 0;
    private List<DeptMemberData> memberList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manager_forward_complaint);

        initViews();
        getIntents();

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Transfer Report");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        departmentList = new ArrayList<>();
        memberList = new ArrayList<>();
        Log.e(TAG, "isInternal: "+ isInternal );
        if (isInternal) {
            //hide dept dropdown
            departmentLL.setVisibility(View.GONE);
            getMembersListAPI();
        } else {
            spinnerMember.setEnabled(false);
            getDepartmentListAPI();

        }
        handleEvents();

    }

    private void initViews() {
        reasonET = (EditText) findViewById(R.id.reason_et);
        submitBtn = (Button) findViewById(R.id.submit);
        spinnerMember = (Spinner) findViewById(R.id.spinner_assign_member);
        spinnerDepartment = (Spinner) findViewById(R.id.spinner_assign_department);
        departmentLL = (LinearLayout) findViewById(R.id.department_spinner_ll);
        memberLL = (LinearLayout) findViewById(R.id.member_spinner_ll);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        detector = new ConnectionDetector(ManagerForwardComplaintActivity.this);
    }

    private void handleEvents() {

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValidate()){
                    Util.hideKeyboard(ManagerForwardComplaintActivity.this, reasonET);
                    if (detector.isConnectingToInternet())
                    forwardComplaintToOtherDeptAPI();
                    else
                        Toast.makeText(ManagerForwardComplaintActivity.this, getString(R.string.con_fail), Toast.LENGTH_SHORT).show();
                }
            }
        });


        spinnerDepartment.setOnTouchListener(new View.OnTouchListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    if (!isInternal && spinnerDepartment.getAdapter().getCount() < 2) {
                        getDepartmentListAPI();
                        return true;
                    }
                }
                return false;
            }
        });

        spinnerDepartment.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                                        @Override
                                                        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                                                            if (detector.isConnectingToInternet()) {
                                                                if (position == 0) {
                                                                    selectedDeptId = 0;
                                                                } else {
                                                                    String selected = adapterView.getItemAtPosition(position).toString().trim();
                                                                    for (int j = 0; j < departmentList.size(); j++) {
                                                                        if (selected.equalsIgnoreCase(departmentList.get(j).getName())) {
                                                                            selectedDeptId = departmentList.get(j).getId();
                                                                            getMembersListAPI();
                                                                            spinnerMember.setEnabled(true);
                                                                            break;
                                                                        }
                                                                    }
                                                                }
                                                            } else
                                                                Toast.makeText(ManagerForwardComplaintActivity.this, getString(R.string.con_fail), Toast.LENGTH_SHORT).show();
                                                        }

                                                        @Override
                                                        public void onNothingSelected(AdapterView<?> adapterView) {

                                                        }
                                                    }
        );


        spinnerMember.setOnTouchListener(new View.OnTouchListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    if (!isInternal && departmentList.size()==0) {
                        Util.showAlert(ManagerForwardComplaintActivity.this,getString(R.string.select_department));
                        return  true;
                    }
                }
                return false;
            }
        });

        spinnerMember.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                                        @Override
                                                        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                                                            if (position == 0) {
                                                                selectedMemberId = 0;
                                                            } else {
                                                                String selected = adapterView.getItemAtPosition(position).toString().trim();
                                                                for (int j = 0; j < memberList.size(); j++) {
                                                                    if (selected.equalsIgnoreCase(memberList.get(j).getName())) {
                                                                        selectedMemberId = memberList.get(j).getId();
//                                                                        getMembersListAPI();
                                                                        break;
                                                                    }
                                                                }
                                                            }

                                                        }

                                                        @Override
                                                        public void onNothingSelected(AdapterView<?> adapterView) {

                                                        }
                                                    }
        );
    }

    private boolean isValidate() {
        if (!isInternal && selectedDeptId==0){
            Util.showAlert(ManagerForwardComplaintActivity.this,getString(R.string.select_department));
            return false;
        }
        if (selectedMemberId==0 ){
            Util.showAlert(ManagerForwardComplaintActivity.this,getString(R.string.please_select_member));
            return false;
        }
       if (reasonET.getText().toString().trim().isEmpty()){
           reasonET.setError("Please provide the Reason!");
           return false;
       }
        return true;
    }

    private void getDepartmentListAPI() {
        final ProgressDialog progressDialog = new ProgressDialog(ManagerForwardComplaintActivity.this);
        progressDialog.setMessage(getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
        multipartRequest = new VolleyMultipartRequest(Request.Method.POST,
                getString(R.string.DEPARTMENT_LIST_API), new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                String resultResponse = new String(response.data);
                Log.e(TAG, "getDepartmentListAPI API: " + getString(R.string.MEMBER_LIST_API));
                Log.e(TAG, "getDepartmentListAPI Response: " + resultResponse);
                progressDialog.hide();
                try {

                    HashMap<String, String> responseData;
                    JSONObject object = new JSONObject(resultResponse);
                    String status = object.getString("status");
                    if (status.equals("1")) {
                        JSONArray resultArray = object.getJSONArray("data");
                        if (resultArray != null && resultArray.length() > 0) {
                            for (int i = 0; i < resultArray.length(); i++) {

                                JSONObject obj = resultArray.getJSONObject(i);
                                int id = obj.getInt("id");
                                String name = obj.getString("name");
                                departmentList.add(new DeptMemberData(name, id));
                            }

                            setDepartmentSpinnerData();

                        } else {
                            Util.showAlert(ManagerForwardComplaintActivity.this, getString(R.string.pta));
                        }
                    } else if (status.equals("0")) {
                        String message = object.getString("message");
                        if (message != null) {
                            if (message.contains("Your session has expired"))
                                Util.showSessionExpiredDialog(ManagerForwardComplaintActivity.this);
                        } else {
                            Util.showAlert(ManagerForwardComplaintActivity.this, getString(R.string.pta));
                        }
//                        Util.showAlert(getActivity(), object.getString("message"));

                        progressDialog.hide();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e(TAG, "onErrorResponse: " + error.getMessage());
//                progressBar.setVisibility(View.INVISIBLE);
                Util.showAlert(ManagerForwardComplaintActivity.this, getString(R.string.pta));
                progressDialog.hide();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("token", Prefrence.getToken(ManagerForwardComplaintActivity.this));
//                params.put("pg", String.valueOf(pageNumber));

                return params;

            }

        };
        multipartRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        AppController.getInstance().addToRequestQueue(multipartRequest);

    }


    private void forwardComplaintToOtherDeptAPI(){
        final ProgressDialog progressDialog = new ProgressDialog(ManagerForwardComplaintActivity.this);
        progressDialog.setMessage(getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
        multipartRequest = new VolleyMultipartRequest(Request.Method.POST,
                getString(R.string.FORWARD_COMPLAIN_API), new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                String resultResponse = new String(response.data);
                Log.e(TAG, "forwardComplaintToOtherDeptAPI API: " + getString(R.string.MEMBER_LIST_API));
                Log.e(TAG, "forwardComplaintToOtherDeptAPI Response: " + resultResponse);
                progressDialog.hide();
                try {

                    HashMap<String, String> responseData;
                    JSONObject object = new JSONObject(resultResponse);
                    String status = object.getString("status");
                    if (status.equals("1")) {
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ManagerForwardComplaintActivity.this);
                        alertDialog.setMessage(object.getString("message"));
                        alertDialog.setCancelable(false);
                        alertDialog.setPositiveButton(getResources().getString(R.string.Ok), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                Intent intent = new Intent(ManagerForwardComplaintActivity.this, ManagerComplaintListActivity.class);
                                intent.putExtra("From", "FORWARD");
                                startActivity(intent);
                                overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);
                                finish();

                            }
                        });
                        alertDialog.show();
                    } else if (status.equals("0")) {
                        String message = object.getString("message");
                        if (message != null) {
                            if (message.contains("Your session has expired"))
                                Util.showSessionExpiredDialog(ManagerForwardComplaintActivity.this);
                        } else {
                            Util.showAlert(ManagerForwardComplaintActivity.this, getString(R.string.pta));
                        }
//                        Util.showAlert(getActivity(), object.getString("message"));

                        progressDialog.hide();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e(TAG, "forwardComplaintToOtherDeptAPI onErrorResponse: " + error.getMessage());
//                progressBar.setVisibility(View.INVISIBLE);
                Util.showAlert(ManagerForwardComplaintActivity.this, getString(R.string.pta));
                progressDialog.hide();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("token", Prefrence.getToken(ManagerForwardComplaintActivity.this));
                params.put("complaint_no", String.valueOf(complaintId));
                params.put("id", String.valueOf(selectedMemberId));
                params.put("comment", reasonET.getText().toString().trim());

                return params;

            }

        };
        multipartRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        AppController.getInstance().addToRequestQueue(multipartRequest);

    }

    private void setMemberSpinnerData() {
        List<String> memberNameList = new ArrayList<>();

        if (memberList.size() > 0) {
            memberNameList.add("Select Member");
            for (int i = 0; i < memberList.size(); i++) {
                DeptMemberData deptMemberData;
                deptMemberData = memberList.get(i);
                memberNameList.add(deptMemberData.getName());
            }
        }
        if (memberNameList.size() > 0) {
            spinnerMember.setAdapter(null);
            spinnerMember.setAdapter(new ArrayAdapter<String>(ManagerForwardComplaintActivity.this, android.R.layout.simple_list_item_1, memberNameList));
        }
    }


    private void setDepartmentSpinnerData() {
        List<String> deptNameList = new ArrayList<>();

        if (departmentList.size() > 0) {
            deptNameList.add("Select Department");
            for (int i = 0; i < departmentList.size(); i++) {
                DeptMemberData deptMemberData;
                deptMemberData = departmentList.get(i);
                deptNameList.add(deptMemberData.getName());
            }
        }
        if (deptNameList.size() > 0) {
            spinnerDepartment.setAdapter(new ArrayAdapter<String>(ManagerForwardComplaintActivity.this, android.R.layout.simple_list_item_1, deptNameList));
        }
    }

    private void getMembersListAPI() {
        final ProgressDialog progressDialog = new ProgressDialog(ManagerForwardComplaintActivity.this);
        progressDialog.setMessage(getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
        multipartRequest = new VolleyMultipartRequest(Request.Method.POST,
                getString(R.string.MEMBER_LIST_API), new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                String resultResponse = new String(response.data);
                Log.e(TAG, "getMembersListAPI API: " + getString(R.string.MEMBER_LIST_API));
                Log.e(TAG, "getMembersListAPI Response: " + resultResponse);

                try {

                    HashMap<String, String> responseData;
                    JSONObject object = new JSONObject(resultResponse);
                    String status = object.getString("status");
                    if (status.equals("1")) {
                        JSONArray resultArray = object.getJSONArray("data");
                        if (resultArray != null && resultArray.length() > 0) {
                            memberList = new ArrayList<>();
                            for (int i = 0; i < resultArray.length(); i++) {

                                JSONObject obj = resultArray.getJSONObject(i);
                                int id = obj.getInt("id");
                                String name = obj.getString("name");
                                memberList.add(new DeptMemberData(name, id));
                            }
                            setMemberSpinnerData();


                        } else {

                        }
                    } else if (status.equals("0")) {
                        String message = object.getString("message");
                        if (message != null) {
                            if (message.contains("Your session has expired"))
                                Util.showSessionExpiredDialog(ManagerForwardComplaintActivity.this);
                        } else {
                            Util.showAlert(ManagerForwardComplaintActivity.this, getString(R.string.pta));
                        }
//                        Util.showAlert(getActivity(), object.getString("message"));


                    }
                    progressDialog.hide();
                } catch (JSONException e) {
                    progressDialog.hide();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e(TAG, "onErrorResponse: " + error.getMessage());
//                progressBar.setVisibility(View.INVISIBLE);
                Util.showAlert(ManagerForwardComplaintActivity.this, getString(R.string.pta));
                progressDialog.hide();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("token", Prefrence.getToken(ManagerForwardComplaintActivity.this));
                if (!isInternal) {
                    params.put("department", String.valueOf(selectedDeptId));
                } else {
                    params.put("department", Prefrence.getDepartmentID(ManagerForwardComplaintActivity.this));
                }

                Log.e(TAG, "getMembersListAPI PARAMS TOKEN: " +Prefrence.getToken(ManagerForwardComplaintActivity.this) );
                Log.e(TAG, "getMembersListAPI PARAMS INTERNAL DEPT: " +Prefrence.getDepartmentID(ManagerForwardComplaintActivity.this) );
                Log.e(TAG, "getMembersListAPI PARAMS EXTERNAL DEPT: " +selectedDeptId );
                return params;

            }

        };
        multipartRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        AppController.getInstance().addToRequestQueue(multipartRequest);

    }


    private void getIntents() {
        if (getIntent() != null) {
            complaintId = getIntent().getIntExtra("complaint_id", 0);
            isInternal = getIntent().getBooleanExtra("isInternal", false);
        }

    }


}
