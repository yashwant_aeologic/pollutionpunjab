package com.aeologic.cctv.GooglePlaceAPI;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;


import com.aeologic.cctv.Utility.MultipartUtility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;


public class GetAddressAsyncTask extends AsyncTask<String, Integer, String> {
    private Context context;
    private EditText addrTxt,addrEdTxt;
    private String TAG = "GetAddressAsyncTask";
    TextView addressText;

    public GetAddressAsyncTask(Context context, EditText addrTxt, TextView addressText, EditText manualAddress) {
        this.context = context;
        this.addrTxt = addrTxt;
        this.addressText=addressText;
        this.addrEdTxt=manualAddress;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            MultipartUtility utility = new MultipartUtility(params[0], "UTF-8");
            Log.v("ADDRESS_URL", params[0]);
            String response = utility.finish();
            if (response != null) {
                return getAddress(response);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();

        }
        return null;
    }

    private String getAddress(String response) {
        try {
            Log.e(TAG, "getAddress: response: " + response);
            JSONObject object = new JSONObject(response);
            JSONArray jsonArray = object.getJSONArray("results");
            JSONObject resultObj = jsonArray.getJSONObject(0);
            return resultObj.getString("formatted_address");
        } catch (JSONException je) {
            je.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if (result != null) {
            addrTxt.setText(result);
            addressText.setText(result);
            addrEdTxt.setText(result);
        } /*else {
            addrTxt.setHint("Address");
//            addrTxt.setText(context.getString(R.string.addr_fail));
        }*/
    }
}
