package com.aeologic.cctv.GooglePlaceAPI;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;


import com.aeologic.cctv.Utility.MultipartUtility;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by sachin varshney on 09/02/2017
 */
public class DownloadLatLongAsync extends AsyncTask<String, String, String> {

    private final OnGeocodeComplete onGeocodeComplete;
    private double lng;
    private double lat;
    String address;
    GoogleMap googleMap;

    public interface OnGeocodeComplete {
        void onGeocodeComplete(Double lat, Double lng);
    }

    public DownloadLatLongAsync(OnGeocodeComplete onGeocodeComplete, GoogleMap googleMap) {
        this.googleMap = googleMap;
        this.onGeocodeComplete = onGeocodeComplete;
    }

    @Override
    protected String doInBackground(String... params) {
        String response = null;
        try {
            String uri = "http://maps.google.com/maps/api/geocode/json?address=" +
                    Uri.encode(params[0]) + "&sensor=false";
            response = getLocationLatLong(uri);
            Log.e("response:", response);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    @Override
    protected void onPostExecute(String result) {
        // TODO Auto-generated method stub
        super.onPostExecute(result);
        if (result != null && !result.isEmpty()) {
            JSONObject jsonObject = null;
            try {

                jsonObject = new JSONObject(result);

                lng = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONObject("geometry").getJSONObject("location")
                        .getDouble("lng");

                lat = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONObject("geometry").getJSONObject("location")
                        .getDouble("lat");

                Log.e("latitude", "" + lat);
                Log.e("longitude", "" + lng);
                googleMap.clear();
//            googleMap.addMarker(new MarkerOptions().position(new LatLng(lat, lng)));
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 15f));
                onGeocodeComplete.onGeocodeComplete(lat, lng);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public String getLocationLatLong(String param) {

        String responseData = null;
        Log.e("API_LOGIN_URL", param);
        try {
            MultipartUtility multipartUtility = new MultipartUtility(param, "UTF-8");
            responseData = multipartUtility.finish();
            Log.e("responseData", responseData);
            return responseData;
        } catch (IOException ioe) {
            ioe.printStackTrace();
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
