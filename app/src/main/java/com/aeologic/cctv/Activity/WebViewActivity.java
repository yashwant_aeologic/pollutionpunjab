package com.aeologic.cctv.Activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.aeologic.cctv.R;


/**
 * Created by Sachin varshney on 28/4/17.
 */

public class WebViewActivity extends AppCompatActivity {
    private String TAG = "WebViewActivity";
    Toolbar toolbar;
    WebView webView;
    int position;
    String title;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        initViews();

        if (getIntent() != null) {
            position = getIntent().getIntExtra("position", 0);
            title = getIntent().getStringExtra("title");
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(title);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startActivity(new Intent(WebViewActivity.this, HomeActivity.class));
                finish();
            }
        });

        WebSettings settings = webView.getSettings();
        settings.setBuiltInZoomControls(true);
        settings.setUseWideViewPort(true);
        settings.setJavaScriptEnabled(true);
        settings.setSupportMultipleWindows(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setLoadsImagesAutomatically(true);
        settings.setDomStorageEnabled(true);
        settings.setLoadWithOverviewMode(true);
//        webView.loadUrl("file:///android_res/raw/faq.html");
/*faqList.add("About Traffic Sentinel Scheme");
        faqList.add("Become a Traffic Sentinel");
        faqList.add("Violation list");
        faqList.add("Sample images of violation");
        faqList.add("Neccessary information");
        faqList.add("Action against Motorist");
        faqList.add("Reward point");
        faqList.add("Status of rejection");
        //---
        faqList.add("Reasons for rejection");
        faqList.add("Liability");
        faqList.add("Low Bandwidth/No internet connection");
        faqList.add("Importance of Geolocation");
        faqList.add("Referal Reward Points");
        faqList.add("Redeem Reward Points");
        faqList.add("Blurred image of violation");*/
        if (position == 0) {
            webView.loadUrl("file:///android_asset/faq.html");//file:///android_asset/aboutcertified.html
        } else if (position == 1) {
            webView.loadUrl("file:///android_asset/become.html");
        } else if (position == 2) {
            webView.loadUrl("file:///android_asset/list.html");
        } else if (position == 3) {
            webView.loadUrl("file:///android_asset/list_image.html");
        } else if (position == 4) {
            webView.loadUrl("file:///android_asset/info.html");
        } else if (position == 5) {
            webView.loadUrl("file:///android_asset/action_against_motorist.html");
        } else if (position == 6) {
            webView.loadUrl("file:///android_asset/point.html");
        }else if (position == 7) {
            webView.loadUrl("file:///android_asset/status_of_rejection.html");
        } else if (position == 8) {
            webView.loadUrl("file:///android_asset/rejection_reasons.html");
        } else if (position == 9) {
            webView.loadUrl("file:///android_asset/liability.html");
        } else if (position == 10) {
            webView.loadUrl("file:///android_asset/offline.html");
        }else if (position == 11) {
            webView.loadUrl("file:///android_asset/geolocation.html");
        }else if (position == 12) {
            webView.loadUrl("file:///android_asset/rewards.html");
        }else if (position == 13) {
            webView.loadUrl("file:///android_asset/redeem_points.html");
        }else if (position == 14) {
            webView.loadUrl("file:///android_asset/blurred_image.html");
        }else if (position == 15) {
            webView.loadUrl("file:///android_asset/privacy.html");
        }
    }

    private void initViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        webView = (WebView) findViewById(R.id.webview);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        startActivity(new Intent(WebViewActivity.this, HomeActivity.class));
        finish();
    }

    @Override
    protected void onStop() {
        super.onStop();
//        Util.finishActivity(FeedbackActivity.this);
    }
}
