package com.aeologic.cctv.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.aeologic.cctv.Async.UpdatePersonalInfo;
import com.aeologic.cctv.Connection.ConnectionDetector;
import com.aeologic.cctv.Database.Database;
import com.aeologic.cctv.R;
import com.aeologic.cctv.Utility.Prefrence;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ProfileActivity extends AppCompatActivity {

    private EditText nameTxt;
    // private EditText userIDText;
    private EditText designationTxt;
    private EditText mobileNoTxt;
    private EditText gdaTxt;
    private TextView nnTxt;
    private TextView shortName;
    private ImageView editPersonalInfo;
    LinearLayout editControl;
    CardView savePersonalInfo, cancelPersonalInfo;
    ConnectionDetector detector;
    Spinner citySpinner;
    ArrayList<HashMap<String, String>> cityList;
    List<String> city = new ArrayList<>();
    String cityValue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        initViews();
        setupToolbar();
        setUserDetails();
        editControl.setVisibility(View.VISIBLE);
        nameTxt.setEnabled(true);
        mobileNoTxt.setEnabled(true);
        detector = new ConnectionDetector(ProfileActivity.this);

        Database database = new Database(ProfileActivity.this);
        cityList = database.getActiveStateList();
        if (cityList != null && cityList.size() > 0) {
            city.clear();
            city.add("Select City");
            for (int i = 0; i < cityList.size(); i++) {
                city.add(cityList.get(i).get("city"));
            }
        }

        cityValue = Prefrence.getCity(ProfileActivity.this);
        int position = getPosition(city, cityValue);
        Log.e( "Position: ", String.valueOf(position));
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(ProfileActivity.this,
                R.layout.spinner_item, city);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        citySpinner.setAdapter(adapter);

        citySpinner.setSelection(position);
        citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                //Change the selected item's text color
                if(position!=0){
                    cityValue=city.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {
            }
        });

        editPersonalInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editControl.setVisibility(View.VISIBLE);
                nameTxt.setEnabled(true);
                mobileNoTxt.setEnabled(true);
            }
        });
        cancelPersonalInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editControl.setVisibility(View.GONE);
                nameTxt.setEnabled(false);
                mobileNoTxt.setEnabled(false);

            }
        });
        savePersonalInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (detector.isConnectingToInternet()) {
                    if (validatePersonalInfo()) {
                        UpdatePersonalInfo updatePersonalInfo = new UpdatePersonalInfo(ProfileActivity.this);
                        updatePersonalInfo.execute(getString(R.string.API_BASE_URL) + getString(R.string.API_UPDATE_PERSONAL_INFO), nameTxt.getText().toString().trim(), mobileNoTxt.getText().toString().trim());

                    }

                } else {
                    //showAlertBox(UserProfile.this, getString(R.string.con_fail), "Ok", "");
                    //  Toast.makeText(UserProfile.this, getString(R.string.con_fail), Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private int getPosition(List<String> city, String cityValue) {
        for (int i = 0; i < city.size(); i++) {
            if (city.get(i).equalsIgnoreCase(cityValue)) {
                return i;
            }
        }
        return -1;
    }

    private void setUserDetails() {
        if (Prefrence.getUsername(ProfileActivity.this) != null) {
            nameTxt.setText(Prefrence.getUsername(ProfileActivity.this));
            shortName.setText(printFirst(Prefrence.getUsername(ProfileActivity.this)));
        }
        if (Prefrence.getMobileNumber(ProfileActivity.this) != null) {
            mobileNoTxt.setText(Prefrence.getMobileNumber(ProfileActivity.this));
        }
        if (Prefrence.getEmail(ProfileActivity.this) != null) {
            designationTxt.setText(Prefrence.getDesignation(ProfileActivity.this));
              Log.e( "setUserDetails: ",Prefrence.getEmail(ProfileActivity.this) );  
        }

    }

    private boolean validatePersonalInfo() {
        if (nameTxt.getText().toString().trim().length() != 0) {
            if (nameTxt.getText().toString().trim().length() > 3) {
                if (mobileNoTxt.length() == 10) {
                    return true;
                } else {
                    mobileNoTxt.requestFocus();
                    mobileNoTxt.setError(getString(R.string.invalid_mobile_no));
                }

            } else {
                nameTxt.requestFocus();
                nameTxt.setError(getString(R.string.valid_name));
            }

        } else if (nameTxt.getText().toString().trim().length() == 0) {
            nameTxt.requestFocus();
            nameTxt.setError(getString(R.string.valid_name));
        }
        return false;
    }

    private void initViews() {
        nameTxt = (EditText) findViewById(R.id.name_txt);
        designationTxt = (EditText) findViewById(R.id.care_of_txt);
        mobileNoTxt = (EditText) findViewById(R.id.mobile_no_txt);
        gdaTxt = (EditText) findViewById(R.id.pin_code_txt);
        nnTxt = (TextView) findViewById(R.id.state_txt);
        editPersonalInfo = (ImageView) findViewById(R.id.edit_personal_info);
        savePersonalInfo = (CardView) findViewById(R.id.save_personal_info);
        cancelPersonalInfo = (CardView) findViewById(R.id.cancel_personal_info);
        editControl = (LinearLayout) findViewById(R.id.rl_edit_control);
        shortName = (TextView) findViewById(R.id.short_name);
        citySpinner = (Spinner) findViewById(R.id.city_spinner);


    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        setTitle(getString(R.string.my_profile));
    }

    static String printFirst(String s) {
        Pattern p = Pattern.compile("\\b[a-zA-Z]");
        Matcher m = p.matcher(s);
        String shortcut = "";

        while (m.find())
            shortcut = shortcut + m.group();
//        System.out.print(m.group());

        return shortcut;
    }

    @Override
    public void onBackPressed() {

        goToActivity(HomeActivity.class);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                goToActivity(HomeActivity.class);

                break;
        }
        return true;
    }

    private void goToActivity(Class aClass) {
        Intent intent = new Intent(ProfileActivity.this, aClass);
        startActivity(intent);
        overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);
        finish();
    }
}
