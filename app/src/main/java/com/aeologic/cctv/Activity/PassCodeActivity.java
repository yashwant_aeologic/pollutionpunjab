package com.aeologic.cctv.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.aeologic.cctv.Async.DownloadImagesAsync;
import com.aeologic.cctv.Connection.ConnectionDetector;
import com.aeologic.cctv.Database.Database;
import com.aeologic.cctv.R;
import com.aeologic.cctv.Utility.AppController;
import com.aeologic.cctv.Utility.Prefrence;
import com.aeologic.cctv.Utility.Util;
import com.aeologic.cctv.Utility.VolleyMultipartRequest;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class PassCodeActivity extends AppCompatActivity {
    private EditText et1, et2, et3, et4;
    private Button num1, num2, num3, num4, num5, num6, num7, num8, num9, num0;
    private ImageView rightImg, removeImg;
    ConnectionDetector detector;
    private String TAG = "PassCodeActivity";
    TextView forgotPassword;
    int counter = 0;
    private boolean isShowingDialog = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pass_code);
        initView();
        et1.requestFocus();

        et1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                et1.requestFocus();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count == 1) {
                    counter++;
                    et2.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        et2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count == 1) {
                    counter++;
                    et3.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        et3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count == 1) {
                    counter++;
                    et4.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        et4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                counter++;
//                    et5.requestFocus();

                if (!et1.getText().toString().isEmpty() &&
                        !et2.getText().toString().isEmpty() &&
                        !et3.getText().toString().isEmpty() &&
                        !et4.getText().toString().isEmpty()) {
                    String passCode = "" + et1.getText().toString() + et2.getText().toString() + et3.getText().toString() + et4.getText().toString();
                    String encryptPasscode = Util.md5Password(passCode);

                    if (Prefrence.getToken(PassCodeActivity.this).equals("")) {
                        //call the login API

                        if (detector.isConnectingToInternet()) {
                            final ProgressDialog progressDialog = new ProgressDialog(PassCodeActivity.this);
                            progressDialog.setMessage(getString(R.string.please_wait));
                            progressDialog.setCancelable(false);
                            progressDialog.show();
                            loginAsync(PassCodeActivity.this,
                                    Prefrence.getMobileNumber(PassCodeActivity.this),
                                    encryptPasscode, progressDialog);
                        } else {
                            Util.showAlert(PassCodeActivity.this, getString(R.string.please_check_internet));
                        }
                    } else if (encryptPasscode.equalsIgnoreCase(Prefrence.getPasscode(PassCodeActivity.this))) {
                        startActivity(new Intent(PassCodeActivity.this, HomeActivity.class));
                        finish();
                    } else {
                        Util.showAlert(PassCodeActivity.this, getString(R.string.wrong_passcode_pta));
                        clearAll();
                    }

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        num0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enterPasscode(0);
            }
        });
        num1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enterPasscode(1);
            }
        });

        num2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enterPasscode(2);
            }
        });

        num3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enterPasscode(3);
            }
        });

        num4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enterPasscode(4);
            }
        });

        num5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enterPasscode(5);
            }
        });

        num6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enterPasscode(6);
            }
        });

        num7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enterPasscode(7);
            }
        });

        num8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enterPasscode(8);
            }
        });

        num9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enterPasscode(9);
            }
        });

        removeImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                removePasscode();
            }
        });

        rightImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String passCode = "" + et1.getText().toString() + et2.getText().toString() + et3.getText().toString() + et4.getText().toString();
                String encyptPasscode = Util.md5Password(passCode);


                if (passCode.trim().length() < 4) {
                    Util.showAlert(PassCodeActivity.this, getString(R.string.please_chcek_your_password));
                    clearAll();
                    return;
                }
                if (Prefrence.getToken(PassCodeActivity.this).equals("")) {
                    //call the login API
                    if (detector.isConnectingToInternet()) {
                        final ProgressDialog progressDialog = new ProgressDialog(PassCodeActivity.this);
                        progressDialog.setMessage(getString(R.string.please_wait));
                        progressDialog.setCancelable(false);
                        progressDialog.show();
                        loginAsync(PassCodeActivity.this,
                                Prefrence.getMobileNumber(PassCodeActivity.this),
                                encyptPasscode, progressDialog);
                    } else {
                        Util.showAlert(PassCodeActivity.this, getString(R.string.please_check_internet));
                    }
                } else if (encyptPasscode.equalsIgnoreCase(Prefrence.getPasscode(PassCodeActivity.this))) {
                    startActivity(new Intent(PassCodeActivity.this, HomeActivity.class));
                    finish();
                } else {
                    Util.showAlert(PassCodeActivity.this, getString(R.string.wrong_passcode_pta));
                    clearAll();
                }

            }
        });

        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (detector.isConnectingToInternet()) {
                    Intent intent = new Intent(PassCodeActivity.this, UserLoginActivity.class);
                    intent.putExtra("forgotPassStatus", true);
                    startActivity(intent);
                    finish();
                } else {
                    Util.showAlert(PassCodeActivity.this, getString(R.string.please_check_internet));
                }

            }
        });

    }

    public void showAlert(String msg) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(PassCodeActivity.this);
        alertDialog.setMessage(msg);
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton(getResources().getString(R.string.Ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                isShowingDialog = false;
            }
        });
//        if (!isShowingDialog) {
        alertDialog.show();
//        }
    }


    @Override
    protected void onStop() {
        super.onStop();
//        Util.finishActivity(PassCodeActivity.this);
    }

    private void initView() {
        et1 = (EditText) findViewById(R.id.et1);
        et2 = (EditText) findViewById(R.id.et2);
        et3 = (EditText) findViewById(R.id.et3);
        et4 = (EditText) findViewById(R.id.et4);

        num1 = (Button) findViewById(R.id.btn_1);
        num2 = (Button) findViewById(R.id.btn_2);
        num3 = (Button) findViewById(R.id.btn_3);
        num4 = (Button) findViewById(R.id.btn_4);
        num5 = (Button) findViewById(R.id.btn_5);
        num6 = (Button) findViewById(R.id.btn_6);
        num7 = (Button) findViewById(R.id.btn_7);
        num8 = (Button) findViewById(R.id.btn_8);
        num9 = (Button) findViewById(R.id.btn_9);
        num0 = (Button) findViewById(R.id.btn_0);

        removeImg = (ImageView) findViewById(R.id.delete_img);
        rightImg = (ImageView) findViewById(R.id.right_img);
        detector = new ConnectionDetector(getApplicationContext());
        forgotPassword = (TextView) findViewById(R.id.forgot_passcode);
    }

    public void enterPasscode(int num) {
        if (et1.hasFocus()) {
            et1.setText("" + num);
        } else if (et2.hasFocus()) {
            et2.setText("" + num);
        } else if (et3.hasFocus()) {
            et3.setText("" + num);
        } else {
            et4.setText("" + num);
        }
    }

    public void removePasscode() {
        if (et1.hasFocus()) {
            et1.setText("");
        } else if (et2.hasFocus()) {
            et2.setText("");
            et1.requestFocus();
        } else if (et3.hasFocus()) {
            et3.setText("");
            et2.requestFocus();
        } else {
            et4.setText("");
            et3.requestFocus();
        }
    }

    public void clearAll() {
        et1.setText("");
        et2.setText("");
        et3.setText("");
        et4.setText("");
        et1.requestFocus();
    }


    public static void saveOffenceListToDb(final Activity activity, final ProgressDialog progressDialog) {
        try {
//            Log.e("TAG", "saveOffenceListToDb: className: " + activity.getLocalClassName());

            Log.e(activity.getLocalClassName(), "doInBackground: API: " + activity.getString(R.string.ACTIVE_STATE_LIST_API));
            StringRequest strReq = new StringRequest(Request.Method.GET,
                    activity.getString(R.string.ACTIVE_STATE_LIST_API), new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
//                    Log.e(activity.getLocalClassName(), "onResponse: Response: " + response);
                    progressDialog.hide();
                    try {
                        Database database = new Database(activity);
                        ArrayList<HashMap<String, String>> offenceList = new ArrayList();
                        HashMap offeceData;
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getString("status").equals("1")) {
                            JSONArray jsonArray = jsonObject.getJSONArray("offence");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                offeceData = new HashMap();
                                JSONObject arrayObj = jsonArray.getJSONObject(i);
                                offeceData.put("offenceName", arrayObj.getString("offence"));
                                offeceData.put("offenceId", arrayObj.getString("offence_id"));
                                offeceData.put("serverImagePath", arrayObj.getString("img"));
                                offenceList.add(offeceData);
                            }

                            // start the service to download the image in app private directory
                           /* Log.d("passcode Activity", "onResponse: start download image service");
                            Intent intent = new Intent(activity, DownloadService.class);
                            intent.putExtra("offenceListMap", offenceList);
                            activity.startService(intent);*/

                            new DownloadImagesAsync(activity).execute(offenceList);

                            // add the offence list in the database
                           /* if (activity.getLocalClassName().equals("Activity.PassCodeActivity")) {
                                database.deleteOffenceTable();
                            }*/
                            database.deleteOffenceTable();
                            database.saveOffenceList(offenceList);
                            activity.startActivity(new Intent(activity, HomeActivity.class));
                            activity.finish();
                        }

                    } catch (JSONException e) {
                        Util.showAlert(activity, activity.getString(R.string.pta));
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d(activity.getLocalClassName(), "Error: " + error.getMessage());
                    Util.showAlert(activity, activity.getString(R.string.pta));
                    progressDialog.hide();
                }
            });
            strReq.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 50000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
            // Adding request to request queue
            AppController.getInstance().addToRequestQueue(strReq, "String Request");


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void loginAsync(final Activity activity, final String phoneNumber, final String passCode, final ProgressDialog pDialog) {

       /* final ProgressDialog pDialog = new ProgressDialog(activity);
        pDialog.setMessage(activity.getString(R.string.please_wait));
        pDialog.show();*/


        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST,
                activity.getString(R.string.LOGIN_API), new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                String resultResponse = new String(response.data);
                Log.e(TAG, "onResponse: Response: " + resultResponse);
//                pDialog.hide();
                try {
//                                JSONArray jsonArray = new JSONArray(String.valueOf(resultResponse));
                    JSONObject jsonObject = new JSONObject(resultResponse);
                    String status = jsonObject.getString("status");
                    if (status.equals("1")) {
                        JSONObject obj = jsonObject.getJSONObject("data");
                        JSONArray jsonArray = obj.getJSONArray("user_detail");
                        JSONObject childObj = jsonArray.getJSONObject(0);

                        String token = childObj.getString("token");
                        String username = childObj.getString("username");
                        String email = childObj.getString("token");
                        Prefrence.setToken(activity, token);
                        Prefrence.setUsername(activity, username);
                        Prefrence.setEmail(activity, email);
                        Prefrence.setPasscode(activity, passCode);

                        //call the another API
                        saveOffenceListToDb(activity, pDialog);
                    } else if (status.equals("0")) {
                        pDialog.hide();
                        Util.showAlert(activity, jsonObject.getString("message"));
                        clearAll();
                    } /*else {
                        //show alert dialog
                        pDialog.hide();
                        Util.showAlert(activity, activity.getString(R.string.pta));
                        clearAll();
                    }
*/
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                pDialog.hide();
                Util.showAlert(activity, activity.getString(R.string.pta));
                clearAll();
            }

        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("phone_number", phoneNumber);
                params.put("password", passCode);
                return params;

            }

        };
        multipartRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        AppController.getInstance().addToRequestQueue(multipartRequest);

    }

}
