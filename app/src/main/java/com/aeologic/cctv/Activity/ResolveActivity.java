package com.aeologic.cctv.Activity;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.aeologic.cctv.Async.ResolveReportAsyncTask;
import com.aeologic.cctv.Async.SendDetailsAsyncTask;
import com.aeologic.cctv.BuildConfig;
import com.aeologic.cctv.Connection.ConnectionDetector;
import com.aeologic.cctv.OfficersActivities.ManagerComplaintListActivity;
import com.aeologic.cctv.OfficersActivities.ManagerDoneDetailActivity;
import com.aeologic.cctv.R;

import com.aeologic.cctv.Service.GPSLocationService;
import com.aeologic.cctv.Utility.SingleShotLocationProvider;
import com.aeologic.cctv.Utility.Util;
import com.afollestad.materialcamera.MaterialCamera;
import com.skyfishjy.library.RippleBackground;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import static com.aeologic.cctv.Utility.Util.decodeMyFile;
import static com.aeologic.cctv.Utility.Util.getOrientation;
import static com.aeologic.cctv.Utility.Util.showAlert;

public class ResolveActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = ResolveActivity.class.getSimpleName();

    private static final int REQUEST_WRITE_STORAGE = 112;
    private static final int RESULT_LOAD_IMAGE = 15;
    private final int REQUEST_ID_MULTIPLE_PERMISSIONS = 3;
    private final int MY_PERMISSIONS_REQUEST_ACCESS_CAMERA = 2;
    private final int MY_PERMISSIONS_REQUEST_ACCESS_VIDEO = 4;

    ImageView camera;
    ImageView videoCamera;
    ImageView audio;
    private final int REQUEST_VIDEO_CAPTURE = 1;
    private final int REQUEST_IMAGE_CAPTURE = 100;
    private MediaRecorder myRecorder;
    private boolean isRecording = false;

    boolean isCameraOpenFirstTime = true;
    boolean isVedioCapturedFirstTime = true;
    boolean isAudioRecordingFirstTime = true;
    private Bitmap bitmap;
    private ImageView imageViewContainer;

    private String outputFile = null;
    private String outVideoFile = null;
    private String outImageFile = null;

    private Uri videoUri;
    private Uri compressedVideoUri;
    private Uri finalVideoUri;
    //    private AlertDialog videoDialog;
    private Uri mImageCaptureUri;
    private Uri finalImageCaptureUri = null;
    private File mediaStorageDir;
    File audioFile;
    private Dialog audioDialog;
    boolean isVideoPlaying = false;
    private boolean videoResumed = false;
    private int stopVideoPosition;
    private File videomediaFile;
    private File compressedVideoMediaFile;
    private File finalvideomediaFile;
    private VideoView videoView;
    private TextView timer, timerPlay, sendComplaint;
    private ImageView retake;
    public boolean startButtonActivate = true;
    public boolean playButtonActivate = false;
    public boolean stopButtonActivate = false;
    public boolean pauseButtonActivate = false;
    private ImageView start, satellite;
    private MediaPlayer mp;
    private Handler myHandler = new Handler();

    private int audioTotaltime;
    private RippleBackground rippleBackground;
    private int audioSeektime = 0;

    private int audioStartTime;
    private TimerTask timerTask;
    private Timer timer1;
    private Dialog photoDialog;
    private Dialog VideoDialog;

    private boolean videoPauseButtonActivate = false;

    private ImageView back;
    public Button send;
    private EditText comment;

    String merchantDetails, commentDetails;
    String workFolder = null;
    String demoVideoFolder = null;
    String demoVideoPath = null;
    String vkLogPath = null;
    private boolean commandValidationFailedFlag = false;
    private String timeStamp;
    private String videoPath;
    public boolean confirmButtonAvtivate = false;
    private ImageView audioCofirm;
    private String token = null;

    private ConnectionDetector detector;
    private boolean userInteractionStatus;
    private Dialog imageSelecetioDialog;
    private CountDownTimer countDownTimer;
    private String complaintNo;
    private int backID;
    String reportedLat, reportedLong;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resolve);
        setupBundle();
        initViews();
        handleEvents();

        // Init all Dialogs
        ShowPhotoPreview();
        showVideoPreview();
        showAudioPreview();
        imageSelectionDialog();
    }

    private void setupBundle() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            complaintNo = bundle.getString("id");
            backID = bundle.getInt("back_id");
            reportedLat = bundle.getString("latitude");
            reportedLong = bundle.getString("longitude");
        } else {
            Toast.makeText(ResolveActivity.this, R.string.error, Toast.LENGTH_SHORT).show();
            goToBack();
        }
    }

    private void initViews() {
        camera = (ImageView) findViewById(R.id.camera);
        videoCamera = (ImageView) findViewById(R.id.video_camera);
        send = (Button) findViewById(R.id.rejectBtn);
        comment = (EditText) findViewById(R.id.comment);
        satellite = (ImageView) findViewById(R.id.satellite);
        detector = new ConnectionDetector(getApplicationContext());
        audio = (ImageView) findViewById(R.id.audio);
        back = (ImageView) findViewById(R.id.imageview);

    }

    private void goToBack() {
        Intent intent = new Intent(ResolveActivity.this, ManagerComplaintListActivity.class);
        intent.putExtra("From", "RESOLVE");
        if (backID == 1) {
            intent = new Intent(ResolveActivity.this, ManagerDoneDetailActivity.class);
            intent.putExtra("id", Integer.parseInt(complaintNo));
            intent.putExtra("tabStatus", 1);
        }
        startActivity(intent);
        overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);
        finish();
    }

    private void handleEvents() {
        back.setOnClickListener(this);
        send.setOnClickListener(this);
        camera.setOnClickListener(this);
        videoCamera.setOnClickListener(this);

        comment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                comment.requestFocus();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.e("onTextChanged: ", "");
                if (start == 249) {
                    comment.setInputType(0);
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(comment.getWindowToken(), 0);
                    Toast.makeText(ResolveActivity.this, getResources().getString(R.string.plz_summarize_in), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 500) {
                    Toast.makeText(ResolveActivity.this, "Character limit reached", Toast.LENGTH_SHORT).show();
                }
            }
        });

        audio.setOnTouchListener(new View.OnTouchListener() {
                                     @Override
                                     public boolean onTouch(View v, MotionEvent event) {
                                         if (event.getAction() == MotionEvent.ACTION_UP) {
                                             audio.setImageResource(R.drawable.offence_mic);
                                             if (CheckAudioPermission()) {
                                                 audioDialog.show();
                                                 confirmButtonAvtivate = false;
                                             }
                                         }
                                         return true;
                                     }
                                 }

        );
    }

    @Override
    public void onClick(View view) {
        if (view == send) {

            //////////////////////////////////////////

            if (Util.checkGPS(ResolveActivity.this, false)) {
                if (GPSLocationService.location != null) {
                    final Location destination = new Location("");
                    destination.setLatitude(Double.parseDouble(reportedLat));
                    destination.setLongitude(Double.parseDouble(reportedLong));
                    Log.e("accuracye", "" + GPSLocationService.location.getAccuracy() + "Lat= " + reportedLat + "LONG= " + reportedLong);
                    final ProgressDialog progressDialog = new ProgressDialog(ResolveActivity.this);
                    progressDialog.setMessage("Please wait! Fetching your geo location.");
                    progressDialog.show();
                    SingleShotLocationProvider.requestSingleUpdate(ResolveActivity.this,
                            new SingleShotLocationProvider.LocationCallback() {

                                @Override
                                public void onNewLocationAvailable(Location location) {

                                    float rangeDistance = location.distanceTo(destination);
                                    //  Log.e("onNewLocationAvailable: ", String.valueOf(rangeDistance));
                                    if (rangeDistance <= 200) {
                                        submitResolvedReport(location, rangeDistance);
                                    } else {
                                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ResolveActivity.this);
                                        alertDialog.setMessage("You must be available within 200 meter range to resolve it.");
                                        alertDialog.setCancelable(false);
                                        alertDialog.setPositiveButton(getResources().getString(R.string.Ok), new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                                goToBack();
                                            }
                                        });

                                        alertDialog.show();
                                        //showAlert(ResolveActivity.this, "Please go near to the site to resolve ticket.");
                                        //  Toast.makeText(ResolveActivity.this, "Please go near to the site to resolve ticket.", Toast.LENGTH_LONG).show();
                                    }
                                    progressDialog.hide();
                                }
                            });

                } else {
                    showAlert(ResolveActivity.this, "Geo-code missing. Please try again.");

                    //Toast.makeText(ResolveActivity.this, "Geo-code missing. Please try again.", Toast.LENGTH_LONG).show();

                }
            } else {
                showAlert(ResolveActivity.this, "Please enable GPS.");

            }
            //////////////////////////////////////

        } else if (view == camera) {
            if (checkCameraPermission()) {
                captureImage();
            }
        } else if (view == videoCamera) {
            recordVideo();
        } else if (view == back) {
            goToBack();
        }
    }

    private void submitResolvedReport(Location location, float rangeDistance) {
        if (isValid()) {
            Util.hideKeyboard(ResolveActivity.this, comment);
            if (detector.isConnectingToInternet()) {
                if (finalImageCaptureUri != null) {
                    outImageFile = Util.compressImage(ResolveActivity.this, finalImageCaptureUri.getPath());
                }
                if (finalVideoUri != null) {
                    outVideoFile = finalVideoUri.getPath();
                } else {
                    outVideoFile = null;
                }

                send.setAlpha(0.4f);
                send.setEnabled(false);
                new ResolveReportAsyncTask(ResolveActivity.this, send).
                        execute(getString(R.string.SUBMIT_RESOLVE_REPORT),
                                complaintNo,
                                comment.getText().toString().trim(),
                                outImageFile,
                                outVideoFile,
                                outputFile,
                                String.valueOf(location.getLatitude()),
                                String.valueOf(location.getLongitude()),
                                String.valueOf(rangeDistance));
            } else {
                Util.showAlert(ResolveActivity.this, getString(R.string.do_you_want_save_offence));
                send.setAlpha(1f);
                send.setEnabled(true);
            }
        }
    }

    private void recordVideo() {
        if (CheckVedioCameraPermission()) {
            if (isVedioCapturedFirstTime) {
                mediaStorageDir = new File(Environment.getExternalStorageDirectory() + "/DCIM/", "pollution");
                if (!mediaStorageDir.exists()) {
                    if (!mediaStorageDir.mkdirs()) {
                        Log.d("MyCameraApp", "failed to create directory");
                    }
                }

                startVideoRecorder();
            } else {
                if (videomediaFile != null) {
                    videoView.setVideoURI(finalVideoUri);
                    Bitmap thumb = ThumbnailUtils.createVideoThumbnail(finalvideomediaFile.toString(),
                            MediaStore.Images.Thumbnails.MINI_KIND);
                    Log.e(TAG, "onTouch: " + thumb.getWidth() + "::" + thumb.getHeight());
                    BitmapDrawable bitmapDrawable = new BitmapDrawable(getResources(), thumb);
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                        videoView.setBackgroundDrawable(bitmapDrawable);
                    } else {
                        videoView.setBackground(bitmapDrawable);
                    }
                }
                VideoDialog.show();
            }
        }
    }

    private boolean isValid() {
        if (finalImageCaptureUri != null && comment.getText().toString().trim().length() > 0) {
            return true;
        } else if (finalImageCaptureUri == null) {
            Util.showAlert(ResolveActivity.this, getString(R.string.image_mandatory));
        } else if (comment.getText().toString().trim().length() == 0) {
            Util.showAlert(ResolveActivity.this,
                    getString(R.string.enter_comment));
        }
        return false;
    }

    private void showAudioPreview() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this, R.style.Theme_Transparent);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_audio, null);
        dialogBuilder.setView(dialogView);
        audioDialog = dialogBuilder.create();

        final ImageView mic = (ImageView) dialogView.findViewById(R.id.mic);
        start = (ImageView) dialogView.findViewById(R.id.start);
        final ImageView cancel = (ImageView) dialogView.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (confirmButtonAvtivate) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(ResolveActivity.this);
                    alertDialog.setMessage(getResources().getString(R.string.audio_warn));
                    alertDialog.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            countDownTimer.cancel();
                            audio.setImageResource(R.drawable.offence_mic);
                            //stop();
                            stopButtonActivate = false;
                            start.setImageResource(R.drawable.audio_play);
                            playButtonActivate = true;
                            audioSeektime = 0;
                            retake.setEnabled(true);
                            retake.setImageResource(R.drawable.audio_restart);
                            rippleBackground.stopRippleAnimation();
                            confirmButtonAvtivate = true;
                            audioCofirm.setImageResource(R.drawable.crct1);
                            timer.setText("00:30 Sec");
                            if (timerPlay.getVisibility() == View.VISIBLE) {
                                timerPlay.setVisibility(View.GONE);
                                timer.setVisibility(View.VISIBLE);

                            }

                            start.setImageResource(R.drawable.audio_start);
                            startButtonActivate = true;
                            retake.setEnabled(false);
                            retake.setImageResource(R.drawable.audio_restarthide);
                            confirmButtonAvtivate = false;
                            audioCofirm.setImageResource(R.drawable.crct);
                            dialog.dismiss();
                            audioDialog.dismiss();

                        }
                    });
                    alertDialog.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    alertDialog.show();
                } else if (isRecording) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(ResolveActivity.this);
                    alertDialog.setMessage(getResources().getString(R.string.audio_warn));
                    alertDialog.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            countDownTimer.cancel();
                            // audio.setImageResource(R.drawable.offence_mic_filled);
                            //stop();
                            stopButtonActivate = false;
                            start.setImageResource(R.drawable.audio_play);
                            playButtonActivate = true;
                            audioSeektime = 0;
                            retake.setEnabled(true);
                            retake.setImageResource(R.drawable.audio_restart);
                            rippleBackground.stopRippleAnimation();
                            confirmButtonAvtivate = true;
                            audioCofirm.setImageResource(R.drawable.crct1);
                            timer.setText("00:30 Sec");
                            if (timerPlay.getVisibility() == View.VISIBLE) {
                                timerPlay.setVisibility(View.GONE);
                                timer.setVisibility(View.VISIBLE);

                            }

                            start.setImageResource(R.drawable.audio_start);
                            startButtonActivate = true;
                            retake.setEnabled(false);
                            retake.setImageResource(R.drawable.audio_restarthide);
                            confirmButtonAvtivate = false;
                            audioCofirm.setImageResource(R.drawable.crct);
                            dialog.dismiss();
                            audioDialog.dismiss();

                        }
                    });
                    alertDialog.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    alertDialog.show();

                } else if (confirmButtonAvtivate && retake.isEnabled()) {
                    //audio.setImageResource(R.drawable.offence_mic_filled);
                    audioDialog.dismiss();
                } else {
                    audioDialog.dismiss();
                }
            }
        });

        timer = (TextView) dialogView.findViewById(R.id.timer);
        timerPlay = (TextView) dialogView.findViewById(R.id.timer1);
        retake = (ImageView) dialogView.findViewById(R.id.retake);
        audioCofirm = (ImageView) dialogView.findViewById(R.id.audio_confirm);
        rippleBackground = (RippleBackground) dialogView.findViewById(R.id.content);
        if (isAudioRecordingFirstTime) {
            retake.setEnabled(false);
            isAudioRecordingFirstTime = false;
        } else {
            retake.setEnabled(true);
            retake.setImageResource(R.drawable.audio_restart);
            start.setImageResource(R.drawable.audio_play);
            stopButtonActivate = false;
            playButtonActivate = true;
        }


        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (startButtonActivate) {
                    start();
                    isRecording = true;
                    start.setImageResource(R.drawable.audio_stop);
                    rippleBackground.startRippleAnimation();
                    startButtonActivate = false;
                    stopButtonActivate = true;
                } else if (stopButtonActivate) {
                    countDownTimer.cancel();
                    audio.setImageResource(R.drawable.offence_mic_filled);
                    stop();
                    stopButtonActivate = false;
                    start.setImageResource(R.drawable.audio_play);
                    playButtonActivate = true;
                    audioSeektime = 0;
                    retake.setEnabled(true);
                    retake.setImageResource(R.drawable.audio_restart);
                    rippleBackground.stopRippleAnimation();
                    confirmButtonAvtivate = true;
                    audioCofirm.setImageResource(R.drawable.crct1);
                } else if (playButtonActivate == true) {

                    mp = new MediaPlayer();
                    try {
                        //you can change the path, here path is external directory(e.g. sdcard) /Music/maine.mp3
                        mp.setDataSource(outputFile);
                        mp.prepare();
                        mp.start();

                        audioTotaltime = mp.getDuration();
                        audioStartTime = mp.getCurrentPosition();
                        if (timer.getVisibility() == View.VISIBLE) {
                            timer.setVisibility(View.GONE);
                            timerPlay.setVisibility(View.VISIBLE);
                            timerPlay.setText(String.format("%d:%d Sec",
                                    TimeUnit.MILLISECONDS.toMinutes((long) audioStartTime),
                                    TimeUnit.MILLISECONDS.toSeconds((long) audioStartTime) -
                                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.
                                                    toMinutes((long) audioStartTime))));
                        }
                        myHandler.postDelayed(UpdateSongTime, 100);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (audioSeektime != 0) {
                        mp.seekTo(audioSeektime);
                        audioStartTime = audioSeektime;
                        audioTotaltime = mp.getDuration();
                        mp.start();

                        timer.setText(String.format("%d min, %d sec",
                                TimeUnit.MILLISECONDS.toMinutes((long) audioStartTime),
                                TimeUnit.MILLISECONDS.toSeconds((long) audioStartTime) -
                                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long) audioStartTime))));
                        myHandler.postDelayed(UpdateSongTime, 100);
                    }
                    mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            start.setImageResource(R.drawable.audio_play);
                            playButtonActivate = true;
                            audioSeektime = 0;
                        }
                    });

                    playButtonActivate = false;
                    start.setImageResource(R.drawable.audio_pause);
                    pauseButtonActivate = true;
                } else if (pauseButtonActivate) {
                    mp.pause();
                    pauseButtonActivate = false;
                    start.setImageResource(R.drawable.audio_play);
                    playButtonActivate = true;
                    audioSeektime = mp.getCurrentPosition();
                }
            }
        });

        retake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timer.setText("00:30 Sec");
                if (timerPlay.getVisibility() == View.VISIBLE) {
                    timerPlay.setVisibility(View.GONE);
                    timer.setVisibility(View.VISIBLE);

                }

                start.setImageResource(R.drawable.audio_start);
                startButtonActivate = true;
                retake.setEnabled(false);
                retake.setImageResource(R.drawable.audio_restarthide);
                confirmButtonAvtivate = false;
                audioCofirm.setImageResource(R.drawable.crct);
            }
        });

        audioCofirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (confirmButtonAvtivate) {
                    //create the final audio uri here
                    audio.setImageResource(R.drawable.offence_mic_filled);
                    audioDialog.dismiss();
                }
            }
        });


    }

    private void stop() {
        try {
            myRecorder.stop();
        } catch (RuntimeException stopException) {
            //handle cleanup here
        }
        myRecorder.release();
        myRecorder = null;
        isRecording = false;
        retake.setEnabled(true);
        retake.setImageResource(R.drawable.audio_restart);
        start.setImageResource(R.drawable.audio_play);
        // Toast.makeText(getApplicationContext(), "Audio recorded successfully", Toast.LENGTH_LONG).show();
    }

    public void start() {

        mediaStorageDir = new File(Environment.getExternalStorageDirectory() + "/pollution/", "pollution");
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("MyCameraApp", "failed to create directory");
            }
        }

        final String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        audioFile = new File(mediaStorageDir.getPath() + File.separator + "pollution" + timeStamp + ".mp3");
        outputFile = audioFile.toString();
        myRecorder = new MediaRecorder();
        myRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        myRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        myRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
        myRecorder.setOutputFile(outputFile);

        countDownTimer = new CountDownTimer(20000, 1000) {

            public void onTick(long millisUntilFinished) {
                timer.setText("00:" + millisUntilFinished / 1000 + " Sec");
                try {
                    myRecorder.prepare();
                    myRecorder.start();
                   /* Toast.makeText(getApplicationContext(), "Start recording...",
                            Toast.LENGTH_SHORT).show();
*/
//                    Log.e("Time Remaaing:", "" + millisUntilFinished / 1000);

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
            }

            public void onFinish() {

                myRecorder.stop();
                myRecorder.release();
                myRecorder = null;
                //     Toast.makeText(getApplicationContext(), "Audio record reach its maximum Length. " +
                //            "Audio recorded successfully", Toast.LENGTH_LONG).show();
                isRecording = false;
                retake.setEnabled(true);
                retake.setImageResource(R.drawable.audio_restart);
                start.setImageResource(R.drawable.audio_play);
                audio.setImageResource(R.drawable.mic2);
                playButtonActivate = true;
                stopButtonActivate = false;
                timer.setText("00:00 Sec");
                audioSeektime = 0;
                retake.setEnabled(true);
                retake.setImageResource(R.drawable.audio_restart);
                rippleBackground.stopRippleAnimation();
                // stop Video recording
                // call a method which will send video via email.
            }
        }.start();


    }

    private boolean CheckAudioPermission() {
        List<String> audioPermissionList = new ArrayList<String>();
        if (ActivityCompat.checkSelfPermission(ResolveActivity
                .this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {

            audioPermissionList.add(Manifest.permission.RECORD_AUDIO);
        }
        if (ActivityCompat.checkSelfPermission(ResolveActivity
                .this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            audioPermissionList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (!audioPermissionList.isEmpty()) {
            ActivityCompat.requestPermissions(this, audioPermissionList.toArray(new String[audioPermissionList.size()]),
                    REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    private void startVideoRecorder() {

        MaterialCamera materialCamera =
                new MaterialCamera(ResolveActivity.this)
                        .saveDir(mediaStorageDir)
                        .showPortraitWarning(false)
                        .defaultToFrontFacing(false)
                        .allowChangeCamera(false)
                        .allowRetry(true)
                        .defaultToFrontFacing(false)
                        .autoSubmit(false)
                        .primaryColor(Color.WHITE)
                        .countdownSeconds(20f)
//                        .videoFrameRate(24)
                        .videoEncodingBitRate(1000 * 1024)
                        .audioEncodingBitRate(50000)
                        .videoPreferredAspect(16f / 9f)           // Sets a custom icon used to pause playback
                        .qualityProfile(MaterialCamera.QUALITY_480P);

        materialCamera.start(REQUEST_VIDEO_CAPTURE);
    }

    private Runnable UpdateSongTime = new Runnable() {
        public void run() {
            audioStartTime = mp.getCurrentPosition();
            //  setTimer(audioStartTime);
            timerPlay.setText(String.format("%d:%d Sec",
                    TimeUnit.MILLISECONDS.toMinutes((long) audioStartTime),
                    TimeUnit.MILLISECONDS.toSeconds((long) audioStartTime) -
                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.
                                    toMinutes((long) audioStartTime))));
            myHandler.postDelayed(this, 100);
        }
    };

    private void captureImage() {
        if (isCameraOpenFirstTime) {
//                        imageSelecetioDialog.show();
            Log.e(TAG, "onClick: isCameraOpenFirstTime");
            mediaStorageDir = new File(Environment.getExternalStorageDirectory() + "/DCIM/", "pollution");
            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    Log.d("MyCameraApp", "failed to create directory");
                }
            }
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

            File mediaFile = new File(mediaStorageDir.getPath() + File.separator + "pollution" + timeStamp + ".jpg");

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                mImageCaptureUri = FileProvider.getUriForFile(ResolveActivity.this,
                        BuildConfig.APPLICATION_ID + ".provider",
                        mediaFile);
            } else {
                mImageCaptureUri = Uri.fromFile(mediaFile);
            }
            finalImageCaptureUri = mImageCaptureUri;
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
            startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
            Log.e(TAG, "onClick: " + mImageCaptureUri.getPath());

        } else {
            if (finalImageCaptureUri != null) {
                Log.e(TAG, "captureImage: " + finalImageCaptureUri);
                imageViewContainer.setImageBitmap(decodeMyFile(new File(finalImageCaptureUri.getPath())));//I(finalImageCaptureUri);
                String ori = getOrientation(finalImageCaptureUri);
                if (ori.equalsIgnoreCase("portrait")) {
                    imageViewContainer.setScaleType(ImageView.ScaleType.FIT_XY);
                } else {
                    imageViewContainer.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                }
                photoDialog.show();
            }
        }
    }

    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable("uri", finalImageCaptureUri);
        outState.putParcelable("uriVideo", finalVideoUri);
        Log.e(TAG, "onSaveInstanceState: " + finalImageCaptureUri);
        super.onSaveInstanceState(outState);
    }

    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        finalImageCaptureUri = savedInstanceState.getParcelable("uri");
        finalVideoUri = savedInstanceState.getParcelable("uriVideo");
        Log.e(TAG, "onRestoreInstanceState: " + finalImageCaptureUri);
        super.onSaveInstanceState(savedInstanceState);
    }


    private void showVideoPreview() {

        VideoDialog = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        VideoDialog.setContentView(R.layout.dialog_video);
        VideoDialog.setCanceledOnTouchOutside(true);
        Window window = VideoDialog.getWindow();
        final ImageView _continue = (ImageView) VideoDialog.findViewById(R.id._continue);
        final ImageView remove = (ImageView) VideoDialog.findViewById(R.id.remove);
        final ImageView retake = (ImageView) VideoDialog.findViewById(R.id.retake);
        videoView = (VideoView) VideoDialog.findViewById(R.id.videoview);
        final ImageView play = (ImageView) VideoDialog.findViewById(R.id.play);

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoView.setBackgroundResource(0);
                if (videoResumed) {
                    videoView.seekTo(stopVideoPosition);

                    videoView.start();
                    play.setVisibility(View.GONE);
                    retake.setVisibility(View.GONE);
                    _continue.setVisibility(View.GONE);
                    remove.setVisibility(View.GONE);
                    play.setImageResource(R.drawable.pause);
                    isVideoPlaying = true;
                    videoResumed = false;
                    videoPauseButtonActivate = true;
                } else if (isVideoPlaying == false) {
                    try {
                        videoView.setVideoURI(finalVideoUri);
                        videoView.requestFocus();
                        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                            @Override
                            public void onPrepared(MediaPlayer mp) {
                                mp.start();
                            }
                        });
                        isVideoPlaying = true;
                        play.setImageResource(R.drawable.pause);
                        play.setVisibility(View.GONE);
                        remove.setVisibility(View.GONE);
                        videoPauseButtonActivate = true;
                        _continue.setVisibility(View.GONE);
                        retake.setVisibility(View.GONE);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    stopVideoPosition = videoView.getCurrentPosition();
                    videoView.pause();
                    isVideoPlaying = false;
                    videoResumed = true;
                    play.setImageResource(R.drawable.play);
                    _continue.setVisibility(View.VISIBLE);
                    retake.setVisibility(View.VISIBLE);
                    remove.setVisibility(View.VISIBLE);
                    videoPauseButtonActivate = false;
                }


            }
        });

        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                play.setImageResource(R.drawable.play);
                play.setVisibility(View.VISIBLE);
                isVideoPlaying = false;
                videoResumed = false;
                _continue.setVisibility(View.VISIBLE);
                retake.setVisibility(View.VISIBLE);
                remove.setVisibility(View.VISIBLE);
                videoPauseButtonActivate = false;
            }
        });

        videoView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (play.getVisibility() == View.GONE) {
                    play.setVisibility(View.VISIBLE);
                } else if (videoPauseButtonActivate) {
                    play.setVisibility(View.GONE);
                }
                return false;
            }
        });

        _continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                new TranscdingBackground(SendComplaintActivity.this).execute();
                VideoDialog.dismiss();
                videoCamera.setImageResource(R.drawable.offence_video_filled);
            }
        });

        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeVideoWarning();
            }
        });

        retake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaStorageDir = new File(Environment.getExternalStorageDirectory() + "/DCIM/", "traffic_sentinel");
                if (!mediaStorageDir.exists()) {
                    if (!mediaStorageDir.mkdirs()) {
                        Log.e("MyCameraApp", "failed to create directory");
                    }
                }
                startVideoRecorder();
                VideoDialog.dismiss();
            }
        });
    }

    public void removeVideoWarning() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ResolveActivity.this);
        alertDialog.setMessage((getResources().getString(R.string.warning_remove_video)));
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                //null the image uri and open as first time
                finalVideoUri = null;
                isVedioCapturedFirstTime = true;
                videoCamera.setImageResource(R.drawable.offence_video);
                VideoDialog.dismiss();
                dialog.dismiss();
            }
        });

        alertDialog.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertDialog.show();
    }

    private void ShowPhotoPreview() {
        photoDialog = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        photoDialog.setContentView(R.layout.dialog_photo);
        final ImageView _continue = (ImageView) photoDialog.findViewById(R.id._continue);
        final ImageView remove = (ImageView) photoDialog.findViewById(R.id.remove);
        final ImageView retake = (ImageView) photoDialog.findViewById(R.id.retake);
        imageViewContainer = (ImageView) photoDialog.findViewById(R.id.image_container);


        _continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                camera.setImageResource(R.drawable.offence_camera_filled);
                photoDialog.dismiss();
            }
        });

        retake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaStorageDir = new File(Environment.getExternalStorageDirectory() + "/DCIM/", "pollution");
                if (!mediaStorageDir.exists()) {
                    if (!mediaStorageDir.mkdirs()) {
                        Log.d("MyCameraApp", "failed to create directory");
                    }
                }
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

                File mediaFile = new File(mediaStorageDir.getPath() + File.separator + "pollution" + timeStamp + ".jpg");
                // mImageCaptureUri = Uri.fromFile(mediaFile);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    mImageCaptureUri = FileProvider.getUriForFile(ResolveActivity.this,
                            BuildConfig.APPLICATION_ID + ".provider",
                            mediaFile);
                } else {
                    mImageCaptureUri = Uri.fromFile(mediaFile);
                }
                intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
                startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
                photoDialog.dismiss();

                photoDialog.dismiss();
//                imageSelecetioDialog.show();
            }
        });

        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removePhotoWarning();
            }
        });
    }

    public void removePhotoWarning() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ResolveActivity.this);
        alertDialog.setMessage((getResources().getString(R.string.warning_remove_photo)));
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                //null the image uri and open as first time
                finalImageCaptureUri = null;
                isCameraOpenFirstTime = true;
                camera.setImageResource(R.drawable.offence_camera);
                photoDialog.dismiss();
                dialog.dismiss();
            }
        });

        alertDialog.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertDialog.show();
    }

    private void imageSelectionDialog() {
        imageSelecetioDialog = new Dialog(this, R.style.CustomBottomDialog);
        imageSelecetioDialog.setContentView(R.layout.dialog_image_selection);
        imageSelecetioDialog.setCanceledOnTouchOutside(true);
        Window window = imageSelecetioDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.BOTTOM;
        wlp.flags &= ~WindowManager.LayoutParams.ALPHA_CHANGED;
        window.setAttributes(wlp);
        imageSelecetioDialog.getWindow().setLayout(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.WRAP_CONTENT);

        Button cancel = (Button) imageSelecetioDialog.findViewById(R.id.cancel_btn);
        TextView openCamera = (TextView) imageSelecetioDialog.findViewById(R.id.open_camera);
        TextView chooseFromGallery = (TextView) imageSelecetioDialog.findViewById(R.id.gallery_select);

        openCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                activityInbackground = true;

                mediaStorageDir = new File(Environment.getExternalStorageDirectory() + "/DCIM/", "pollution");
                if (!mediaStorageDir.exists()) {
                    if (!mediaStorageDir.mkdirs()) {
                        Log.d("MyCameraApp", "failed to create directory");
                    }
                }
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

                File mediaFile = new File(mediaStorageDir.getPath() + File.separator + "pollution" + timeStamp + ".jpg");

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    mImageCaptureUri = FileProvider.getUriForFile(ResolveActivity.this,
                            BuildConfig.APPLICATION_ID + ".provider",
                            mediaFile);
                } else {
                    mImageCaptureUri = Uri.fromFile(mediaFile);
                }
                intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
                startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
                Log.e(TAG, "onClick: " + mImageCaptureUri.getPath());
                //dismiss the dialog
                imageSelecetioDialog.dismiss();
            }
        });

        chooseFromGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                activityInbackground = true;
                // Create intent to Open Image applications like Gallery, Google Photos
                Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                // Start the Intent
                startActivityForResult(galleryIntent, RESULT_LOAD_IMAGE);

                //dismiss the dialog
                imageSelecetioDialog.dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageSelecetioDialog.dismiss();
            }
        });


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e(TAG, "onActivityResult: RequestCOde: " + requestCode);
        Log.e(TAG, "onActivityResult: " + mImageCaptureUri);
        if (requestCode == REQUEST_IMAGE_CAPTURE) {
            if (resultCode == RESULT_OK) {
                try {
                    bitmap = decodeMyFile(new File(mImageCaptureUri.getPath()));
                    Log.e(TAG, "onActivityResult: " + bitmap.getWidth() + ":" + bitmap.getHeight());
                    isCameraOpenFirstTime = false;
                    camera.setImageResource(R.drawable.offence_camera_filled);
                    userInteractionStatus = true;
                    finalImageCaptureUri = mImageCaptureUri;
                } catch (Exception e) {
                    Log.e("onActivityResult: ", e.toString());
                    Toast.makeText(ResolveActivity.this, "Problem with camera...Please try again!", Toast.LENGTH_SHORT).show();
                }
            } else if (resultCode == RESULT_CANCELED) {
            }
        }

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            Log.e("MainActivity", "onActivityResult: ImagePath: " + picturePath);
            cursor.close();
            mImageCaptureUri = Uri.parse(picturePath);
            finalImageCaptureUri = mImageCaptureUri;
            Log.e(TAG, "onClick: " + mImageCaptureUri.getPath());
            imageViewContainer.setImageURI(mImageCaptureUri);
            String ori = getOrientation(mImageCaptureUri);
            if (ori.equalsIgnoreCase("portrait")) {
                imageViewContainer.setScaleType(ImageView.ScaleType.FIT_XY);
            } else {
                imageViewContainer.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            }

            isCameraOpenFirstTime = false;
            camera.setImageResource(R.drawable.offence_camera_filled);
            userInteractionStatus = true;
        }

        //for video
        if (requestCode == REQUEST_VIDEO_CAPTURE) {
            if (resultCode == RESULT_OK) {
                final File videomediaFile = new File(data.getData().getPath());
                Log.e(TAG, "onActivityResult: " + String.format("Saved to: %s, size: %s", videomediaFile.getAbsolutePath(), Util.fileSize(videomediaFile)));
                videoCamera.setImageResource(R.drawable.offence_video_filled);
                isVedioCapturedFirstTime = false;
                finalVideoUri = Uri.parse(videomediaFile.getPath());
                finalvideomediaFile = videomediaFile;
                userInteractionStatus = true;
            } else if (data != null) {
                Exception e = (Exception) data.getSerializableExtra(MaterialCamera.ERROR_EXTRA);
                if (e != null) {
                    e.printStackTrace();
                    Toast.makeText(this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    public boolean checkCameraPermission() {
        List<String> cameraPermissionList = new ArrayList<String>();
        if (ActivityCompat.checkSelfPermission(ResolveActivity
                .this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            cameraPermissionList.add(Manifest.permission.CAMERA);
        }

        if (ActivityCompat.checkSelfPermission(ResolveActivity
                .this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            cameraPermissionList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (!cameraPermissionList.isEmpty()) {
            ActivityCompat.requestPermissions(this, cameraPermissionList.toArray(new String[cameraPermissionList.size()]),
                    MY_PERMISSIONS_REQUEST_ACCESS_CAMERA);
            return false;
        }
        return true;
    }

    public boolean CheckVedioCameraPermission() {
        List<String> videoPermissionList = new ArrayList<String>();
        if (ActivityCompat.checkSelfPermission(ResolveActivity
                .this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            videoPermissionList.add(Manifest.permission.CAMERA);
        }
        if (ActivityCompat.checkSelfPermission(ResolveActivity
                .this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            videoPermissionList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (!videoPermissionList.isEmpty()) {
            ActivityCompat.requestPermissions(this, videoPermissionList.toArray(new String[videoPermissionList.size()]),
                    MY_PERMISSIONS_REQUEST_ACCESS_VIDEO);
            return false;
        }
        return true;


    }

    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        int a = 0;
        switch (requestCode) {

            case MY_PERMISSIONS_REQUEST_ACCESS_CAMERA: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e(TAG, "onRequestPermissionsResult: imageSelecetioDialog.show()");
                    captureImage();
//                    imageSelecetioDialog.show();
                } else {
                    Toast.makeText(ResolveActivity.this, R.string.camera_permission_is_required_to_click_a_picture,
                            Toast.LENGTH_SHORT).show();

                }
                break;
            }

            case MY_PERMISSIONS_REQUEST_ACCESS_VIDEO: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mediaStorageDir = new File(Environment.getExternalStorageDirectory() + "/DCIM/", "pollution");
                    startVideoRecorder();

                } else {
                    Toast.makeText(ResolveActivity.this, R.string.camera_premission_is_reuquired_to_record_a_video,
                            Toast.LENGTH_SHORT).show();
                }
                break;
            }

            case REQUEST_ID_MULTIPLE_PERMISSIONS: {

                Map<String, Integer> perms = new HashMap<>();
                // Initialize the map with both permissions
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.RECORD_AUDIO, PackageManager.PERMISSION_GRANTED);
                // Fill with actual results from user
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    // Check for both permissions
                    if (perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED) {
                        //Permission Granted
                        audioDialog.show();

                    } else {
                        Toast.makeText(ResolveActivity.this, R.string.All_permission_are_reuired_to_record_an_audio,
                                Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        startService(new Intent(this, GPSLocationService.class));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        goToBack();
    }
}
