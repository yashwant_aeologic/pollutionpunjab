package com.aeologic.cctv.Activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsMessage;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.aeologic.cctv.Async.GetOtpAsyncTask;
import com.aeologic.cctv.Connection.ConnectionDetector;
import com.aeologic.cctv.R;
import com.aeologic.cctv.Utility.Prefrence;
import com.aeologic.cctv.Utility.Util;
import com.alimuzaffar.lib.pin.PinEntryEditText;


/**
 * Created by u104 on 27/7/16.
 */
public class OtpActivity extends AppCompatActivity {
    //    EditText et1, et2, et3, et4, et5, et6;
    TextView textView, otpTitle, msg, resendMSG;
    int counter = 0;
    private ConnectionDetector detector;
    ImageView resendSMS;
    String number, countryCode, gcmToken;
    private TextView otpTimer;
    public String token, username, otp, emailId;
    public boolean isRegisterd = false;
    Button verifyButton;
    private BroadcastReceiver mReceiver;
    public final String SMS_EXTRA_NAME = "pdus";
    public String address = "";
    public String body = "";
    private String TAG = "OtpActivity";
    public boolean forgotPassStatus = false;

    PinEntryEditText otpET;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        initViews();
        detector = new ConnectionDetector(getApplicationContext());
        number = getIntent().getStringExtra("number");
        countryCode = getIntent().getStringExtra("COUNTRY_CODE");
        forgotPassStatus = getIntent().getBooleanExtra("forgotPassStatus", false);
        new GetOtpAsyncTask(OtpActivity.this, resendSMS, otpTimer, verifyButton, msg)
                .execute(getString(R.string.OTP_API), number);

        String textPart1 = getResources().getString(R.string.auto_detect_sms);
        String textPart2 = "<b>" + " " + countryCode + " " + number + "." + " " + "</b>";
        String textPart3 = "<font color='#00A1D8'>" + getResources().getString(R.string.wrong_number) + "</font>";
//        et1.requestFocus();
//        textView.setText(Html.fromHtml(textPart1 + textPart2 + textPart3));
        textView.setText(Util.fromHtml(textPart1 + textPart2 + textPart3));

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OtpActivity.this, UserLoginActivity.class);
                startActivity(intent);
            }
        });

        otpTitle.setText(getResources().getString(R.string.verify) + " " + countryCode + " " + number);

        if (otpET != null) {
            otpET.setAnimateText(true);
            otpET.setOnPinEnteredListener(new PinEntryEditText.OnPinEnteredListener() {
                @Override
                public void onPinEntered(CharSequence str) {
                    if (str.toString().equals(otp)) {
                        Prefrence.setMobileNumber(OtpActivity.this, number);

                        if (isRegisterd /*&& !forgotPassStatus*/) {
                            // set the is Register true in prefrence
                            Prefrence.setIsRegister(OtpActivity.this, true);
                            Intent intent1 = new Intent(OtpActivity.this, HomeActivity.class);
//                            Intent intent1 = new Intent(OtpActivity.this, PassCodeActivity.class);
                            startActivity(intent1);
                            finish();

                        } else {
                            Intent intent = new Intent(OtpActivity.this, RegistrationActivity.class);
                            intent.putExtra("PHONE_NUMBER", number);
                            startActivity(intent);
                            finish();
                        }
                    } else {
                        Util.showAlert(OtpActivity.this, getResources().getString(R.string.please_check_otp));
                    }
                }
            });
        }
        verifyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (otpET != null &&
                        !otpET.getText().toString().isEmpty()
                        && otpET.getText().toString().equals(otp)) {
                    //save the numner in prefrence
                    Prefrence.setMobileNumber(OtpActivity.this, number);

                    if (isRegisterd /*&& !forgotPassStatus*/) {
                        // set the is Register true in prefrence
                        Prefrence.setIsRegister(OtpActivity.this, true);
                        Intent intent1 = new Intent(OtpActivity.this, HomeActivity.class);
//                        Intent intent1 = new Intent(OtpActivity.this, PassCodeActivity.class);
                        startActivity(intent1);
                        finish();

                    }/* else
                    if (isRegisterd *//*&& forgotPassStatus*//*) {
                        // expire the token and passsword from prefrence
                        Prefrence.setToken(getApplicationContext(), "");
                        Prefrence.setPasscode(getApplicationContext(), "");

                        // goto the setpasscode Activity
                        Intent intent = new Intent(OtpActivity.this, HomeActivity.class);
//                        Intent intent = new Intent(OtpActivity.this, SetPassCodeActivity.class);
                        intent.putExtra("phoneNumber", Prefrence.getPhoneNumber(getApplicationContext()));
                        intent.putExtra("name", Prefrence.getUsername(getApplicationContext()));
                        intent.putExtra("email", Prefrence.getEmail(getApplicationContext()));
                        startActivity(intent);
                        finish();
                    } */else {
                        Intent intent = new Intent(OtpActivity.this, RegistrationActivity.class);
                        intent.putExtra("PHONE_NUMBER", number);
                        startActivity(intent);
                        finish();
                    }

                } else {
                    Util.showAlert(OtpActivity.this, getResources().getString(R.string.please_check_otp));
                }
            }
        });

        resendSMS.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                resendSMS.setImageResource(R.drawable.resend_pressed);
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    // image released
                    if (detector.isConnectingToInternet()) {
                        resendSMS.setImageResource(R.drawable.resend);
                        /*et1.setText("");
                        et2.setText("");
                        et3.setText("");
                        et4.setText("");
                        et5.setText("");
                        et6.setText("");*/

                        otpET.setText("");
                        new GetOtpAsyncTask(OtpActivity.this, resendSMS, otpTimer, verifyButton, msg)
                                .execute(getString(R.string.OTP_API), number);

                    } else {
                        resendSMS.setImageResource(R.drawable.resend);
                        Util.showAlert(OtpActivity.this, getResources().getString(R.string.please_check_internet));
//                        Toast.makeText(OtpActivity.this, getResources().getString(R.string.please_check_internet), Toast.LENGTH_SHORT).show();
                    }

                }
                return true;
            }
        });

    }

    @Override
    public void onBackPressed() {

       /* Intent intent = new Intent(OtpActivity.this, ManagerLoginActivity.class);
        intent.putExtra("num", number);
        intent.putExtra("con", countryCode);
        startActivity(intent);
        finish();*/
    }

    @Override
    protected void onStop() {
        super.onStop();
//        Util.finishActivity(OtpActivity.this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter(
                "android.provider.Telephony.SMS_RECEIVED");

        mReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                Bundle extras = intent.getExtras();
                if (extras != null) {
                    Object[] smsExtra = (Object[]) extras.get(SMS_EXTRA_NAME);
                    String format = extras.getString("format");
                    for (int i = 0; i < smsExtra.length; ++i) {
                        SmsMessage sms;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            sms = SmsMessage.createFromPdu((byte[]) smsExtra[i], format);
                        } else {
                            sms = SmsMessage.createFromPdu((byte[]) smsExtra[i]);
                        }
//                        SmsMessage sms = SmsMessage.createFromPdu((byte[]) smsExtra[i]);
                        address = sms.getOriginatingAddress();
                        body = sms.getMessageBody().toString();
                    }


                    if (address.contains("VAAHAN")) {
                        if (body != null && body.trim().length() > 0) {
//                            Log.e(TAG, "onReceive: Message: " + body);
                            String otp = body.replaceAll("\\D+", "");
//                            Log.e(TAG, "onReceive: otp: " + otp);
                            otpET.setText(otp);
                            /*et1.setText(Character.toString(otp.charAt(0)));
                            et2.setText(Character.toString(otp.charAt(1)));
                            et3.setText(Character.toString(otp.charAt(2)));
                            et4.setText(Character.toString(otp.charAt(3)));
                            et5.setText(Character.toString(otp.charAt(4)));
                            et6.setText(Character.toString(otp.charAt(5)));

                            // get focus on right side
                            et6.setSelection(et6.getText().length());*/


                        }
                    }

                }

                this.abortBroadcast();


            }
        };

        this.registerReceiver(mReceiver, intentFilter);
    }

    private void initViews() {
        /*et1 = (EditText) findViewById(R.id.et1);
        et2 = (EditText) findViewById(R.id.et2);
        et3 = (EditText) findViewById(R.id.et3);
        et4 = (EditText) findViewById(R.id.et4);
        et5 = (EditText) findViewById(R.id.et5);
        et6 = (EditText) findViewById(R.id.et6);*/

        otpET = (PinEntryEditText) findViewById(R.id.otp_et);
        textView = (TextView) findViewById(R.id.text);
        otpTitle = (TextView) findViewById(R.id.otp_title);
        msg = (TextView) findViewById(R.id.msg);
        resendMSG = (TextView) findViewById(R.id.resend_msg);
        resendSMS = (ImageView) findViewById(R.id.resend_sms);
        otpTimer = (TextView) findViewById(R.id.timer_resend);
        verifyButton = (Button) findViewById(R.id.verify_otp);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mReceiver);
    }
}
