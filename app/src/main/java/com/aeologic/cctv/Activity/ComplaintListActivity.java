package com.aeologic.cctv.Activity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.aeologic.cctv.Adapter.DoneListAdapter;
import com.aeologic.cctv.Adapter.PagerAdapter;
import com.aeologic.cctv.Async.GetZoneAsyncTask;
import com.aeologic.cctv.Connection.ConnectionDetector;
import com.aeologic.cctv.Database.Database;
import com.aeologic.cctv.Fragment.DoneFragment;
import com.aeologic.cctv.R;
import com.aeologic.cctv.Utility.AppController;
import com.aeologic.cctv.Utility.Prefrence;
import com.aeologic.cctv.Utility.RecyclerViewDisabler;
import com.aeologic.cctv.Utility.Util;
import com.aeologic.cctv.Utility.VolleyMultipartRequest;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;


import com.ogaclejapan.smarttablayout.SmartTabLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

import static com.aeologic.cctv.Utility.Util.parseDate;


public class ComplaintListActivity extends AppCompatActivity {
    private static final String TAG = ComplaintListActivity.class.getSimpleName();
    SmartTabLayout tabLayout;
    //    TabLayout tabLayout;
    public ViewPager viewPager;
    int tabStatus;
    public static boolean isComplaintListActive;
    Toolbar toolbar;
    private PagerAdapter pagerAdapter;
    private RecyclerView recyclerView;
    ImageView warningLogo;
    TextView warningText;
    Database database;
    int pageNumber = 0;
    public static ProgressBar progressBar;
    private boolean allDataLoadSuccessfully = false;
    private boolean swipeRefreshBarStatus = false;
    private boolean responseStatus;
    ArrayList<HashMap<String, String>> responseList = new ArrayList<>();
    private ConnectionDetector detector;
    private DoneListAdapter doneListAdapter;
    private SwipeRefreshLayout materialRefreshLayout;
    RecyclerViewDisabler recyclerViewDisabler;
    boolean isFirstTime = true;
    private VolleyMultipartRequest multipartRequest;
    private CardView filterContainerCard;
    private ArrayList<String> actionList;
    private ArrayList<String> offenceKeyList;
    private ArrayList<String> offenceValueList;
    private Spinner categorySpinner;
    private Spinner actionSpinner;
    private Spinner zoneSpinner;
    private CardView searchCard;
    private String filterOffenceName;
    private String filterOffenceID;
    private String actionSelected;
    private LinkedHashMap<String, String> offenceMap;
    private EditText from, to;
    ImageView fromCal, toCal;
    Calendar fromCalendar, toCalendar;
    private boolean filterStatus;
    String fromText, toText;
    String offenceId, callFrom;
    RadioButton gda, nn;
    Boolean zoneStatus = false;
    String zoneSelected, zoneType, toAnalytics, fromAnalytics;
    EditText offenceNumberET;
    CardView searchCardSingle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complaint_list);
        initViews();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Report List");
        if (getIntent().getStringExtra("From") != null) {
            callFrom = getIntent().getStringExtra("From");
        }
        if (getIntent().getStringExtra("id") != null) {
            offenceId = getIntent().getStringExtra("id");
        }
        if (getIntent().hasExtra("TO")) {
            if (getIntent().getStringExtra("TO") != null) {
                toAnalytics = getIntent().getStringExtra("TO");
            }
        }
        if (getIntent().hasExtra("FROMVAL")) {
            if (getIntent().getStringExtra("FROMVAL") != null) {
                fromAnalytics = getIntent().getStringExtra("FROMVAL");
            }
        }
        recyclerView.setLayoutManager(new LinearLayoutManager(ComplaintListActivity.this));
        detector = new ConnectionDetector(ComplaintListActivity.this);
        recyclerViewDisabler = new RecyclerViewDisabler();
        fromCalendar = Calendar.getInstance();
        toCalendar = Calendar.getInstance();
        from.setText(getCalculatedDate("dd-MM-yyyy", -7));
        to.setText(getCalculatedDate("dd-MM-yyyy", 0));
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                fromCalendar.set(Calendar.YEAR, year);
                fromCalendar.set(Calendar.MONTH, monthOfYear);
                fromCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();


            }

        };

        final DatePickerDialog.OnDateSetListener date1 = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                toCalendar.set(Calendar.YEAR, year);
                toCalendar.set(Calendar.MONTH, monthOfYear);
                toCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel1();


            }

        };
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ComplaintListActivity.this, HomeActivity.class));
                finish();
            }
        });

        //////////////////////// SEARCH FILTER ///////////////////

        database = new Database(ComplaintListActivity.this);
        offenceMap = database.getOffencesMap();
        if (offenceMap == null) offenceMap = new LinkedHashMap<>();
        offenceKeyList = new ArrayList<>();
        offenceValueList = new ArrayList<>();
        actionList = new ArrayList<>();
        offenceKeyList.addAll(offenceMap.keySet());
        offenceKeyList.add(0, "0");
        offenceValueList.addAll(offenceMap.values());
        offenceValueList.add(0, getString(R.string.select_category));
        actionList.add(0, "Select Action");
        actionList.add(0, "Select Action");
        actionList.add(1, "Deleted");
        actionList.add(2, "Pending");
        actionList.add(3, "Rejected");
        actionList.add(4, "Resolved");
        actionList.add(5, "Unrelated");


        final ArrayAdapter<String> sectionAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, offenceValueList);
        sectionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        categorySpinner.setAdapter(sectionAdapter);
        categorySpinner.setPrompt(getString(R.string.select_category));

        ///////// Action Spinner //////////

        final ArrayAdapter<String> actionAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, actionList);
        sectionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        categorySpinner.setAdapter(sectionAdapter);
        categorySpinner.setPrompt(getString(R.string.select_action));

        //////////////////////////////////////////


        if (callFrom.equalsIgnoreCase("ANALYTICS")) {
            filterContainerCard.setVisibility(View.VISIBLE);
            from.setText(fromAnalytics);
            to.setText(toAnalytics);
            filterStatus = true;
            if (offenceId != null) {
                categorySpinner.setSelection(offenceKeyList.indexOf(offenceId));
            }

        } else {
            filterContainerCard.setVisibility(View.GONE);
        }
        categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    filterOffenceName = parent.getSelectedItem().toString();
                    filterOffenceID = offenceKeyList.get(position);
                } else {
                    filterOffenceName = null;
                    filterOffenceID = null;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        actionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    actionSelected = parent.getSelectedItem().toString();
                    Log.e("onItemSelected: ", actionSelected);

                } else {
                    actionSelected = null;


                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        zoneSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    zoneSelected = String.valueOf(position);
                    Log.e("onItemSelected: ", zoneSelected);

                } else {
                    zoneSelected = null;


                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        searchCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadData();
            }
        });

        searchCardSingle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadData();
            }
        });

        fromCal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog =
                        new DatePickerDialog(ComplaintListActivity.this, date, fromCalendar
                                .get(Calendar.YEAR), fromCalendar.get(Calendar.MONTH),
                                fromCalendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });

        toCal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog =
                        new DatePickerDialog(ComplaintListActivity.this, date1, toCalendar
                                .get(Calendar.YEAR), toCalendar.get(Calendar.MONTH),
                                toCalendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });

        from.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog =
                        new DatePickerDialog(ComplaintListActivity.this, date, fromCalendar
                                .get(Calendar.YEAR), fromCalendar.get(Calendar.MONTH),
                                fromCalendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });

        to.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog =
                        new DatePickerDialog(ComplaintListActivity.this, date1, toCalendar
                                .get(Calendar.YEAR), toCalendar.get(Calendar.MONTH),
                                toCalendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });

        //////////////////////// SEARCH FILTER ///////////////////

        //Adding adapter to pager


        pagerAdapter = new PagerAdapter(getSupportFragmentManager(), ComplaintListActivity.this);
        viewPager.setAdapter(pagerAdapter);
        tabLayout.setViewPager(viewPager);
//        tabLayout.setupWithViewPager(viewPager);

        if (getIntent() != null) {
            tabStatus = getIntent().getIntExtra("tabStatus", 0);
            viewPager.setCurrentItem(tabStatus);
        }

        Log.e(TAG, "onCreate: register reciever for connectionchange");
        IntentFilter intentFilter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(connectionChangeReciever, intentFilter);

        //////////////////////////////////


        //    recyclerView.setHasFixedSize(true);

        ////////////////////  ZONE Filter /////////////////////////////
        gda.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    zoneType = "GDA";
                    zoneStatus = true;
                    GetZoneAsyncTask getZoneAsyncTask = new GetZoneAsyncTask(ComplaintListActivity.this, zoneSpinner);
                    getZoneAsyncTask.execute(getString(R.string.API_BASE_URL) + getString(R.string.ZONE), "GDA");
                }
            }
        });
        nn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    zoneType = "NN";
                    zoneStatus = true;
                    GetZoneAsyncTask getZoneAsyncTask = new GetZoneAsyncTask(ComplaintListActivity.this, zoneSpinner);
                    getZoneAsyncTask.execute(getString(R.string.API_BASE_URL) + getString(R.string.ZONE), "NN");
                }
            }
        });
        if (doneListAdapter != null) {
            recyclerView.setAdapter(doneListAdapter);
        } else {
            doneListAdapter = new DoneListAdapter(ComplaintListActivity.this, responseList, recyclerView);
            recyclerView.setAdapter(doneListAdapter);
        }


        materialRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadData();
            }


        });

        if (detector.isConnectingToInternet()) {
            // call the API here
//            new DoneListAsync(getActivity(), recyclerView, warningLogo, warningText,pageNumber).execute(getString(R.string.COMPLAINT_LIST_API));
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    //Do something after 100ms
                    getUploadData(pageNumber);
                }
            }, 200);

            doneListAdapter.setOnLoadMoreListener(new DoneListAdapter.OnLoadMoreListener()

            {
                @Override
                public void onLoadMore() {
                    //add progress item
                    //responseList.add(null);
                    // doneListAdapter.notifyItemInserted(responseList.size() - 1);

                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //remove progress item
                            if (responseStatus && !allDataLoadSuccessfully) {
                                // responseList.remove(responseList.size() - 1);
                                // doneListAdapter.notifyItemRemoved(responseList.size());
                                pageNumber = pageNumber + 1;
                                getUploadData(pageNumber);
                            }
                            //     new DoneListAsync(context, recyclerView, warningLogo, warningText,pageNumber).execute(context.getString(R.string.COMPLAINT_LIST_API));

                            //or you can add all at once but do not forget to call mAdapter.notifyDataSetChanged();
                        }
                    }, 600);
                    System.out.println("load");
                }
            });

        } else {
            if (!isFirstTime) {
                Util.showAlert(ComplaintListActivity.this, getString(R.string.please_check_internet));
            }
            recyclerView.setVisibility(View.GONE);
            warningLogo.setImageResource(R.drawable.wifi);
            warningText.setText(getString(R.string.no_internet_connections));
            warningLogo.setVisibility(View.VISIBLE);
            warningText.setVisibility(View.VISIBLE);
        }
    }

    private void initViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tabLayout = (SmartTabLayout) findViewById(R.id.tab_layout);
        viewPager = (ViewPager) findViewById(R.id.pager);
        warningLogo = (ImageView) findViewById(R.id.warning_logo1);
        warningText = (TextView) findViewById(R.id.warning_text1);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view3);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        materialRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh);
        filterContainerCard = (CardView) findViewById(R.id.filter_container_card);
        filterContainerCard.setVisibility(View.GONE);
        categorySpinner = (Spinner) findViewById(R.id.category_spinner);
        actionSpinner = (Spinner) findViewById(R.id.action_spinner);
        zoneSpinner = (Spinner) findViewById(R.id.zone_spinner);
        searchCard = (CardView) findViewById(R.id.search_card);
        from = (EditText) findViewById(R.id.from_et);
        to = (EditText) findViewById(R.id.to_et);
        fromCal = (ImageView) findViewById(R.id.from_cal);
        toCal = (ImageView) findViewById(R.id.to_cal);
        gda = (RadioButton) findViewById(R.id.gda);
        nn = (RadioButton) findViewById(R.id.nn);
        offenceNumberET = (EditText) findViewById(R.id.rc_no_et);
        searchCardSingle = (CardView) findViewById(R.id.search_card1);
    }

    public static String getCalculatedDate(String dateFormat, int days) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat s = new SimpleDateFormat(dateFormat);
        cal.add(Calendar.DAY_OF_YEAR, days);
        return s.format(new Date(cal.getTimeInMillis()));
    }

    private void updateLabel() {
        String myFormat = "dd-MM-yyyy";
        String myFormat1 = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        SimpleDateFormat sdf1 = new SimpleDateFormat(myFormat1, Locale.US);
        from.setText(sdf.format(fromCalendar.getTime()));
        fromText = sdf1.format(fromCalendar.getTime());
    }

    private void updateLabel1() {
        String myFormat = "dd-MM-yyyy";
        String myFormat1 = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        SimpleDateFormat sdf1 = new SimpleDateFormat(myFormat1, Locale.US);
        to.setText(sdf.format(toCalendar.getTime()));
        toText = sdf1.format(toCalendar.getTime());
    }


    public void loadData() {
        if (ComplaintListActivity.this != null) {
            if (new ConnectionDetector(ComplaintListActivity.this).isConnectingToInternet()) {
                recyclerView.addOnItemTouchListener(recyclerViewDisabler);
                if (multipartRequest != null) {
                    multipartRequest.cancel();
                }
                swipeRefreshBarStatus = true;
                materialRefreshLayout.setEnabled(true);
                responseList.clear();
                pageNumber = 0;
                getUploadData(pageNumber);
                allDataLoadSuccessfully = false;
                recyclerView.getRecycledViewPool().clear();
                if (doneListAdapter != null) {
                    doneListAdapter.notifyDataSetChanged();
                }

            } else {
                materialRefreshLayout.setRefreshing(false);
                warningLogo.setImageResource(R.drawable.wifi);
                warningText.setText(getString(R.string.no_internet_connections));
                Util.showAlert(ComplaintListActivity.this, getString(R.string.please_check_internet));
            }
        }
    }

    private void getUploadData(final int pageNumber) {
        responseStatus = false;
        final ProgressDialog progressDialog = new ProgressDialog(ComplaintListActivity.this);
        progressDialog.setMessage(getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
        if (isFirstTime && !ComplaintListActivity.this.isFinishing()) {

            isFirstTime = false;
        }


        multipartRequest = new VolleyMultipartRequest(Request.Method.POST,
                getString(R.string.COMPLAINT_LIST_API), new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                String resultResponse = new String(response.data);
                Log.e(TAG, "onResponse: Response: " + resultResponse);
                progressDialog.hide();
                try {

                    if (recyclerViewDisabler != null) {
                        recyclerView.removeOnItemTouchListener(recyclerViewDisabler);
                        swipeRefreshBarStatus = false;
                        materialRefreshLayout.setRefreshing(false);
                    }
                    HashMap<String, String> responseData;
                    JSONObject object = new JSONObject(resultResponse);
                    String status = object.getString("status");
                    if (status.equals("1")) {
                        responseStatus = true;
                        JSONArray resultArray = object.getJSONArray("data");
                        if (resultArray != null && resultArray.length() > 0) {
                            for (int i = 0; i < resultArray.length(); i++) {
                                responseData = new HashMap<>();
                                JSONObject obj = resultArray.getJSONObject(i);
                                responseData.put("id", obj.getString("id"));
                                responseData.put("createdAt", obj.getString("survey_date"));
                                //  responseData.put("rcNumber", obj.getString("rc_number"));
                                responseData.put("offence", obj.getString("offences"));
                                responseData.put("action", obj.getString("action"));
                                responseData.put("address", obj.getString("address"));
                                responseData.put("latitude", obj.getString("latitude"));
                                responseData.put("longitude", obj.getString("longitude"));
                                responseData.put("reported_by", obj.getString("name") + "(" + obj.getString("email") + ")");
                                responseList.add(responseData);
                            }

                            doneListAdapter.notifyItemInserted(responseList.size());
                            doneListAdapter.setLoaded();

                            progressBar.setVisibility(View.INVISIBLE);
                            recyclerView.setVisibility(View.VISIBLE);
                            warningLogo.setVisibility(View.GONE);
                            warningText.setVisibility(View.GONE);
                        } else {
                            allDataLoadSuccessfully = true;
                            progressBar.setVisibility(View.GONE);
                            if (pageNumber == 0) {
                                recyclerView.setVisibility(View.GONE);
                                warningLogo.setImageResource(R.drawable.round_error_symbol);
                                warningText.setText(getString(R.string.no_complaint_found));
                                warningLogo.setVisibility(View.VISIBLE);
                                warningText.setVisibility(View.VISIBLE);
                            }
                        }
                    } else if (status.equals("0")) {
                        String message = object.getString("message");
                        if (message != null) {
                            if ((responseList.size() == 0) && message.contains("Your session has expired"))
                                Util.showSessionExpiredDialog(ComplaintListActivity.this);
                        } else {
                            Util.showAlert(ComplaintListActivity.this, getString(R.string.pta));
                        }
//                        Util.showAlert(getActivity(), object.getString("message"));
                        progressBar.setVisibility(View.INVISIBLE);
                        progressDialog.hide();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e(TAG, "onErrorResponse: " + error.getMessage());
                progressBar.setVisibility(View.INVISIBLE);
                Util.showAlert(ComplaintListActivity.this, getString(R.string.pta));
                progressDialog.hide();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("token", Prefrence.getToken(ComplaintListActivity.this));
                params.put("pg", String.valueOf(pageNumber));
                if (filterStatus && filterOffenceID != null) {
                    params.put("offence", filterOffenceID);
                }
                if (from.getText() != null && from.getText().toString().length() > 4 && to.getText() != null && to.getText().toString().length() > 4) {


                    params.put("start_date", parseDate(from.getText().toString(), "dd-MM-yyyy", "yyyy-MM-dd"));
                    params.put("end_date", parseDate(to.getText().toString(), "dd-MM-yyyy", "yyyy-MM-dd"));

                } else {

                    params.put("start_date", getCalculatedDate("yyyy-MM-dd", -7));
                    params.put("end_date", getCalculatedDate("yyyy-MM-dd", -0));
                }

                if (actionSelected != null && actionSelected.length() > 3) {
                    params.put("action", actionSelected);
                }
                if (zoneStatus && zoneSelected != null) {
                    params.put("zone_id", zoneSelected);
                    params.put("zone_type", zoneType);
                }
                if (offenceNumberET.getText() != null && offenceNumberET.getText().toString().length() > 0) {
                    params.put("report_no", offenceNumberET.getText().toString());
                }
                return params;

            }

        };
        multipartRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        AppController.getInstance().addToRequestQueue(multipartRequest);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(ComplaintListActivity.this, HomeActivity.class));
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        isComplaintListActive = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        isComplaintListActive = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(connectionChangeReciever);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_filter) {
            if (filterContainerCard.getVisibility() == View.VISIBLE) {
                filterContainerCard.setVisibility(View.GONE);
                filterStatus = false;
                categorySpinner.setSelection(0);
                filterOffenceID = null;
                filterOffenceName = null;
            } else {
                filterContainerCard.setVisibility(View.VISIBLE);
                filterStatus = true;
            }
        }
        return true;
    }


    public BroadcastReceiver connectionChangeReciever = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetInfo = connectivityManager.getActiveNetworkInfo();
            Log.e(TAG, "onReceive: ");
            //capture connection_info in sendcomplaint from xml in a static variable and show and hide the view from here.
            if (activeNetInfo != null && activeNetInfo.isConnected()) {
                Log.d(TAG, "onReceive: net is connected");
                //start the service to sync the offence
                ((DoneFragment) pagerAdapter.getItem(1)).loadData();
            }
        }
    };
}




