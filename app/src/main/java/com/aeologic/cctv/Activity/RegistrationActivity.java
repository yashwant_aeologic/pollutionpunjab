package com.aeologic.cctv.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.aeologic.cctv.Database.Database;
import com.aeologic.cctv.R;
import com.aeologic.cctv.Utility.AppController;
import com.aeologic.cctv.Utility.CommonUtilities;
import com.aeologic.cctv.Utility.Prefrence;
import com.aeologic.cctv.Utility.Util;
import com.aeologic.cctv.Utility.VolleyMultipartRequest;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by u104 on 4/8/16.
 */
public class RegistrationActivity extends AppCompatActivity {
    EditText name, email;
    AppCompatButton continueButton;
    String phoneNumber;
    private EditText referenceNumber;
    private TextView statusTxt;
    Spinner citySpinner;
    ArrayList<HashMap<String, String>> cityList;
    List<String> city = new ArrayList<>();
    String cityValue;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        initViews();


        if (getIntent() != null) {
            phoneNumber = getIntent().getStringExtra("PHONE_NUMBER");
        }
        referenceNumber.setText(phoneNumber);
        Database database = new Database(RegistrationActivity.this);
        cityList = database.getActiveStateList();
        if (cityList != null && cityList.size() > 0) {
            city.clear();
            city.add("Select District");
            for (int i = 0; i < cityList.size(); i++) {
                city.add(cityList.get(i).get("city"));
            }
        }


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(RegistrationActivity.this,
                R.layout.spinner_item, city);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        citySpinner.setAdapter(adapter);
        citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                //Change the selected item's text color
              if(position!=0){
                  cityValue=city.get(position);
              }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {
            }
        });
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (name.getText() != null &&
                        !name.getText().toString().isEmpty()
                        && isValidName(name.getText().toString().trim())) {
                    if (email.getText() != null
                            && !email.getText().toString().isEmpty()
                            && isValidEmail(email.getText().toString().trim())) {
                        if(cityValue!=null){
                            registerApi(RegistrationActivity.this,
                                    phoneNumber, name.getText().toString(), email.getText().toString(), null, cityValue);
                        }else{
                            CommonUtilities.showDialog(RegistrationActivity.this
                                    , "Please select city");
                        }





                    } else {
                        CommonUtilities.showDialog(RegistrationActivity.this
                                , getResources().getString(R.string.blank_email));

                    }
                } else {
                    CommonUtilities.showDialog(RegistrationActivity.this
                            , getResources().getString(R.string.blank_name));
                }

            }
        });


        referenceNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                referenceNumber.requestFocus();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.e("onTextChanged: ", String.valueOf(start));
                if (start == 9 && before == 0) {

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(referenceNumber.getWindowToken(), 0);




                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    @Override
    protected void onStop() {
        super.onStop();
//        Util.finishActivity(RegistrationActivity.this);
    }

    private boolean isValidEmail(String s) {
        boolean check;
        Pattern p;
        Matcher m;

        String EMAIL_STRING = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        p = Pattern.compile(EMAIL_STRING);

        m = p.matcher(s);
        check = m.matches();
        return check;
    }


    private boolean isValidName(String s) {

        if (s.length() < 3) {
            return false;
        }
        Pattern ps = Pattern.compile("^[a-zA-Z ]+$");
        Matcher ms = ps.matcher(s);
        boolean bs = ms.matches();
        return bs != false;

    }

    private void initViews() {
        name = (EditText) findViewById(R.id.name);
        email = (EditText) findViewById(R.id.phone_number);
        referenceNumber = (EditText) findViewById(R.id.reference_number);
        statusTxt = (TextView) findViewById(R.id.status_txt);
        continueButton = (AppCompatButton) findViewById(R.id._continue);
        citySpinner = (Spinner) findViewById(R.id.city_spinner);


    }

    public void changeStatus(String msg, int status) {
        statusTxt.setVisibility(View.VISIBLE);
        if (status == 1) {
            statusTxt.setTextColor(ContextCompat.getColor(RegistrationActivity.this, android.R.color.holo_green_light));
            statusTxt.setText(msg);
        } else {
            statusTxt.setTextColor(ContextCompat.getColor(RegistrationActivity.this, android.R.color.holo_red_light));
            statusTxt.setText(msg);
        }

    }


    public void registerApi(final Context context, final String number,
                            final String name, final String email,
                            final String passcode, final String refered_by) {

        final ProgressDialog pDialog = new ProgressDialog(context);
        pDialog.setMessage(context.getString(R.string.please_wait));
        pDialog.setCancelable(false);
        if (!isFinishing())
            pDialog.show();


        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST,
                context.getString(R.string.REGISTER_API), new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                String resultResponse = new String(response.data);
                Log.e("RegisterApi", "onResponse: Response: " + resultResponse);
                try {
                    JSONObject jsonObject = new JSONObject(resultResponse);
                    String status = jsonObject.getString("status");
                    if (status.equals("1")) {
                        JSONObject obj = jsonObject.getJSONObject("data");
                        String token = obj.getString("token");
                        Prefrence.setToken(context, token);
                        Prefrence.setIsRegister(context, true);
                        Prefrence.setUsername(context, name);
                        Prefrence.setEmail(context, email);
                        Prefrence.setMobileNumber(context, phoneNumber);
                        Prefrence.setCity(context, cityValue);
                        pDialog.hide();
                         Intent intent = new Intent(RegistrationActivity.this, HomeActivity.class);
                        intent.putExtra("phoneNumber", phoneNumber);
                        intent.putExtra("name", name);
                        intent.putExtra("email", email);
                        intent.putExtra("refered_by", referenceNumber.getText().toString());
                        startActivity(intent);
                        finish();

                        //call the another API
                       // PassCodeActivity.saveOffenceListToDb(RegistrationActivity.this, pDialog);
                    } else {
                        //show alert dialog
                        Util.showAlert(context, context.getString(R.string.pta));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    pDialog.hide();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                pDialog.hide();
                Util.showAlert(context, error.getLocalizedMessage());
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("phone_number", number);
                params.put("username", name);
                params.put("email_id", email);

                params.put("city", cityValue);
                Log.e("ParamsRegister", "getParams: " + params.toString());
                return params;
            }

        };
        multipartRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        AppController.getInstance().addToRequestQueue(multipartRequest);

    }

}
