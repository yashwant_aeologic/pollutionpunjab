package com.aeologic.cctv.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aeologic.cctv.Adapter.FeedbackListAdapter;
import com.aeologic.cctv.Connection.ConnectionDetector;
import com.aeologic.cctv.Database.Database;
import com.aeologic.cctv.OfficersActivities.ManagerHomeActivity;
import com.aeologic.cctv.R;
import com.aeologic.cctv.Utility.AppController;
import com.aeologic.cctv.Utility.Prefrence;
import com.aeologic.cctv.Utility.RecyclerViewDisabler;
import com.aeologic.cctv.Utility.Util;
import com.aeologic.cctv.Utility.VolleyMultipartRequest;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by Sachin varshney on 29/7/16.
 */
public class FeedbackListActivity extends AppCompatActivity {
    Toolbar toolbar;

    private String TAG = "FeedbackList";
    public static FeedbackListActivity feedbackListActivity;
    private RecyclerView recyclerView;
    ImageView warningLogo;
    TextView warningText;
    Database database;
    LinearLayout totalFeedbackCard;
    int pageNumber = 0;
    public static ProgressBar progressBar;
    private boolean allDataLoadSuccessfully = false;
    private boolean swipeRefreshBarStatus = false;
    private boolean responseStatus;
    ArrayList<HashMap<String, String>> responseList = new ArrayList<>();

    private ConnectionDetector detector;
    private FeedbackListAdapter doneListAdapter;
    private SwipeRefreshLayout materialRefreshLayout;
    RecyclerViewDisabler recyclerViewDisabler;
    boolean isFirstTime = true;
    RelativeLayout relativeLayoutNoItemFound;
    private TextView totalComplaint;
    private LinearLayout totalComplaintCard;
    private VolleyMultipartRequest multipartRequest;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            startActivity(new Intent(FeedbackListActivity.this, HomeActivity.class));
            overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback_list);
        feedbackListActivity = FeedbackListActivity.this;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("All Feedbacks");
        database = new Database(FeedbackListActivity.this);
        warningLogo = (ImageView) findViewById(R.id.warning_logo1);
        warningText = (TextView) findViewById(R.id.warning_text1);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view2);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        relativeLayoutNoItemFound = (RelativeLayout) findViewById(R.id.no_item_found_layout);
        totalComplaint = (TextView)findViewById(R.id.total_complaint);
        totalComplaintCard = (LinearLayout) findViewById(R.id.feedback_total_count);
        materialRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh);
        //    recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        detector = new ConnectionDetector(FeedbackListActivity.this);
        recyclerViewDisabler = new RecyclerViewDisabler();
        totalFeedbackCard = (LinearLayout) findViewById(R.id.feedback_total_count);
        if (doneListAdapter != null) {
            recyclerView.setAdapter(doneListAdapter);
        } else {
            doneListAdapter = new FeedbackListAdapter(this, responseList, recyclerView);
            recyclerView.setAdapter(doneListAdapter);
        }


        materialRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (detector.isConnectingToInternet()) {
                    recyclerView.addOnItemTouchListener(recyclerViewDisabler);
                    if (multipartRequest!=null) {
                        multipartRequest.cancel();
                    }
                    swipeRefreshBarStatus = true;
                    materialRefreshLayout.setEnabled(true);
                    responseList.clear();
                    pageNumber = 0;
                    getUploaddata(pageNumber);
                    allDataLoadSuccessfully = false;
                    recyclerView.getRecycledViewPool().clear();
                    doneListAdapter.notifyDataSetChanged();
                } else {
                    materialRefreshLayout.setRefreshing(false);
                    Util.showAlert(FeedbackListActivity.this, getString(R.string.please_check_internet));
                }
            }


        });

        if (detector.isConnectingToInternet()) {
            // call the API here
            getUploaddata(pageNumber);
            final Handler handler = new Handler();

            doneListAdapter.setOnLoadMoreListener(new FeedbackListAdapter.OnLoadMoreListener() {
                @Override
                public void onLoadMore() {
                    //add progress item
                    //responseList.add(null);
                    // doneListAdapter.notifyItemInserted(responseList.size() - 1);

                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //remove progress item
                            if (responseStatus && !allDataLoadSuccessfully) {
                                pageNumber = pageNumber + 1;
                                getUploaddata(pageNumber);
                            }
                            //     new DoneListAsync(context, recyclerView, warningLogo, warningText,pageNumber).execute(context.getString(R.string.COMPLAINT_LIST_API));

                            //or you can add all at once but do not forget to call mAdapter.notifyDataSetChanged();
                        }
                    }, 600);
                    System.out.println("load");
                }
            });

        } else {
            /*if (!isFirstTime) {
                Util.showAlert(this, getString(R.string.please_check_internet));
            }*/
//            Util.showAlert(this, getString(R.string.please_check_internet));
            recyclerView.setVisibility(View.GONE);
            warningLogo.setImageResource(R.drawable.wifi);
            warningText.setText(getString(R.string.no_internet_connections));
            warningLogo.setVisibility(View.VISIBLE);
            warningText.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(FeedbackListActivity.this, ManagerHomeActivity.class));
        overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);
        finish();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void getUploaddata(final int pageNumber) {
        responseStatus = false;
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.please_wait));
        progressDialog.setCancelable(false);

        if (isFirstTime) {
            if (!isFinishing()){
                progressDialog.show();
            }
            isFirstTime = false;
        } else {
            if (swipeRefreshBarStatus) {
                progressBar.setVisibility(View.INVISIBLE);
            } else {
                progressBar.setVisibility(View.VISIBLE);
            }
        }


        multipartRequest = new VolleyMultipartRequest(Request.Method.POST,
                getString(R.string.FEEDBACK_LIST_API), new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                String resultResponse = new String(response.data);
                Log.e(TAG, "onResponse: Response: " + resultResponse);
                progressDialog.hide();
                try {

                    if (recyclerViewDisabler != null) {
                        recyclerView.removeOnItemTouchListener(recyclerViewDisabler);
                        swipeRefreshBarStatus = false;
                        materialRefreshLayout.setRefreshing(false);
                    }
                    HashMap<String, String> responseData;
                    JSONObject object = new JSONObject(resultResponse);

                    //{"status":0,"message":"No Feedback Found."}
                    String status = object.getString("status");
                    if (status.equals("1")) {
                        responseStatus = true;
                        JSONArray resultArray = object.getJSONArray("data");
                        if (resultArray != null && resultArray.length() > 0) {
                            for (int i = 0; i < resultArray.length(); i++) {
                                responseData = new HashMap<>();
                                JSONObject obj = resultArray.getJSONObject(i);
                                responseData.put("id", obj.getString("id"));
                                responseData.put("created_at", obj.getString("created_at"));
                                responseData.put("title", obj.getString("title"));
                                responseData.put("feedback", obj.getString("feedback"));
                                responseData.put("reply", obj.getString("reply"));
                                responseData.put("update_at", obj.getString("updated_at"));
                                responseList.add(responseData);
                            }
                            try {
                                int complaintCount = Integer.parseInt(object.getString("total"));
                                if (complaintCount>0){
                                    totalComplaint.setText(""+object.getString("total"));
                                    totalFeedbackCard.setVisibility(View.VISIBLE);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            doneListAdapter.notifyItemInserted(responseList.size());
                            doneListAdapter.setLoaded();

                            progressBar.setVisibility(View.INVISIBLE);
                            recyclerView.setVisibility(View.VISIBLE);
                            warningLogo.setVisibility(View.GONE);
                            warningText.setVisibility(View.GONE);
                            relativeLayoutNoItemFound.setVisibility(View.GONE);
                        } else {
                            allDataLoadSuccessfully = true;
                            progressBar.setVisibility(View.INVISIBLE);
                            if (pageNumber == 0) {
                                recyclerView.setVisibility(View.GONE);
                                warningLogo.setImageResource(R.drawable.round_error_symbol);
                                warningText.setText(getString(R.string.no_feedback_found));
                                warningLogo.setVisibility(View.VISIBLE);
                                warningText.setVisibility(View.VISIBLE);
                                relativeLayoutNoItemFound.setVisibility(View.VISIBLE);
                            }
                        }
                    } else if (status.equals("0")) {
                        if (pageNumber == 0) {
                            recyclerView.setVisibility(View.GONE);
                            warningLogo.setVisibility(View.VISIBLE);
                            warningText.setVisibility(View.VISIBLE);
                            relativeLayoutNoItemFound.setVisibility(View.VISIBLE);
                            totalFeedbackCard.setVisibility(View.GONE);
                        }
                        String message = object.getString("message");
                        if (message!=null){
                            if ((responseList.size()==0)&&message.contains("Your session has expired"))
                                Util.showSessionExpiredDialog(FeedbackListActivity.this);
                        }else{
                            Util.showAlert(FeedbackListActivity.this, getString(R.string.pta));
                        }
                        progressBar.setVisibility(View.INVISIBLE);
                        progressDialog.hide();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressBar.setVisibility(View.INVISIBLE);
                Util.showAlert(FeedbackListActivity.this, getString(R.string.pta));
                progressDialog.hide();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("token", Prefrence.getToken(FeedbackListActivity.this));
                params.put("pg", String.valueOf(pageNumber));
                Log.e(TAG, "getParams: Token: " + Prefrence.getToken(FeedbackListActivity.this));
                return params;
            }
        };
        multipartRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        AppController.getInstance().addToRequestQueue(multipartRequest);
    }

    public void onClickAddFeedback(View view) {
        startActivity(new Intent(this, FeedbackActivity.class));
        overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
        //finish();
    }
}




