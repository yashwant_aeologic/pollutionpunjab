package com.aeologic.cctv.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.aeologic.cctv.Adapter.DrawerItemAdapter;
import com.aeologic.cctv.Async.DownloadImagesAsync;
import com.aeologic.cctv.Async.LogoutAsyncTask;
import com.aeologic.cctv.Connection.ConnectionDetector;
import com.aeologic.cctv.Database.Database;
import com.aeologic.cctv.R;
import com.aeologic.cctv.Receiver.ConnectionChangeReceiver;
import com.aeologic.cctv.Service.UploadOfflineSurvey;
import com.aeologic.cctv.Utility.AppController;
import com.aeologic.cctv.Utility.Prefrence;
import com.aeologic.cctv.Utility.Util;
import com.aeologic.cctv.Utility.VolleyMultipartRequest;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;


import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by u104 on 28/7/16.
 */
public class HomeActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    CardView reportOffense, reportedOffense, offenceAnalytics;
    Toolbar toolbar;
    private ConnectionDetector detector;
    boolean doubleBackToExitPressedOnce = false;
    private final int MY_PERMISSIONS_REQUEST_ACCESS_LOCATION = 5;
    private static final int REQUEST_CHECK_SETTINGS = 100;
    private GoogleApiClient googleApiClient;
    private String TAG = "HomeActivity";
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    android.support.v7.app.ActionBarDrawerToggle mDrawerToggle;
    TextView username;
    TextView phoneNumber;
    ProgressBar progressBar;
    ConnectionChangeReceiver connectionChangeReceiver;
    String phoneNumberStr, name, email, refered_by;
    ProgressDialog progressDialog;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_citizen_offence);
        initViews();

        setupToolbar();
        progressDialog = new ProgressDialog(HomeActivity.this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
        saveOffenceListToDb1(HomeActivity.this,progressDialog);

        if (getIntent() != null) {
            phoneNumberStr = getIntent().getStringExtra("userId");
            name = getIntent().getStringExtra("name");
            email = getIntent().getStringExtra("email");
            refered_by = getIntent().getStringExtra("refered_by");
        }

        if (detector.isConnectingToInternet()) {
            //start service to upload data
            if (!UploadOfflineSurvey.isServiceRunning) {
                startService(new Intent(HomeActivity.this, UploadOfflineSurvey.class));
            }
        }
       /* if (Prefrence.getUserType(getApplicationContext()).equalsIgnoreCase("1")) {
            reportOffense.setVisibility(View.VISIBLE);
        } else {
            reportOffense.setVisibility(View.GONE);
        }*/
        offenceAnalytics.setVisibility(View.GONE);
        connectionChangeReceiver = new ConnectionChangeReceiver();

        DrawerItemAdapter adapter = new DrawerItemAdapter(HomeActivity.this);
        mDrawerList.setAdapter(adapter);
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLayout.addDrawerListener(mDrawerToggle);

        mDrawerToggle = new android.support.v7.app.ActionBarDrawerToggle(this, mDrawerLayout, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        //This is necessary to change the icon of the Drawer Toggle upon state change.
        mDrawerToggle.syncState();

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDrawerLayout.isDrawerOpen(Gravity.START)) {
                    mDrawerLayout.closeDrawer(Gravity.START);
                } else {
                    //drawer is opening
                    mDrawerLayout.openDrawer(Gravity.START);
                    setRewardPoint();
                }

            }
        });

        Log.e("onCreate: ", Prefrence.getUsername(getApplicationContext()));

        username.setText(Prefrence.getUsername(getApplicationContext()));
        phoneNumber.setText(Prefrence.getMobileNumber(getApplicationContext()));

        reportOffense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Util.isAirplaneModeOn(HomeActivity.this)) {
                    Util.showAlert(HomeActivity.this, getString(R.string.disable_flightmode));
                } else {
                    checkLocationEnable();
                }
            }
        });

        reportedOffense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, ComplaintListActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.putExtra("From", "HOME");
                startActivity(intent);
                overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                finish();
            }
        });

        offenceAnalytics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, OffenceAnalyticsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                overridePendingTransition(R.anim.activity_in, R.anim.activity_out);

            }
        });

    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Home");
        getSupportActionBar().setDisplayShowTitleEnabled(true);
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }

    }


    private void selectItem(int position) {

       /* if (position == 0) {
            Intent intent = new Intent(HomeActivity.this, FeedbackListActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
            finish();

        } else if (position == 1) {
            Intent intent = new Intent(HomeActivity.this, ProfileActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
            finish();
        }else*/ if (position == 0) {
            if (detector.isConnectingToInternet()) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(HomeActivity.this);
                alertDialog.setMessage(getResources().getString(R.string.logout_msg));
                alertDialog.setCancelable(false);
                alertDialog.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //call the logout API
                        new LogoutAsyncTask(HomeActivity.this).execute(getString(R.string.LOGOUT_API));
                        dialog.dismiss();
                    }
                });

                alertDialog.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alertDialog.show();
            } else {
                Util.showAlert(HomeActivity.this, getString(R.string.please_check_internet));
            }
        }
    }

    private void initViews() {
        reportOffense = (CardView) findViewById(R.id.report_offense);
        reportedOffense = (CardView) findViewById(R.id.reported_offense);
        offenceAnalytics = (CardView) findViewById(R.id.offence_analytics_card);
        detector = new ConnectionDetector(getApplicationContext());
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        mDrawerList = (ListView) findViewById(R.id.drawer_list);
        username = (TextView) findViewById(R.id.username);
        phoneNumber = (TextView) findViewById(R.id.phone_number);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
    }


    private void gotoSendComplaintActivity() {
        Intent intent = new Intent(HomeActivity.this, SendComplaintActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
        finish();
    }

    @Override
    public void onBackPressed() {

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            if (doubleBackToExitPressedOnce) {
                finish();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, getResources().getString(R.string.exit_warm), Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }

    }
    public static void saveOffenceListToDb1(final Activity activity, final ProgressDialog progressDialog) {
        try {
            StringRequest strReq = new StringRequest(Request.Method.POST,
                    activity.getString(R.string.OFFENCE_LIST_API), new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {

                    progressDialog.hide();
                    try {
                        Database database = new Database(activity);
                        ArrayList<HashMap<String, String>> offenceList = new ArrayList();
                        HashMap offeceData;
                        Log.e("onResponse: ", response);
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getString("status").equals("1")) {
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                offeceData = new HashMap();
                                JSONObject arrayObj = jsonArray.getJSONObject(i);
                                offeceData.put("offenceName", arrayObj.getString("offence"));
                                offeceData.put("offenceId", arrayObj.getString("offence_id"));
                                offeceData.put("serverImagePath", arrayObj.getString("img"));
                                offenceList.add(offeceData);
                            }


                            new DownloadImagesAsync(activity).execute(offenceList);


                            database.deleteOffenceTable();
                            database.saveOffenceList(offenceList);

                        }

                    } catch (JSONException e) {
                        Util.showAlert(activity, activity.getString(R.string.pta));
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error != null) {
                        if (error.getCause() != null) {
                            if (error.getCause().getMessage() != null)
                                Util.showAlert(activity, error.getCause().getMessage());
                            else
                                Util.showAlert(activity, activity.getString(R.string.pta));
                            progressDialog.hide();
                        }

                    }

                }

            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    // String state = Prefrence.getCircleID(activity);
                    //  Log.e("getParams: ", state);
                    params.put("state", "PB");
                    // params.put("state_code", "DL");
                    return params;

                }

            };
            ;

            strReq.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 50000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
            // Adding request to request queue
            AppController.getInstance().addToRequestQueue(strReq, "String Request");


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    public void checkLocationEnable() {

        googleApiClient = new GoogleApiClient.Builder(HomeActivity.this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);

        //**************************
        builder.setAlwaysShow(true); //this is the key ingredient
        //**************************

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Log.e(TAG, "onResult: success");

                        if (checkLocationPermission()) {
                            gotoSendComplaintActivity();
                        }
                        // All location settings are satisfied. The client can do location related task here
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            Log.e(TAG, "onResult: Location is fixed now");
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(
                                    HomeActivity.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.e(TAG, "onResult: Location is not satisfied");
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }


        });
    }

    public boolean checkLocationPermission() {
        if (ActivityCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(HomeActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_ACCESS_LOCATION);
            return false;

        }
        return true;
    }

    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {

            case MY_PERMISSIONS_REQUEST_ACCESS_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // Location related task you need to do.
                    gotoSendComplaintActivity();

                } else {

                    Toast.makeText(HomeActivity.this, "Location Permission is required to use this feature",
                            Toast.LENGTH_SHORT).show();

                }
                break;

            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
// Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
//                        startLocationUpdates();
                        Log.e(TAG, "onActivityResult: location is enabled now");
                        if (checkLocationPermission()) {
                            gotoSendComplaintActivity();
                        }
                        break;
                    case Activity.RESULT_CANCELED:
                        Toast.makeText(HomeActivity.this,
                                R.string.enable_location_to_use_this_feature, Toast.LENGTH_LONG).show();

//                        settingsrequest();//keep asking if imp or do whatever
                        break;
                }
                break;
        }
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    public void setRewardPoint() {

        progressBar.setVisibility(View.VISIBLE);
        //  userCredit.setVisibility(View.GONE);

        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST,
                getString(R.string.CREDIT_API), new Response.Listener<NetworkResponse>() {

            @Override
            public void onResponse(NetworkResponse response) {
                String resultResponse = new String(response.data);
//                Log.e(TAG, "onResponse: Response: " + resultResponse);

                //   userCredit.setVisibility(View.VISIBLE);
//                pDialog.hide();
                progressBar.setVisibility(View.GONE);
                try {
//                                JSONArray jsonArray = new JSONArray(String.valueOf(resultResponse));
                    JSONObject jsonObject = new JSONObject(resultResponse);
                    String status = jsonObject.getString("status");
                    if (status.equals("1")) {
                        JSONObject obj = jsonObject.getJSONObject("data");
                        String creditPoint = obj.getString("credit_points");
                        //set the credit point
                        //   userCredit.setText(creditPoint);
                    } else {
                        //set the credit point to NA
                        //userCredit.setText(getString(R.string.na));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    // userCredit.setText(getString(R.string.na));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressBar.setVisibility(View.GONE);
                //   userCredit.setVisibility(View.VISIBLE);
                //   userCredit.setText(getString(R.string.na));
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("token", Prefrence.getToken(HomeActivity.this));
                return params;
            }

        };
        multipartRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        AppController.getInstance().addToRequestQueue(multipartRequest);

    }

    public static void shareApp(Context context) {
        final String appPackageName = context.getPackageName();
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "Check out Traffic Sentinel App at: https://play.google.com/store/apps/details?id=" + appPackageName);
        sendIntent.setType("text/plain");
        context.startActivity(sendIntent);
    }

    @Override
    protected void onStart() {
        super.onStart();
        /*if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            this.registerReceiver(connectionChangeReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        }*/
    }
}
