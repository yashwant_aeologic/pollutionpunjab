package com.aeologic.cctv.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.aeologic.cctv.Async.GetRCDetailsAsync;
import com.aeologic.cctv.Connection.ConnectionDetector;
import com.aeologic.cctv.R;


/**
 * Created by 105 on 04-Nov-16.
 */


public class FitnessRCSearchActivity extends AppCompatActivity {
    static EditText RCNumber;
    static CardView searchRC;
    Toolbar toolbar;
    private ConnectionDetector detector;
    String rcNo, callFrom, backHandle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_fitness_rcsearch);
        initViews();
        setupToolbar("Search Vehicle Details");
        detector = new ConnectionDetector(this);
        searchRC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard(RCNumber);
                if (detector.isConnectingToInternet()) {
                    if (RCNumber.getText().toString().length() == 0) {
                        RCNumber.setError(getString(R.string.reg_no_empty));

                    } else if (RCNumber.getText().toString().length() > 3) {
                        if (!RCNumber.getText().toString().matches("^[0-9]*$") && !RCNumber.getText().toString().matches("[a-zA-Z]+")) {

                            rcNo = RCNumber.getText().toString();
                            String reg1 = rcNo.substring(0, rcNo.length() - 4);
                            String reg2 = rcNo.substring(rcNo.length() - 4);

                            GetRCDetailsAsync getRCDetailsAsync = new GetRCDetailsAsync(FitnessRCSearchActivity.this);
                            getRCDetailsAsync.execute(getString(R.string.BASE_URL) + getString(R.string.RC_DETAILS), RCNumber.getText().toString().trim());


                        } else {
                            RCNumber.requestFocus();
                            RCNumber.setError(getString(R.string.valid_rc_no));
                        }
                    } else {
                        RCNumber.requestFocus();
                        RCNumber.setError(getString(R.string.reg_no_validite));
                    }

                   /* Intent intent = new Intent(FitnessRCSearchActivity.this, FitnessDetails.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                    finish();*/

                } else {
                    Toast.makeText(FitnessRCSearchActivity.this, getResources().getString(R.string.con_fail), Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void hideKeyboard(EditText editText) {
        InputMethodManager mImMan = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        mImMan.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    private void initViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        RCNumber = (EditText) findViewById(R.id.rc_edit);
        searchRC = (CardView) findViewById(R.id.search_rc);
    }

    private void setupToolbar(String title) {

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        setTitleColor(R.color.white);
        setTitle(title);
    }

    @Override
    public void onBackPressed() {
        goToActivity(HomeActivity.class);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                goToActivity(HomeActivity.class);
                break;
        }
        return true;
    }

    private void goToActivity(Class aClass) {

        Intent intent = new Intent(FitnessRCSearchActivity.this, aClass);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);
        finish();
    }


}

