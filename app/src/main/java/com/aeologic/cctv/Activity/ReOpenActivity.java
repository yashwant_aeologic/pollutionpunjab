package com.aeologic.cctv.Activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.aeologic.cctv.Connection.ConnectionDetector;
import com.aeologic.cctv.OfficersActivities.ManagerComplaintListActivity;
import com.aeologic.cctv.OfficersActivities.ManagerForwardComplaintActivity;
import com.aeologic.cctv.R;
import com.aeologic.cctv.Utility.AppController;
import com.aeologic.cctv.Utility.Prefrence;
import com.aeologic.cctv.Utility.Util;
import com.aeologic.cctv.Utility.VolleyMultipartRequest;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ReOpenActivity extends AppCompatActivity {

    private static final String TAG = ReOpenActivity.class.getName();
    EditText title, feedback;
    int complaintId;
    Button submit;
    ConnectionDetector detector;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reopen_report);
        initViews();
        getIntents();
//        this.registerReceiver(connectionChangeReceiver,new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Re-Open Report");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (detector.isConnectingToInternet()) {
                    if (isValidate()) {
                        //call the feedback api

                        submit.setEnabled(false);
                        submit.setAlpha(0.4f);

                        final ProgressDialog progressDialog = new ProgressDialog(ReOpenActivity.this);
                        progressDialog.setCancelable(false);
                        progressDialog.setMessage(getString(R.string.please_wait));
                        progressDialog.show();
                        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST,
                                getString(R.string.REOPEN_API), new Response.Listener<NetworkResponse>() {
                            @Override
                            public void onResponse(NetworkResponse response) {
                                String resultResponse = new String(response.data);

                                Log.e(TAG, "API: " + getString(R.string.REOPEN_API));
                                Log.e(TAG, "onResponse: Response: " + resultResponse);
                                progressDialog.dismiss();
                                try {
                                    JSONObject jsonObject = new JSONObject(resultResponse);
                                    //{"status":1,"message":"Feedback submitted successfully"}
                                    String status = jsonObject.getString("status");
                                    if (status.equals("1")) {
                                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ReOpenActivity.this);
                                        alertDialog.setMessage(jsonObject.getString("message"));
                                        alertDialog.setCancelable(false);
                                        alertDialog.setPositiveButton(getResources().getString(R.string.Ok), new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {

                                                dialog.dismiss();
                                                Intent intent = new Intent(ReOpenActivity.this, ComplaintListActivity.class);
                                                intent.putExtra("From", "REOPEN");
                                                startActivity(intent);
                                                overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);
                                                finish();


                                            }
                                        });
                                        alertDialog.show();
                                    } else {
                                        String message = jsonObject.getString("message");
                                        if (message != null) {
                                            if (message.contains("Your session has expired"))
                                                Util.showSessionExpiredDialog(ReOpenActivity.this);
                                        } else {
                                            Util.showAlert(ReOpenActivity.this, getString(R.string.pta));
                                        }
                                        submit.setAlpha(1.0f);
                                        submit.setEnabled(true);
                                    }
                                } catch (JSONException e) {
                                    submit.setAlpha(1.0f);
                                    submit.setEnabled(true);
                                    Util.showAlert(ReOpenActivity.this, getString(R.string.pta));
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                error.printStackTrace();
                                progressDialog.hide();
                                submit.setAlpha(1.0f);
                                submit.setEnabled(true);
                                Util.showAlert(ReOpenActivity.this, getString(R.string.pta));
                            }
                        }) {
                            @Override
                            protected Map<String, String> getParams() {
                                Map<String, String> params = new HashMap<String, String>();
                                Log.e(TAG, "getParams: TOKEN"+ Prefrence.getToken(ReOpenActivity.this) );
                                params.put("token", Prefrence.getToken(ReOpenActivity.this));
                                params.put("comment", feedback.getText().toString());
                                params.put("complaint_no", String.valueOf(complaintId));
                                return params;

                            }
                        };
                        multipartRequest.setRetryPolicy(new RetryPolicy() {
                            @Override
                            public int getCurrentTimeout() {
                                return 50000;
                            }

                            @Override
                            public int getCurrentRetryCount() {
                                return 50000;
                            }

                            @Override
                            public void retry(VolleyError error) throws VolleyError {
                                error.printStackTrace();
                            }
                        });
                        AppController.getInstance().addToRequestQueue(multipartRequest);
                    }
                } else {
                    Util.showAlert(ReOpenActivity.this, getString(R.string.please_check_internet));
                }
            }
        });
    }

    private void getIntents() {
        Intent intent = getIntent();
         complaintId = intent.getIntExtra("complaintId", 0);
        Log.e(TAG, "onCreate: ID "+ complaintId);
    }

    private boolean isValidate() {
        /*if (title.getText().toString().isEmpty()) {
            title.setError("Please provide the title!");
            return false;
        }*/
        if (feedback.getText().toString().isEmpty()) {
            feedback.setError("Please provide the Description!");
            return false;
        }
        return true;
    }


    private void initViews() {

        feedback = (EditText) findViewById(R.id.feedback_message);
        submit = (Button) findViewById(R.id.submit);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        detector = new ConnectionDetector(ReOpenActivity.this);

        feedback.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (s.length() > 500) {
                    Toast.makeText(ReOpenActivity.this, "Character limit exceeded", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onStop() {
        super.onStop();
        /*try {
            unregisterReceiver(connectionChangeReceiver);
        }catch (IllegalArgumentException e){
            e.printStackTrace();
        }*/
    }
}