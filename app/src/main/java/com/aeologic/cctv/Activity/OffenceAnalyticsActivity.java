package com.aeologic.cctv.Activity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.aeologic.cctv.Adapter.OffenceAnalyticsAdapter;
import com.aeologic.cctv.Async.DownloadPDFAsyncTask;
import com.aeologic.cctv.Connection.ConnectionDetector;
import com.aeologic.cctv.OfficersActivities.ManagerHomeActivity;
import com.aeologic.cctv.R;
import com.aeologic.cctv.Utility.AppController;
import com.aeologic.cctv.Utility.Prefrence;
import com.aeologic.cctv.Utility.RecyclerViewDisabler;
import com.aeologic.cctv.Utility.Util;
import com.aeologic.cctv.Utility.VolleyMultipartRequest;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.aeologic.cctv.Activity.ComplaintListActivity.getCalculatedDate;
import static com.aeologic.cctv.Utility.Util.parseDate;


public class OffenceAnalyticsActivity extends AppCompatActivity {
    private static final String TAG = OffenceAnalyticsActivity.class.getSimpleName();
    private Toolbar toolbar;
    private RecyclerView recyclerView;
    ImageView warningLogo;
    TextView warningText;
    public static ProgressBar progressBar;
    private boolean allDataLoadSuccessfully = false;
    private boolean swipeRefreshBarStatus = false;
    private boolean responseStatus;
    ArrayList<HashMap<String, String>> responseList = new ArrayList<>();
    private ConnectionDetector detector;
    private OffenceAnalyticsAdapter adapter;
    private SwipeRefreshLayout materialRefreshLayout;
    RecyclerViewDisabler recyclerViewDisabler;
    boolean isFirstTime = true;
    private VolleyMultipartRequest multipartRequest;
    private int pageNumber;
    private CardView filterContainerCard;
    private CardView searchCard;
    private EditText from, to;
    ImageView fromCal, toCal;
    Calendar fromCalendar, toCalendar;
    private boolean filterStatus;
    String fromText, toText;
    TextView totalCount, totalPending, totalRejected, totalResolved, totalUnrelated, totalDeleted;
    int totalCount1, totalPending1, totalRejected1, totalResolved1, totalUnrelated1, totalDeleted1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offence_analytics_list);
        initViews();
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Service Analytics");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(OffenceAnalyticsActivity.this, ManagerHomeActivity.class));
                finish();
            }
        });


        Log.e(TAG, "onCreate: register reciever for connectionchange");
        IntentFilter intentFilter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
        fromCalendar = Calendar.getInstance();
        toCalendar = Calendar.getInstance();
        fromText = getCalculatedDate("dd-MM-yyyy", -7);
        from.setText(fromText);
        toText = getCalculatedDate("dd-MM-yyyy", 0);
        to.setText(toText);
        setTo(getCalculatedDate("dd-MM-yyyy", 0));
        setFrom(getCalculatedDate("dd-MM-yyyy", -7));
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                fromCalendar.set(Calendar.YEAR, year);
                fromCalendar.set(Calendar.MONTH, monthOfYear);
                fromCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();


            }

        };

        final DatePickerDialog.OnDateSetListener date1 = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                toCalendar.set(Calendar.YEAR, year);
                toCalendar.set(Calendar.MONTH, monthOfYear);
                toCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel1();


            }

        };
        recyclerView.setLayoutManager(new LinearLayoutManager(OffenceAnalyticsActivity.this));
        detector = new ConnectionDetector(OffenceAnalyticsActivity.this);
        recyclerViewDisabler = new RecyclerViewDisabler();

        if (adapter != null) {
            recyclerView.setAdapter(adapter);
        } else {
            adapter = new OffenceAnalyticsAdapter(OffenceAnalyticsActivity.this, responseList, recyclerView, to.getText().toString(), from.getText().toString());
            recyclerView.setAdapter(adapter);
        }


        materialRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadData();
            }


        });
        searchCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadData();
            }
        });
        fromCal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog =
                        new DatePickerDialog(OffenceAnalyticsActivity.this, date, fromCalendar
                                .get(Calendar.YEAR), fromCalendar.get(Calendar.MONTH),
                                fromCalendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });

        toCal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog =
                        new DatePickerDialog(OffenceAnalyticsActivity.this, date1, toCalendar
                                .get(Calendar.YEAR), toCalendar.get(Calendar.MONTH),
                                toCalendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });

        from.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog =
                        new DatePickerDialog(OffenceAnalyticsActivity.this, date, fromCalendar
                                .get(Calendar.YEAR), fromCalendar.get(Calendar.MONTH),
                                fromCalendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });

        to.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog =
                        new DatePickerDialog(OffenceAnalyticsActivity.this, date1, toCalendar
                                .get(Calendar.YEAR), toCalendar.get(Calendar.MONTH),
                                toCalendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });

        if (detector.isConnectingToInternet()) {
            // call the API here
//            new DoneListAsync(getActivity(), recyclerView, warningLogo, warningText,pageNumber).execute(getString(R.string.COMPLAINT_LIST_API));
            getUploaddata(pageNumber);

            final Handler handler = new Handler();

            adapter.setOnLoadMoreListener(new OffenceAnalyticsAdapter.OnLoadMoreListener() {
                @Override
                public void onLoadMore() {
                    //add progress item
                    //responseList.add(null);
                    // adapter.notifyItemInserted(responseList.size() - 1);

                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //remove progress item
                            if (responseStatus && !allDataLoadSuccessfully) {
                                // responseList.remove(responseList.size() - 1);
                                // adapter.notifyItemRemoved(responseList.size());
                                pageNumber = pageNumber + 1;
                                getUploaddata(pageNumber);
                            }
                            //     new DoneListAsync(context, recyclerView, warningLogo, warningText,pageNumber).execute(context.getString(R.string.COMPLAINT_LIST_API));

                            //or you can add all at once but do not forget to call mAdapter.notifyDataSetChanged();
                        }
                    }, 600);
                    System.out.println("load");
                }
            });

        } else {
            if (!isFirstTime) {
                Util.showAlert(OffenceAnalyticsActivity.this, getString(R.string.please_check_internet));
            }
            recyclerView.setVisibility(View.GONE);
            warningLogo.setImageResource(R.drawable.wifi);
            warningText.setText(getString(R.string.no_internet_connections));
            warningLogo.setVisibility(View.VISIBLE);
            warningText.setVisibility(View.VISIBLE);
        }
    }

    public void setTo(String calculatedDate) {
        this.toText = calculatedDate;
    }

    public String getTo() {
        return toText;
    }

    public void setFrom(String calculatedDate) {
        this.fromText = calculatedDate;
    }

    public String getFrom() {
        return fromText;
    }

    private void initViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        warningLogo = (ImageView) findViewById(R.id.warning_logo1);
        warningText = (TextView) findViewById(R.id.warning_text1);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view3);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        filterContainerCard = (CardView) findViewById(R.id.filter_container_card);
        materialRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh);
        searchCard = (CardView) findViewById(R.id.search_card);
        from = (EditText) findViewById(R.id.from_et);
        to = (EditText) findViewById(R.id.to_et);
        fromCal = (ImageView) findViewById(R.id.from_cal);
        toCal = (ImageView) findViewById(R.id.to_cal);
        totalCount = (TextView) findViewById(R.id.total_count);
        totalPending = (TextView) findViewById(R.id.total_pending);
        totalRejected = (TextView) findViewById(R.id.total_rejected);
        totalResolved = (TextView) findViewById(R.id.total_resolved);
        totalUnrelated = (TextView) findViewById(R.id.total_unrelated);
        totalDeleted = (TextView) findViewById(R.id.delete_count);
    }

    public void loadData() {
        totalCount1 = 0;
        totalPending1 = 0;
        totalRejected1 = 0;
        totalResolved1 = 0;
        totalUnrelated1 = 0;

        if (OffenceAnalyticsActivity.this != null) {
            if (new ConnectionDetector(OffenceAnalyticsActivity.this).isConnectingToInternet()) {
                recyclerView.addOnItemTouchListener(recyclerViewDisabler);
                if (multipartRequest != null) {
                    multipartRequest.cancel();
                }
                swipeRefreshBarStatus = true;
                materialRefreshLayout.setEnabled(true);
                responseList.clear();
                pageNumber = 0;
                getUploaddata(pageNumber);
                allDataLoadSuccessfully = false;
                recyclerView.getRecycledViewPool().clear();
                adapter.notifyDataSetChanged();
            } else {
                materialRefreshLayout.setRefreshing(false);
                warningLogo.setImageResource(R.drawable.wifi);
                warningText.setText(getString(R.string.no_internet_connections));
                Util.showAlert(OffenceAnalyticsActivity.this, getString(R.string.please_check_internet));
            }
        }
    }

    private void getUploaddata(final int pageNumber) {
        responseStatus = false;
        final ProgressDialog progressDialog = new ProgressDialog(OffenceAnalyticsActivity.this);
        progressDialog.setMessage(getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();

        if (isFirstTime && !OffenceAnalyticsActivity.this.isFinishing()) {
            // progressDialog.show();
            isFirstTime = false;
        }


        multipartRequest = new VolleyMultipartRequest(Request.Method.POST,
                getString(R.string.OFFENCE_ANALYTICS_API), new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                String resultResponse = new String(response.data);
                Log.e(TAG, "onResponse: Response: " + resultResponse);
                progressDialog.hide();
                try {
                    if (recyclerViewDisabler != null) {
                        recyclerView.removeOnItemTouchListener(recyclerViewDisabler);
                        swipeRefreshBarStatus = false;
                        materialRefreshLayout.setRefreshing(false);
                    }
                    HashMap<String, String> responseData;
                    JSONObject object = new JSONObject(resultResponse);
                    String status = object.getString("status");
                    if (status.equals("1")) {
                        responseStatus = true;
                        JSONArray resultArray = object.getJSONArray("data");
                        if (resultArray != null && resultArray.length() > 0) {
                            for (int i = 0; i < resultArray.length(); i++) {
                                responseData = new HashMap<>();
                                JSONObject obj = resultArray.getJSONObject(i);
                                responseData.put("name", obj.getString("name"));
                             /*   totalCount1 = totalCount1 + Integer.parseInt(obj.getString("count"));
                                totalPending1 = totalPending1 + Integer.parseInt(obj.getString("pending_count"));
                                totalRejected1 = totalRejected1 + Integer.parseInt(obj.getString("rejected_count"));
                                totalResolved1 = totalResolved1 + Integer.parseInt(obj.getString("resolved_count"));
                                totalUnrelated1 = totalUnrelated1 + Integer.parseInt(obj.getString("unrelated_count"));*/
                                responseData.put("count", obj.getString("count"));
                                responseData.put("pending_count", obj.getString("pending_count"));
                                responseData.put("resolved_count", obj.getString("resolved_count"));
                                responseData.put("rejected_count", obj.getString("rejected_count"));
                                responseData.put("unrelated_count", obj.getString("unrelated_count"));
                                responseData.put("deleted_count", obj.getString("deleted_count"));
                                responseData.put("offence_id", obj.getString("offence_id"));
                                responseList.add(responseData);
                            }
                            JSONObject chartObject = object.getJSONObject("chart_data");
                            {
                                if (chartObject != null) {
                                    totalCount1 = Integer.parseInt(chartObject.getString("total_count"));
                                    totalPending1 = Integer.parseInt(chartObject.getString("total_pending_count"));
                                    totalRejected1 = Integer.parseInt(chartObject.getString("total_rejected_count"));
                                    totalResolved1 = Integer.parseInt(chartObject.getString("total_resolved_count"));
                                    totalUnrelated1 = Integer.parseInt(chartObject.getString("total_unrelated_count"));
                                    totalDeleted1 = Integer.parseInt(chartObject.getString("total_deleted_count"));
                                }
                            }
                            adapter.notifyItemInserted(responseList.size());
                            adapter.setLoaded();
                            totalUnrelated.setText(String.valueOf(totalUnrelated1));
                            totalPending.setText(String.valueOf(totalPending1));
                            totalResolved.setText(String.valueOf(totalResolved1));
                            totalRejected.setText(String.valueOf(totalRejected1));
                            totalDeleted.setText(String.valueOf(totalDeleted1));
                            totalCount.setText("Total: " + String.valueOf(totalCount1));
                            progressBar.setVisibility(View.INVISIBLE);
                            recyclerView.setVisibility(View.VISIBLE);
                            warningLogo.setVisibility(View.GONE);
                            warningText.setVisibility(View.GONE);
                        } else {
                            allDataLoadSuccessfully = true;
                            progressBar.setVisibility(View.INVISIBLE);
                            if (pageNumber == 0) {
                                recyclerView.setVisibility(View.GONE);
                                warningLogo.setImageResource(R.drawable.round_error_symbol);
                                warningText.setText(getString(R.string.no_complaint_found));
                                warningLogo.setVisibility(View.VISIBLE);
                                warningText.setVisibility(View.VISIBLE);
                            }
                        }
                    } else if (status.equals("0")) {
                        String message = object.getString("message");
                        if (message != null) {
                            if ((responseList.size() == 0) && message.contains("Your session has expired"))
                                Util.showSessionExpiredDialog(OffenceAnalyticsActivity.this);
                        } else {
                            Util.showAlert(OffenceAnalyticsActivity.this, getString(R.string.pta));
                        }
                        progressBar.setVisibility(View.INVISIBLE);
                        progressDialog.hide();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                //  Log.e(TAG, "onErrorResponse: " + error.getMessage());
                progressBar.setVisibility(View.INVISIBLE);
                Util.showAlert(OffenceAnalyticsActivity.this, getString(R.string.pta));
                progressDialog.hide();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("token", Prefrence.getToken(OffenceAnalyticsActivity.this));
                if (from.getText() != null && from.getText().toString().length() > 4 && to.getText() != null && to.getText().toString().length() > 4) {

                    params.put("start_date", parseDate(from.getText().toString(), "dd-MM-yyyy", "yyyy-MM-dd"));
                    params.put("end_date", parseDate(to.getText().toString(), "dd-MM-yyyy", "yyyy-MM-dd"));

                } else {
                    params.put("start_date", getCalculatedDate("yyyy-MM-dd", -7));
                    params.put("end_date", getCalculatedDate("yyyy-MM-dd", -0));
                }

                return params;

            }

        };
        multipartRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        AppController.getInstance().addToRequestQueue(multipartRequest);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(OffenceAnalyticsActivity.this, ManagerHomeActivity.class));
        finish();
    }

    private void updateLabel() {
        String myFormat = "dd-MM-yyyy";
        String myFormat1 = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        SimpleDateFormat sdf1 = new SimpleDateFormat(myFormat1, Locale.US);
        from.setText(sdf.format(fromCalendar.getTime()));
        fromText = sdf1.format(fromCalendar.getTime());
        setFrom(sdf.format(fromCalendar.getTime()));
    }

    private void updateLabel1() {
        String myFormat = "dd-MM-yyyy";
        String myFormat1 = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        SimpleDateFormat sdf1 = new SimpleDateFormat(myFormat1, Locale.US);
        to.setText(sdf.format(toCalendar.getTime()));
        toText = sdf1.format(toCalendar.getTime());
        setTo(sdf.format(toCalendar.getTime()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Log.e("onCreateOptionsMenu: ", Prefrence.getDownload(OffenceAnalyticsActivity.this));
        if (Prefrence.getDownload(OffenceAnalyticsActivity.this).equalsIgnoreCase("1")) {
            //   Log.e("onCreateOptionsMenu: ", "IN");
            getMenuInflater().inflate(R.menu.menu_analytics, menu);
        } else {
            // Log.e("onCreateOptionsMenu: ", "OUT");
            getMenuInflater().inflate(R.menu.menu_main, menu);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_filter) {
            if (filterContainerCard.getVisibility() == View.VISIBLE) {
                filterContainerCard.setVisibility(View.GONE);
                filterStatus = false;

            } else {
                filterContainerCard.setVisibility(View.VISIBLE);
                filterStatus = true;
            }
        } else if (id == R.id.action_download) {
            Log.e("onOptionsItemSelected: ", "Download Clicked");
            DownloadPDFAsyncTask downloadPDFAsyncTask = new DownloadPDFAsyncTask(OffenceAnalyticsActivity.this);
            downloadPDFAsyncTask.execute(getString(R.string.API_BASE_URL1) + getString(R.string.DOWNLOAD_PDF), parseDate(from.getText().toString(), "dd-MM-yyyy", "yyyy-MM-dd"), parseDate(to.getText().toString(), "dd-MM-yyyy", "yyyy-MM-dd"));


        }
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}




