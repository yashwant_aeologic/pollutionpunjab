package com.aeologic.cctv.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.aeologic.cctv.Async.ChangePasswordAsyncTask;
import com.aeologic.cctv.R;

public class ChangePasswordActivity extends AppCompatActivity {
    EditText oldPassword, newPassword, confirmPassword;
    Button changePassword;
    ImageView backButton;
    private String token = null;
    CheckBox showPassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        initViews();
        showPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    newPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    confirmPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    oldPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                } else {
                    newPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    confirmPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    oldPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
            }
        });
        changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (validate()) {
                    ChangePasswordAsyncTask changePasswordAsyncTask = new ChangePasswordAsyncTask(ChangePasswordActivity.this, changePassword);
                    changePasswordAsyncTask.execute(getString(R.string.API_BASE_URL)+getString(R.string.CHANGE_PASSWORD_API),oldPassword.getText().toString(),newPassword.getText().toString(),confirmPassword.getText().toString());
                }
            }
        });
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToBack();
            }
        });
    }

    private void goToBack() {
        Intent intent = new Intent(ChangePasswordActivity.this, HomeActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        goToBack();
    }

    private boolean validate() {
        if (oldPassword.getText() != null && oldPassword.getText().toString().length() > 3) {
            if (newPassword.getText() != null && newPassword.getText().toString().length() > 3) {
                if (confirmPassword.getText() != null && confirmPassword.getText().toString().length() > 3) {
                    if (confirmPassword.getText().toString().equalsIgnoreCase(newPassword.getText().toString())) {
                        if (!newPassword.getText().toString().equalsIgnoreCase(oldPassword.getText().toString())) {
                            return true;
                        } else {
                            Toast.makeText(ChangePasswordActivity.this, "New Password and Old password can not be same", Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        //      confirmPassword.setError("New Password and Confirm password should be same");
                        Toast.makeText(ChangePasswordActivity.this, "New Password and Confirm password should be same", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    //    confirmPassword.setError("Please enter valid password");
                    Toast.makeText(ChangePasswordActivity.this, "Please enter valid password", Toast.LENGTH_SHORT).show();
                }

            } else {
                //  newPassword.setError("Please enter valid password");
                Toast.makeText(ChangePasswordActivity.this, "Please enter valid password", Toast.LENGTH_SHORT).show();
            }

        } else {
            //oldPassword.setError("Please enter valid password");
            Toast.makeText(ChangePasswordActivity.this, "Please enter valid password", Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    private void initViews() {
        oldPassword = (EditText) findViewById(R.id.old_password);
        newPassword = (EditText) findViewById(R.id.pwd_et);
        confirmPassword = (EditText) findViewById(R.id.confirm_pwd_et);
        changePassword = (Button) findViewById(R.id.change_pwd);
        backButton = (ImageView) findViewById(R.id.back_btn);
        showPassword = (CheckBox) findViewById(R.id.checkbox);
    }
}
