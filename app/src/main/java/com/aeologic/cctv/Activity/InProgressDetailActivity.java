package com.aeologic.cctv.Activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaPlayer;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.VideoView;


import com.aeologic.cctv.Async.SendDetailsAsyncTask;
import com.aeologic.cctv.Connection.ConnectionDetector;
import com.aeologic.cctv.Database.Database;
import com.aeologic.cctv.R;
import com.aeologic.cctv.Utility.Util;


import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * Created by u104 on 29/7/16.
 */
public class InProgressDetailActivity extends AppCompatActivity {
    TextView rcNumber, address, comment, offense;
    public Button send;
    ImageView image;
    VideoView videoView;
    int tabStatus;
    private String id;
    String TAG = "DetailsActivity";
    Database database;
    ImageView playPuase;
    Toolbar toolbar;
    FrameLayout videoContainer;
    FrameLayout imageContainer;
    Uri videoUri;
    ConnectionDetector detector;
    private HashMap<String, String> detailmap;
    private String imagePath = null;
    private String videoPath = null;
    String offenceList = null;
    String commentText = null;
    LinkedHashMap<String, String> offenceMap;
    private AlertDialog imageDialog;
    private ImageView fullImage;
    private AlertDialog videoDialog;
    private VideoView fullVideoView;
    LinearLayout statusContainer;
    private LinearLayout reasonConatiner;
   // private EasyVideoPlayer player;
//    private SeekBar seekBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        initViews();

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setTitle("Details");

        offenceMap = database.getOffenceMap();
//        setImageDialog();
//        setVideoDialog();

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent intent = new Intent(InProgressDetailActivity.this, ComplaintListActivity.class);
                intent.putExtra("tabStatus", tabStatus);
                startActivity(intent);*/
                finish();
            }
        });


        if (getIntent() != null) {
            id = getIntent().getStringExtra("id");
            tabStatus = getIntent().getIntExtra("tabStatus", 0);
            Log.e(TAG, "onCreate: tabStatus:" + tabStatus);
            if (id != null && !id.isEmpty()) {
                detailmap = database.getDetails(id);
                if (detailmap != null && detailmap.size() > 0) {
                    setDatavalue(detailmap);
                } else {
                    finish();
                }
            }
        }
        playPuase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                videoView.start();
                if (videoView.isPlaying()) {
                    videoView.pause();
                    playPuase.setImageResource(R.drawable.play);
                } else {
                    if (videoUri != null) {

                        videoView.setBackgroundResource(0);
                        videoView.setVideoURI(videoUri);
                        videoView.requestFocus();
                        videoView.start();
                        playPuase.setImageResource(R.drawable.pause);
                    }

                }
            }
        });

       /* seekBar.getProgressDrawable().setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN);
        seekBar.getThumb().setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN);

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                seekBar.setMax(videoView.getDuration());
                seekBar.postDelayed(onEverySecond, 1000);
            }
        });*/

      /*  seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {

                if (fromUser) {
                    // this is when actually seekbar has been seeked to a new position
                    videoView.seekTo(progress);
                }
            }
        });*/

        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                playPuase.setImageResource(R.drawable.play);
            }
        });

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (detector.isConnectingToInternet()) {
                    send.setAlpha(0.4f);
                    send.setEnabled(false);
                    new SendDetailsAsyncTask(InProgressDetailActivity.this).
                            execute(getString(R.string.CREATE_COMPLAINT_API),
                                    rcNumber.getText().toString().trim(),
                                    offenceList,
                                    commentText,
                                    imagePath,
                                    videoPath,
                                    detailmap.get("address").toString(),
                                    detailmap.get("latitude").toString(),
                                    detailmap.get("longitude").toString(),
                                    detailmap.get("createdAt").toString(),
                                    "offline", id,
                                    detailmap.get("manualAddress").toString());
                } else {
                    Util.showAlert(InProgressDetailActivity.this, getString(R.string.please_check_internet));
                    send.setEnabled(true);
                    send.setAlpha(1.0f);
                }
            }
        });

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imagePath != null) {
                   /* fullImage.setImageURI(Uri.parse(imagePath));
                    imageDialog.show();*/
                    Intent intent = new Intent(InProgressDetailActivity.this, FullImageActivity.class);
                    intent.putExtra("activityName", "inProgress");
                    intent.putExtra("imageUri", imagePath);
                    intent.putExtra("latlng", detailmap.get("latitude").toString() + "," + detailmap.get("longitude").toString());
                    intent.putExtra("timeStamp", detailmap.get("createdAt").toString());
                    startActivity(intent);
                }
            }
        });

        videoContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                videoDialog.show();
               /* Intent intent = new Intent(InProgressDetailActivity.this, FullVideoViewActivity.class);
                intent.putExtra("videoPath", videoUri.toString());
                startActivity(intent);*/
            }
        });
    }

   /* private Runnable onEverySecond = new Runnable() {

        @Override
        public void run() {

            if (seekBar != null) {
                seekBar.setProgress(videoView.getCurrentPosition());
            }

            if (videoView.isPlaying()) {
                seekBar.postDelayed(onEverySecond, 1000);
            }

        }
    };*/

    @Override
    protected void onStop() {
        super.onStop();
//        Util.finishActivity(InProgressDetailActivity.this);
    }

    private void setDatavalue(HashMap datavalue) {
        if (datavalue != null && datavalue.size() > 0) {
            if (datavalue.get("rcNumber") != null && !datavalue.get("rcNumber").toString().trim().isEmpty()) {
                rcNumber.setText(datavalue.get("rcNumber").toString());
            }


            if (datavalue.get("offence") != null && !datavalue.get("offence").toString().trim().isEmpty()) {
                offenceList = datavalue.get("offence").toString();
                String[] offenceArray = offenceList.split(",");
                offense.setText(database.getOffenceName(offenceArray));
            } else {
                offense.setText("");
            }
            if (datavalue.get("comment") != null && !datavalue.get("comment").toString().trim().isEmpty()) {
                commentText = datavalue.get("comment").toString();
                comment.setText(commentText);
            } else {
                comment.setText("");
            }
            if (datavalue.get("manualAddress") != null && !datavalue.get("manualAddress").toString().trim().isEmpty()) {
                address.setText(datavalue.get("manualAddress").toString());
            } else {
                address.setText(datavalue.get("address").toString());
            }

            if (datavalue.get("timestamp") != null && !datavalue.get("timestamp").toString().trim().isEmpty()) {
                String timeStamp = Util.formatDateFromTimeStamp("yyyy-MM-dd HH:mm:ss", "yyyyMMddHHmmss", datavalue.get("timestamp").toString());
                getSupportActionBar().setTitle("#TEMP_" + timeStamp);
            }

            if (datavalue.get("imageUri") != null && !datavalue.get("imageUri").toString().trim().isEmpty()) {
//                File imageFile = new File(datavalue.get("imageUri").toString());
//                Bitmap myBitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
//                image.setImageBitmap(myBitmap);
                try {
                    imagePath = datavalue.get("imageUri").toString();
                    Uri imageUri = Uri.parse(imagePath);
                    image.setImageURI(imageUri);
                    imageContainer.setVisibility(View.VISIBLE);
                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                }

            } else {
                imageContainer.setVisibility(View.GONE);
            }
            if (datavalue.get("videoUri") != null && !datavalue.get("videoUri").toString().trim().isEmpty()) {
                videoPath = datavalue.get("videoUri").toString();
                videoUri = Uri.parse(videoPath);
                Log.e(TAG, "setDatavalue: videoUri: " + videoUri);
                videoView.setVideoURI(videoUri);
                videoContainer.setVisibility(View.VISIBLE);
                Bitmap thumb = ThumbnailUtils.createVideoThumbnail(datavalue.get("videoUri").toString(),
                        MediaStore.Images.Thumbnails.MINI_KIND);
                Log.e(TAG, "onTouch: " + thumb.getWidth() + "::" + thumb.getHeight());
                BitmapDrawable bitmapDrawable = new BitmapDrawable(getResources(), thumb);
                videoView.setBackground(bitmapDrawable);
//                fullVideoView.setBackground(bitmapDrawable);
            } else {
                videoContainer.setVisibility(View.GONE);
            }

        }
    }

    /*public void setImageDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(InProgressDetailActivity.this,
                R.style.AppTheme_NoActionBar_FullScreen);
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_full_image, null);
        dialogBuilder.setView(dialogView);
        imageDialog = dialogBuilder.create();
        imageDialog.setCancelable(false);

        fullImage = (ImageView) dialogView.findViewById(R.id.full_image);
        ImageView cancel = (ImageView) dialogView.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageDialog.dismiss();
            }
        });
    }

    public void setVideoDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(InProgressDetailActivity.this,
                R.style.AppTheme_NoActionBar_FullScreen);
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.activity_full_videoview, null);
        dialogBuilder.setView(dialogView);
        videoDialog = dialogBuilder.create();
        videoDialog.setCancelable(false);

        // Grabs a reference to the player view
        player = (EasyVideoPlayer) findViewById(R.id.player);

        // Sets the callback to this Activity, since it inherits EasyVideoCallback
        player.setCallback(new EasyVideoCallback() {
            @Override
            public void onStarted(EasyVideoPlayer player) {

            }

            @Override
            public void onPaused(EasyVideoPlayer player) {

            }

            @Override
            public void onPreparing(EasyVideoPlayer player) {

            }

            @Override
            public void onPrepared(EasyVideoPlayer player) {

            }

            @Override
            public void onBuffering(int percent) {

            }

            @Override
            public void onError(EasyVideoPlayer player, Exception e) {

            }

            @Override
            public void onCompletion(EasyVideoPlayer player) {

            }

            @Override
            public void onRetry(EasyVideoPlayer player, Uri source) {

            }

            @Override
            public void onSubmit(EasyVideoPlayer player, Uri source) {

            }
        });

        *//*fullVideoView = (VideoView) dialogView.findViewById(R.id.full_videoview);
        ImageView cancel = (ImageView) dialogView.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoDialog.dismiss();
            }
        });

        ImageView playPause = (ImageView) dialogView.findViewById(R.id.play);

        playPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fullVideoView.isPlaying()) {
                    fullVideoView.stopPlayback();
                    playPuase.setImageResource(R.drawable.play);
                } else {
                    if (videoUri != null) {
                        fullVideoView.setBackgroundResource(0);
                        fullVideoView.setVideoURI(videoUri);
                        fullVideoView.requestFocus();
                        fullVideoView.start();
                        playPuase.setImageResource(R.drawable.pause);
                    }

                }
            }
        });*//*

       *//* fullVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                playPuase.setImageResource(R.drawable.play);
            }
        });*//*
    }*/

    private void initViews() {

        rcNumber = (TextView) findViewById(R.id.rcNumber);
        address = (TextView) findViewById(R.id.address);
        offense = (TextView) findViewById(R.id.offense);
        comment = (TextView) findViewById(R.id.comment);
        send = (Button) findViewById(R.id.rejectBtn);
        playPuase = (ImageView) findViewById(R.id.playPause);
        image = (ImageView) findViewById(R.id.imageview);
        videoView = (VideoView) findViewById(R.id.videoview);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        database = new Database(getApplicationContext());
        detector = new ConnectionDetector(getApplicationContext());
        videoContainer = (FrameLayout) findViewById(R.id.videoContainer);
        imageContainer = (FrameLayout) findViewById(R.id.image_container);
        statusContainer = (LinearLayout) findViewById(R.id.statusContainer);
        statusContainer.setVisibility(View.GONE);
        reasonConatiner = (LinearLayout) findViewById(R.id.reasonContainer);
        reasonConatiner.setVisibility(View.GONE);
//        seekBar = (SeekBar) findViewById(R.id.seekbar);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        /*Intent intent = new Intent(InProgressDetailActivity.this, ComplaintListActivity.class);
        intent.putExtra("tabStatus", tabStatus);
        startActivity(intent);*/
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        playPuase.setImageResource(R.drawable.play);
        videoView.seekTo(100);
    }
}
