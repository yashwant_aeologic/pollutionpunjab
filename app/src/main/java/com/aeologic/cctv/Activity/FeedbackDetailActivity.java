package com.aeologic.cctv.Activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.aeologic.cctv.Connection.ConnectionDetector;
import com.aeologic.cctv.R;
import com.aeologic.cctv.Receiver.ConnectionChangeReceiver;
import com.aeologic.cctv.Utility.Util;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Sachin varshney on 28/4/17.
 */

public class FeedbackDetailActivity extends AppCompatActivity {
    ConnectionDetector detector;
    Toolbar toolbar;
    private String TAG = "FeedbackDetails";
    ConnectionChangeReceiver connectionChangeReceiver;
    private TextView title, feedback, createAt, reply, updateAt;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback_details);
        initViews();
        setValuesFromIntent();
        connectionChangeReceiver = new ConnectionChangeReceiver();
//        this.registerReceiver(connectionChangeReceiver,new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Feedback");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void setValuesFromIntent() {
        Bundle extras = getIntent().getExtras();
        if (extras.getString("title")!=null&&!extras.get("title").equals("null")){
            title.setText(extras.getString("title"));
            title.setVisibility(View.VISIBLE);
        }else{
            title.setVisibility(View.GONE);
        }
        if (extras.getString("feedback")!=null&&!extras.get("feedback").equals("null")){
            feedback.setText(extras.getString("feedback"));
            feedback.setVisibility(View.VISIBLE);
        }else{
            feedback.setVisibility(View.GONE);
        }
        if (extras.getString("createdAt")!=null&&!extras.get("createdAt").equals("null")){
            String createdAt = extras.getString("createdAt");
            String createdAtDate = Util.formatDateFromTimeStamp("yyyy-MM-dd HH:mm:ss", "dd MMM, yyyy", createdAt);
            String createdAtTime = Util.formatDateFromTimeStamp("yyyy-MM-dd HH:mm:ss", "hh:mm a", createdAt);
            Log.e(TAG, "onBindViewHolder: Time: " + createdAtDate+" "+createdAtTime);
            createAt.setText(createdAtDate+" "+createdAtTime);
            createAt.setVisibility(View.VISIBLE);
        }else{
            createAt.setVisibility(View.GONE);
        }
        if (extras.getString("reply")!=null&&!extras.get("reply").equals("null")){
            reply.setText(extras.getString("reply"));
            reply.setVisibility(View.VISIBLE);
            String updatedAt = extras.getString("updatedAt");
            String updatedAtDate = Util.formatDateFromTimeStamp("yyyy-MM-dd HH:mm:ss", "dd MMM, yyyy", updatedAt);
            String updatedAtTime = Util.formatDateFromTimeStamp("yyyy-MM-dd HH:mm:ss", "hh:mm a", updatedAt);
            Log.e(TAG, "onBindViewHolder: Time: " + updatedAtDate+" "+updatedAtTime);
            updateAt.setText(updatedAtDate+" "+updatedAtTime);
            updateAt.setVisibility(View.VISIBLE);
            findViewById(R.id.container_card3).setVisibility(View.VISIBLE);
        }else{
            reply.setVisibility(View.GONE);
            updateAt.setVisibility(View.GONE);
            findViewById(R.id.container_card3).setVisibility(View.GONE);
        }
    }


    private void initViews() {
        title = (TextView) findViewById(R.id.title);
        feedback = (TextView) findViewById(R.id.feedback);
        createAt = (TextView) findViewById(R.id.created_at);
        reply = (TextView) findViewById(R.id.reply);
        updateAt = (TextView) findViewById(R.id.updated_at);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        detector = new ConnectionDetector(FeedbackDetailActivity.this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
//            unregisterReceiver(connectionChangeReceiver);
        }catch (IllegalArgumentException e){
            e.printStackTrace();
        }
    }
}
/*submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (detector.isConnectingToInternet()) {
                    if (!feedback.getText().toString().isEmpty()) {
                        //call the feedback api

                        submit.setEnabled(false);
                        submit.setAlpha(0.4f);

                        final ProgressDialog progressDialog = new ProgressDialog(FeedbackDetailActivity.this);
                        progressDialog.setCancelable(false);
                        progressDialog.setMessage(getString(R.string.please_wait));
                        progressDialog.show();
                        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST,
                                getString(R.string.FEEDBACK_API), new Response.Listener<NetworkResponse>() {
                            @Override
                            public void onResponse(NetworkResponse response) {
                                String resultResponse = new String(response.data);
                                Log.e(TAG, "onResponse: Response: " + resultResponse);
                                progressDialog.hide();
                                try {
                                    JSONObject jsonObject = new JSONObject(resultResponse);
                                    //{"status":1,"message":"Feedback submitted successfully"}
                                    String status = jsonObject.getString("status");
                                    if (status.equals("1")) {
                                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(FeedbackDetailActivity.this);
                                        alertDialog.setMessage(jsonObject.getString("message"));
                                        alertDialog.setCancelable(false);
                                        alertDialog.setPositiveButton(getResources().getString(R.string.Ok), new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                startActivity(new Intent(FeedbackDetailActivity.this, HomeActivity.class));
                                                finish();
                                                dialog.dismiss();
                                            }
                                        });
                                        alertDialog.show();
                                    } else {
                                        submit.setAlpha(1.0f);
                                        submit.setEnabled(true);
                                        Util.showAlert(FeedbackDetailActivity.this, getString(R.string.pta));
                                    }

                                } catch (JSONException e) {
                                    submit.setAlpha(1.0f);
                                    submit.setEnabled(true);
                                    Util.showAlert(FeedbackDetailActivity.this, getString(R.string.pta));
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                error.printStackTrace();
                                progressDialog.hide();
                                submit.setAlpha(1.0f);
                                submit.setEnabled(true);
                                Util.showAlert(FeedbackDetailActivity.this, getString(R.string.pta));
                            }
                        }) {
                            @Override
                            protected Map<String, String> getParams() {
                                Map<String, String> params = new HashMap<String, String>();
                                params.put("token", Prefrence.getToken(getApplicationContext()));
                                params.put("feedback", feedback.getText().toString());
                                params.put("title", title.getText().toString());
                                return params;

                            }
                        };
                        AppController.getInstance().addToRequestQueue(multipartRequest);
                    }
                } else {
                    Util.showAlert(FeedbackDetailActivity.this, getString(R.string.please_check_internet));
                }
            }
        });*/