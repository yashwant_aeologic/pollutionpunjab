package com.aeologic.cctv.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;


import com.aeologic.cctv.Adapter.FAQAdapter;
import com.aeologic.cctv.Adapter.HidingScrollListener;
import com.aeologic.cctv.R;
import com.aeologic.cctv.Receiver.ConnectionChangeReceiver;


import java.util.ArrayList;
import java.util.List;


/**
 * Created by Sachin Varshney on 04-Nov-16.
 */

public class FAQActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    ConnectionChangeReceiver connectionChangeReceiver;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faqs);
        initViews();
        setupToolbar();

//        connectionChangeReceiver= new ConnectionChangeReceiver();
//        this.registerReceiver(connectionChangeReceiver,new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));

        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(new FAQAdapter(FAQActivity.this, getFaqList()));

        recyclerView.addOnScrollListener(new HidingScrollListener() {
            @Override
            public void onHide() {
                //helpCv.animate().translationY(-helpCv.getHeight()).setInterpolator(new AccelerateInterpolator(2));
            }

            @Override
            public void onShow() {
                //helpCv.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2));
            }
        });
    }

    private List<String> getFaqList() {
        List<String> faqList = new ArrayList<String>();
        faqList.add("About Traffic Sentinel Scheme");
        faqList.add("Become a Traffic Sentinel");
        faqList.add("Violation list");
        faqList.add("Sample images of violation");
        faqList.add("Neccessary information");
        faqList.add("Action against Motorist");
        faqList.add("Reward point");
        faqList.add("Status of rejection");
        //---
        faqList.add("Reasons for rejection");
        faqList.add("Liability");
        faqList.add("Low Bandwidth/No internet connection");
        faqList.add("Importance of Geolocation");
        faqList.add("Referal Reward Points");
        faqList.add("Redeem Reward Points");
        faqList.add("Blurred image of violation");
        faqList.add("Privacy");
        return faqList;
    }

    private void initViews() {
        recyclerView = (RecyclerView) findViewById(R.id.help_recycler);
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        setTitle(getString(R.string.help_and_faqs));
    }

    @Override
    protected void onStart() {
        super.onStart();
//        this.registerReceiver(connectionChangeReceiver,new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
//            unregisterReceiver(connectionChangeReceiver);
        }catch (IllegalArgumentException e){
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        goToBack();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                goToBack();
                break;
        }
        return true;
    }

    private void goToBack() {
        startActivity(new Intent(FAQActivity.this, HomeActivity.class));
        finish();
    }
}
