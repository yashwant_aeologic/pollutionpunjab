package com.aeologic.cctv.Activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.aeologic.cctv.Connection.ConnectionDetector;
import com.aeologic.cctv.OfficersActivities.ManagerLoginActivity;
import com.aeologic.cctv.R;
import com.aeologic.cctv.Utility.Prefrence;
import com.aeologic.cctv.Utility.Util;


import java.util.regex.Pattern;


public class UserLoginActivity extends AppCompatActivity {
    public static final String MyPREFERENCES = "MyPrefs";
    private static final int MY_PERMISSIONS_REQUEST_ACCESS = 1;
    private static final int MY_PERMISSION_SMS_ACCESS = 2;
    Button otp;
    /* EditText emailTxt;
     EditText password;*/
    EditText phoneNumber, countryCode;
    boolean doubleBackToExitPressedOnce = false;
    private ConnectionDetector detector;
    String device_id;
    boolean forgotpassStatus = false;
    SharedPreferences sharedpreferences;
    private TextView managerLogin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initViews();
        countryCode.setKeyListener(null);
        phoneNumber.setFocusable(true);
        phoneNumber.requestFocus();

        if (checkPhoneStatePermission()) {
            saveImei();
        }

        if (getIntent() != null) {
            forgotpassStatus = getIntent().getBooleanExtra("forgotPassStatus", false);
        }

       /* showPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                } else {
                    password.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
            }
        });*/

        otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (detector.isConnectingToInternet()) {
                    if (countryCode.getText().toString().trim().matches("^\\+[0-9]{2}$")) {
                        if (phoneNumber.getText().toString().trim().length() != 0) {
                            if (isValidMobile(phoneNumber.getText().toString())) {

                                sendToOtpActivity();


                            } else {
                                Util.showAlert(UserLoginActivity.this, getResources().getString(R.string.inavlid_mob));
                            }
                        } else {
                            Util.showAlert(UserLoginActivity.this, getResources().getString(R.string.blank_mob));
                        }
                    } else {
                        Util.showAlert(UserLoginActivity.this, "Not valid Country code");
                    }

                } else {
                    Util.showAlert(UserLoginActivity.this, getResources().getString(R.string.please_check_internet));
                }


            }
        });

        managerLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(UserLoginActivity.this, ManagerLoginActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                finish();
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
//        Util.finishActivity(ManagerLoginActivity.this);
    }

    private boolean isValidMobile(String phone) {
        if (phone.length() != 10) {
            return false;
        } else {
            return android.util.Patterns.PHONE.matcher(phone).matches();
        }
    }

    private boolean isValidEmaillId(String email) {

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }


    private void initViews() {
        otp = (Button) findViewById(R.id.otp);
        // emailTxt = (EditText) findViewById(R.id.number_et);
        // password = (EditText) findViewById(R.id.pwd_et);
        phoneNumber = (EditText) findViewById(R.id.number_et);
        countryCode = (EditText) findViewById(R.id.code);
        detector = new ConnectionDetector(getApplicationContext());
        managerLogin = (TextView) findViewById(R.id.hyperlink);
        // showPassword = (CheckBox) findViewById(R.id.checkbox);

    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, getResources().getString(R.string.confirm_exit), Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    private void saveImei() {
        TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        device_id = tm.getDeviceId();
        Prefrence.setIMEI(UserLoginActivity.this, device_id);
    }


    public boolean checkPhoneStatePermission() {
        if (ActivityCompat.checkSelfPermission(UserLoginActivity
                .this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {


            ActivityCompat.requestPermissions(UserLoginActivity.this,
                    new String[]{Manifest.permission.READ_PHONE_STATE},
                    MY_PERMISSIONS_REQUEST_ACCESS);
            return false;
        }
        return true;
    }

    public boolean checkSmsPermission() {
        if (ActivityCompat.checkSelfPermission(UserLoginActivity
                .this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED) {


            ActivityCompat.requestPermissions(UserLoginActivity.this,
                    new String[]{Manifest.permission.READ_SMS},
                    MY_PERMISSION_SMS_ACCESS);
            return false;
        }
        return true;
    }

    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission is granted do the task here
                    saveImei();
                }
                break;

            }


        }
    }

    private void sendToOtpActivity() {
        Intent intent = new Intent(UserLoginActivity.this, OtpActivity.class);
        intent.putExtra("number", phoneNumber.getText().toString());
        intent.putExtra("COUNTRY_CODE", countryCode.getText().toString());
        intent.putExtra("forgotPassStatus", forgotpassStatus);
        startActivity(intent);
        finish();
    }


}
