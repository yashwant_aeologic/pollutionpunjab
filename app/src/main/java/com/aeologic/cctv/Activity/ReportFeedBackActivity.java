package com.aeologic.cctv.Activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.aeologic.cctv.Connection.ConnectionDetector;
import com.aeologic.cctv.R;
import com.aeologic.cctv.Utility.AppController;
import com.aeologic.cctv.Utility.Prefrence;
import com.aeologic.cctv.Utility.Util;
import com.aeologic.cctv.Utility.VolleyMultipartRequest;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ReportFeedBackActivity extends AppCompatActivity {

    private static final String TAG = ReportFeedBackActivity.class.getName();
    Toolbar toolbar;
    private EditText commentET;
    private Button submitBtn;
    private int complaintId;
    private ConnectionDetector detector;
    private String feedback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_feed_back);

        initView();


        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Complaint Feedback");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        if (getIntent() != null) {
            complaintId = getIntent().getIntExtra("complaintId", 0);
            feedback = getIntent().getStringExtra("feedback");
        }

        commentET.setText(feedback);

        Log.e(TAG, "onCreate: ID "+ complaintId );
        Log.e(TAG, "onCreate: FEEDBACK "+ feedback );


        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (detector.isConnectingToInternet()) {
                    if (isValidate()) {
                        //call the feedback api

                        submitBtn.setEnabled(false);
                        submitBtn.setAlpha(0.4f);

                        final ProgressDialog progressDialog = new ProgressDialog(ReportFeedBackActivity.this);
                        progressDialog.setCancelable(false);
                        progressDialog.setMessage(getString(R.string.please_wait));
                        progressDialog.show();
                        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST,
                                getString(R.string.COMPLAIN_FEEDBACK_API), new Response.Listener<NetworkResponse>() {
                            @Override
                            public void onResponse(NetworkResponse response) {
                                String resultResponse = new String(response.data);
                                Log.e(TAG, "onResponse: Response: " + resultResponse);
                                progressDialog.dismiss();
                                try {
                                    JSONObject jsonObject = new JSONObject(resultResponse);
                                    //{"status":1,"message":"Feedback submitted successfully"}
                                    String status = jsonObject.getString("status");
                                    if (status.equals("1")) {
                                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ReportFeedBackActivity.this);
                                        alertDialog.setMessage(jsonObject.getString("message"));
                                        alertDialog.setCancelable(false);
                                        alertDialog.setPositiveButton(getResources().getString(R.string.Ok), new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                                Intent intent = new Intent(ReportFeedBackActivity.this, ComplaintListActivity.class);
                                                intent.putExtra("From", "REOPEN");
                                                startActivity(intent);
                                                overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);
                                                finish();
                                                /* dialog.dismiss();
//                                                FeedbackListActivity.feedbackListActivity.finish();
//                                                startActivity(new Intent(ReportFeedBackActivity.this, FeedbackListActivity.class));
                                                finish();*/

                                            }
                                        });
                                        alertDialog.show();
                                    } else {
                                        String message = jsonObject.getString("message");
                                        if (message != null) {
                                            if (message.contains("Your session has expired"))
                                                Util.showSessionExpiredDialog(ReportFeedBackActivity.this);
                                        } else {
                                            Util.showAlert(ReportFeedBackActivity.this, getString(R.string.pta));
                                        }
                                        submitBtn.setAlpha(1.0f);
                                        submitBtn.setEnabled(true);
                                    }
                                } catch (JSONException e) {
                                    submitBtn.setAlpha(1.0f);
                                    submitBtn.setEnabled(true);
                                    Util.showAlert(ReportFeedBackActivity.this, getString(R.string.pta));
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                error.printStackTrace();
                                progressDialog.hide();
                                submitBtn.setAlpha(1.0f);
                                submitBtn.setEnabled(true);
                                Util.showAlert(ReportFeedBackActivity.this, getString(R.string.pta));
                            }
                        }) {
                            @Override
                            protected Map<String, String> getParams() {
                                Map<String, String> params = new HashMap<String, String>();
                                params.put("token", Prefrence.getToken(getApplicationContext()));
                                params.put("feedback", commentET.getText().toString());
                                params.put("complaint_no", String.valueOf(complaintId));
//                                params.put("title", title.getText().toString());
                                return params;

                            }
                        };
                        multipartRequest.setRetryPolicy(new RetryPolicy() {
                            @Override
                            public int getCurrentTimeout() {
                                return 50000;
                            }

                            @Override
                            public int getCurrentRetryCount() {
                                return 50000;
                            }

                            @Override
                            public void retry(VolleyError error) throws VolleyError {
                                error.printStackTrace();
                            }
                        });
                        AppController.getInstance().addToRequestQueue(multipartRequest);
                    }
                } else {
                    Util.showAlert(ReportFeedBackActivity.this, getString(R.string.please_check_internet));
                }
            }
        });
    }

    private boolean isValidate() {
        if (commentET.getText().toString().isEmpty()) {
            commentET.setError("Please provide the Description!");
            return false;
        }
        return true;
    }

    private void initView() {
        commentET = (EditText) findViewById(R.id.comment_et);
        submitBtn = (Button) findViewById(R.id.submit_btn);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        detector = new ConnectionDetector(ReportFeedBackActivity.this);
        commentET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (s.length() > 500) {
                    Toast.makeText(ReportFeedBackActivity.this, "Character limit exceeded", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}
