package com.aeologic.cctv.Utility;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;


import com.aeologic.cctv.Activity.UserLoginActivity;
import com.aeologic.cctv.R;
import com.aeologic.cctv.Service.NetworkJobService;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by u104 on 12/12/16.
 */

public class Util {
    private static final String TAG = Util.class.getSimpleName();
    private static LocationManager locationManager;
    private static boolean isGPSEnabled;

    public static String formatDateFromTimeStamp(String inputFormat, String outputFormat, String inputDate) {

        Date parsed = null;
        String outputDate = "";

        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, java.util.Locale.getDefault());
        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, java.util.Locale.getDefault());

        try {
            parsed = df_input.parse(inputDate);
            outputDate = df_output.format(parsed);

        } catch (ParseException e) {
            Log.e("exception", "ParseException - dateFormat");
        }

        return outputDate;

    }

    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String source) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(source, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(source);
        }
    }

    public static String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    public static String getCurrentDate() {
        String date = new SimpleDateFormat("dd/MM/yyyy").format(new Date());
        return date;
    }

    public static String getCurrentTime() {
        String date = new SimpleDateFormat("hh:mm a").format(new Date());
        return date;
    }

    public static void showAlert(final Context context, final String msg) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setMessage(msg);
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton(context.getResources().getString(R.string.Ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        alertDialog.show();
    }

    public static final String md5Password(final String s) {
        final String MD5 = "MD5";
        try {
// Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

// Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static void finishActivity(Activity activity) {
        activity.finish();
    }

    @SuppressWarnings("deprecation")
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public static boolean isAirplaneModeOn(Context context) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return Settings.System.getInt(context.getContentResolver(),
                    Settings.System.AIRPLANE_MODE_ON, 0) != 0;
        } else {
            return Settings.Global.getInt(context.getContentResolver(),
                    Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
        }
    }

    public static String fileSize(File file) {
        return readableFileSize(file.length());
    }

    public static String readableFileSize(long size) {
        if (size <= 0) return size + " B";
        final String[] units = new String[]{"B", "KB", "MB", "GB", "TB"};
        int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
        return new DecimalFormat("#,##0.##").format(size / Math.pow(1024, digitGroups))
                + " "
                + units[digitGroups];
    }

    public static void showSessionExpiredDialog(final Context context) {
        final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setCancelable(false);
        alertDialog.setTitle("Session expired!");
        alertDialog.setMessage(context.getString(R.string.token_expired));
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Exit", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
                ((Activity) context).finish();
            }
        });

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Login now", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                alertDialog.dismiss();
                Prefrence.setToken(context, "");
                //clear resgister preference
                Prefrence.setIsRegister(context, false);
                Intent intent = new Intent(context, UserLoginActivity.class);
                context.startActivity(intent);
                ((Activity) context).finish();
            }
        });
        alertDialog.show();
    }

    // schedule the start of the service every 10 - 30 seconds
    @RequiresApi(api = Build.VERSION_CODES.M)
    public static void scheduleJob(Context context) {
        ComponentName serviceComponent = new ComponentName(context, NetworkJobService.class);
        JobInfo.Builder builder = new JobInfo.Builder(0, serviceComponent);
//        builder.setMinimumLatency(1 * 1000); // wait at least
//        builder.setOverrideDeadline(3 * 1000); // maximum delay
        builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY); // require unmetered network
        //builder.setRequiresDeviceIdle(true); // device should be idle
        //builder.setRequiresCharging(false); // we don't care if the device is charging or not
        JobScheduler jobScheduler = context.getSystemService(JobScheduler.class);
        jobScheduler.schedule(builder.build());
    }

    public static Bitmap decodeMyFile(File f) {
        try {
            Bitmap tempBitmap = null;
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            tempBitmap = BitmapFactory.decodeStream(new FileInputStream(f), null, o);
            // The new size we want to scale to
            final int REQUIRED_SIZE = 640;
            // Find the correct scale value. It should be the logout of 2.
            int scale = 1;
            Matrix matrix = new Matrix();
            while (o.outWidth / scale >= REQUIRED_SIZE
                    && o.outHeight / scale >= REQUIRED_SIZE) {
                if (scale <= 8) {
                    scale *= 2;
                } else {
                    break;
                }
            }
            Log.e(TAG, "decodeMyFile:scale " + scale);
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            ExifInterface ei = new ExifInterface(f.toString());
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    matrix.postRotate(90);
                    tempBitmap = BitmapFactory.decodeStream(new FileInputStream(f),
                            null, o2);
                    tempBitmap = Bitmap.createBitmap(tempBitmap, 0, 0,
                            tempBitmap.getWidth(), tempBitmap.getHeight(), matrix,
                            true);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    matrix.postRotate(180);
                    tempBitmap = BitmapFactory.decodeStream(new FileInputStream(f),
                            null, o2);
                    tempBitmap = Bitmap.createBitmap(tempBitmap, 0, 0,
                            tempBitmap.getWidth(), tempBitmap.getHeight(), matrix,
                            true);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    matrix.postRotate(270);
                    tempBitmap = BitmapFactory.decodeStream(new FileInputStream(f),
                            null, o2);
                    tempBitmap = Bitmap.createBitmap(tempBitmap, 0, 0,
                            tempBitmap.getWidth(), tempBitmap.getHeight(), matrix,
                            true);
                    break;
                default:
                    matrix.postRotate(0);
                    tempBitmap = BitmapFactory.decodeStream(new FileInputStream(f),
                            null, o2);
                    tempBitmap = Bitmap.createBitmap(tempBitmap, 0, 0,
                            tempBitmap.getWidth(), tempBitmap.getHeight(), matrix,
                            true);
                    break;
            }
            Log.e(TAG, "decodeMyFile: " + tempBitmap.getWidth() + "::" + tempBitmap.getHeight());
            return tempBitmap;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getOrientation(Uri uri) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        String orientation = "landscape";
        try {
            String image = new File(uri.getPath()).getAbsolutePath();
            BitmapFactory.decodeFile(image, options);
            int imageHeight = options.outHeight;
            int imageWidth = options.outWidth;
            if (imageHeight > imageWidth) {
                orientation = "portrait";
            }
        } catch (Exception e) {
            //Do nothing
        }
        return orientation;
    }

    public static String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "pollution/Images");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;
    }

    public static String compressImage(Context context, String imageUri) {
        String filePath = getRealPathFromURI(context, imageUri);
        Bitmap scaledBitmap = null;

        scaledBitmap = decodeMyFile(new File(filePath));
        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);
            Log.e(TAG, "compressImage: " + scaledBitmap.getWidth() + "x" + scaledBitmap.getHeight());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return filename;

    }

    public static String getRealPathFromURI(Context context, String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = context.getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public static void hideKeyboard(Context context, EditText comment) {
        InputMethodManager mImMan = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        mImMan.hideSoftInputFromWindow(comment.getWindowToken(), 0);
    }

    public static boolean checkGPS(final Context context, final boolean shouldClose) {
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

//Added by arun on 03/09/2018
        GoogleApiClient mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API).build();
        mGoogleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {

                    case LocationSettingsStatusCodes.SUCCESS:

// Toast.makeText(getApplicationContext(),"yes",Toast.LENGTH_SHORT).show();
                        Log.d("onlocation", "yes");


// All location settings are satisfied. The client can initialize location
// requests here.
                        isGPSEnabled = true;
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:

//Toast.makeText(getApplicationContext(),"yes_required",Toast.LENGTH_SHORT).show(); // Location settings are not satisfied. But could be fixed by showing the user
// a dialog_timer.
                        try {
// Show the dialog_timer by calling startResolutionForResult(),
// and check the result in onActivityResult().
                            status.startResolutionForResult(
                                    (Activity) context, 1000);
                        } catch (IntentSender.SendIntentException e) {
// Ignore the error.
                        }
                        break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:

//Toast.makeText(getApplicationContext(),"yesxsaxd_required",Toast.LENGTH_SHORT).show();
// Location settings are not satisfied. However, we have no way to fix the
// settings so we won't show the dialog_timer.
                        break;
                }
            }
        });
        return isGPSEnabled;


    }
    public static String parseDate(String timestamp, String sourceFormat, String targetFormat) {
        try {
            return new SimpleDateFormat(targetFormat).format(new SimpleDateFormat(sourceFormat).parse(timestamp));
        } catch (ParseException e) {
            e.printStackTrace();
            return timestamp;
        }
    }

}