package com.aeologic.cctv.Utility;

import android.content.Context;
import android.content.DialogInterface;

public final class CommonUtilities {
    public static final String TAG = "Common Utility";

/*    public static void showAlertDialog(Context context, String message, String positive, String negative) {
        final Dialog dialog = new Dialog(context, R.style.CustomStyle);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_alert);

        TextView text = (TextView) dialog.findViewById(R.id.text);
//        Typeface custom_font = Typeface.createFromAsset(context.getAssets(), "font/hroman.ttf");

//        text.setTypeface(custom_font);
        text.setText(message);
        Button dialognoBtn = (Button) dialog.findViewById(R.id.noBtn);
//        dialognoBtn.setTypeface(custom_font);
        if (negative.length() > 0) {
            dialognoBtn.setText(negative);
            dialognoBtn.setVisibility(View.VISIBLE);
            dialognoBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
        } else {
            dialognoBtn.setVisibility(View.GONE);
        }
        Button dialogButton = (Button) dialog.findViewById(R.id.okBtn);
        dialogButton.setText(positive);
//        dialogButton.setTypeface(custom_font);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        dialog.show();

    }*/

    public static void showDialog(Context context, String message) {
        new android.app.AlertDialog.Builder(context)
                .setCancelable(false)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.cancel();
                    }
                })

                .show();
    }

}
