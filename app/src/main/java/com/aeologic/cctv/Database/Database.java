package com.aeologic.cctv.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


import com.aeologic.cctv.Utility.Util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;


public class Database extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "TrafficMitra";
    Context context;
    private static final String TAG = "Database";

    public static final String TABLE_NAME = "TrafficMitraTable";
    public static final String ID = "id";
    public static final String IMAGE_URI = "imageUri";
    public static final String VIDEO_URI = "videoUri";
    public static final String AUDIO_URI = "audioUri";
    public static final String RCNUMBER = "rcNumner";
    public static final String OFFENCE_NAME = "offenceName";
    public static final String CITY = "city";
    public static final String CITY_ID = "city_id";
    public static final String STATE = "state";
    public static final String COMMENT = "comment";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String ADDRESS = "address";
    public static final String MANUAL_ADDRESS = "manualAddress";
    public static final String CREATED_AT = "createdAt";
    public static final String TIMESTAMP = "timestamp";
    public static final String SYNC = "sync";

    public static final String OFFENCE_TABLE_NAME = "OffenceTable";
    public static final String ACTIVE_STATE_TABLE_NAME = "ActiveStateTable";
    public static final String OFFENCE_ID = "offenceId";
    public static final String SERVER_IMAGE_PATH = "serverImagePath";
    public static final String LOCAL_IMAGE_PATH = "localImagePath";
    public static final String REWARD_POINT = "rewardPoint";


    public Database(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
//Create Main Table
        final String TABLE_QUERY = "CREATE TABLE "
                + TABLE_NAME
                + " ("
                + ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + RCNUMBER + " TEXT, "
                + OFFENCE_NAME + " TEXT, "
                + COMMENT + " TEXT, "
                + IMAGE_URI + " TEXT, "
                + VIDEO_URI + " TEXT, "
                + AUDIO_URI + " TEXT, "
                + LATITUDE + " DOUBLE DEFAULT NULL, "
                + LONGITUDE + " DOUBLE DEFAULT NULL, "
                + ADDRESS + " TEXT, "
                + MANUAL_ADDRESS + " TEXT, "
                + CREATED_AT + " TEXT, "
                + TIMESTAMP + " TEXT, "
                + SYNC + " INTEGER DEFAULT 0 " +
                ");";

        Log.e("MAIN QUERY", TABLE_QUERY);
        db.execSQL(TABLE_QUERY);
        Log.e(TAG, "onCreate: Main table created successfully");

        //Create offence table
        final String OFFENCE_TABLE_QUERY = "CREATE TABLE "
                + OFFENCE_TABLE_NAME
                + " ("
                + ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + OFFENCE_ID + " TEXT,"
                + OFFENCE_NAME + " TEXT,"
                + REWARD_POINT + " TEXT,"
                + SERVER_IMAGE_PATH + " TEXT,"
                + LOCAL_IMAGE_PATH + " TEXT"
                + ");";

        Log.e("OFFENCE TABLE QUERY", OFFENCE_TABLE_QUERY);
        db.execSQL(OFFENCE_TABLE_QUERY);
        Log.e(TAG, "onCreate: Offence table created successfully");

        final String ACTIVE_STATE_TABLE_QUERY = "CREATE TABLE "
                + ACTIVE_STATE_TABLE_NAME
                + " ("
                + ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + CITY_ID + " TEXT,"
                + CITY + " TEXT,"
                + STATE + " TEXT"

                + ");";

        Log.e("OFFENCE TABLE QUERY", ACTIVE_STATE_TABLE_QUERY);
        db.execSQL(ACTIVE_STATE_TABLE_QUERY);
        Log.e(TAG, "onCreate: Offence table created successfully");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        // Create tables again
        onCreate(db);
    }

    public boolean AddDataValueOffline(String rcNumber, String offense, String comment, String imageUri,
                                       String videoUri,String audioUri, String address, String manualAddress, double lat, double lng, int sync, String timeStamp) {

        try {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
//            values.put(KEY_TOKEN, token);
            values.put(RCNUMBER, rcNumber);
            values.put(OFFENCE_NAME, offense);
            values.put(COMMENT, comment);
            values.put(LATITUDE, lat);
            values.put(LONGITUDE, lng);
            values.put(IMAGE_URI, imageUri);
            values.put(VIDEO_URI, videoUri);
            values.put(AUDIO_URI, audioUri);
            values.put(ADDRESS, address);
            values.put(MANUAL_ADDRESS, manualAddress);
            values.put(SYNC, sync);
            values.put(CREATED_AT, timeStamp);
            values.put(TIMESTAMP, Util.getDateTime());
            Log.e(TAG, "AddDataValueOffline: " + values.toString());
            long id = db.insert(TABLE_NAME, null, values);
            Log.e("AddValueDatabase: ", String.valueOf(id));
            db.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }


    public void saveOffenceList(ArrayList<HashMap<String,String>> offenceList) {

        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values;
            HashMap offenceData;
            for (int i = 0; i < offenceList.size(); i++) {
                values = new ContentValues();
                offenceData = offenceList.get(i);
                values.put(OFFENCE_NAME, offenceData.get("offenceName").toString());
                values.put(OFFENCE_ID, offenceData.get("offenceId").toString());
                values.put(SERVER_IMAGE_PATH, offenceData.get("serverImagePath").toString());
                long id = db.insert(OFFENCE_TABLE_NAME, null, values);
//                Log.e("SaveOffenceList: ", String.valueOf(id));
            }
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<HashMap<String, String>> getOffenceList() {
        ArrayList<HashMap<String, String>> arrayList = new ArrayList<>();
        HashMap hashMap;
        SQLiteDatabase database = this.getReadableDatabase();
        String offenceQuery = "SELECT " +
                OFFENCE_NAME +
                "," + OFFENCE_ID +
                "," + LOCAL_IMAGE_PATH +
                "," + SERVER_IMAGE_PATH +
                " FROM " + OFFENCE_TABLE_NAME;
        Cursor cursor = database.rawQuery(offenceQuery, null);
        if (cursor.moveToFirst()) {
            do {
                hashMap = new HashMap();

                hashMap.put("offenceName", cursor.getString(0));
                hashMap.put("offenceId", cursor.getString(1));
                hashMap.put("imagePath", cursor.getString(2));
                hashMap.put("serverImagePath", cursor.getString(3));
                Log.v("OFFENCE","ID: "+ cursor.getString(1)+"  <=>   NAME:"+cursor.getString(0));
                arrayList.add(hashMap);
            } while (cursor.moveToNext());
        }
        return arrayList;
    }

    public void saveActiveStateList(ArrayList<HashMap<String,String>> offenceList) {

        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values;
            HashMap offenceData;
            for (int i = 0; i < offenceList.size(); i++) {
                values = new ContentValues();
                offenceData = offenceList.get(i);
                values.put(CITY, offenceData.get("city").toString());
                values.put(CITY_ID, offenceData.get("city_id").toString());
                values.put(STATE, offenceData.get("state").toString());
                long id = db.insert(ACTIVE_STATE_TABLE_NAME, null, values);
               Log.e("SaveOffenceList: ", String.valueOf(id));
            }
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<HashMap<String, String>> getActiveStateList() {
        ArrayList<HashMap<String, String>> arrayList = new ArrayList<>();
        HashMap hashMap;
        SQLiteDatabase database = this.getReadableDatabase();
        String offenceQuery = "SELECT " +
                CITY +
                "," + CITY_ID +
                "," + STATE +
                " FROM " + ACTIVE_STATE_TABLE_NAME;
        Cursor cursor = database.rawQuery(offenceQuery, null);
        if (cursor.moveToFirst()) {
            do {
                hashMap = new HashMap();

                hashMap.put("city", cursor.getString(0));
                hashMap.put("city_id", cursor.getString(1));
                hashMap.put("state", cursor.getString(2));

             //   Log.v("OFFENCE","ID: "+ cursor.getString(1)+"  <=>   NAME:"+cursor.getString(0));
                arrayList.add(hashMap);
            } while (cursor.moveToNext());
        }
        return arrayList;
    }

    public ArrayList<String> getOffencesList() {
        ArrayList<String> list = new ArrayList<>();
        SQLiteDatabase database = this.getReadableDatabase();
        String offenceQuery = "SELECT " +
                OFFENCE_NAME +
                " FROM " + OFFENCE_TABLE_NAME;
        Cursor cursor = database.rawQuery(offenceQuery, null);
        if (cursor.moveToFirst()) {
            do {
                list.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }
        return list;
    }

    public LinkedHashMap<String, String> getOffencesMap() {
        LinkedHashMap<String, String> hashMap = new LinkedHashMap<String, String>();
        SQLiteDatabase database = this.getReadableDatabase();
        String offenceQuery = "SELECT " +
                OFFENCE_ID +"," +
                OFFENCE_NAME +
                " FROM " + OFFENCE_TABLE_NAME;
        Cursor cursor = database.rawQuery(offenceQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Log.v("OFFENCE","ID: "+ cursor.getString(0)+"  <=>   NAME:"+cursor.getString(1));
                hashMap.put(cursor.getString(0), cursor.getString(1));
            } while (cursor.moveToNext());
        }
        return hashMap;
    }


    public void updateOffenceData(String imageUri, String offenceId) {

        try {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(LOCAL_IMAGE_PATH, imageUri);
            db.update(OFFENCE_TABLE_NAME, values, OFFENCE_ID + " = ?",
                    new String[]{offenceId});
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public String getOffenceName(String[] offenceId) {
        SQLiteDatabase database = this.getReadableDatabase();
        String offenceName = "";
        for (int i = 0; i < offenceId.length; i++) {
            String offenceQuery = "SELECT " +
                    OFFENCE_NAME +
                    " FROM " + OFFENCE_TABLE_NAME + " WHERE " + OFFENCE_ID + "='" + offenceId[i] + "'";
            Cursor cursor = database.rawQuery(offenceQuery, null);
            if (cursor.moveToFirst()) {
                offenceName = offenceName + cursor.getString(0) + ",";
            }
        }

        if (!offenceName.isEmpty() && offenceName.charAt(offenceName.length() - 1) == ',') {
            offenceName = offenceName.substring(0, offenceName.length() - 1);
        }
        return offenceName;
    }

    public String getOffenceId(String[] offenceName) {
        SQLiteDatabase database = this.getReadableDatabase();
        String offenceId = "";
        for (int i = 0; i < offenceName.length; i++) {
            String offenceQuery = "SELECT " +
                    OFFENCE_ID +
                    " FROM " + OFFENCE_TABLE_NAME + " WHERE " + OFFENCE_NAME + "='" + offenceName[i] + "'";
            Cursor cursor = database.rawQuery(offenceQuery, null);
            if (cursor.moveToFirst()) {
                offenceId = offenceId + cursor.getString(0) + ",";
            }
        }

        if (!offenceId.isEmpty() && offenceId.charAt(offenceId.length() - 1) == ',') {
            offenceId = offenceId.substring(0, offenceId.length() - 1);
        }
        return offenceId;
    }

    public LinkedHashMap<String, String> getOffenceMap() {
        LinkedHashMap<String, String> offenceMap = new LinkedHashMap<>();
        SQLiteDatabase database = this.getReadableDatabase();
        String offenceQuery = "SELECT " +
                OFFENCE_ID +
                "," + OFFENCE_NAME +
                " FROM " + OFFENCE_TABLE_NAME;
        Cursor cursor = database.rawQuery(offenceQuery, null);
        if (cursor.moveToFirst()) {
            do {
                offenceMap.put(cursor.getString(0), cursor.getString(1));
            } while (cursor.moveToNext());
        }
        return offenceMap;
    }

    public void deleteOffenceTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(OFFENCE_TABLE_NAME, null, null);
        db.close();
    }

    public void deleteActiveStateTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(ACTIVE_STATE_TABLE_NAME, null, null);
        db.close();
    }

    public int getDataCount() {
        int count = 0;
        String countQuery = "SELECT  * FROM " + TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                count++;
            } while (cursor.moveToNext());
        }

        // return contact list
        return count;
    }

    // get details of a particular survey
    public HashMap<String, String> getDetails(String id) {
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            String query = "SELECT " + OFFENCE_NAME + "," + TIMESTAMP + "," + COMMENT + "," + IMAGE_URI + "," +
                    VIDEO_URI + "," + LATITUDE + "," + LONGITUDE + "," + ADDRESS + ","
                    + RCNUMBER + "," + MANUAL_ADDRESS + "," + CREATED_AT +
                    " FROM " + TABLE_NAME + " WHERE " + ID + " = '" + id + "'";
            Log.e("DetailQuery", query);
            Cursor cursor = db.rawQuery(query, null);
            HashMap<String, String> hashMap = null;
            if (!cursor.equals(null) && cursor.getCount() > 0) {
                cursor.moveToFirst();
                hashMap = new HashMap<String, String>();
                hashMap.put("offence", cursor.getString(0));
                hashMap.put("timestamp", cursor.getString(1));
                hashMap.put("comment", cursor.getString(2));
                hashMap.put("imageUri", cursor.getString(3));
                hashMap.put("videoUri", cursor.getString(4));
                hashMap.put("latitude", cursor.getString(5));
                hashMap.put("longitude", cursor.getString(6));
                hashMap.put("address", cursor.getString(7));
                hashMap.put("rcNumber", cursor.getString(8));
                hashMap.put("manualAddress", cursor.getString(9));
                hashMap.put("createdAt", cursor.getString(10));

                return hashMap;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public ArrayList<HashMap<String, String>> getDataList(int status) {
        ArrayList<HashMap<String, String>> arrayList = new ArrayList<>();

        SQLiteDatabase database = this.getReadableDatabase();
        String detailQuery = "SELECT " +
//                TOKEN + " ," +
                RCNUMBER + " ," +
                CREATED_AT + " ," +
                TIMESTAMP + " ," +
                ID +
                " FROM " + TABLE_NAME + " WHERE " + SYNC + " =" + status + " ORDER BY " + CREATED_AT + " DESC";
        Cursor cursor = database.rawQuery(detailQuery, null);
        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> responseData = new HashMap<>();
                responseData.put("rcNumber", cursor.getString(0));
                responseData.put("createdAt", cursor.getString(1));
                responseData.put("timeStamp", cursor.getString(2));
                responseData.put("id", cursor.getString(3));
                arrayList.add(responseData);
            } while (cursor.moveToNext());
        }
        return arrayList;
    }

    public ArrayList<HashMap<String, String>> getDetailList(int status) {
        ArrayList<HashMap<String, String>> arrayList = new ArrayList<>();

        SQLiteDatabase database = this.getReadableDatabase();
        String detailQuery = "SELECT " + ID + "," + OFFENCE_NAME + "," + CREATED_AT + "," + COMMENT + "," + IMAGE_URI + "," +
                VIDEO_URI + "," + AUDIO_URI + "," + LATITUDE + "," + LONGITUDE + "," + ADDRESS + "," + RCNUMBER + "," + MANUAL_ADDRESS
                + "," + MANUAL_ADDRESS + " FROM " + TABLE_NAME + " WHERE " + SYNC + " =" + status /*+ " ORDER BY " + CREATED_AT + " DESC"*/;
        Cursor cursor = database.rawQuery(detailQuery, null);
        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> responseData = new HashMap<>();
                responseData.put("id", cursor.getString(0));
                responseData.put("offenceName", cursor.getString(1));
                responseData.put("timestamp", cursor.getString(2));
                responseData.put("comment", cursor.getString(3));
                responseData.put("imageUri", cursor.getString(4));
                responseData.put("videoUri", cursor.getString(5));
                responseData.put("audioUri", cursor.getString(6));
                responseData.put("latitude", cursor.getString(7));
                responseData.put("longitude", cursor.getString(8));
                responseData.put("address", cursor.getString(9));
                responseData.put("rcNumber", cursor.getString(10));
                responseData.put("manualAddress", cursor.getString(11));
                arrayList.add(responseData);
            } while (cursor.moveToNext());
        }
        return arrayList;
    }


    public boolean deleteComplaint(String id) {
        SQLiteDatabase database = this.getWritableDatabase();
        int count = database.delete(TABLE_NAME, ID + "=?", new String[]{id});
        if (count > 0) {
            return true;
        } else return false;
    }

}
