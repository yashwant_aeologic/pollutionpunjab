package com.aeologic.cctv.Service;

import android.app.Service;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;


@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class NetworkJobService extends JobService {
    public static String TAG = NetworkJobService.class.getSimpleName();
    public NetworkJobService() {
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public boolean onStartJob(JobParameters params) {
        Log.e(TAG, "onStartJob: "+params.toString());
        Intent service = new Intent(getApplicationContext(), UploadOfflineSurvey.class);
        getApplicationContext().startService(service);
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return false;
    }
}
