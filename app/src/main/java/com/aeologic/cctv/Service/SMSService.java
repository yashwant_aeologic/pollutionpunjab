package com.aeologic.cctv.Service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

/**
 * Created by u104 on 27/3/17.
 */

public class SMSService extends Service {

    private static String TAG = "SMSService";

    @Override
    public IBinder onBind(Intent arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void onStart(Intent intent, int startId) {
        // TODO Auto-generated method stub
        super.onStart(intent, startId);
        Log.d(TAG, "SMSService started");

        Intent i = new Intent("android.provider.Telephony.SMS_RECEIVED").putExtra("some_msg", "I will be sent!");
        this.sendBroadcast(i);

        this.stopSelf();
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        Log.d(TAG, "SMSService destroyed");
    }

}
