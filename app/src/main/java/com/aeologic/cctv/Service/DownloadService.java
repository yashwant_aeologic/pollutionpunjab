package com.aeologic.cctv.Service;

import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.aeologic.cctv.Database.Database;
import com.aeologic.cctv.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import static android.content.ContentValues.TAG;

/**
 * Created by successive on 5/1/17.
 */

public class DownloadService extends Service {

    ArrayList<HashMap<String, String>> offenceListMap;
    public String outPutImagePath;
    Database database;
//    private DBhelper DbHelper;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        try {
            offenceListMap = (ArrayList<HashMap<String, String>>) intent.getSerializableExtra("offenceListMap");

            Log.e("DownloadService", "db " + offenceListMap.size());
            database = new Database(getApplicationContext());
            storeImageDb(offenceListMap);
        } catch (Exception e) {
            stopSelf();
        }
        return START_STICKY;
    }


    public void storeImageDb(final ArrayList<HashMap<String, String>> offenceListMap) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                int size = offenceListMap.size();

                HashMap hashMap;
                for (int i = 0; i < size; i++) {
                    hashMap = offenceListMap.get(i);
                    Bitmap bitmap = null;
                    try {
                        // Log.e("before list size=","db"+imagelist.get(i));
                        String storagePath = getString(R.string.offence_image_path);
                        bitmap = getBitmap(storagePath + hashMap.get("imagePath").toString());
                        //Glide.with(this).load("https://www.google.es/images/srpr/logo11w.png").asBitmap().into(-1, -1).get();

                        Log.e("list size=", "db" + bitmap);
                        outPutImagePath = getImagePath();
                        Log.e("list size=", "db" + outPutImagePath);
                        saveFile(bitmap, outPutImagePath, hashMap.get("offenceId").toString());

                    } catch (Exception e) {
                        Log.e("catch list size=", "db" + outPutImagePath);
                        Log.e("catch size=", "db" + bitmap);
                    }

                }

            }
        });

        thread.start();
    }


    public void saveFile(Bitmap b, String outPutImagePath, String offenceId) {

        try {


            OutputStream fOut = null;
            File file = new File(outPutImagePath);

            if (file.exists())
                file.delete();

            file.createNewFile();
            fOut = new FileOutputStream(file);
            // 100 means no compression, the lower you go, the stronger the compression
            b.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();

            // save image path in database
            database.updateOffenceData(outPutImagePath, offenceId);


        } catch (FileNotFoundException e) {
            Log.d(TAG, "file not found");
            e.printStackTrace();
        } catch (IOException e) {
            Log.d(TAG, "io exception");
            e.printStackTrace();
        }

    }

    public String getImagePath() {
        File dir;


        String file_path = "/data/data/" + getPackageName() + "/ImagePath";
        dir = new File(file_path);
        //  if(!dir.exists())
        dir.mkdirs();

        String outputFile = dir.getAbsolutePath() + "/img" + System.currentTimeMillis() + ".png";
        return outputFile;

    }

    public Bitmap getBitmap(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(input);
            return bitmap;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("Download Service: ", "onDestroy: ");
    }
}
