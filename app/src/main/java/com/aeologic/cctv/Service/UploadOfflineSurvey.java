package com.aeologic.cctv.Service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;


import com.aeologic.cctv.Activity.ComplaintListActivity;
import com.aeologic.cctv.Connection.ConnectionDetector;
import com.aeologic.cctv.Database.Database;
import com.aeologic.cctv.Fragment.InProgressFragment;
import com.aeologic.cctv.R;
import com.aeologic.cctv.Utility.MultipartUtility;
import com.aeologic.cctv.Utility.Prefrence;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by u104 on 5/7/16.
 */
public class UploadOfflineSurvey extends Service {
    private static final String TAG = "UploadOfflineSurvey";
    private ArrayList<HashMap<String, String>> DataList;
    private Database db;
    private ConnectionDetector detector;
    public static boolean isServiceRunning = false;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG, "onStartCommand: ");
        isServiceRunning = true;
        detector = new ConnectionDetector(getApplicationContext());
        if (detector.isConnectingToInternet()) {
            getDataListFromDatabase();
        }
        return START_NOT_STICKY;
    }

    private void getDataListFromDatabase() {
        db = new Database(getApplicationContext());

        DataList = db.getDetailList(0);
        Log.e("Offline Records", "getPorListFromDatabase: " + DataList.toString());
        if (DataList.size() > 0) {
//            upload data to server
            new UploadData().execute(UploadOfflineSurvey.this);
        } else {
            //stop the service
            Log.d(TAG, "Upload data service is stopped ");
            stopSelf();
        }
    }

/*    public void uploadData() {
        final HashMap<String, String> singleRecord = DataList.get(0);
        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST
                , getString(R.string.UploadApi)
                , new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                String resultResponse = new String(response.data);
                Log.e(TAG, "onResponse: Response: " + resultResponse);
                try {
//                                JSONArray jsonArray = new JSONArray(String.valueOf(resultResponse));
                    JSONObject jsonObject = new JSONObject(resultResponse);
                    String status = jsonObject.getString("Status");
                    if (status.equals("Success")) {
                        db.deleteComplaint(singleRecord.get("id").toString());
                        DataList.remove(0);
                        if (DataList.size() > 0) {
                            uploadData();
                        } else {
                            stopSelf();
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Latitude", singleRecord.get("lat").toString());
                Log.e(TAG, "getParams: Latitude: " + singleRecord.get("lat").toString());
                params.put("Longitude", singleRecord.get("lng").toString());
                Log.e(TAG, "getParams: Latitude: " + singleRecord.get("lng").toString());
                String imageFile = SendComplainActivity.getEncodedString(singleRecord.get("imageUri").toString());
                params.put("ImageFile", imageFile);
                if (singleRecord.get("comment") != null && !singleRecord.get("comment").toString().isEmpty()) {
                    params.put("comment", singleRecord.get("comment"));
                    Log.e(TAG, "getParams: comment: " + singleRecord.get("comment").toString());
                } else {
                    params.put("comment", "");
                }
                return params;

            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                // file name could found file base or direct access from real path
                // for now just get bitmap data from ImageView
                if (singleRecord.get("videoUri") != null && !singleRecord.get("videoUri").toString().isEmpty()) {
                    byte[] videoData = SendComplainActivity.convertFileToBiteArray(singleRecord.get("videoUri").toString());
                    params.put("VideoFile", new DataPart("Video", videoData, "image/jpeg"));
                }

                return params;
            }
        };

                    *//*multipartRequest.setRetryPolicy(new DefaultRetryPolicy(1000 * 60, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));*//*

        AppController.getInstance().addToRequestQueue(multipartRequest);

    }*/

    public class UploadData extends AsyncTask<Context, Integer, Boolean> {

        private HashMap<String, String> singleRecord;

        @Override
        protected void onPreExecute() {
            singleRecord = DataList.get(0);
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(Context... params) {


            try {
                MultipartUtility utility = new MultipartUtility(getResources().getString(R.string.CREATE_COMPLAINT_API), "UTF-8");
                Log.v("OTP_URL", getResources().getString(R.string.CREATE_COMPLAINT_API));
                utility.addFormField("token", Prefrence.getToken(getApplicationContext()));
                utility.addFormField("other_offence", singleRecord.get("rcNumber"));
                utility.addFormField("offences", singleRecord.get("offenceName"));
                utility.addFormField("comment", singleRecord.get("comment"));
                if (singleRecord.get("imageUri") != null) {
                    utility.addFilePart("photo", new File(singleRecord.get("imageUri")));
                } else {
                    utility.addFormField("photo", null);
                }

                if (singleRecord.get("videoUri") != null) {
                    utility.addFilePart("video", new File(singleRecord.get("videoUri")));
                } else {
                    utility.addFormField("video", null);
                }

                if (singleRecord.get("audioUri") != null) {
                    utility.addFilePart("audio", new File(singleRecord.get("audioUri")));
                } else {
                    utility.addFormField("audio", null);
                }

                utility.addFormField("address", singleRecord.get("address"));
                utility.addFormField("latitude", singleRecord.get("latitude"));
                utility.addFormField("longitude", singleRecord.get("longitude"));
                utility.addFormField("survey_date", singleRecord.get("timestamp"));
                utility.addFormField("survey_mode", "offline");
                utility.addFormField("reward_point", "1");
                utility.addFormField("is_android", "1");
                utility.addFormField("manual_address", singleRecord.get("manualAddress"));

                String response = utility.finish();
                if (response != null) {
                    Log.v("SERVER_RESPONSE", "" + singleRecord.get("id") + response);
                    JSONObject object = new JSONObject(response);
                    if (object.getString("status").equals("1")) {
                        return true;
                    }
//                    sendDetailResponse(response, Integer.parseInt(singleRecord.get("id")));
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();

            }
            return null;
        }

        private HashMap sendDetailResponse(String response, int s) {
            try {
                HashMap<String, String> responseData = new HashMap<>();
                JSONObject object = new JSONObject(response);
                responseData.put("status", object.getString("status"));
                responseData.put("message", object.getString("message"));
                if (object.getString("status").equals("1")) {
                    db.deleteComplaint(String.valueOf(s));

                }
                return responseData;
            } catch (JSONException je) {
                je.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if (result != null) {
                Log.d(TAG, "onPostExecute: Offence upload successfully");
                db.deleteComplaint(singleRecord.get("id").toString());
                DataList.remove(0);
                if (ComplaintListActivity.isComplaintListActive){
                    InProgressFragment.mProgressList.remove(0);
//                    InProgressFragment.adapter.notifyItemRemoved(0);
                    InProgressFragment.adapter.notifyDataSetChanged();
                }
                if (DataList.size() > 0) {
                    new UploadData().execute(UploadOfflineSurvey.this);
                }
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isServiceRunning = false;
    }
}
