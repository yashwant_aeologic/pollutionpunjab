package com.aeologic.cctv.Async;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.aeologic.cctv.Activity.ChangePasswordActivity;
import com.aeologic.cctv.Activity.UserLoginActivity;
import com.aeologic.cctv.R;
import com.aeologic.cctv.Utility.MultipartUtility;
import com.aeologic.cctv.Utility.Prefrence;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by u104 on 4/8/16.
 */
public class ChangePasswordAsyncTask extends AsyncTask<String, Integer, HashMap<String, String>> {
    private static final String TAG = "GetOtpAsyncTask";
    private Context context;
    String phoneNumber;
    ProgressDialog progressDialog;
    ImageView resendSMS;
    TextView otpTimer;
    Button changePassword;
    TextView msg;


    public ChangePasswordAsyncTask(Context context, Button changePassword) {
        this.context = context;
        this.changePassword = changePassword;

    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        changePassword.setEnabled(true);
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(context.getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    protected HashMap<String, String> doInBackground(String... params) {
        phoneNumber = params[1];
        try {
            MultipartUtility utility = new MultipartUtility(params[0], "UTF-8");
            Log.e("URL: ", params[0]);
            utility.addFormField("token", Prefrence.getToken(context));
            utility.addFormField("old_password", params[1]);
            utility.addFormField("new_password", params[2]);
            utility.addFormField("confirm_password", params[3]);
            String response = utility.finish();
            if (response != null) {
                Log.e("doInBackground: ", response);
                return getOTPDetails(response);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private HashMap getOTPDetails(String response) {
        try {
            HashMap<String, String> responseData = new HashMap<>();
            JSONObject object = new JSONObject(response);
            responseData.put("status", object.getString("status"));
            responseData.put("message", object.getString("message"));

            return responseData;
        } catch (JSONException je) {
            je.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    protected void onPostExecute(final HashMap<String, String> result) {
        super.onPostExecute(result);
        progressDialog.dismiss();

        if (result != null) {

            AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
            alertDialog.setCancelable(false);
            alertDialog.setMessage(result.get("message"));
            alertDialog.setPositiveButton(context.getResources().getString(R.string.Ok), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    context.startActivity(new Intent(context, UserLoginActivity.class));
                    ((ChangePasswordActivity) context).finish();
                    dialog.dismiss();
                }
            });
            alertDialog.show();

        } else {
            Log.v("SUBMIT_ERROR", "Failed");
            showDialog();
        }
    }

    public void showDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setCancelable(false);
        alertDialog.setMessage(context.getResources().getString(R.string.pta));
        alertDialog.setPositiveButton(context.getResources().getString(R.string.Ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                context.startActivity(new Intent(context, UserLoginActivity.class));
                ((ChangePasswordActivity) context).finish();
                dialog.dismiss();
            }
        });
        alertDialog.show();
    }


}



