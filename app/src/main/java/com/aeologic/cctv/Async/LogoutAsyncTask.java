package com.aeologic.cctv.Async;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;


import com.aeologic.cctv.Activity.HomeActivity;
import com.aeologic.cctv.Activity.UserLoginActivity;
import com.aeologic.cctv.R;
import com.aeologic.cctv.Utility.MultipartUtility;
import com.aeologic.cctv.Utility.Prefrence;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;


public class LogoutAsyncTask extends AsyncTask<String, Integer, HashMap<String, String>> {
    Context context;
    ProgressDialog progressDialog;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(context.getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public LogoutAsyncTask(Context context) {
        this.context = context;

    }

    @Override
    protected HashMap<String, String> doInBackground(String... params) {
        try {
            MultipartUtility utility = new MultipartUtility(params[0], "UTF-8");
            Log.v("OTP_URL", params[0]);
            utility.addFormField("token", Prefrence.getToken(context));
            String response = utility.finish();
            if (response != null) {
                Log.e("doInBackground: ", response);
                return getLogoutDetails(response);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();

        }
        return null;
    }

    @Override
    protected void onPostExecute(HashMap<String, String> result) {
        super.onPostExecute(result);
        progressDialog.dismiss();
        Prefrence.setToken(context, "");

        //clear resgister preference
        Prefrence.setIsRegister(context, false);

        Intent intent = new Intent(context, UserLoginActivity.class);
        context.startActivity(intent);
        ((HomeActivity) context).finish();



        /*if (result != null) {
           *//* if (result.get("status").equals("1")) {*//*
            // delete token

                *//*
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                alertDialog.setMessage(context.getString(R.string.logout_success));
                alertDialog.setCancelable(false);
                alertDialog.setPositiveButton(context.getResources().getString(R.string.Ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(context, PassCodeActivity.class);
                        context.startActivity(intent);
                        dialog.dismiss();
                    }
                });
                alertDialog.show();*//*
            *//*} else {
                Log.v("SUBMIT_ERROR", "Failed");
                Util.showAlert(context, context.getString(R.string.pta));
            }*//*
        }*/
    }

    private HashMap<String, String> getLogoutDetails(String response) {
        try {
            HashMap<String, String> responseData = new HashMap<>();
            JSONObject object = new JSONObject(response);
            responseData.put("status", object.getString("status"));
            responseData.put("message", object.getString("message"));
            return responseData;
        } catch (JSONException je) {
            je.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


}
