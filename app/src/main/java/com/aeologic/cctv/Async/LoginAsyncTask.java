package com.aeologic.cctv.Async;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.aeologic.cctv.OfficersActivities.ManagerHomeActivity;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.aeologic.cctv.Activity.HomeActivity;
import com.aeologic.cctv.Activity.UserLoginActivity;
import com.aeologic.cctv.Database.Database;
import com.aeologic.cctv.R;
import com.aeologic.cctv.Utility.AppController;
import com.aeologic.cctv.Utility.MultipartUtility;
import com.aeologic.cctv.Utility.Prefrence;
import com.aeologic.cctv.Utility.Util;
import com.aeologic.cctv.session.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by u105 on 20/12/16.
 */

public class LoginAsyncTask extends AsyncTask<String, Integer, HashMap<String, String>> {
    public Context context;
    ProgressDialog progressDialog;
    SharedPreferences sharedpreferences;
    String state;

    public LoginAsyncTask(Context context) {
        this.context = context;
    }

    @Override
    protected void onPostExecute(HashMap<String, String> result) {
        super.onPostExecute(result);
        progressDialog.dismiss();
        if (result != null) {
            if (result.get("status").equals("1")) {
                Log.v("SUBMIT_ERROR", "ok");
                SessionManager session = new SessionManager(context);
                session.createUserSession();
                Intent intent = new Intent(context, ManagerHomeActivity.class);
                sharedpreferences = context.getSharedPreferences(UserLoginActivity.MyPREFERENCES, Context.MODE_PRIVATE);
                context.startActivity(intent);
                ((Activity) context).overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                ((Activity) context).finish();


            } else if (result.get("status").equals("0")) {
                Toast.makeText(context, result.get("message"), Toast.LENGTH_SHORT).show();
            }

        } else {
            Log.v("SUBMIT_ERROR", "Failed");
        }

        saveOffenceListToDb((Activity) context, progressDialog);
    }

    public static void saveOffenceListToDb(final Activity activity, final ProgressDialog progressDialog) {
        try {
            StringRequest strReq = new StringRequest(Request.Method.POST,
                    activity.getString(R.string.ACTIVE_STATE_LIST_API), new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {

                    progressDialog.hide();
                    try {
                        Database database = new Database(activity);
                        ArrayList<HashMap<String, String>> offenceList = new ArrayList();
                        HashMap offeceData;
                        Log.e("onResponse: ", response);
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getString("status").equals("1")) {
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                offeceData = new HashMap();
                                JSONObject arrayObj = jsonArray.getJSONObject(i);
                                offeceData.put("offenceName", arrayObj.getString("offence"));
                                offeceData.put("offenceId", arrayObj.getString("offence_id"));
                                offeceData.put("serverImagePath", arrayObj.getString("img"));
                                offenceList.add(offeceData);
                            }


                            new DownloadImagesAsync(activity).execute(offenceList);


                            database.deleteOffenceTable();
                            database.saveOffenceList(offenceList);

                        }

                    } catch (JSONException e) {
                        Util.showAlert(activity, activity.getString(R.string.pta));
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error != null) {
                        if (error.getCause() != null) {
                            if (error.getCause().getMessage() != null)
                                Util.showAlert(activity, error.getCause().getMessage());
                            else
                                Util.showAlert(activity, activity.getString(R.string.pta));
                            progressDialog.hide();
                        }

                    }

                }

            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    String state = Prefrence.getCircleID(activity);
                    // Log.e("getParams: ", state);
                    params.put("state", state);
                    params.put("state_code", "DL");
                    // params.put("pg", String.valueOf(pageNumber));
                    //  Log.e(TAG, "getParams: Token: " + Prefrence.getToken(ComplaintListActivity.this));
                    return params;

                }

            };
            ;

            strReq.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 50000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
            // Adding request to request queue
            AppController.getInstance().addToRequestQueue(strReq, "String Request");


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected HashMap<String, String> doInBackground(String... params) {
        try {
            MultipartUtility utility = new MultipartUtility(params[0], "UTF-8");
            Log.v("OTP_URL", params[0]);
            utility.addFormField("email_id", params[1]);
            utility.addFormField("password", params[2]);
            String response = utility.finish();
            if (response != null) {
                Log.e("doInBackground: ", response);
                return getRegisterDetails(response);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();

        }
        return null;
    }

    private HashMap getRegisterDetails(String response) {
        try {
            HashMap<String, String> responseData = new HashMap<>();
            JSONObject object = new JSONObject(response);
            responseData.put("status", object.getString("status"));
            responseData.put("message", object.getString("message"));
            if (object.getString("status").equals("1")) {
                JSONObject jsonObject = object.getJSONObject("data");
                JSONArray resultArray = jsonObject.getJSONArray("user_detail");
                JSONObject resultObj = resultArray.getJSONObject(0);
                responseData.put("token", resultObj.getString("token"));
                sharedpreferences = context.getSharedPreferences(UserLoginActivity.MyPREFERENCES, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                Prefrence.setUsername(context, resultObj.getString("name"));
                Prefrence.setToken(context, resultObj.getString("token"));
                Prefrence.setMobileNumber(context, resultObj.getString("mobile"));
                Prefrence.setCircleID(context, resultObj.getString("state"));
                Prefrence.setUserType(context, resultObj.getString("user_role"));
                Prefrence.setDesignation(context, resultObj.getString("designation"));
                Prefrence.setGDA(context, resultObj.getString("gda_zone"));
                Prefrence.setNN(context, resultObj.getString("nng_zone"));
                Prefrence.setUserID(context, resultObj.getString("email"));
                Prefrence.setDownload(context, resultObj.getString("can_download_report"));
                Prefrence.setDepartmentID(context, String.valueOf(resultObj.has("department_id")?resultObj.getString("department_id"):0));
                Prefrence.setIsRegister(context, true);
//                Prefrence.setResolve(context, resultObj.getString("can_resolve"));
                Prefrence.setResolve(context, "1");
//                Prefrence.setDelete(context, resultObj.getString("can_delete"));
                Prefrence.setDelete(context, "1");
                Prefrence.checkIsManager(context,true);
                editor.commit();
            }
            return responseData;
        } catch (JSONException je) {
            je.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(context.getResources().getString(R.string.please_wait));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();


    }


}

