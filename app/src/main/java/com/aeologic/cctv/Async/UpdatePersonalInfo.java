package com.aeologic.cctv.Async;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.widget.Toast;

import com.aeologic.cctv.Activity.HomeActivity;
import com.aeologic.cctv.Activity.UserLoginActivity;
import com.aeologic.cctv.R;
import com.aeologic.cctv.Utility.MultipartUtility;
import com.aeologic.cctv.Utility.Prefrence;
import com.aeologic.cctv.session.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import static com.aeologic.cctv.Async.LoginAsyncTask.saveOffenceListToDb;


/**
 * Created by u105 on 10/07/16.
 */

public class UpdatePersonalInfo extends AsyncTask<String, Integer, HashMap<String, String>> {
    private Context context;
    CardView updateRelative, addMore;
    ProgressDialog progressDialog;
    String mobileNumber;
    SharedPreferences sharedpreferences;


    public UpdatePersonalInfo(Context context) {

        this.context = context;
        sharedpreferences = context.getSharedPreferences(UserLoginActivity.MyPREFERENCES, Context.MODE_PRIVATE);

    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Please Wait ...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();


    }

    @Override
    protected HashMap<String, String> doInBackground(String... params) {

        try {
            MultipartUtility utility = new MultipartUtility(params[0], "UTF-8");
            // Log.v("OTP_URL", params[0]);
            mobileNumber = params[1];
            utility.addFormField("name", params[1]);
            utility.addFormField("mobile", params[2]);
            utility.addFormField("token", Prefrence.getToken(context));

            String response = utility.finish();
            if (response != null) {
                Log.e("doInBackground: ", response);
                return getRegisterDetails(response);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();

        }
        return null;
    }

    @Override
    protected void onPostExecute(HashMap<String, String> result) {
        super.onPostExecute(result);
        progressDialog.dismiss();
        if (result != null) {
            if (result.get("status").equals("1")) {
                Log.v("SUBMIT_ERROR", "ok");
                SessionManager session = new SessionManager(context);
                session.createUserSession();
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                alertDialog.setCancelable(false);
                alertDialog.setMessage(result.get("message"));
                alertDialog.setPositiveButton(context.getResources().getString(R.string.Ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(context, HomeActivity.class);
                        sharedpreferences = context.getSharedPreferences(UserLoginActivity.MyPREFERENCES, Context.MODE_PRIVATE);
                        context.startActivity(intent);
                        ((Activity) context).overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                        ((Activity) context).finish();
                    }
                });
                alertDialog.show();


            } else if (result.get("status").equals("0")) {
                Toast.makeText(context, result.get("message"), Toast.LENGTH_SHORT).show();
            }

        } else {
            Log.v("SUBMIT_ERROR", "Failed");
        }

        saveOffenceListToDb((Activity) context, progressDialog);
    }

    private HashMap getRegisterDetails(String response) {
        try {
            HashMap<String, String> responseData = new HashMap<>();
            JSONObject object = new JSONObject(response);
            responseData.put("status", object.getString("status"));
            responseData.put("message", object.getString("message"));
            if (object.getString("status").equals("1")) {
                JSONObject jsonObject = object.getJSONObject("data");
                JSONArray resultArray = jsonObject.getJSONArray("user_detail");
                JSONObject resultObj = resultArray.getJSONObject(0);
                responseData.put("token", resultObj.getString("token"));
                sharedpreferences = context.getSharedPreferences(UserLoginActivity.MyPREFERENCES, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                Prefrence.setUsername(context, resultObj.getString("name"));
                Prefrence.setToken(context, resultObj.getString("token"));
                Prefrence.setMobileNumber(context, resultObj.getString("mobile"));
                Prefrence.setCircleID(context, resultObj.getString("state"));
                Prefrence.setUserType(context, resultObj.getString("user_role"));
                Prefrence.setDesignation(context, resultObj.getString("designation"));
                Prefrence.setGDA(context, resultObj.getString("gda_zone"));
                Prefrence.setNN(context, resultObj.getString("nng_zone"));
                Prefrence.setUserID(context, resultObj.getString("email"));
                Prefrence.setIsRegister(context, true);
                editor.commit();
            }
            return responseData;
        } catch (JSONException je) {
            je.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


}
