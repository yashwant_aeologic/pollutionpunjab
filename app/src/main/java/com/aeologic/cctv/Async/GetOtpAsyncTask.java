package com.aeologic.cctv.Async;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.aeologic.cctv.Activity.UserLoginActivity;
import com.aeologic.cctv.Activity.OtpActivity;
import com.aeologic.cctv.Database.Database;
import com.aeologic.cctv.R;
import com.aeologic.cctv.Utility.AppController;
import com.aeologic.cctv.Utility.MultipartUtility;
import com.aeologic.cctv.Utility.Prefrence;
import com.aeologic.cctv.Utility.Util;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by u104 on 4/8/16.
 */
public class GetOtpAsyncTask extends AsyncTask<String, Integer, HashMap<String, String>> {
    private static final String TAG = "GetOtpAsyncTask";
    private Context context;
    String phoneNumber;
    ProgressDialog progressDialog;
    ImageView resendSMS;
    TextView otpTimer;
    private Button verifyButton;
    private TextView msg;


    public GetOtpAsyncTask(Context context, ImageView resendSMS, TextView otpTimer, Button verifyButton, TextView msg) {
        this.context = context;
        this.resendSMS = resendSMS;
        this.otpTimer = otpTimer;
        this.verifyButton = verifyButton;
        this.msg = msg;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        verifyButton.setEnabled(true);
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(context.getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    protected HashMap<String, String> doInBackground(String... params) {
        phoneNumber = params[1];
        try {
            MultipartUtility utility = new MultipartUtility(params[0], "UTF-8");
            Log.v("OTP_URL", params[0]);
            utility.addFormField("phone_number", params[1]);
            String response = utility.finish();
            if (response != null) {
               Log.e("doInBackgroundOTP: ", response);
                return getOTPDetails(response);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private HashMap getOTPDetails(String response) {
        try {
            HashMap<String, String> responseData = new HashMap<>();
            JSONObject object = new JSONObject(response);
            responseData.put("status", object.getString("status"));
            responseData.put("message", object.getString("message"));
            if (object.getString("status").equals("1")) {
                JSONObject resultObj = object.getJSONObject("data");
                responseData.put("OTP", resultObj.getString("otp"));
                responseData.put("isRegistered", resultObj.getString("isRegistered"));
//                Log.e("getOTPDetails: ", resultObj.getString("isRegistered"));

                if (resultObj.getString("isRegistered").equals("1")) {
                    // update the value in prefences
                    JSONArray jsonArray = resultObj.getJSONArray("user_detail");
                    JSONObject childJsonObj = jsonArray.getJSONObject(0);
                    Prefrence.setUsername(context, childJsonObj.getString("username"));
                    Prefrence.setEmail(context, childJsonObj.getString("email_id"));

                    JSONArray jsonArray1 = resultObj.getJSONArray("user_detail");
                    JSONObject childObj = jsonArray1.getJSONObject(0);

                    String token = childObj.getString("token");
                    String username = childObj.getString("username");
                    String email = childObj.getString("token");
                    Prefrence.setToken(context, token);
                    Prefrence.setUsername(context, username);
                    Prefrence.setEmail(context, email);
                    Prefrence.checkIsManager(context,false);
//                    Prefrence.setPasscode(activity, passCode);
                }
                /*if (tempStr.equalsIgnoreCase("1")) {
                    responseData.put("username", resultObj.getString("username"));
                    responseData.put("email_id", resultObj.getString("email_id"));

                }*/
            }
            return responseData;
        } catch (JSONException je) {
            je.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    protected void onPostExecute(final HashMap<String, String> result) {
        super.onPostExecute(result);
        progressDialog.dismiss();

        if (result != null) {
            if (result.get("status").equals("1")) {
                if (result.get("isRegistered").equals("0")) {
                    msg.setVisibility(View.VISIBLE);
//                    Log.e(TAG, "onPostExecute: Preference edit");
                    ((OtpActivity) context).otp = result.get("OTP");
                    ((OtpActivity) context).token = result.get("token");
                    ((OtpActivity) context).isRegisterd = false;
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //Do something after 100ms
                            msg.setVisibility(View.GONE);

                        }
                    }, 2000);

                    new CountDownTimer(300000, 1000) {

                        public void onTick(long millisUntilFinished) {
                            resendSMS.setImageResource(R.drawable.rsend);
                            resendSMS.setEnabled(false);
                            otpTimer.setVisibility(View.VISIBLE);
                            otpTimer.setText(("" + String.format("%d : %d ",
                                    TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                                    TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)))));
                            //here you can have your logic to set text to edittext
                        }

                        public void onFinish() {
                            verifyButton.setEnabled(false);
//                            otpTimer.setText("Resend SMS");
                            otpTimer.setVisibility(View.GONE);
                            resendSMS.setImageResource(R.drawable.resend);
                            resendSMS.setEnabled(true);
                        }

                    }.start();

                } else if (result.get("isRegistered").equals("1")) {
//                    Log.e(TAG, "onPostExecute: Preference edit");
//                    Prefrence.setIsRegister(context, true);
//                    Prefrence.setUsername(context,result.get("username"));
//                    Prefrence.setEmail(context,result.get("email_id"));
                    ((OtpActivity) context).otp = result.get("OTP");
                    ((OtpActivity) context).isRegisterd = true;
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //Do something after 100ms
                            msg.setVisibility(View.GONE);

                        }
                    }, 2000);
                    new CountDownTimer(300000, 1000) {
                        public void onTick(long millisUntilFinished) {
                            resendSMS.setImageResource(R.drawable.rsend);
                            resendSMS.setEnabled(false);
                            otpTimer.setVisibility(View.VISIBLE);
                            otpTimer.setText(("" + String.format("%d : %d ",
                                    TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                                    TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)))));
                            //here you can have your logic to set text to edittext
                        }

                        public void onFinish() {
//                            otpTimer.setText("Resend SMS");
                            otpTimer.setVisibility(View.GONE);
                            resendSMS.setImageResource(R.drawable.resend);
                            resendSMS.setEnabled(true);
                        }

                    }.start();
                }
            }
            saveOffenceListToDb((Activity) context,progressDialog);
        } else {
            Log.v("SUBMIT_ERROR", "Failed");
            showDialog();
        }
    }

    public void showDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setCancelable(false);
        alertDialog.setMessage(context.getResources().getString(R.string.pta));
        alertDialog.setPositiveButton(context.getResources().getString(R.string.Ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                context.startActivity(new Intent(context, UserLoginActivity.class));
                ((OtpActivity) context).finish();
                dialog.dismiss();
            }
        });
        alertDialog.show();
    }

    public static void saveOffenceListToDb(final Activity activity, final ProgressDialog progressDialog) {
        try {
            StringRequest strReq = new StringRequest(Request.Method.GET,
                    activity.getString(R.string.ACTIVE_STATE_LIST_API), new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    Log.e(activity.getLocalClassName(), "doInBackground: API: " + activity.getString(R.string.ACTIVE_STATE_LIST_API));
                    Log.e(activity.getLocalClassName(), "onResponse: Response: " + response);
                    progressDialog.hide();
                    try {
                        Database database = new Database(activity);
                        ArrayList<HashMap<String, String>> offenceList = new ArrayList();
                        HashMap offeceData;
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getString("status").equals("1")) {
                            JSONArray jsonArray = jsonObject.getJSONArray("city");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                offeceData = new HashMap();
                                JSONObject arrayObj = jsonArray.getJSONObject(i);
                                offeceData.put("city_id", arrayObj.getString("id"));
                                offeceData.put("city", arrayObj.getString("city"));
                                offeceData.put("state", arrayObj.getString("state"));
                                offenceList.add(offeceData);
                            }


                           // new DownloadImagesAsync(activity).execute(offenceList);


                            database.deleteActiveStateTable();
                            database.saveActiveStateList(offenceList);
                         //   saveOffenceListToDb1(activity,progressDialog);

                        }

                    } catch (JSONException e) {
                        Util.showAlert(activity, activity.getString(R.string.pta));
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error.getCause().getMessage()!=null)
                        Util.showAlert(activity, error.getCause().getMessage());
                    else
                        Util.showAlert(activity, activity.getString(R.string.pta));
                    progressDialog.hide();
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    String state = Prefrence.getCircleID(activity);
                    // Log.e("getParams: ", state);
//                    params.put("state", state);
                    params.put("state_code", "DL");
                    // params.put("pg", String.valueOf(pageNumber));
                      Log.v(TAG, "getParams: state_code: DL"  );
                    return params;

                }

            }
                    ;
            strReq.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 50000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
            // Adding request to request queue
            AppController.getInstance().addToRequestQueue(strReq, "String Request");


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void saveOffenceListToDb1(final Activity activity, final ProgressDialog progressDialog) {
        try {
            StringRequest strReq = new StringRequest(Request.Method.POST,
                    activity.getString(R.string.OFFENCE_LIST_API), new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {

                    progressDialog.hide();
                    try {
                        Database database = new Database(activity);
                        ArrayList<HashMap<String, String>> offenceList = new ArrayList();
                        HashMap offeceData;
                        Log.e("onResponse: ", response);
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getString("status").equals("1")) {
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                offeceData = new HashMap();
                                JSONObject arrayObj = jsonArray.getJSONObject(i);
                                offeceData.put("offenceName", arrayObj.getString("offence"));
                                offeceData.put("offenceId", arrayObj.getString("offence_id"));
                                offeceData.put("serverImagePath", arrayObj.getString("img"));
                                offenceList.add(offeceData);
                            }


                            new DownloadImagesAsync(activity).execute(offenceList);


                            database.deleteOffenceTable();
                            database.saveOffenceList(offenceList);

                        }

                    } catch (JSONException e) {
                        Util.showAlert(activity, activity.getString(R.string.pta));
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error != null) {
                        if (error.getCause() != null) {
                            if (error.getCause().getMessage() != null)
                                Util.showAlert(activity, error.getCause().getMessage());
                            else
                                Util.showAlert(activity, activity.getString(R.string.pta));
                            progressDialog.hide();
                        }

                    }

                }

            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    String state = Prefrence.getCircleID(activity);
                    // Log.e("getParams: ", state);
                  //  params.put("state", "DL");
                   // params.put("state_code", "DL");
                    return params;

                }

            };
            ;

            strReq.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 50000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
            // Adding request to request queue
            AppController.getInstance().addToRequestQueue(strReq, "String Request");


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}



