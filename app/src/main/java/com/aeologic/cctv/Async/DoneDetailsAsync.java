package com.aeologic.cctv.Async;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.aeologic.cctv.Activity.DoneDetailActivity;
import com.aeologic.cctv.R;
import com.aeologic.cctv.Utility.MultipartUtility;
import com.aeologic.cctv.Utility.Prefrence;
import com.aeologic.cctv.Utility.Util;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import static com.aeologic.cctv.Activity.DoneDetailActivity.dLatitude;
import static com.aeologic.cctv.Activity.DoneDetailActivity.dLong;

/**
 * Created by u105 on 19/8/16.
 */
public class DoneDetailsAsync extends AsyncTask<String, Integer, HashMap<String, String>> {
    Context context;
    ProgressDialog progressDialog;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(context.getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public DoneDetailsAsync(Context context) {
        this.context = context;
    }

    @Override
    protected HashMap<String, String> doInBackground(String[] params) {
        try {
            MultipartUtility utility = new MultipartUtility(params[0], "UTF-8");
            Log.v("Create_complaint_url ", params[0]);
            utility.addFormField("token", Prefrence.getToken(context));
            utility.addFormField("complaint_number", params[1]);
            String response = utility.finish();
            if (response != null) {
                Log.v("SERVER_RESPONSE", response);
                return getDetails(response);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();

        }
        return null;
    }

    private HashMap getDetails(String response) {
        try {
            HashMap<String, String> responseData = new HashMap<>();
            JSONObject object = new JSONObject(response);
            responseData.put("status", object.getString("status"));
            responseData.put("message", object.getString("message"));
            if (object.getString("status").equals("1")) {
                JSONArray jsonArray = object.getJSONArray("data");
                JSONObject obj = jsonArray.getJSONObject(0);

                if (obj.isNull("rc_number")) {
                    responseData.put("rcNumber", "");
                } else {
                    responseData.put("rcNumber", obj.getString("rc_number"));
                }
                if (obj.isNull("offences")) {
                    responseData.put("offence", "");
                } else {
                    responseData.put("offence", obj.getString("offences"));
                }

                if (obj.isNull("latitude")) {
                    responseData.put("latitude", "");
                } else {
                    dLatitude = Double.parseDouble(obj.getString("latitude"));
                }

                if (obj.isNull("longitude")) {
                    responseData.put("longitude", "");
                } else {
                    dLong = Double.parseDouble(obj.getString("longitude"));
                }

                if (obj.isNull("comment")) {
                    responseData.put("comment", "");
                } else {
                    responseData.put("comment", obj.getString("comment"));
                }
                if (obj.isNull("circle_name")) {
                    responseData.put("circle_name", "");
                } else {
                    responseData.put("circle_name", obj.getString("circle_name"));
                }
                if (obj.isNull("address")) {
                    responseData.put("address", "");
                } else {
                    responseData.put("address", obj.getString("address"));
                }
                if (obj.isNull("photo_path")) {
                    responseData.put("imageUri", "");
                } else {
                    responseData.put("imageUri", obj.getString("photo_path"));
                }
                if (obj.isNull("video_path")) {
                    responseData.put("videoUri", "");
                } else {
                    responseData.put("videoUri", obj.getString("video_path"));
                }

                if (obj.isNull("audio_path")) {
                    responseData.put("audio_path", "");
                } else {
                    responseData.put("audio_path", obj.getString("audio_path"));
                }
                if (obj.isNull("resolved_photo_path")) {
                    responseData.put("resolved_photo_path", "");
                } else {
                    responseData.put("resolved_photo_path", obj.getString("resolved_photo_path"));
                }
                if (obj.isNull("resolved_video_path")) {
                    responseData.put("resolved_video_path", "");
                } else {
                    responseData.put("resolved_video_path", obj.getString("resolved_video_path"));
                }

                if (obj.isNull("resolved_audio_path")) {
                    responseData.put("resolved_audio_path", "");
                } else {
                    responseData.put("resolved_audio_path", obj.getString("resolved_audio_path"));
                }

                if (obj.isNull("manual_address")) {
                    responseData.put("manual_address", "");
                } else {
                    responseData.put("manualAddress", obj.getString("manual_address"));
                }
                if (obj.isNull("action")) {
                    responseData.put("action", "");
                } else {
                    responseData.put("action", obj.getString("action"));
                }
                if (obj.isNull("action_comment")) {
                    responseData.put("action_comment", "");
                } else {
                    responseData.put("action_comment", obj.getString("action_comment"));
                }
                if (!obj.isNull("latitude") && !obj.isNull("longitude")) {
                    responseData.put("latlng", obj.getString("latitude") + "," + obj.getString("longitude"));
                }
                if (obj.isNull("survey_date")) {
                    responseData.put("survey_date", "");
                } else {
                    responseData.put("timeStamp", obj.getString("survey_date"));
                }
                if (obj.isNull("is_forward")) {
                    responseData.put("isForward", "");
                } else {
                    responseData.put("isForward", obj.getString("is_forward"));
                }
                if (obj.isNull("other_offence")) {
                    responseData.put("other_offence", "");
                } else {
                    responseData.put("other_offence", obj.getString("other_offence"));
                }
                if (obj.isNull("ward_name")) {
                    responseData.put("ward_name", "");
                } else {
                    responseData.put("ward_name", obj.getString("ward_name"));
                }
                if (obj.isNull("mobile")) {
                    responseData.put("mobile", "");
                } else {
                    responseData.put("mobile", obj.getString("mobile"));
                }
                if (obj.isNull("name")) {
                    responseData.put("name", "");
                } else {
                    responseData.put("name", obj.getString("name") + "(" + obj.getString("email") + ")");
                }
                if (obj.isNull("resolved_by_name") && obj.isNull("resolved_by_email")) {
                    responseData.put("resolved_by_name", "");
                } else {
                    responseData.put("resolved_by_name", obj.getString("resolved_by_name") + "(" + obj.getString("resolved_by_email") + ")");
                }
                if (obj.isNull("resolved_by_mobile")) {
                    responseData.put("resolved_by_mobile", "");
                } else {
                    responseData.put("resolved_by_mobile", obj.getString("resolved_by_mobile"));
                }
                if (obj.isNull("action_at")) {
                    responseData.put("action_at", "");
                } else {
                    responseData.put("action_at", obj.getString("action_at"));
                }
                if (obj.isNull("resolved_by_designation")) {
                    responseData.put("resolved_by_designation", "");
                } else {
                    responseData.put("resolved_by_email", obj.getString("resolved_by_designation"));
                }
                if (obj.isNull("feedback")) {
                    responseData.put("feedback", "");
                } else {
                    responseData.put("feedback", obj.getString("feedback"));
                }


              /*  responseData.put("offence", obj.getString("offences"));
                responseData.put("comment", obj.getString("comment"));
                responseData.put("address", obj.getString("address"));
                responseData.put("imageUri", obj.getString("photo_path"));
                responseData.put("videoUri", obj.getString("video_path"));*/
            }
            return responseData;
        } catch (JSONException je) {
            je.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    protected void onPostExecute(HashMap<String, String> result) {
        super.onPostExecute(result);
        progressDialog.dismiss();
        if (result != null && result.size() > 0) {
            if (result.get("status").equals("1")) {
                Log.e("onPostExecute: ", result.get("message"));
                ((DoneDetailActivity) context).setDatavalue(result);
            } else {
                Log.v("SUBMIT_ERROR", "Failed");
                String message = result.get("message");
                if (message != null) {
                    if (message.contains("Your session has expired"))
                        Util.showSessionExpiredDialog(context);
                } else {
                    Util.showAlert(context, context.getString(R.string.pta));
                }
            }
        }
    }

}
