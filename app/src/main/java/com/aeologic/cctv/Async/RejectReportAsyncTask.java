package com.aeologic.cctv.Async;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Button;

import com.aeologic.cctv.Activity.ComplaintListActivity;
import com.aeologic.cctv.Activity.ResolveActivity;
import com.aeologic.cctv.OfficersActivities.ManagerComplaintListActivity;
import com.aeologic.cctv.R;
import com.aeologic.cctv.Utility.MultipartUtility;
import com.aeologic.cctv.Utility.Prefrence;
import com.aeologic.cctv.Utility.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;


/**
 * Created by u105 on 19/8/16.
 */
public class RejectReportAsyncTask extends AsyncTask<String, Integer, HashMap<String, String>> {
    private String TAG = RejectReportAsyncTask.class.getSimpleName();
    private Button rejectBtn;
    private Context context;
    private final android.support.v7.app.AlertDialog dialog;
    ProgressDialog progressDialog;
    String complaintNo;


    public RejectReportAsyncTask(Context context, android.support.v7.app.AlertDialog rejectDialog, Button rejectBtn) {
        this.context = context;
        this.dialog = rejectDialog;
        this.rejectBtn = rejectBtn;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(context.getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    protected HashMap<String, String> doInBackground(String[] params) {
        try {
            MultipartUtility utility = new MultipartUtility(params[0], "UTF-8");
            Log.v("URL ", params[0]);
            utility.addFormField("token", Prefrence.getToken(context));
            complaintNo = params[1];
            utility.addFormField("complaint_no", complaintNo);
            utility.addFormField("comment", params[2]);
            utility.addFormField("is_android", "1");
            String response = utility.finish();
            if (response != null) {
                Log.e("SERVER_RESPONSE", response);
                return sendDetailResponse(response);
            }
        } catch (IOException e) {

            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private HashMap sendDetailResponse(String response) throws JSONException {
        try {
            HashMap<String, String> responseData = new HashMap<>();
            JSONObject object = new JSONObject(response);
            responseData.put("status", object.getString("status"));
            responseData.put("message", object.getString("message"));
            if (object.getString("status").equals("1")) {
                if (object.has("data")) {
                    JSONObject resultObj = object.getJSONObject("data");
                    String obj = resultObj.getString("complaint_number");
                    responseData.put("complaintNumber", obj);
                }
            }
            return responseData;
        } catch (JSONException je) {
            je.printStackTrace();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    protected void onPostExecute(HashMap<String, String> result) {
        super.onPostExecute(result);
        progressDialog.dismiss();
        if (rejectBtn != null) {
            rejectBtn.setEnabled(true);
            rejectBtn.setAlpha(1f);
        }
        if (result != null) {
            if (result.get("status").equals("1")) {
                dialog.dismiss();
                Log.e(TAG, "MESSAGE: " + result.get("message"));

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                alertDialog.setCancelable(false);
                alertDialog.setMessage(result.get("message"));
                alertDialog.setPositiveButton(context.getResources().getString(R.string.Ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        Intent intent = new Intent(context, ManagerComplaintListActivity.class);
                        intent.putExtra("From", "Reject");
                        context.startActivity(intent);
                        ((Activity) context).finish();
                        dialog.dismiss();
                    }
                });
                alertDialog.show();

            } else {
                Util.showAlert(context, context.getString(R.string.pta));
                rejectBtn.setEnabled(true);
                rejectBtn.setAlpha(1.0f);
            }
        } else {
            Util.showAlert(context, context.getString(R.string.pta));
            rejectBtn.setEnabled(true);
            rejectBtn.setAlpha(1.0f);
        }
    }

}
