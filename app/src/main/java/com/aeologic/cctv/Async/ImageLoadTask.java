package com.aeologic.cctv.Async;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;


import com.aeologic.cctv.Activity.DoneDetailActivity;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by u104 on 24/1/17.
 */

public class ImageLoadTask extends AsyncTask<String, Void, Bitmap> {

    private ImageView imageView;
    private ProgressBar progressBar;
    private String TAG = "ImageLoadTask";
    private Bitmap myBitmap;
    public Context context;

    public ImageLoadTask(Context context, ImageView imageView, ProgressBar progressBar) {
        this.imageView = imageView;
        this.progressBar = progressBar;
        this.context = context;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        imageView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    protected Bitmap doInBackground(String... params) {
        try {
            URL urlConnection = new URL(params[0]);
            Log.e(TAG, "doInBackground: imageUrl: " + params[0]);
            HttpURLConnection connection = (HttpURLConnection) urlConnection
                    .openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    protected void onPostExecute(Bitmap result) {
        super.onPostExecute(result);
        progressBar.setVisibility(View.GONE);
        imageView.setImageBitmap(result);
        imageView.setVisibility(View.VISIBLE);
        ((DoneDetailActivity) context).fullScreenImage.setVisibility(View.VISIBLE);
        ((DoneDetailActivity) context).fullScreenImage1.setVisibility(View.VISIBLE);
        ((DoneDetailActivity) context).imageBitmap = result;
    }

}
