package com.aeologic.cctv.Async;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.aeologic.cctv.Activity.OffenceAnalyticsActivity;
import com.aeologic.cctv.R;
import com.aeologic.cctv.Utility.MultipartUtility;
import com.aeologic.cctv.Utility.Prefrence;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Yashwant on 21/11/18.
 */

public class GetZoneAsyncTask extends AsyncTask<String, Integer, List<HashMap<String, String>>> {
    public Context context;
    ProgressDialog progressDialog;
    SharedPreferences sharedpreferences;
    Spinner zoneSpinner;
    String type;

    public GetZoneAsyncTask(Context context, Spinner zoneSpinner) {
        this.context = context;
        this.zoneSpinner = zoneSpinner;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(context.getResources().getString(R.string.please_wait));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();


    }


    @Override
    protected List<HashMap<String, String>> doInBackground(String... params) {
        try {
            MultipartUtility utility = new MultipartUtility(params[0], "UTF-8");
            Log.v("OTP_URL", params[0]);
            type = params[1];
            utility.addFormField("type", params[1]);
            utility.addFormField("token", Prefrence.getToken(context));
            String response = utility.finish();
            if (response != null) {
                Log.e("doInBackground: ", response);
                return getZoneDetails(response);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();

        }
        return null;
    }

    @Override
    protected void onPostExecute(List<HashMap<String, String>> result) {
        super.onPostExecute(result);
        progressDialog.dismiss();
        progressDialog.dismiss();
        List<String> list = new ArrayList<String>();
        list.add("Select Zone");
        // db.AddOffenceList("0", "Select Offence");

        for (int i = 0; i < result.size(); i++) {
            if (type.equalsIgnoreCase("NN")) {
                list.add(result.get(i).get("zone_id") + " " + result.get(i).get("zone_name"));
            } else {
                list.add(result.get(i).get("zone_name"));
            }

        }
        ArrayAdapter adapter = new ArrayAdapter(context, android.R.layout.simple_spinner_item, list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        zoneSpinner.setAdapter(adapter);
        zoneSpinner.setPrompt("Select Zone");
    }


    private List<HashMap<String, String>> getZoneDetails(String response) {
        try {
            List<HashMap<String, String>> responseDataList = null;
            JSONObject object = new JSONObject(response);
            if (object.getString("status").equals("1")) {
                responseDataList = new ArrayList<HashMap<String, String>>();
                JSONArray myListsAll = object.getJSONArray("data");
                for (int i = 0; i < myListsAll.length(); i++) {
                    JSONObject jsonobject = (JSONObject) myListsAll.get(i);
                    HashMap<String, String> responseData = new HashMap<>();
                    responseData.put("zone_id", jsonobject.getString("zone_id"));
                    responseData.put("zone_name", jsonobject.getString("zone_name"));
                    responseDataList.add(responseData);
                }

            }
            return responseDataList;
        } catch (JSONException je) {
            je.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


}

