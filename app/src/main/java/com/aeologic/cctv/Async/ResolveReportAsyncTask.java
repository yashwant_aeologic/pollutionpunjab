package com.aeologic.cctv.Async;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.widget.Button;

import com.aeologic.cctv.Activity.ComplaintListActivity;
import com.aeologic.cctv.Activity.HomeActivity;
import com.aeologic.cctv.Activity.InProgressDetailActivity;
import com.aeologic.cctv.Activity.ResolveActivity;
import com.aeologic.cctv.Activity.SendComplaintActivity;
import com.aeologic.cctv.Database.Database;
import com.aeologic.cctv.OfficersActivities.ManagerComplaintListActivity;
import com.aeologic.cctv.R;
import com.aeologic.cctv.Utility.MultipartUtility;
import com.aeologic.cctv.Utility.Prefrence;
import com.aeologic.cctv.Utility.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;


/**
 * Created by u105 on 19/8/16.
 */
public class ResolveReportAsyncTask extends AsyncTask<String, Integer, HashMap<String, String>> {
    private String TAG = ResolveReportAsyncTask.class.getSimpleName();
    private Button send;
    private Context context;
    ProgressDialog progressDialog;
    String complaintNo;

    public ResolveReportAsyncTask(Context context, Button send) {
        this.context = context;
        this.send = send;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(context.getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    protected HashMap<String, String> doInBackground(String[] params) {
        try {
            MultipartUtility utility = new MultipartUtility(params[0], "UTF-8");
            Log.v("URL ", params[0]);
            utility.addFormField("token", Prefrence.getToken(context));
            complaintNo = params[1];
            utility.addFormField("complaint_no", complaintNo);
            utility.addFormField("comment", params[2]);
            if (params[3] != null) {
                utility.addFilePart("photo", new File(params[3]));
                Log.e(TAG, "IMAGE: " + params[3]);
            }
            if (params[4] != null) {
                utility.addFilePart("video", new File(params[4]));
                Log.e(TAG, "VIDEO: " + params[4]);
            }
            if (params[5] != null) {
                utility.addFilePart("audio", new File(params[5]));
                Log.e(TAG, "AUDIO: " + params[5]);
            }
            utility.addFormField("resolved_lat", params[6]);
            utility.addFormField("resolved_long", params[7]);
            utility.addFormField("resolved_distance", params[8]);
            utility.addFormField("is_android", "1");
            String response = utility.finish();
            Log.e("SERVER_RESPONSE", "Response" + response);
            if (response != null) {

                return sendDetailResponse(response);
            }
        } catch (IOException e) {

            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private HashMap sendDetailResponse(String response) throws JSONException {
        try {
            HashMap<String, String> responseData = new HashMap<>();
            JSONObject object = new JSONObject(response);
            responseData.put("status", object.getString("status"));
            responseData.put("message", object.getString("message"));
            if (object.getString("status").equals("1")) {
                if (object.has("data")) {
                    JSONObject resultObj = object.getJSONObject("data");
                    String obj = resultObj.getString("complaint_number");
                    responseData.put("complaintNumber", obj);
                }
            }
            return responseData;
        } catch (JSONException je) {
            je.printStackTrace();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    protected void onPostExecute(HashMap<String, String> result) {
        super.onPostExecute(result);
        progressDialog.dismiss();
        if (send != null) {
            send.setEnabled(true);
            send.setAlpha(1f);
        }
        if (result != null) {
            if (result.get("status").equals("1")) {
                Log.e(TAG, "MESSAGE: " + result.get("message"));

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                alertDialog.setCancelable(false);
                alertDialog.setMessage(result.get("message"));
                alertDialog.setPositiveButton(context.getResources().getString(R.string.Ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        Intent intent = new Intent(context, ManagerComplaintListActivity.class);
                        intent.putExtra("From", "RESOLVE");
                        context.startActivity(intent);
                        ((Activity) context).finish();
                        dialog.dismiss();
                    }
                });
                alertDialog.show();

            } else {
                Util.showAlert(context, context.getString(R.string.pta));
                ((ResolveActivity) context).send.setEnabled(true);
                ((ResolveActivity) context).send.setAlpha(1.0f);
            }
        } else {
            Util.showAlert(context, context.getString(R.string.pta));
            ((ResolveActivity) context).send.setEnabled(true);
            ((ResolveActivity) context).send.setAlpha(1.0f);
        }
    }

}
