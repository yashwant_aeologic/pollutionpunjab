package com.aeologic.cctv.Async;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.util.Log;


import com.aeologic.cctv.Activity.SendComplaintActivity;
import com.aeologic.cctv.R;
import com.aeologic.cctv.Utility.MultipartUtility;
import com.aeologic.cctv.model.RCDetails;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;


/**
 * Created by Yash on 03-Jan-16.
 */


public class GetRCDetailsAsync extends AsyncTask<String, Void, RCDetails> {
    private Context context;
    private ProgressDialog dialog;
    String response;
    String rcData;
    String rcNumber;

    public GetRCDetailsAsync(Context context) {

        this.context = context;


    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog = new ProgressDialog(context);
        dialog.setMessage(context.getString(R.string.please_wait));
        dialog.setCancelable(false);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.show();
    }

    @Override
    protected RCDetails doInBackground(String... params) {
        try {
            MultipartUtility utility = new MultipartUtility(params[0], "UTF-8");
            Log.v("URL", params[0]);
            rcNumber = params[1];
            utility.addFormField("rc_number", params[1]);
            response = utility.finish();
            if (response != null) {
                Log.e("doInBackground: ", response);

                return getRCDetails(response);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();

        }
        return null;


    }

    public RCDetails getRCDetails(String response) {
        RCDetails details;
        JSONObject jObjMain;

        try {
            jObjMain = new JSONObject(response);
            if (jObjMain.getInt("status") == 200) {
                details = new RCDetails();
                details.setStatusCode(jObjMain.getInt("status"));
                details.setStatusMsg(jObjMain.getString("message"));
                if (jObjMain.has("result")) {
                    //AES aes = new AES();
                    //APIController controller = APIController.getInstance();
                    //JSONObject jObjResult = new JSONObject(aes.decrypt(jObjMain.getString("result"), controller.getSecretKey2()));
                    JSONObject jObjResult = jObjMain.getJSONObject("result");
                    rcData = jObjResult.toString();
                    //   Log.v("RCDetails: ", jObjResult.toString());
                    if (jObjResult.has("stautsMessage") && jObjResult.getString("stautsMessage").equalsIgnoreCase("OK")) {
                        details.setStatus(true);
                        if (jObjResult.has("rc_regn_no")) {
                            String rcNo = jObjResult.getString("rc_regn_no");
                            if (rcNo != null && rcNo.length() > 0) {
                                rcNo = rcNo.substring(0, rcNo.length() - 4).trim() + rcNo.substring(rcNo.length() - 4).trim();
                            }
                            details.setRegNo(rcNo);
                        } else {
                            details.setRegNo("");
                        }
                        if (jObjResult.has("rc_regn_dt")) {
                            details.setRegDate(jObjResult.getString("rc_regn_dt"));
                        } else {
                            details.setRegDate("");
                        }
                        if (jObjResult.has("rc_owner_sr")) {
                            details.setOwnerSr(jObjResult.getString("rc_owner_sr"));
                        } else {
                            details.setOwnerSr("");
                        }
                        if (jObjResult.has("rc_owner_name")) {
                            details.setOwnerName(jObjResult.getString("rc_owner_name"));
                        } else {
                            details.setOwnerName("");
                        }
                        if (jObjResult.has("rc_f_name")) {
                            details.setFatherName(jObjResult.getString("rc_f_name"));
                        } else {
                            details.setFatherName("");
                        }
                        if (jObjResult.has("rc_present_address")) {
                            details.setPresentAddr(jObjResult.getString("rc_present_address"));
                        } else {
                            details.setPresentAddr("");
                        }
                        if (jObjResult.has("rc_permanent_address")) {
                            details.setPermanentAddr(jObjResult.getString("rc_permanent_address"));
                        } else {
                            details.setPermanentAddr("");
                        }
                        if (jObjResult.has("rc_vh_class_desc")) {
                            details.setVehicleClassDesc(jObjResult.getString("rc_vh_class_desc"));
                            details.setVehicleClass(jObjResult.getString("rc_vh_class_desc"));
                        } else {
                            details.setVehicleClassDesc("");
                            details.setVehicleClass("");
                        }
                        if (jObjResult.has("rc_chasi_no")) {
                            details.setChasisNo(jObjResult.getString("rc_chasi_no"));
                        } else {
                            details.setChasisNo("");
                        }
                        if (jObjResult.has("rc_eng_no")) {
                            details.setEngineNo(jObjResult.getString("rc_eng_no"));
                        } else {
                            details.setEngineNo("");
                        }
                        if (jObjResult.has("rc_maker_desc")) {
                            details.setMakerDesc(jObjResult.getString("rc_maker_desc"));
                        } else {
                            details.setMakerDesc("");
                        }
                        if (jObjResult.has("rc_maker_model")) {
                            details.setMakerModel(jObjResult.getString("rc_maker_model"));
                        } else {
                            details.setMakerModel("");
                        }
                        if (jObjResult.has("rc_body_type_desc")) {
                            details.setBodyTypeDesc(jObjResult.getString("rc_body_type_desc"));
                        } else {
                            details.setBodyTypeDesc("");
                        }
                        if (jObjResult.has("rc_fuel_desc")) {
                            details.setFuelType(jObjResult.getString("rc_fuel_desc"));
                        } else {
                            details.setFuelType("");
                        }
                        if (jObjResult.has("rc_color")) {
                            details.setColor(jObjResult.getString("rc_color"));
                        } else {
                            details.setColor("");
                        }
                        if (jObjResult.has("rc_norms_desc")) {
                            details.setNormsDesc(jObjResult.getString("rc_norms_desc"));
                        } else {
                            details.setNormsDesc("");
                        }
                        if (jObjResult.has("rc_fit_upto")) {
                            details.setFitUpTo(jObjResult.getString("rc_fit_upto"));
                        } else {
                            details.setFitUpTo("");
                        }
                        if (jObjResult.has("rc_tax_upto")) {
                            details.setTaxUpTo(jObjResult.getString("rc_tax_upto"));
                        } else {
                            details.setTaxUpTo("");
                        }
                        if (jObjResult.has("rc_np_no")) {
                            details.setPermitNo(jObjResult.getString("rc_np_no"));
                        } else {
                            details.setPermitNo("");
                        }
                        if (jObjResult.has("rc_np_upto")) {
                            details.setPermitUpTo(jObjResult.getString("rc_np_upto"));
                        } else {
                            details.setPermitUpTo("");
                        }
                        if (jObjResult.has("rc_financer")) {
                            details.setFinancer(jObjResult.getString("rc_financer"));
                        } else {
                            details.setFinancer("");
                        }
                        if (jObjResult.has("rc_insurance_comp")) {
                            details.setInsuranceCompany(jObjResult.getString("rc_insurance_comp"));
                        } else {
                            details.setInsuranceCompany("");
                        }
                        if (jObjResult.has("rc_insurance_policy_no")) {
                            details.setInsurancePolicyNo(jObjResult.getString("rc_insurance_policy_no"));
                        } else {
                            details.setInsurancePolicyNo("");
                        }
                        if (jObjResult.has("rc_insurance_upto")) {
                            details.setInsuranceUpto(jObjResult.getString("rc_insurance_upto"));
                        } else {
                            details.setInsuranceUpto("");
                        }
                        if (jObjResult.has("rc_manu_month_yr")) {
                            details.setManufecturingMonth(jObjResult.getString("rc_manu_month_yr"));
                        } else {
                            details.setManufecturingMonth("");
                        }
                        if (jObjResult.has("rc_unld_wt")) {
                            details.setUnladenWeight(jObjResult.getString("rc_unld_wt"));
                        } else {
                            details.setUnladenWeight("");
                        }
                        if (jObjResult.has("rc_gvw")) {
                            details.setLadenWeight(jObjResult.getString("rc_gvw"));
                        } else {
                            details.setLadenWeight("");
                        }
                        if (jObjResult.has("rc_no_cyl")) {
                            details.setNoOfCylender(jObjResult.getString("rc_no_cyl"));
                        } else {
                            details.setNoOfCylender("");
                        }
                        if (jObjResult.has("rc_cubic_cap")) {
                            details.setCublicCap(jObjResult.getString("rc_cubic_cap"));
                        } else {
                            details.setCublicCap("");
                        }
                        if (jObjResult.has("rc_seat_cap")) {
                            details.setSeatingCap(jObjResult.getString("rc_seat_cap"));
                        } else {
                            details.setSeatingCap("");
                        }
                        if (jObjResult.has("rc_sleeper_cap")) {
                            details.setSleeperCap(jObjResult.getString("rc_sleeper_cap"));
                        } else {
                            details.setSleeperCap("");
                        }
                        if (jObjResult.has("rc_stand_cap")) {
                            details.setStandingCap(jObjResult.getString("rc_stand_cap"));
                        } else {
                            details.setStandingCap("");
                        }
                        if (jObjResult.has("rc_wheelbase")) {
                            details.setWheelbase(jObjResult.getString("rc_wheelbase"));
                        } else {
                            details.setWheelbase("");
                        }
                        if (jObjResult.has("rc_registered_at")) {
                            details.setRegisteredAt(jObjResult.getString("rc_registered_at"));
                        } else {
                            details.setRegisteredAt("");
                        }
                        if (jObjResult.has("rc_status_as_on")) {
                            details.setStatusAsOn(jObjResult.getString("rc_status_as_on"));
                        } else {
                            details.setStatusAsOn("");
                        }
                    }
                }
                return details;
            } else if (jObjMain.getInt("status") == 501 || jObjMain.getInt("status") == 204) {
                details = new RCDetails();
                details.setStatusCode(jObjMain.getInt("status"));
                details.setStatusMsg(jObjMain.getString("message"));
                return details;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(RCDetails details) {
        super.onPostExecute(details);
        dialog.dismiss();

        if (details != null) {
            if (details.isStatus()) {
                Intent intent = new Intent(context, SendComplaintActivity.class);
                intent.putExtra("RESPONSE", rcData);
                intent.putExtra("RC", rcNumber);
                intent.putExtra("CHASSIS", details.getChasisNo());
                intent.putExtra("OWNER", details.getOwnerName());
                intent.putExtra("MODEL", details.getMakerDesc()+" , "+details.getMakerModel());
                intent.putExtra("ENGINE", details.getEngineNo());
                intent.putExtra("RA", details.getRegisteredAt());
                intent.putExtra("VC", details.getVehicleClassDesc());
                intent.putExtra("FT", details.getFuelType());
                intent.putExtra("RD", details.getRegDate());
                intent.putExtra("IVU", details.getInsuranceUpto());
                intent.putExtra("FVU", details.getFitUpTo());
                intent.putExtra("TVU", details.getTaxUpTo());
                intent.putExtra("BTD", details.getBodyTypeDesc());

               /* intent.putExtra("MODEL", details.getMakerModel());
                intent.putExtra("MODEL", details.getMakerModel());*/
                context.startActivity(intent);
                ((Activity) context).overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                ((Activity) context).finish();
            } else {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                alertDialog.setMessage(details.getStatusMsg());
                alertDialog.setCancelable(false);
                alertDialog.setPositiveButton(context.getResources().getString(R.string.Ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alertDialog.show();
            }
        }
    }

}
