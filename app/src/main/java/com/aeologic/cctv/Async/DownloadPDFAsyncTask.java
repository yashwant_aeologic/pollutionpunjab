package com.aeologic.cctv.Async;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.StrictMode;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.widget.Toast;

import com.aeologic.cctv.BuildConfig;
import com.aeologic.cctv.R;
import com.aeologic.cctv.Utility.Prefrence;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;


public class DownloadPDFAsyncTask extends AsyncTask<String, Integer, Boolean> {
    Context context;
    String myURL;
    String FROM;
    String TO;
    int downloadedFileSize = 0;
    int totalFileSize;
    File downloadedFile = null;
    float per = 0;
    String filePath;
    private File file;
    private String FileName;
    private ProgressDialog dialog;
    boolean isStoragePermissionReq;
    // boolean isApplicationForm;


    public DownloadPDFAsyncTask(Context context) {
        this.context = context;

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog = new ProgressDialog(context);
        dialog.setMessage(context.getString(R.string.download_msg));
        dialog.setCancelable(false);
        dialog.setIndeterminate(false);
        dialog.setMax(100);
        dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        dialog.show();
    }

    @Override
    protected Boolean doInBackground(String... params) {
        myURL = params[0];
        FROM = params[1];
        TO = params[2];
        try {
            //FileName = getFileName(myURL)+".pdf";
            FileName = FROM + ".pdf";
            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
            File folder = new File(extStorageDirectory, "pollution");
            folder.mkdir();
            file = new File(folder, FileName);
            filePath = extStorageDirectory + "/pollution/" + FileName;
            file.createNewFile();
            /* if(isApplicationForm) {*/
            return DownloadFilePOST(myURL, file, FROM, TO);
           /* } else {
                return DownloadFileGET(myURL, file);
            }*/
        } catch (IOException e1) {
            e1.printStackTrace();
            Log.v("FNFE", "PERMISSION_REQUIRED!");
            isStoragePermissionReq = true;
            return false;
        }
    }


    public boolean DownloadFileGET(String fileURL, File directory) {
        try {
            FileOutputStream file = new FileOutputStream(directory);
            URI uri = new URI(fileURL);
            Log.v("URL", uri.toString());
            URL url = new URL(uri.toString());
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setDoOutput(true);
            connection.connect();
            int fileLength = connection.getContentLength();
            InputStream input = connection.getInputStream();
            byte[] buffer = new byte[1024];
            long total = 0;
            int count;
            while ((count = input.read(buffer)) > 0) {
                total += count;
                if (fileLength > 0)
                    publishProgress((int) (total * 100 / fileLength));
                Log.v("PROGRESS", (total * 100 / fileLength) + "");
                file.write(buffer, 0, count);
            }
            file.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean DownloadFilePOST(String fileURL, File directory, String FROM, String TO) {
        try {
            FileOutputStream file = new FileOutputStream(directory);
            URI uri = new URI(fileURL);
            Log.v("URL", uri.toString());
            String data = URLEncoder.encode("token", "UTF-8") + "=" + URLEncoder.encode(Prefrence.getToken(context), "UTF-8");
            data += "&" + URLEncoder.encode("start_date", "UTF-8") + "=" + URLEncoder.encode(FROM, "UTF-8");
            data += "&" + URLEncoder.encode("end_date", "UTF-8") + "=" + URLEncoder.encode(TO, "UTF-8");
            URL url = new URL(uri.toString());
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            //connection.connect();
            OutputStreamWriter wr = new OutputStreamWriter(connection.getOutputStream());
            wr.write(data);
            wr.flush();
            int fileLength = connection.getContentLength();
            InputStream input = connection.getInputStream();
            byte[] buffer = new byte[1024];
            long total = 0;
            int count;
            while ((count = input.read(buffer)) > 0) {
                total += count;
                if (fileLength > 0)
                    publishProgress((int) (total * 100 / fileLength));
                Log.v("PROGRESS", (total * 100 / fileLength) + "");
                file.write(buffer, 0, count);
            }
            file.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    protected void onPostExecute(Boolean status) {
        super.onPostExecute(status);
        dialog.dismiss();
        if (status) {
            Log.v("PATH", filePath);

            File file = new File(filePath);
            showPDF(file);

        } else {
            if (isStoragePermissionReq) {
                Toast.makeText(context, "Enable Storage Permission from Settings!", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(context, "Failed to Download Certificate!", Toast.LENGTH_SHORT).show();
            }
        }
    }


    @Override
    protected void onProgressUpdate(Integer... progress) {
        super.onProgressUpdate(progress);
        Log.v("PROGRESS", progress[0] + " %");
        if (progress[0] == 100) {
        }
    }

    private String getFileName(String path) {
        File file = new File(path);
        return file.getName();
    }

    public void showPDF(File file) {
        Log.v("PDF_PATH", file.getPath());
        try {
            if (Build.VERSION.SDK_INT >= 24) {
                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
            }
            File pdfFile = new File(file.getPath());
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.setDataAndType(Uri.fromFile(pdfFile), "application/pdf");
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            context.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
            // Toast.makeText(Information.this, getString(R.string.pdf_viewer_not_found), Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();

        }
    }
}
