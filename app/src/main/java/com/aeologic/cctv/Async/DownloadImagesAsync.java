package com.aeologic.cctv.Async;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import com.aeologic.cctv.Database.Database;
import com.aeologic.cctv.R;



import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;


/**
 * Created by u104 on 17/5/17.
 */

public class DownloadImagesAsync extends AsyncTask<ArrayList<HashMap<String, String>>, Integer, Boolean> {
    private final String TAG = "DownloadImagesAsync";
    Context context;
    ProgressDialog progressDialog;
    ArrayList<HashMap<String, String>> offenceListMap;
    public String outPutImagePath;
    Database database;

    public DownloadImagesAsync(Context context) {
        this.context = context;
        database = new Database(context);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
       /* progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(context.getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();*/
    }

    @Override
    protected Boolean doInBackground(ArrayList<HashMap<String, String>>... params) {
        Log.d(TAG, "doInBackground: ");
        offenceListMap = params[0];


        HashMap hashMap;
        Bitmap bitmap = null;
        for (int i = 0; i < offenceListMap.size(); i++) {
            hashMap = offenceListMap.get(i);
            try {

                String storagePath = context.getString(R.string.offence_image_path);
                bitmap = getBitmap(storagePath + hashMap.get("serverImagePath").toString());
                //Glide.with(this).load("https://www.google.es/images/srpr/logo11w.png").asBitmap().into(-1, -1).get();
                Log.e("list size=", "db" + bitmap);
                outPutImagePath = getImagePath();
                Log.e("list size=", "db" + outPutImagePath);
                saveFile(bitmap, outPutImagePath, hashMap.get("offenceId").toString());

            } catch (Exception e) {
                Log.e("catch list size=", "db" + outPutImagePath);
                Log.e("catch size=", "db" + bitmap);
            }

        }

        return true;
    }


    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        if (result) {
//            SendComplaintActivity.allImageDownload = true;
        }
//        progressDialog.cancel();
    }

    public void saveFile(Bitmap b, String outPutImagePath, String offenceId) {

        try {


            OutputStream fOut = null;
            File file = new File(outPutImagePath);

            if (file.exists())
                file.delete();

            file.createNewFile();
            fOut = new FileOutputStream(file);
            // 100 means no compression, the lower you go, the stronger the compression
            b.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();

            // save image path in database
            database.updateOffenceData(outPutImagePath, offenceId);


        } catch (FileNotFoundException e) {
            Log.d(TAG, "file not found");
            e.printStackTrace();
        } catch (IOException e) {
            Log.d(TAG, "io exception");
            e.printStackTrace();
        }

    }

    public String getImagePath() {
        File dir;


        String file_path = "/data/data/" + context.getPackageName() + "/ImagePath";
        dir = new File(file_path);
        //  if(!dir.exists())
        dir.mkdirs();

        String outputFile = dir.getAbsolutePath() + "/img" + System.currentTimeMillis() + ".png";
        return outputFile;

    }

    public Bitmap getBitmap(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(input);
            return bitmap;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;

        }
    }
}
