package com.aeologic.cctv.Adapter;

/**
 * Created by vishal on 9/12/15.
 */

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aeologic.cctv.Activity.ComplaintListActivity;
import com.aeologic.cctv.Activity.DoneDetailActivity;
import com.aeologic.cctv.Activity.OffenceAnalyticsActivity;
import com.aeologic.cctv.Connection.ConnectionDetector;
import com.aeologic.cctv.OfficersActivities.ManagerComplaintListActivity;
import com.aeologic.cctv.R;

import java.util.ArrayList;
import java.util.HashMap;

public class OffenceAnalyticsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public ArrayList<HashMap<String, String>> responseList;
    private Context context;
    private int visibleThreshold = 2;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;
    String toVal, fromVal;


    public OffenceAnalyticsAdapter(Context context, ArrayList<HashMap<String, String>> responseList, RecyclerView recyclerView, String toVal, String fromVal) {
        this.responseList = responseList;
        this.context = context;
        this.toVal = toVal;
        this.fromVal = fromVal;
        Log.e("OffenceAnalyticsA: ", toVal);
       /* if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    if (dy > 0) {
                        totalItemCount = linearLayoutManager.getItemCount();
                        lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                        if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                            if (onLoadMoreListener != null) {
                                onLoadMoreListener.onLoadMore();
                            }
                            loading = true;
                        }
                    } else {
                        OffenceAnalyticsActivity.progressBar.setVisibility(View.INVISIBLE);
                    }
                }
            });
        }*/
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.offence_analytics_item, parent, false);
        vh = new TextViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof TextViewHolder) {
            final HashMap<String, String> detailsMap = responseList.get(position);
            if (detailsMap != null) {
                ((TextViewHolder) holder).titleTxt.setText(detailsMap.get("name"));
                ((TextViewHolder) holder).countTxt.setText(detailsMap.get("count"));
                ((TextViewHolder) holder).pendingCount.setText(detailsMap.get("pending_count"));
                ((TextViewHolder) holder).rejectedCount.setText(detailsMap.get("rejected_count"));
                ((TextViewHolder) holder).resolvedCount.setText(detailsMap.get("resolved_count"));
                ((TextViewHolder) holder).unrelatedCount.setText(detailsMap.get("unrelated_count"));
                ((TextViewHolder) holder).deletedCount.setText(detailsMap.get("deleted_count"));
            }
            ((TextViewHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (detailsMap != null) {
                        String id = (detailsMap.get("offence_id"));
                        Intent intent = new Intent(context, ManagerComplaintListActivity.class);
                        intent.putExtra("id", id);
                        intent.putExtra("From", "ANALYTICS");
                        intent.putExtra("TO", ((OffenceAnalyticsActivity) context).getTo());
                        intent.putExtra("FROMVAL", ((OffenceAnalyticsActivity) context).getFrom());
                        context.startActivity(intent);
//                    ((Activity) context).finish();
                    }
                }
            });
        }

    }

    public void setLoaded() {
        loading = false;
    }

    @Override
    public int getItemCount() {
        return responseList.size();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public interface OnLoadMoreListener {
        void onLoadMore();
    }

    public static class TextViewHolder extends RecyclerView.ViewHolder {
        TextView titleTxt, countTxt, pendingCount, rejectedCount, resolvedCount, unrelatedCount, deletedCount;


        public TextViewHolder(View v) {
            super(v);
            titleTxt = (TextView) itemView.findViewById(R.id.name_txt);
            countTxt = (TextView) itemView.findViewById(R.id.count_txt);
            pendingCount = (TextView) itemView.findViewById(R.id.pending_count);
            rejectedCount = (TextView) itemView.findViewById(R.id.rejected_count);
            resolvedCount = (TextView) itemView.findViewById(R.id.resolved_count);
            deletedCount = (TextView) itemView.findViewById(R.id.deleted_count);
            unrelatedCount = (TextView) itemView.findViewById(R.id.unrelated_count);

        }
    }
}