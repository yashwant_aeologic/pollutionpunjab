package com.aeologic.cctv.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.aeologic.cctv.R;


/**
 * Created by Sachin varshney on 30/04/17.
 */
public class DrawerItemAdapter extends BaseAdapter {

    Context mContext;
    String[] mNavigationDrawerItemTitles = {"Logout"};
    int[] mNavigationDrawerItemIcon = {R.drawable.logout};

    private LayoutInflater inflater = null;

    public DrawerItemAdapter(Context mContext) {

        this.mContext = mContext;
        inflater = (LayoutInflater) mContext.getSystemService(mContext.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder = new Holder();
        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_view_item_row, null);
        holder.titleTxt = (TextView) convertView.findViewById(R.id.textViewName);
        holder.titleImg = (ImageView) convertView.findViewById(R.id.imageViewIcon);


        holder.titleTxt.setText(mNavigationDrawerItemTitles[position]);
        holder.titleImg.setImageResource(mNavigationDrawerItemIcon[position]);

        return convertView;
    }

    private class Holder {
        TextView titleTxt;
        ImageView titleImg;

    }

    @Override
    public int getCount() {
        return mNavigationDrawerItemTitles.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

}

