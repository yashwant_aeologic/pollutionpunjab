package com.aeologic.cctv.Adapter;

import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.aeologic.cctv.R;


/**
 * Created by u104 on 22/6/16.
 */
public class FAQItemViewHolder extends RecyclerView.ViewHolder {

    private final TextView tv1,tv2;

    public FAQItemViewHolder(final View parent, TextView tv1, TextView tv2) {
        super(parent);
        this.tv1 = tv1;
        this.tv2=tv2;
    }

    public static FAQItemViewHolder newInstance(View parent) {
        TextView itemTextView1 = (TextView) parent.findViewById(R.id.number);
        TextView itemTextView2 = (TextView) parent.findViewById(R.id.question_tv);
        return new FAQItemViewHolder(parent, itemTextView1,itemTextView2);
    }
    public void setNumber(int number){
        tv1.setText(String.valueOf(number));
    }

    public void setItemText(CharSequence text) {
        tv2.setText(text);
    }

    public void setcolor(int randomAndroidColor) {

        GradientDrawable bgShape = (GradientDrawable) tv1.getBackground();
        bgShape.setColor(randomAndroidColor);

    }
}

