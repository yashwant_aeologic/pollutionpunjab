package com.aeologic.cctv.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.aeologic.cctv.Activity.InProgressDetailActivity;
import com.aeologic.cctv.R;
import com.aeologic.cctv.Utility.Util;


import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by u104 on 29/7/16.
 */
public class InProgressAdapter extends RecyclerView.Adapter<InProgressAdapter.MyViewHolder> {
    private static final String TAG = "InProgressAdapter";
    Context context;
    public ArrayList<HashMap<String, String>> list;
    boolean status;

    public InProgressAdapter(Context context, ArrayList<HashMap<String, String>> responseList) {
        this.context = context;
        this.list = responseList;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.e(TAG, "onCreateViewHolder: view type:" + viewType);
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_row, parent, false);
        return new MyViewHolder(itemView);


    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        Log.e(TAG, "onBindViewHolder: position " + position);
        final HashMap<String, String> detailsMap = list.get(position);
        if (detailsMap != null) {
            holder.rcNumber.setText(detailsMap.get("rcNumber").toString());
            holder.statustxt.setText(context.getString(R.string.please_upload_this_complaint));
            String timeStamp = detailsMap.get("timeStamp").toString();
            String createdAt = detailsMap.get("createdAt").toString();
            Log.e(TAG, "onBindViewHolder: createdAT: " + createdAt);
            Log.e(TAG, "onBindViewHolder: timestamp: " + timeStamp);
            String date = Util.formatDateFromTimeStamp("yyyy-MM-dd HH:mm", "dd/MM/yyyy", createdAt);
            String time = Util.formatDateFromTimeStamp("yyyy-MM-dd HH:mm", "h:mm a", createdAt);
            String finalTimeStamp = Util.formatDateFromTimeStamp("yyyy-MM-dd HH:mm:ss", "yyyyMMddHHmmss", timeStamp);
            Log.d(TAG, "onBindViewHolder: finalTimeStamp:" + finalTimeStamp);
            holder.complaintNUmber.setText("#TEMP_" + finalTimeStamp);
            holder.date.setText(date);
            holder.time.setText(time);
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final HashMap<String, String> detailsMap = list.get(position);
                if (detailsMap != null) {
                    int id = Integer.parseInt((detailsMap.get("id")));
                    Intent intent = new Intent(context, InProgressDetailActivity.class);
                    intent.putExtra("id", detailsMap.get("id"));
                    intent.putExtra("tabStatus", 0);
                    context.startActivity(intent);
//                    ((Activity) context).finish();
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView rcNumber, date, time, statustxt, complaintNUmber;

        public MyViewHolder(View itemView) {
            super(itemView);
            date = (TextView) itemView.findViewById(R.id.date);
            time = (TextView) itemView.findViewById(R.id.time);
            statustxt = (TextView) itemView.findViewById(R.id.status_txt);
            rcNumber = (TextView) itemView.findViewById(R.id.rcNumber);
            complaintNUmber = (TextView) itemView.findViewById(R.id.complaint_number);

        }
    }
}
