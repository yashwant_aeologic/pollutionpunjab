package com.aeologic.cctv.Adapter;

/**
 * Created by vishal on 9/12/15.
 */

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.aeologic.cctv.Activity.FeedbackDetailActivity;
import com.aeologic.cctv.Activity.FeedbackListActivity;
import com.aeologic.cctv.R;
import com.aeologic.cctv.Utility.Util;


import java.util.ArrayList;
import java.util.HashMap;

public class FeedbackListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    public ArrayList<HashMap<String, String>> responseList;
    String TAG = "DoneListAdapter";
    Context context;

    // The minimum amount of items to have below your current scroll position before loading more.
    private int visibleThreshold = 2;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;

    public FeedbackListAdapter(Context context, ArrayList<HashMap<String, String>> responseList, RecyclerView recyclerView) {
        this.responseList = responseList;
        this.context = context;
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    if (dy > 0) {
                        totalItemCount = linearLayoutManager.getItemCount();
                        lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                        if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                            // End has been reached
                            // Do something
                            if (onLoadMoreListener != null) {
                                onLoadMoreListener.onLoadMore();
                            }
                            loading = true;
                        }
                    } else {

                        FeedbackListActivity.progressBar.setVisibility(View.INVISIBLE);
                        // ((Activity)(context).progressBar.setVisibility(View.INVISIBLE);
                    }

                }
            });
        }
    }

  /*  @Override
    public int getItemViewType(int position) {
        return responseList.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }*/

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        //  if (viewType == VIEW_ITEM) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_item_feedback, parent, false);
        vh = new TextViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof TextViewHolder) {
            Log.e(TAG, "onBindViewHolder: position " + position);
            final HashMap<String, String> detailsMap = responseList.get(position);
            if (detailsMap != null) {
                ((TextViewHolder) holder).title.setText(detailsMap.get("title").toString());
                ((TextViewHolder) holder).description.setText(detailsMap.get("feedback").toString());
                if (detailsMap.get("reply")!=null&&!detailsMap.get("reply").equals("null")) {
                    ((TextViewHolder) holder).statustxt.setText("Replied");
                   // ((TextViewHolder) holder).cardview_feedback_team.setCardBackgroundColor(Color.parseColor("#5CB85C"));
                }else{
                    ((TextViewHolder) holder).statustxt.setText("Pending");
                   // ((TextViewHolder) holder).cardview_feedback_team.setCardBackgroundColor(Color.parseColor("#D9534F"));
                }

                String createdAt = detailsMap.get("created_at").toString();
                Log.e(TAG, "onBindViewHolder: timestamp: " + createdAt);
                String date = Util.formatDateFromTimeStamp("yyyy-MM-dd HH:mm:ss", "dd/MM/yyyy", createdAt);
                String time = Util.formatDateFromTimeStamp("yyyy-MM-dd HH:mm:ss", "hh:mm a", createdAt);
                Log.e(TAG, "onBindViewHolder: Time: " + time);

                ((TextViewHolder) holder).date.setText(date);
                ((TextViewHolder) holder).time.setText(time);
                ((TextViewHolder) holder).itemView.setTag(position);
                ((TextViewHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int pos = (int) view.getTag();
                        final HashMap<String, String> detailsMap = responseList.get(pos);
                        if (detailsMap != null) {
                            int id = Integer.parseInt((detailsMap.get("id")));
                            Intent intent = new Intent(context, FeedbackDetailActivity.class);
                            intent.putExtra("id", id);
                            intent.putExtra("title", detailsMap.get("title"));
                            intent.putExtra("feedback", detailsMap.get("feedback"));
                            intent.putExtra("createdAt", detailsMap.get("created_at"));
                            intent.putExtra("reply", detailsMap.get("reply"));
                            intent.putExtra("updatedAt", detailsMap.get("update_at"));
                            context.startActivity(intent);
                        }
                    }
                });
            }
        }
    }

    public void setLoaded() {
        loading = false;
    }

    @Override
    public int getItemCount() {
        return responseList.size();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public interface OnLoadMoreListener {
        void onLoadMore();
    }

    public static class TextViewHolder extends RecyclerView.ViewHolder {
        TextView title, date, time, description, statustxt;
      // CardView cardview_feedback_team;

        public TextViewHolder(View v) {
            super(v);
            date = (TextView) itemView.findViewById(R.id.date);
            time = (TextView) itemView.findViewById(R.id.time);
            statustxt = (TextView) itemView.findViewById(R.id.status_txt);
            title = (TextView) itemView.findViewById(R.id.feedback_title);
            description = (TextView) itemView.findViewById(R.id.feedback_message);

        }
    }

    /*public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
        }
    }*/
}