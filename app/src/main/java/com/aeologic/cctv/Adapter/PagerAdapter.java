package com.aeologic.cctv.Adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;


import com.aeologic.cctv.Fragment.DoneFragment;
import com.aeologic.cctv.Fragment.InProgressFragment;
import com.aeologic.cctv.R;



import java.util.ArrayList;


/**
 * Created by u104 on 29/7/16.
 */
public class PagerAdapter extends FragmentStatePagerAdapter {
    private final DoneFragment doneFragment;
    private final InProgressFragment inProgressFragment;
    ArrayList<Fragment> listFragment = new ArrayList<>();
    Context context;

    public PagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
        inProgressFragment = new InProgressFragment();
        doneFragment = new DoneFragment();
        listFragment.add(inProgressFragment);
        listFragment.add(doneFragment);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment;
        if (position == 0) {
            return inProgressFragment;
        }
        if (position == 1) {
           return doneFragment;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return context.getString(R.string.in_progress);
            case 1:
                return context.getString(R.string.done);
        }
        return null;
    }
}
