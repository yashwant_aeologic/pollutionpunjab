package com.aeologic.cctv.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.aeologic.cctv.Activity.WebViewActivity;
import com.aeologic.cctv.R;

import java.util.List;

/**
 * Created by u104 on 20/6/16.
 */
public class FAQAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    private final int TYPE_HEADER = 2;
    private final int TYPE_ITEM = 1;
    private List<String> list;
    private int lastPosition;
    private Intent intent;

    public FAQAdapter(Context context, List<String> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        if (viewType == TYPE_ITEM) {
            final View view = LayoutInflater.from(context).inflate(R.layout.faq_item_list, parent, false);
            return FAQItemViewHolder.newInstance(view);
        } else if (viewType == TYPE_HEADER) {
            final View view = LayoutInflater.from(context).inflate(R.layout.faq_header, parent, false);
            return new FAQHeaderViewHolder(view);
        }
//        throw new RuntimeException("There is no type that matches the type " + viewType + " + make sure your using types correctly");
        final View view = LayoutInflater.from(context).inflate(R.layout.faq_item_list, parent, false);
        return FAQItemViewHolder.newInstance(view);
    }

    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        //if (!isPositionHeader(position)) {
        FAQItemViewHolder holder = (FAQItemViewHolder) viewHolder;
        final String itemText = list.get(position);
        // header
        holder.setNumber(position + 1);
        holder.setItemText(itemText);
            /*int[] androidColors = context.getResources().getIntArray(R.array.androidcolors);
            int randomAndroidColor = androidColors[new Random().nextInt(androidColors.length)];
            holder.setcolor(randomAndroidColor);
            if (position > lastPosition) {
                Animation animation = AnimationUtils.loadAnimation(context,
                        R.anim.up_from_bottom);
                viewHolder.itemView.startAnimation(animation);
                lastPosition = position;
            }*/
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, WebViewActivity.class);
                intent.putExtra("position", position);
                intent.putExtra("title", list.get(position));
                context.startActivity(intent);
            }
        });
        // }
    }

    public int getBasicItemCount() {
        return list == null ? 0 : list.size();
    }

    /*@Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position)) {
            return TYPE_HEADER;
        }

        return TYPE_ITEM;
    }*/

    /*@Override
    public int getItemCount() {
        return getBasicItemCount() + 1; // header
    }*/
    @Override
    public int getItemCount() {
        return list.size(); // header
    }

    /*private boolean isPositionHeader(int position) {
        return position == 0;
    }*/


}
