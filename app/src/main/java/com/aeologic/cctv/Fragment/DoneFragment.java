package com.aeologic.cctv.Fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.aeologic.cctv.Adapter.DoneListAdapter;
import com.aeologic.cctv.Connection.ConnectionDetector;
import com.aeologic.cctv.Database.Database;
import com.aeologic.cctv.R;
import com.aeologic.cctv.Utility.AppController;
import com.aeologic.cctv.Utility.Prefrence;
import com.aeologic.cctv.Utility.RecyclerViewDisabler;
import com.aeologic.cctv.Utility.Util;
import com.aeologic.cctv.Utility.VolleyMultipartRequest;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;




import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class DoneFragment extends Fragment {
    private String TAG = "DoneFragment";
    private RecyclerView recyclerView;
    ImageView warningLogo;
    TextView warningText;
    Database database;
    int pageNumber = 0;
    public static ProgressBar progressBar;
    private boolean allDataLoadSuccessfully = false;
    private boolean swipeRefreshBarStatus = false;
    private boolean responseStatus;
    ArrayList<HashMap<String, String>> responseList = new ArrayList<>();

    //    public SwipeRefreshLayout mSwipeRefreshLayout;
    private ConnectionDetector detector;
    private DoneListAdapter doneListAdapter;
    private SwipeRefreshLayout materialRefreshLayout;
    RecyclerViewDisabler recyclerViewDisabler;
    boolean isFirstTime = true;
    private VolleyMultipartRequest multipartRequest;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_done, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        mSwipeRefreshLayout = (SwipeRefreshLayout) getActivity().findViewById(R.id.swipeRefreshLayout1);
        database = new Database(getActivity());
        warningLogo = (ImageView) getActivity().findViewById(R.id.warning_logo1);
        warningText = (TextView) getActivity().findViewById(R.id.warning_text1);
        recyclerView = (RecyclerView) getActivity().findViewById(R.id.recycler_view2);
        progressBar = (ProgressBar) getActivity().findViewById(R.id.progress_bar);

        materialRefreshLayout = (SwipeRefreshLayout) getActivity().findViewById(R.id.refresh);
        //    recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        detector = new ConnectionDetector(getContext());
        recyclerViewDisabler = new RecyclerViewDisabler();

        if (doneListAdapter != null) {
            recyclerView.setAdapter(doneListAdapter);
        } else {
            doneListAdapter = new DoneListAdapter(getActivity(), responseList, recyclerView);
            recyclerView.setAdapter(doneListAdapter);
        }


        materialRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadData();
            }


        });

        if (detector.isConnectingToInternet()) {
            // call the API here
//            new DoneListAsync(getActivity(), recyclerView, warningLogo, warningText,pageNumber).execute(getString(R.string.COMPLAINT_LIST_API));
            getUploaddata(pageNumber);

            final Handler handler = new Handler();

            doneListAdapter.setOnLoadMoreListener(new DoneListAdapter.OnLoadMoreListener() {
                @Override
                public void onLoadMore() {
                    //add progress item
                    //responseList.add(null);
                    // doneListAdapter.notifyItemInserted(responseList.size() - 1);

                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //remove progress item
                            if (responseStatus && !allDataLoadSuccessfully) {
                                // responseList.remove(responseList.size() - 1);
                                // doneListAdapter.notifyItemRemoved(responseList.size());
                                pageNumber = pageNumber + 1;
                                getUploaddata(pageNumber);
                            }
                            //     new DoneListAsync(context, recyclerView, warningLogo, warningText,pageNumber).execute(context.getString(R.string.COMPLAINT_LIST_API));

                            //or you can add all at once but do not forget to call mAdapter.notifyDataSetChanged();
                        }
                    }, 600);
                    System.out.println("load");
                }
            });

        } else {
            if(!isFirstTime){
                Util.showAlert(getActivity(), getString(R.string.please_check_internet));
            }
            recyclerView.setVisibility(View.GONE);
            warningLogo.setImageResource(R.drawable.wifi);
            warningText.setText(getString(R.string.no_internet_connections));
            warningLogo.setVisibility(View.VISIBLE);
            warningText.setVisibility(View.VISIBLE);
        }
    }

    public void loadData() {
        if (getActivity()!=null){
            if (new ConnectionDetector(getActivity()).isConnectingToInternet()) {
                recyclerView.addOnItemTouchListener(recyclerViewDisabler);
                if (multipartRequest!=null) {
                    multipartRequest.cancel();
                }
                swipeRefreshBarStatus = true;
                materialRefreshLayout.setEnabled(true);
                responseList.clear();
                pageNumber = 0;
                getUploaddata(pageNumber);
                allDataLoadSuccessfully = false;
                recyclerView.getRecycledViewPool().clear();
                doneListAdapter.notifyDataSetChanged();
            } else {
                materialRefreshLayout.setRefreshing(false);
                warningLogo.setImageResource(R.drawable.wifi);
                warningText.setText(getString(R.string.no_internet_connections));
                Util.showAlert(getActivity(), getContext().getString(R.string.please_check_internet));
            }
        }
    }

    private void getUploaddata(final int pageNumber) {
        responseStatus = false;
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getString(R.string.please_wait));
        progressDialog.setCancelable(false);

        if (isFirstTime&&!getActivity().isFinishing()) {
            progressDialog.show();
            isFirstTime = false;
        } else {
            if (swipeRefreshBarStatus) {
                progressBar.setVisibility(View.INVISIBLE);
            } else {
                progressBar.setVisibility(View.VISIBLE);
            }
        }


        multipartRequest = new VolleyMultipartRequest(Request.Method.POST,
                getString(R.string.COMPLAINT_LIST_API), new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                String resultResponse = new String(response.data);
                Log.e(TAG, "onResponse: Response: " + resultResponse);
                progressDialog.hide();
                try {

                    if (recyclerViewDisabler != null) {
                        recyclerView.removeOnItemTouchListener(recyclerViewDisabler);
                        swipeRefreshBarStatus = false;
                        materialRefreshLayout.setRefreshing(false);
                    }
                    HashMap<String, String> responseData;
                    JSONObject object = new JSONObject(resultResponse);
                    String status = object.getString("status");
                    if (status.equals("1")) {
                        responseStatus = true;
                        JSONArray resultArray = object.getJSONArray("data");
                        if (resultArray != null && resultArray.length() > 0) {
                            for (int i = 0; i < resultArray.length(); i++) {
                                responseData = new HashMap<>();
                                JSONObject obj = resultArray.getJSONObject(i);
                                responseData.put("id", obj.getString("id"));
                                responseData.put("createdAt", obj.getString("survey_date"));
                                responseData.put("rcNumber", obj.getString("rc_number"));
                                responseData.put("offence", obj.getString("offences"));
                                responseData.put("action", obj.getString("action"));
                                responseList.add(responseData);
                            }

                            doneListAdapter.notifyItemInserted(responseList.size());
                            doneListAdapter.setLoaded();

                            progressBar.setVisibility(View.INVISIBLE);
                            recyclerView.setVisibility(View.VISIBLE);
                            warningLogo.setVisibility(View.GONE);
                            warningText.setVisibility(View.GONE);
                        } else {
                            allDataLoadSuccessfully = true;
                            progressBar.setVisibility(View.INVISIBLE);
                            if (pageNumber == 0) {
                                recyclerView.setVisibility(View.GONE);
                                warningLogo.setImageResource(R.drawable.round_error_symbol);
                                warningText.setText(getString(R.string.no_complaint_found));
                                warningLogo.setVisibility(View.VISIBLE);
                                warningText.setVisibility(View.VISIBLE);
                            }
                        }
                    } else if (status.equals("0")) {
                        String message = object.getString("message");
                        if (message!=null){
                            if ((responseList.size()==0)&&message.contains("Your session has expired"))
                                Util.showSessionExpiredDialog(getActivity());
                        }else{
                            Util.showAlert(getActivity(), getString(R.string.pta));
                        }
//                        Util.showAlert(getActivity(), object.getString("message"));
                        progressBar.setVisibility(View.INVISIBLE);
                        progressDialog.hide();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e(TAG, "onErrorResponse: "+error.getMessage() );
                progressBar.setVisibility(View.INVISIBLE);
                Util.showAlert(getContext(), getString(R.string.pta));
                progressDialog.hide();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("token", Prefrence.getToken(getActivity()));
                params.put("pg", String.valueOf(pageNumber));
                Log.e(TAG, "getParams: Token: " + Prefrence.getToken(getActivity()));
                return params;

            }

        };
        multipartRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        AppController.getInstance().addToRequestQueue(multipartRequest);
    }


}
