package com.aeologic.cctv.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.aeologic.cctv.Adapter.InProgressAdapter;
import com.aeologic.cctv.Database.Database;
import com.aeologic.cctv.R;

import java.util.ArrayList;
import java.util.HashMap;


public class InProgressFragment extends Fragment {

    private String TAG = "InProgressFragment";
    private RecyclerView recyclerView;
    ImageView warningLogo;
    TextView warningText;
    Database database;
    String val = "InProgress";
    public static InProgressAdapter adapter;

    public static ArrayList<HashMap<String, String>> mProgressList = new ArrayList<HashMap<String, String>>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_inprogress, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        database = new Database(getActivity());
        recyclerView = (RecyclerView) getActivity().findViewById(R.id.recycler_view);
        warningLogo = (ImageView) getActivity().findViewById(R.id.warning_logo);
        warningText = (TextView) getActivity().findViewById(R.id.warning_text);
        mProgressList = database.getDataList(0);
        Log.e(TAG, "onActivityCreated: progress List size: " + mProgressList.size());
        setRecyclerAdapter(mProgressList);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (adapter != null) {
            mProgressList = database.getDataList(0);
            setRecyclerAdapter(mProgressList);
        }
    }

    public void setRecyclerAdapter(ArrayList<HashMap<String, String>> mProgressList) {
        if (mProgressList != null && mProgressList.size() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
            warningLogo.setVisibility(View.GONE);
            warningText.setVisibility(View.GONE);
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            adapter = new InProgressAdapter(getActivity(), mProgressList);
            recyclerView.setAdapter(adapter);
            recyclerView.invalidate();
        } else {
            recyclerView.setVisibility(View.GONE);
            warningLogo.setVisibility(View.VISIBLE);
            warningText.setVisibility(View.VISIBLE);
        }

    }
}
