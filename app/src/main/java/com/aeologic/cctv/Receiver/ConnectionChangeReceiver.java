package com.aeologic.cctv.Receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.Log;

import com.aeologic.cctv.Service.UploadOfflineSurvey;


/**
 * Created by Deep on 08/09/16.
 */

public class ConnectionChangeReceiver extends BroadcastReceiver {
    private static final String TAG = "ConnReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetInfo = connectivityManager.getActiveNetworkInfo();
        Log.e(TAG, "onReceive: ");
        //capture connection_info in sendcomplaint from xml in a static variable and show and hide the view from here.
        if (activeNetInfo != null && activeNetInfo.isConnected()&& Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            Log.d(TAG, "onReceive: net is connected");
            //start the service to sync the offence
            if (!UploadOfflineSurvey.isServiceRunning) {
                context.startService(new Intent(context, UploadOfflineSurvey.class));
            }
        }
    }
}
