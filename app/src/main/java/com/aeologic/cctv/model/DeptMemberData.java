package com.aeologic.cctv.model;

public class DeptMemberData {
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    String name;
    int id;

    public DeptMemberData(String name, int id) {
        this.name = name;
        this.id = id;
    }

    public DeptMemberData() {
    }
}
