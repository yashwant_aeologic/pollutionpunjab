package com.aeologic.cctv.model;

/**
 * Created by Yash on 13-07-18.
 */
public class RCDetails {
    private boolean status;
    private String statusMsg;
    private int statusCode;
    private String regNo;
    private String regDate;
    private String regUptoDate;
    private String ownerSr;
    private String ownerName;
    private String fatherName;
    private String presentAddr;
    private String permanentAddr;
    private String vehicleClass;
    private String vehicleClassDesc;
    private String chasisNo;
    private String engineNo;
    private String makerDesc;
    private String makerModel;
    private String bodyTypeDesc;
    private String fuelType;
    private String color;
    private String normsDesc;
    private String fitUpTo;
    private String taxUpTo;
    private String financer;
    private String insuranceCompany;
    private String insurancePolicyNo;
    private String insuranceUpto;
    private String unladenWeight;
    private String ladenWeight;
    private String noOfCylender;
    private String cublicCap;
    private String manufecturingMonth;
    private String seatingCap;
    private String standingCap;
    private String sleeperCap;
    private String wheelbase;
    private String registeredAt;
    private String statusAsOn;
    private String permitNo;
    private String permitUpTo;
    private String rtoCode;
    private String rtoName;
    private String lastUpdated;

    public RCDetails() {
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getStatusMsg() {
        return statusMsg;
    }

    public void setStatusMsg(String statusMsg) {
        this.statusMsg = statusMsg;
    }

    public String getRegNo() {
        return regNo;
    }

    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    public String getRegDate() {
        return regDate;
    }

    public void setRegDate(String regDate) {
        this.regDate = regDate;
    }

    public String getRegUptoDate() {
        return regUptoDate;
    }

    public void setRegUptoDate(String regUptoDate) {
        this.regUptoDate = regUptoDate;
    }

    public String getOwnerSr() {
        return ownerSr;
    }

    public void setOwnerSr(String ownerSr) {
        this.ownerSr = ownerSr;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getPresentAddr() {
        return presentAddr;
    }

    public void setPresentAddr(String presentAddr) {
        this.presentAddr = presentAddr;
    }

    public String getPermanentAddr() {
        return permanentAddr;
    }

    public void setPermanentAddr(String permanentAddr) {
        this.permanentAddr = permanentAddr;
    }

    public String getVehicleClass() {
        return vehicleClass;
    }

    public void setVehicleClass(String vehicleClass) {
        this.vehicleClass = vehicleClass;
    }

    public String getVehicleClassDesc() {
        return vehicleClassDesc;
    }

    public void setVehicleClassDesc(String vehicleClassDesc) {
        this.vehicleClassDesc = vehicleClassDesc;
    }

    public String getChasisNo() {
        return chasisNo;
    }

    public void setChasisNo(String chasisNo) {
        this.chasisNo = chasisNo;
    }

    public String getEngineNo() {
        return engineNo;
    }

    public void setEngineNo(String engineNo) {
        this.engineNo = engineNo;
    }

    public String getMakerDesc() {
        return makerDesc;
    }

    public void setMakerDesc(String makerDesc) {
        this.makerDesc = makerDesc;
    }

    public String getMakerModel() {
        return makerModel;
    }

    public void setMakerModel(String makerModel) {
        this.makerModel = makerModel;
    }

    public String getBodyTypeDesc() {
        return bodyTypeDesc;
    }

    public void setBodyTypeDesc(String bodyTypeDesc) {
        this.bodyTypeDesc = bodyTypeDesc;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getNormsDesc() {
        return normsDesc;
    }

    public void setNormsDesc(String normsDesc) {
        this.normsDesc = normsDesc;
    }

    public String getFitUpTo() {
        return fitUpTo;
    }

    public void setFitUpTo(String fitUpTo) {
        this.fitUpTo = fitUpTo;
    }

    public String getTaxUpTo() {
        return taxUpTo;
    }

    public void setTaxUpTo(String taxUpTo) {
        this.taxUpTo = taxUpTo;
    }

    public String getFinancer() {
        return financer;
    }

    public void setFinancer(String financer) {
        this.financer = financer;
    }

    public String getInsuranceCompany() {
        return insuranceCompany;
    }

    public void setInsuranceCompany(String insuranceCompany) {
        this.insuranceCompany = insuranceCompany;
    }

    public String getInsurancePolicyNo() {
        return insurancePolicyNo;
    }

    public void setInsurancePolicyNo(String insurancePolicyNo) {
        this.insurancePolicyNo = insurancePolicyNo;
    }

    public String getInsuranceUpto() {
        return insuranceUpto;
    }

    public void setInsuranceUpto(String insuranceUpto) {
        this.insuranceUpto = insuranceUpto;
    }

    public String getUnladenWeight() {
        return unladenWeight;
    }

    public void setUnladenWeight(String unladenWeight) {
        this.unladenWeight = unladenWeight;
    }

    public String getLadenWeight() {
        return ladenWeight;
    }

    public void setLadenWeight(String ladenWeight) {
        this.ladenWeight = ladenWeight;
    }

    public String getNoOfCylender() {
        return noOfCylender;
    }

    public void setNoOfCylender(String noOfCylender) {
        this.noOfCylender = noOfCylender;
    }

    public String getCublicCap() {
        return cublicCap;
    }

    public void setCublicCap(String cublicCap) {
        this.cublicCap = cublicCap;
    }

    public String getManufecturingMonth() {
        return manufecturingMonth;
    }

    public void setManufecturingMonth(String manufecturingMonth) {
        this.manufecturingMonth = manufecturingMonth;
    }

    public String getSeatingCap() {
        return seatingCap;
    }

    public void setSeatingCap(String seatingCap) {
        this.seatingCap = seatingCap;
    }

    public String getStandingCap() {
        return standingCap;
    }

    public void setStandingCap(String standingCap) {
        this.standingCap = standingCap;
    }

    public String getSleeperCap() {
        return sleeperCap;
    }

    public void setSleeperCap(String sleeperCap) {
        this.sleeperCap = sleeperCap;
    }

    public String getWheelbase() {
        return wheelbase;
    }

    public void setWheelbase(String wheelbase) {
        this.wheelbase = wheelbase;
    }

    public String getRegisteredAt() {
        return registeredAt;
    }

    public void setRegisteredAt(String registeredAt) {
        this.registeredAt = registeredAt;
    }

    public String getStatusAsOn() {
        return statusAsOn;
    }

    public void setStatusAsOn(String statusAsOn) {
        this.statusAsOn = statusAsOn;
    }

    public String getPermitNo() {
        return permitNo;
    }

    public void setPermitNo(String permitNo) {
        this.permitNo = permitNo;
    }

    public String getPermitUpTo() {
        return permitUpTo;
    }

    public void setPermitUpTo(String permitUpTo) {
        this.permitUpTo = permitUpTo;
    }

    public String getRtoCode() {
        return rtoCode;
    }

    public void setRtoCode(String rtoCode) {
        this.rtoCode = rtoCode;
    }

    public String getRtoName() {
        return rtoName;
    }

    public void setRtoName(String rtoName) {
        this.rtoName = rtoName;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }
}
