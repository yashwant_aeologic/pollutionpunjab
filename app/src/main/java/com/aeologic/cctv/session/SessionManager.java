package com.aeologic.cctv.session;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;


public class SessionManager {
    private static SharedPreferences userCredentialsPrefs;
    private SharedPreferences.Editor credentialEditor;
    private static SharedPreferences applicationSettingsPrefs;
    private SharedPreferences.Editor applicationSettingsEditor;
    public final static String PREF_USER_CREDENTIALS = "USER_CREDENTIALS";
    public final static String PREF_APPLICATION_SETTINGS = "APPLICATION_SETTINGS";
    public final static String KEY_IS_LOGIN = "IS_LOGIN";
    public final static String KEY_USER_ID = "USER_ID";
    public final static String KEY_USER_NAME = "USER_NAME";
    public final static String KEY_MOBILE_NO = "MOBILE_NO";
    public final static String KEY_AADHAAR_NO = "AADHAAR_NO";
    public final static String KEY_EMAIL = "EMAIL";
    public final static String CREATED_AT = "CREATED_AT";
    public final static String UPDATED_AT = "UPDATED_AT";
    public final static String CARE_OF = "CARE_OF";
    public final static String GENDER = "GENDER";
    public final static String BLOOD_GROUP = "BLOOD_GROUP";
    public final static String HEIGHT = "HEIGHT";
    public final static String WEIGHT = "WEIGHT";
    public final static String DOB = "DOB";
    public final static String VILLAGE = "VILLAGE";
    public final static String PIN_CODE = "PIN_CODE";
    public final static String POST_OFFICE = "POST_OFFICE";
    public final static String LANDMARK = "LANDMARK";
    public final static String LOCALITY = "LOCALITY";
    public final static String STREET = "STREET";
    public final static String STATE = "STATE";
    public final static String DISTRICT = "DISTRICT";
    public final static String SUB_DISTRICT = "SUB_DISTRICT";
    public final static String PROFILE_IMAGE_URL = "PROFILE_IMAGE_URL";
    public final static String PROFILE_IMAGE = "PROFILE_IMAGE";
    public final static String RELATIVE_MOBILE_1 = "RELATIVE_MOBILE_1";
    public final static String RELATIVE_MOBILE_2 = "RELATIVE_MOBILE_2";
    public final static String RELATIVE_MOBILE_3 = "RELATIVE_MOBILE_3";
    public final static String RELATIVE_MOBILE_4 = "RELATIVE_MOBILE_4";
    public final static String RELATIVE_NAME_1 = "RELATIVE_NAME_1";
    public final static String RELATIVE_NAME_2 = "RELATIVE_NAME_2";
    public final static String RELATIVE_NAME_3 = "RELATIVE_NAME_3";
    public final static String RELATIVE_NAME_4 = "RELATIVE_NAME_4";
    public final static String RELATIVE_RELATION_1 = "RELATIVE_RELATION_1";
    public final static String RELATIVE_RELATION_2 = "RELATIVE_RELATION_2";
    public final static String RELATIVE_RELATION_3 = "RELATIVE_RELATION_3";
    public final static String RELATIVE_RELATION_4 = "RELATIVE_RELATION_4";
    public final static String RELATIVE_ID_1 = "RELATIVE_ID_1";
    public final static String RELATIVE_ID_2 = "RELATIVE_ID_2";
    public final static String RELATIVE_ID_3 = "RELATIVE_ID_3";
    public final static String RELATIVE_ID_4 = "RELATIVE_ID_4";
    public final static String GOOGLE_ADDRESS_API_COUNT = "GOOGLE_ADDRESS_API_COUNT";
    public final static String KEY_NOTIFICATION_TIME_STAMP = "KEY_NOTIFICATION_TIME_STAMP";


    public final static String KEY_IMEI = "IMEI";
    public final static String KEY_TOKEN = "TOKEN";
    public final static String KEY_OTP = "OTP";
    public final static String KEY_TIMESTAMP = "TIMESTAMP";
    public final static String KEY_IS_FIRST_TIME = "IS_FIRST_TIME";
    public final static String KEY_IS_FIRST_TIME_GENERAL_NOTIFICATION = "IS_FIRST_TIME";

    public final static String KEY_UPDATE_STATUS = "UPDATE_STATUS";
    public final static String KEY_UPDATE_PRIORITY = "UPDATE_PRIORITY";
    public final static String KEY_UPDATE_DIALOG_OPENED = "UPDATE_DIALOG_OPENED";
    public final static String KEY_APP_VERSION = "APP_VERSION";

    Context _context;

    @SuppressLint("CommitPrefEdits")
    public SessionManager(Context cntx) {
        _context = cntx;
        userCredentialsPrefs = cntx.getSharedPreferences(PREF_USER_CREDENTIALS, Context.MODE_PRIVATE);
        applicationSettingsPrefs = cntx.getSharedPreferences(PREF_APPLICATION_SETTINGS, Context.MODE_PRIVATE);
        credentialEditor = userCredentialsPrefs.edit();
        applicationSettingsEditor = applicationSettingsPrefs.edit();
    }

    public SharedPreferences getUserCredentialsPrefs() {
        return userCredentialsPrefs;
    }

    public SharedPreferences.Editor getUserCredentialsEditor() {
        return credentialEditor;
    }

    public SharedPreferences.Editor getSettingsPrefsEditor() {
        return applicationSettingsEditor;
    }

    public void createUserSession() {
        credentialEditor = userCredentialsPrefs.edit();


        credentialEditor.putInt(GOOGLE_ADDRESS_API_COUNT, 0);
        credentialEditor.commit();




    }




    public SharedPreferences getApplicationSettingsPrefs() {
        return applicationSettingsPrefs;
    }
    public SharedPreferences.Editor getApplicationSettingsEditor() {
        return applicationSettingsEditor;
    }

    public SharedPreferences getUserPrefs() {
        return userCredentialsPrefs;
    }


    public String getStringEntity(String key) {
        return userCredentialsPrefs.getString(key, null);
    }

    public int getIntEntity(String key) {
        return userCredentialsPrefs.getInt(key, 0);
    }

    public boolean getBooleanEntity(String key) {
        return userCredentialsPrefs.getBoolean(key, false);
    }

    public boolean isLoggedIn() {
        return userCredentialsPrefs.getBoolean(KEY_IS_LOGIN, false);
    }



    public void clearUserCredentialsEditor() {
        credentialEditor.clear();
        credentialEditor.commit();
    }
}
