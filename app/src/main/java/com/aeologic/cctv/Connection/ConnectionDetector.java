package com.aeologic.cctv.Connection;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.Log;

public class ConnectionDetector {

    private static final String TAG = ConnectionDetector.class.getSimpleName();
    private Context context;

    public ConnectionDetector(Context context) {
        this.context = context;
    }

    public boolean isConnectingToInternet() {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetInfo = connectivityManager.getActiveNetworkInfo();
        Log.e(TAG, "onReceive: ");

        //capture connection_info in sendcomplaint from xml in a static variable and show and hide the view from here.
        if (activeNetInfo != null && activeNetInfo.isConnected()) {
            return true;
        }
        return false;
    }
}
